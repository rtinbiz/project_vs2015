#pragma once

#include <vector>
#include <codecvt>
#include <memory>
#ifdef _LINUX
#include <c++/5/bits/locale_conv.h>
#endif

#ifdef _WINDOWS
#ifndef _BUILD_NIPL
#ifdef _DEBUG
#pragma comment (lib, "NIPL_d.lib")
#else
#pragma comment (lib, "NIPL.lib")
#endif  // _DEBUG
#endif  // _BUILD_NIPL
#endif  //_WINDOWS

#ifdef _LINUX
typedef signed char         INT8, *PINT8;
typedef signed short        INT16, *PINT16;
typedef unsigned char       UINT8, *PUINT8;
typedef unsigned short      UINT16, *PUINT16;
typedef float               FLOAT;
#define AFX_EXT_CLASS
#endif

using namespace std;

#include "NIPLParam.h"
#include "NIPLResult.h"

enum NIPL_ERR {
	NIPL_ERR_NONE = -1,
	NIPL_ERR_SUCCESS = 0,
	NIPL_ERR_FAIL = 1,
	NIPL_ERR_PASS = 2,
	NIPL_ERR_OUT_OF_MEMORY,

	NIPL_ERR_FAIL_TO_OPEN_FILE = 100,
	NIPL_ERR_FAIL_TO_SAVE_FILE,
	NIPL_ERR_FAIL_TO_LOAD_IMAGE,
	NIPL_ERR_FAIL_TO_SAVE_IMAGE,
	NIPL_ERR_FAIL_TO_CONVERT_IMAGE,
	NIPL_ERR_FAIL_INVALID_IMAGE,
	NIPL_ERR_FAIL_INVALID_PARAM_EXPRESSION,
	NIPL_ERR_FAIL_INVALID_DATA,
	NIPL_ERR_FAIL_EMPTY_IMAGE,
	NIPL_ERR_FAIL_NOT_SUPPORT_PROCESS,
	NIPL_ERR_FAIL_NOT_SUPPORT_GPU_PROCESS,
	NIPL_ERR_FAIL_NOT_MATCH_IMAGE_SIZE,
	NIPL_ERR_FAIL_NOT_MATCH_MASK_SIZE,

	NIPL_ERR_NO_INPUT = 200,
	NIPL_ERR_NO_OUTPUT,
	NIPL_ERR_NO_PARAM,
	NIPL_ERR_NO_PARAMS,
	NIPL_ERR_NO_OBJECT,

	NIPL_ERR_INVALID_IMAGE_TYPE = 300,
	NIPL_ERR_INVALID_PARAM_VALUE,
	NIPL_ERR_INVALID_TEMPLATE
};

#define NIPL_SUCCESS(nErr) (nErr == NIPL_ERR_SUCCESS)
#define NIPL_PASS(nErr) (nErr == NIPL_ERR_PASS)
#define NIPL_FAIL(nErr) (!NIPL_SUCCESS(nErr) && !NIPL_PASS(nErr))
#define VERIFY_PARAMS() if(pInput == 0x00 || pOutput == 0x00 || pInput->m_pParam == 0x00) return NIPL_ERR_NO_PARAMS;
#define CHECK_EMPTY_IMAGE(dImg) (dImg.data == 0x00)
#define CHECK_OPTION_SHOW_IMAGE() (m_pOption != 0x00 && m_pOption->m_bShowImage)
#define CHECK_EXCEPTION(Func) \
	try { Func; } \
	catch (Exception e) { \
		ShowErrorDesc(e.func.c_str(), e.err.c_str()); \
		return NIPL_ERR_INVALID_PARAM_VALUE; \
	}

#define NIPL_METHOD_DECL(FuncName) NIPL_ERR FuncName(NIPLInput *pInput, NIPLOutput *pOutput)
#define NIPL_METHOD_IMPL(FuncName) NIPL_ERR NIPL::FuncName(NIPLInput *pInput, NIPLOutput *pOutput)
#define VERIFY_MASK(dImg, dMask) \
	if (!CHECK_EMPTY_IMAGE(dMask) && dImg.size == dMask.size) { \
		if (dImg.channels() != dMask.channels()) { \
			if (dImg.channels() == 1) { \
				cvtColor(dMask, dMask, CV_BGR2GRAY); \
			} \
			else { \
				cvtColor(dMask, dMask, CV_GRAY2BGR); \
			} \
		} \
	} \
	else { \
		dMask = Mat::zeros(dImg.rows, dImg.cols, dImg.type()); \
	}

inline wstring str2wstr(string str) {
	wstring_convert<codecvt_utf8_utf16<wchar_t>> converter;
	return converter.from_bytes(str);
}

inline string wstr2str(wstring str) {
	wstring_convert<codecvt_utf8_utf16<wchar_t>> converter;
	return converter.to_bytes(str);
}

#ifdef _WINDOWS
inline int wstricmp(wstring str1, wstring str2) { return _wcsicmp(str1.c_str(), str2.c_str()); }
inline bool CHECK_STRING(string str1, string str2) { return (_stricmp(str1.c_str(), str2.c_str()) == 0); }
#endif
#ifdef _LINUX
inline int wstricmp(wstring str1, wstring str2) { return wcscasecmp(str1.c_str(), str2.c_str()); }
inline bool CHECK_STRING(string str1, string str2) { return (strcasecmp(str1.c_str(), str2.c_str()) == 0); }
#endif 

inline bool CHECK_STRING(wstring str1, wstring str2) { return (wstricmp(str1, str2) == 0); }
inline bool CHECK_STRING(wstring str1, string str2) { return CHECK_STRING(str1, str2wstr(str2)); }
inline bool CHECK_STRING(string str1, wstring str2) { return CHECK_STRING(str2, str1); }

inline bool STRING_TO_BOOL(string str) { return CHECK_STRING(str, "true") ? true : false; }
inline bool STRING_TO_BOOL(wstring str) { return CHECK_STRING(str, L"true") ? true : false; }
inline wstring BOOL_TO_WSTRING(bool b) { return b ? L"true" : L"false"; }
inline string BOOL_TO_STRING(bool b) { return b ? "true" : "false"; }

typedef Scalar CvColor;
inline wstring COLOR_TO_WSTRING(CvColor color) {
	wchar_t szColor[16];
#ifdef _WINDOWS    
	swprintf_s(szColor, L"%.0f,%.0f,%.0f", color[2], color[1], color[0]);
#endif
#ifdef _LINUX    
	swprintf(szColor, 16, L"%.0f,%.0f,%.0f", color[2], color[1], color[0]);
#endif
	
	return wstring(szColor);
}
inline string COLOR_TO_STRING(CvColor color) {
	return wstr2str(COLOR_TO_WSTRING(color));
}
inline CvColor STRING_TO_COLOR(string strColor) {
	CvColor dColor;
	if (strColor.size() > 0) {
		istringstream ss(strColor);
		string strNumber;
		int dNumber[3] = { 0, 0, 0 };
		int nIndex = 0;
		while (nIndex < 3 && ss >> dNumber[nIndex]) {
			if (ss.peek() == ',') {
				ss.ignore();
			}
			nIndex++;
		}
		dColor = CV_RGB(dNumber[0], dNumber[1], dNumber[2]);
	}
	return dColor;
}
inline CvColor STRING_TO_COLOR(wstring strColor) {
	return STRING_TO_COLOR(wstr2str(strColor));
}

inline string strlower(string str) {
	string str2; 
	transform(str.begin(), str.end(), str2.begin(), ::tolower);
	return str2;
}

inline wstring wstrlower(wstring wstr) {
	string str = wstr2str(wstr);
	return str2wstr(strlower(str));
}

inline string strupper(string str) {
	string str2;
	transform(str.begin(), str.end(), str2.begin(), ::tolower);
	return str2;
}

inline wstring wstrupper(wstring wstr) {
	string str = wstr2str(wstr);
	return str2wstr(strupper(str));
}

template<typename T> 
inline T sign(T value) { return (value == 0) ? 0 : (value > 0 ? 1 : -1); }

template<typename T>
inline T clamp(T value, T low, T high) { return value < low ? low : (value > high ? high : value); }

struct AFX_EXT_CLASS NIPLInput
{
	NIPLParam *m_pParam;

	Mat m_dImg;
	Mat m_dMask;
	Mat m_dTemplateImg;
	int m_nTemplateImgCount;

	string m_strImgPath;
	string m_strTemplateImgPath;

	NIPLInput() {
		Init();
	}

	~NIPLInput() {
		Clear();
	}

	void Init()
	{
		m_pParam = nullptr;
	}

	void Clear() {
		// don't need to delete m_pParam because it's responsibility of user
		m_dImg.release();
		m_dTemplateImg.release();
		m_dMask.release();
		m_strImgPath.clear();
		m_strTemplateImgPath.clear();

		Init();
	}
};


struct AFX_EXT_CLASS NIPLOutput
{
	shared_ptr<NIPLResult> m_pResult;
	Mat m_dImg;
//	Mat m_dMask;

	NIPLOutput() {
		Init();
	}

	~NIPLOutput() {
		Clear();
	}

	void Init()
	{
		m_pResult.reset();
	}

	void Clear(bool bClearAll = false) {
		if (bClearAll) {
			ClearResult();
		}

		m_dImg.release();
//		m_dMask.release();

		Init();
	}

	NIPLOutput &operator=(const NIPLOutput &dProcess) {
		Clear();

		m_dImg = dProcess.m_dImg;
		m_pResult = dProcess.m_pResult;

		return *this;
	}

	void ClearResult() {
		m_pResult.reset();
	}

	template <class T>
	shared_ptr<T> GetResult()
	{
		return static_pointer_cast<T>(m_pResult);
	}

	bool IsResultType(wstring strType) {
		if (m_pResult == nullptr) {
			return false;
		}

		return m_pResult->IsType(strType);
	}
};

struct AFX_EXT_CLASS NIPLOption
{
	bool m_bUseGPU;
	bool m_bShowImage;

	NIPLOption() {
		Init();
	}

	~NIPLOption() {
		Clear();
	}

	void Init() {
		m_bUseGPU = false;
		m_bShowImage = false;
	}

	void Clear() {
		Init();
	}
};

class AFX_EXT_CLASS NIPL {
public :
	static shared_ptr<NIPL> GetInstance(bool bNew = false);

	NIPL();
	virtual ~NIPL();

	void SetOption(NIPLOption *pOption);

	NIPL_ERR LoadImage(wstring strPath, Mat &dImg, int nFlags = CV_LOAD_IMAGE_ANYCOLOR);
	NIPL_ERR LoadImage(string strPath, Mat &dImg, int nFlags = CV_LOAD_IMAGE_ANYCOLOR);
	NIPL_ERR SaveImage(wstring strPath, Mat dImg);
	NIPL_ERR SaveImage(string strPath, Mat dImg);

	void DebugPrint(const wchar_t* szDebugString, ...);
	void DebugPrint(const char* szDebugString, ...);

	void DebugPrintImageProperty(wstring strTitle, Mat dImg);
	void DebugPrintImageProperty(string strTitle, Mat dImg);

	void ShowErrorDesc(wstring strFunc, wstring strMessage);
	void ShowErrorDesc(string strFunc, string strMessage);

	NIPL_METHOD_DECL(CreateImage);
	NIPL_METHOD_DECL(LoadTemplateImage);
	NIPL_METHOD_DECL(Color2Gray);
	NIPL_METHOD_DECL(Copy);
	NIPL_METHOD_DECL(Invert);
	NIPL_METHOD_DECL(Reduce);
	NIPL_METHOD_DECL(Operate);
	NIPL_METHOD_DECL(MorphologyOperate);
	NIPL_METHOD_DECL(Move);
	NIPL_METHOD_DECL(Rotate);
	NIPL_METHOD_DECL(CopySubImageFrom);
	NIPL_METHOD_DECL(CopySubImageTo);
	NIPL_METHOD_DECL(SetCircleROI);
	NIPL_METHOD_DECL(Thresholding);
	NIPL_METHOD_DECL(ColorThresholding);
	NIPL_METHOD_DECL(Smoothing);
	NIPL_METHOD_DECL(EdgeDetecting);
	NIPL_METHOD_DECL(Thinning);
	NIPL_METHOD_DECL(ConvexHull);

protected:
	NIPLOption *m_pOption;

private:
	void ColorThresholding_SetThreshold(int nMethod, float nMinValue, float nMaxVlue, int &nMin, int &nMax);
	void Thinning_Iteration(Mat &dImg, int nIter);
};
