#pragma once

#include <vector>
#include <list>
#include <xstring>
using namespace std;

#include "opencv2/opencv.hpp"
using namespace cv;

#ifndef _BUILD_NIPL_AUTO_REPAIR
#ifdef _DEBUG
#pragma comment (lib, "NIPL_AutoRepair_d.lib")
#else
#pragma comment (lib, "NIPL_AutoRepair.lib")
#endif
#endif

//
// Define Strings
//

// Product Id and version
#define STR_XML_ID_AUTO_REPAIR "AutoRepair"
#define STR_XML_VERSION_AUTO_REPAIR "1.32"

// strings for 'Element' used in XML data
#define STR_XML_ELEMENT_VIDATA "VIData"
#define STR_XML_ELEMENT_LAYER "Layer"
#define STR_XML_ELEMENT_DRAWING "Drawing"
#define STR_XML_ELEMENT_INSPECTION "Inspection"
#define STR_XML_ELEMENT_MASK "Mask"
#define STR_XML_ELEMENT_DRAWING_MASK "DrawingMask"
#define STR_XML_ELEMENT_TEMPLATE "Template"
#define STR_XML_ELEMENT_TEMPLATE_PIXEL "TemplatePixel"
#define STR_XML_ELEMENT_ACTIVE "Active"
#define STR_XML_ELEMENT_GATE "Gate"
#define STR_XML_ELEMENT_SD "SD"
#define STR_XML_ELEMENT_DRAW_SET "DrawSet"
#define STR_XML_ELEMENT_DEFAULT_PARAM "DefaultParam"
#define STR_XML_ELEMENT_BINARIZE "Binarize"
#define STR_XML_ELEMENT_APPLY "Apply"
#define STR_XML_ELEMENT_DEFECT "Defect"
#define STR_XML_ELEMENT_CONDITION "Condition"
#define STR_XML_ELEMENT_PIXEL "Pixel"
#define STR_XML_ELEMENT_DRAW "Draw"
#define STR_XML_ELEMENT_CHECK "Check"

#define STR_XML_KEY_VERSION "version"
#define STR_XML_KEY_ENABLE "enable"
#define STR_XML_KEY_ID "id"
#define STR_XML_KEY_NAME "name"
#define STR_XML_KEY_TYPE "type"
#define STR_XML_KEY_ROOT_PATH "root_path"
#define STR_XML_KEY_PATH "path"
#define STR_XML_KEY_ROTATION "rotation"
#define STR_XML_KEY_VERT_FLIP "vert_flip"
#define STR_XML_KEY_HORIZ_FLIP "horiz_flip"
#define STR_XML_KEY_REPLACE "replace"
#define STR_XML_KEY_MIN_SIZE_RATIO "min_size_ratio"
#define STR_XML_KEY_BASE_DEFECT_ID "base_defect_id"
#define STR_XML_KEY_CONDITION_ID "condition_id"
#define STR_XML_KEY_MASK_ID "mask_id"
#define STR_XML_KEY_COLOR_MASK_ID "color_mask_id"
#define STR_XML_KEY_DARK "dark"
#define STR_XML_KEY_SEED "seed"
#define STR_XML_KEY_CONNECTED_DEFECT "connected_defect"
#define STR_XML_KEY_BASE_DEFECT_DRAW "base_defect_draw"
#define STR_XML_KEY_KEEP_PIXEL_SEQ "keep_pixel_seq"
#define STR_XML_KEY_ACCPET_ALL "accept_all"
#define STR_XML_KEY_CONNECTED "connected"
#define STR_XML_KEY_BINARIZE_THRESHOLD "binarize_threshold"
#define STR_XML_KEY_COLOR_SIMILARITY_RED "color_similarity_red"
#define STR_XML_KEY_COLOR_SIMILARITY_GREEN "color_similarity_green"
#define STR_XML_KEY_COLOR_SIMILARITY_BLUE "color_similarity_blue"
#define STR_XML_KEY_DEFECT_MIN_SIZE "defect_min_size"
#define STR_XML_KEY_SEED_MIN_SIZE "seed_min_size"
#define STR_XML_KEY_COLOR "color"
#define STR_XML_KEY_ALPHA "alpha"
#define STR_XML_KEY_STATUS "status"
#define STR_XML_KEY_DARK_COLOR "dark_color"
#define STR_XML_KEY_DARK_COLOR_RATIO "dark_color_ratio"
#define STR_XML_KEY_DARK_COLOR_EXTEND_SIZE_RATIO "dark_color_extend_size_ratio"
#define STR_XML_KEY_HV_MIN_LENGTH "hv_min_length"
#define STR_XML_KEY_CONNECTED_EXTEND_SIZE "connected_extend_size"
#define STR_XML_KEY_CONNECTED_DEFECT_EXTEND_SIZE "connected_defect_extend_size"
#define STR_XML_KEY_DRAW_SET_ID "draw_set_id"
#define STR_XML_KEY_EXTEND_SIZE "extend_size"
#define STR_XML_KEY_FILL_SIZE "fill_size"
#define STR_XML_KEY_MODE "mode"
#define STR_XML_KEY_ADJUST_IMAGE_BACKGROUND "adjust_image_background"
#define STR_XML_KEY_APPLY_WHOLE_PIXEL "apply_whole_pixel"
#define STR_XML_KEY_MERGE_RESULT "merge_result"
#define STR_XML_KEY_MERGE_FILL_SIZE "merge_fill_size"

// strings for 'Value' used in XML data
#define STR_XML_VALUE_ROTATION_0 L"0"
#define STR_XML_VALUE_ROTATION_90 L"90"
#define STR_XML_VALUE_ROTATION_180 L"180"
#define STR_XML_VALUE_ROTATION_270 L"270"
#define STR_XML_VALUE_CONNECTED_HORIZ L"horiz"
#define STR_XML_VALUE_CONNECTED_VERT L"vert"
#define STR_XML_VALUE_CONNECTED_DONT_CARE L"dont_care"
#define STR_XML_VALUE_DRAW_TYPE_DRAW_SET L"DRAW_SET"
#define STR_XML_VALUE_DRAW_TYPE_MASK L"MASK"
#define STR_XML_VALUE_DRAW_TYPE_DEFECT L"DEFECT"
#define STR_XML_VALUE_DRAW_TYPE_DMD L"DMD"
#define STR_XML_VALUE_DRAW_TYPE_DEFECT_BOX L"DEFECT_BOX"
#define STR_XML_VALUE_DRAW_MODE_NORMAL L"normal"
#define STR_XML_VALUE_DRAW_MODE_OVERLAP L"overlap"
#define STR_XML_VALUE_DRAW_MODE_NOT_OVERLAP L"not_overlap"
#define STR_XML_VALUE_DRAW_MODE_OVERLAP_MASK L"overlap_mask"
#define STR_XML_VALUE_DRAW_MODE_NOT_OVERLAP_MASK L"not_overlap_mask"

// strings for 'Value' used in library
#define STR_XML_VALUE_CHECK_STATUS_ON L"on"
#define STR_XML_VALUE_CHECK_STATUS_OFF L"off"
#define STR_XML_VALUE_CHECK_STATUS_CONNECTED L"connected"
#define STR_XML_VALUE_CHECK_STATUS_CONNECTED_NEIGHBOR L"connected_neighbor"

namespace NIPL_AutoRepair {
	typedef Scalar CvColor;

	enum class Layer {
		ACTIVE = 0,
		GATE,
		SD
	};

	enum class Rotation {
		ROTATION_0,
		ROTATION_90,
		ROTATION_180,
		ROTATION_270
	};

	class AFX_EXT_CLASS VIMask {
	public:
		enum Type {
			MASK = 0,
			DRAWING_MASK,
			TEMPLATE,
			TEMPLATE_PIXEL,
			ALL
		};

		wstring m_strId;
		wstring m_strPath;
		Mat m_dImg;
		Type m_nType;

		VIMask() {
			Init();
		}
		virtual ~VIMask() {
			Clear();
		}

		void Init() {
			m_nType = MASK;
		}

		void Clear() {
			m_strId.clear();
			m_strPath.clear();
			m_dImg.release();

			Init();
		}

		void SetType(wstring strType);
		wstring GetType(bool bEmptyByDefault = false) const;
		vector<wstring> GetTypeList();
	};

	class AFX_EXT_CLASS VITemplatePixel {
	public:
		wstring m_strId;

		int m_nIndex;					// Template Pixel 배치에 따른 Index
		Rect2f m_rcPixelRatio;			// Template Pixel 위치 및 크기에 대한 전체 Template 대비 비율

		VITemplatePixel() {
			Init();
		}
		virtual ~VITemplatePixel() {
			Clear();
		}

		void Init() {
			m_nIndex = 0;
			m_rcPixelRatio = Rect2f(0.f, 0.f, 0.f, 0.f);
		}

		void Clear() {
			m_strId.clear();

			Init();
		}
	};

	class AFX_EXT_CLASS VITemplate {
	public:
		wstring m_strId;
		Rotation m_nRotation;
		bool m_bVerFlip;
		bool m_bHorizFlip;
		bool m_bReplace;
		float m_nMinSizeRatio;

		bool m_bMultiPixel;							
		list<VITemplatePixel> m_listTemplatePixel;	

		Mat m_dImg;						// Template 이미지
		Mat m_dPixelImg;				// Template 이미지에서 Template Pixel 영역에 해당하는 부분
		Mat m_dDiffImg;					// Template 자체 차분 이미지
		Mat m_dPixelDiffImg;			// Template 자체 차분 이미지에서 Template Pixel 영역에 해당하는 부분

		VITemplate() {
			Init();
		}
		virtual ~VITemplate() {
			Clear();
		}

		void Init() {
			m_nRotation = Rotation::ROTATION_0;
			m_bVerFlip = false;
			m_bHorizFlip = false;
			m_nMinSizeRatio = 0.f;

			m_bMultiPixel = false;
		}

		void Clear() {
			m_strId.clear();
			m_dImg.release();
			m_dPixelImg.release();
			m_dDiffImg.release();
			m_dPixelDiffImg.release();

			m_listTemplatePixel.clear();

			Init();
		}

		void SetRotation(wstring strRotation);
		wstring GetRotation(bool bEmptyByDefault = false) const;
		vector<wstring> GetRotationList();

		void SortTemplatePixelList();
		VITemplatePixel &AddTemplatePixel();
		void DeleteTemplatePixel(VITemplatePixel &dPixel);

		//
		// Template 이미지 변환 관련 함수
		//
		void ResizeTemplateImage(Size size, bool bAlsoDiffImg = false);
		void MakeTemplateDiffImage();
		Mat &GetTemplatePixelDiffImage(Rect rcTemplatePixel);
		Mat &GetTemplateImage();
		Mat &GetTemplatePixelImage(Rect rcTemplatePixel);
		Rect GetTemplatePixelArea(int nIndex);
		Rect GetPrevTemplatePixelArea(int nIndex, bool bVert = false);
		Rect GetNextTemplatePixelArea(int nIndex, bool bVert = false);
	};

	class AFX_EXT_CLASS VIDefaultParam {
	public:
		float m_nBinarizeThreshold;
		float m_nColorSimilarityRed;
		float m_nColorSimilarityGreen;
		float m_nColorSimilarityBlue;
		int m_nMinDefectSize;
		int m_nMinSeedSize;
		CvColor m_dDarkColor;
		float m_nDarkColorRatio;
		float m_nDarkColorExtendSizeRatio;
		int m_nHVMinLength;
		int m_nConnectedExtendSize;
		int m_nConnectedDefectExtendSize;
		int m_nExtendSize;
		int m_nFillSize;
		bool m_bAdjustImageBackground;

		VIDefaultParam() {
			Init();
		}
		virtual ~VIDefaultParam() {
			Clear();
		}

		void Init() {
			m_nBinarizeThreshold = 0.f;
			m_nColorSimilarityRed = 0.f;
			m_nColorSimilarityGreen = 0.f;
			m_nColorSimilarityBlue = 0.f;
			m_nMinDefectSize = 0;
			m_nMinSeedSize = 0;
			m_dDarkColor = CV_RGB(0, 0, 0);
			m_nDarkColorRatio = 0.f;
			m_nDarkColorExtendSizeRatio = 0.f;
			m_nHVMinLength = 0;
			m_nConnectedExtendSize = 0;
			m_nConnectedDefectExtendSize = 0;
			m_nExtendSize = 0;
			m_nFillSize = 0;
			m_bAdjustImageBackground = false;
		}

		void Clear() {
			Init();
		}
	};

	class AFX_EXT_CLASS VIApply {
	public:
		wstring m_strMaskId;
		wstring m_strColorMaskId;
		float m_nBinarizeThreshold;
		int m_nMinDefectSize;
		int m_nHVMinLength;
		bool m_bMergeResult;
		int m_nMergeFillSize;

		VIApply() {
			Init();
		}
		virtual ~VIApply() {
			Clear();
		}

		void Init() {
			m_nBinarizeThreshold = 0.f;
			m_nMinDefectSize = 0;
			m_nHVMinLength = 0;
			m_bMergeResult = false;
			m_nMergeFillSize = 0;
		}

		void Clear() {
			m_strMaskId.clear();
			m_strColorMaskId.clear();

			Init();
		}
	};

	class AFX_EXT_CLASS VIBinarize {
	public:
		bool m_bApplyWholePixel;
		wstring m_strColorMaskId;
		list<VIApply> m_listApply;

		VIBinarize() {
			Init();
		}
		virtual ~VIBinarize() {
			Clear();
		}

		void Init() {
			m_bApplyWholePixel = false;
		}

		void Clear() {
			m_strColorMaskId.clear();
			m_listApply.clear();

			Init();
		}

		void OrderUpApply(VIApply &dApply);
		void OrderDownApply(VIApply &dApply);
		VIApply &AddApply();
		void DeleteApply(VIApply &dApply);
	};

	class AFX_EXT_CLASS VICheck {
	public:
		enum Status {
			ON,
			OFF,
			CONNECTED,
			CONNECTED_NEIGHBOR
		};

		wstring m_strMaskId;
		wstring m_strColorMaskId;
		Status m_nStatus;
		int m_nMinDefectSize;
		int m_nConnectedExtendSize;
		float m_nColorSimilarityRed;
		float m_nColorSimilarityGreen;
		float m_nColorSimilarityBlue;

		VICheck() {
			Init();
		}
		virtual ~VICheck() {
			Clear();
		}

		void Init() {
			m_nStatus = ON;
			m_nMinDefectSize = 0;
			m_nConnectedExtendSize = 0;
			m_nColorSimilarityRed = 0.f;
			m_nColorSimilarityGreen = 0.f;
			m_nColorSimilarityBlue = 0.f;
		}

		void Clear() {
			m_strMaskId.clear();
			m_strColorMaskId.clear();

			Init();
		}

		void SetStatus(wstring strStatus);
		wstring GetStatus(bool bEmptyByDefault = false) const;
		vector<wstring> GetStatusList();
	};

	class AFX_EXT_CLASS VIDraw {
	public:
		enum Type {
			DRAW_SET = 0,
			MASK,
			DEFECT,
			DMD,
			DEFECT_BOX
		};

		enum Mode {
			NORMAL = 0,			// Draw always
			OVERLAP,			// Draw overlapping area with overlap mask
			NOT_OVERLAP,		// Draw not overlapping area with overlap mask
			OVERLAP_MASK,		// Draw to overlap mask	
			NOT_OVERLAP_MASK	// Erase from overlap mask
		};

		Type m_nType;
		wstring m_strDrawSetId;
		wstring m_strMaskId;
		CvColor m_dColor;
		float m_nAlpha;
		int m_nExtendSize;
		int m_nFillSize;
		Mode m_nMode;

		VIDraw() {
			Init();
		}
		virtual ~VIDraw() {
			Clear();
		}

		void Init() {
			m_nType = DRAW_SET;
			m_dColor = CV_RGB(0, 0, 0);
			m_nAlpha = 1.f;
			m_nExtendSize = 0;
			m_nFillSize = 0;
			m_nMode = NORMAL;
		}

		void Clear() {
			m_strDrawSetId.clear();
			m_strMaskId.clear();

			Init();
		}

		void SetType(wstring strType);
		wstring GetType(bool bEmptyByDefault = false) const;
		vector<wstring> GetTypeList();

		void SetMode(wstring strMode);
		wstring GetMode(bool bEmptyByDefault = false) const;
		vector<wstring> GetModeList();
	};

	class AFX_EXT_CLASS VIDrawSet {
	public:
		wstring m_strId;
		list<VIDraw> m_listDraw;

		VIDrawSet() {
			Init();
		}
		virtual ~VIDrawSet() {
			Clear();
		}

		void Init() {
		}

		void Clear() {
			m_strId.clear();
			m_listDraw.clear();

			Init();
		}

		VIDraw &AddDraw();
		void DeleteDraw(VIDraw &dDraw);
		void OrderUpDraw(VIDraw &dDraw);
		void OrderDownDraw(VIDraw &dDraw);
	};

	class AFX_EXT_CLASS VIPixel {
	public:
		wstring m_strBaseDefectId;
		bool m_bBaseDefectDraw;
		bool m_bAcceptAll;
		list<VICheck> m_listCheck;

		bool m_bConnectedNeighbor;

		VIPixel() {
			Init();
		}
		virtual ~VIPixel() {
			Clear();
		}

		void Init() {
			m_bBaseDefectDraw = false;
			m_bAcceptAll = false;
			m_bConnectedNeighbor = false;
		}

		void Clear() {
			m_strBaseDefectId.clear();
			m_listCheck.clear();

			Init();
		}

		void SortCheckList();
		VICheck &AddCheck();
		void DeleteCheck(VICheck &dCheck);
	};

	class AFX_EXT_CLASS VIDefect {
	public:
		enum Connected {
			DONT_CARE,
			HORIZ,
			VERT
		};

		enum DrawMode {
			NORMAL,				// only draw defined in m_listDraw
			BASE_DEFECT			// include drawing defined in base defect
		};

		wstring m_strId;
		bool m_bDark;
		bool m_bSeed;
		Connected m_nConnected;
		bool m_bConnectedDefect;
		bool m_bKeepPixelSeq;
		wstring m_strBaseDefectId;
		bool m_bBaseDefectDraw;

		// these are not parameters from external data file, just for internal use
		int m_nPixelCount;				
		Mat m_dConnectedDefectImg;

		list<VIPixel> m_listPixel;
		list<VIDraw> m_listDraw;

		VIDefect() {
			Init();
		}
		virtual ~VIDefect() {
			Clear();
		}

		virtual void Init() {
			m_bDark = false;
			m_bSeed = false;
			m_nConnected = DONT_CARE;
			m_bConnectedDefect = false;
			m_bBaseDefectDraw = false;
			m_bKeepPixelSeq = false;

			m_nPixelCount = 0;
		}

		virtual void Clear() {
			m_strId.clear();
			m_strBaseDefectId.clear();

			m_listPixel.clear();
			m_listDraw.clear();

			m_dConnectedDefectImg.release();

			Init();
		}

		void SetConnected(wstring strConnected);
		wstring GetConnected(bool bEmptyByDefault = false) const;
		vector<wstring> GetConnectedList();

		VIPixel &AddPixel();
		void DeletePixel(VIPixel &dPixel);
		void OrderUpPixel(VIPixel &dPixel);
		void OrderDownPixel(VIPixel &dPixel);

		VIDraw &AddDraw();
		void DeleteDraw(VIDraw &dDraw);
		void OrderUpDraw(VIDraw &dDraw);
		void OrderDownDraw(VIDraw &dDraw);
	};

	class AFX_EXT_CLASS VILayer {
	public:
		wstring m_strMaskRootPath;
		list<VIMask> m_listMask;

		VILayer() {
			Init();
		}
		virtual ~VILayer() {
			Clear();
		}

		void Init() {
		}

		void Clear() {
			m_strMaskRootPath.clear();
			m_listMask.clear();

			Init();
		}

		void SortMaskList();
		VIMask &AddMask();
		void DeleteMask(VIMask &dMask);

		vector<wstring> GetMaskIdList(VIMask::Type nType = VIMask::Type::ALL, bool bIncludeEmptyId = false);
	};

	class AFX_EXT_CLASS VIDrawing {
	public:
		list<VIDrawSet> m_listDrawSet;

		VIDrawing() {
			Init();
		}
		virtual ~VIDrawing() {
			Clear();
		}

		void Init() {
		}

		void Clear() {
			m_listDrawSet.clear();

			Init();
		}

		void SortDrawSetList();
		VIDrawSet &AddDrawSet();
		void DeleteDrawSet(VIDrawSet &dDrawSet);

		vector<wstring> GetDrawSetIdList(bool bIncludeEmptyId = false);
	};

	class AFX_EXT_CLASS VIInspection {
	public:
		Layer m_nLayer;
		VITemplate m_dTemplate;
		VIDefaultParam m_dDefaultParam;
		VIBinarize m_dBinarize;
		list<VIDefect> m_listDefect;

		VIInspection() {
			Init();
		}
		virtual ~VIInspection() {
			Clear();
		}

		void Init() {
			m_nLayer = Layer::ACTIVE;
		}

		void Clear() {
			m_dTemplate.Clear();
			m_dDefaultParam.Clear();
			m_dBinarize.Clear();

			m_listDefect.clear();

			Init();
		}

		void SetLayer(wstring strLayer);
		wstring GetLayer(bool bEmptyByDefault = false) const;

		VIDefect &AddDefect();
		void DeleteDefect(VIDefect &dDefect);
		void OrderUpDefect(VIDefect &dDefect);
		void OrderDownDefect(VIDefect &dDefect);

		vector<wstring> GetDefectIdList(bool bIncludeEmptyId = false);
	};

	class AFX_EXT_CLASS VIData {
	public:
		VILayer m_dLayer;
		VIDrawing m_dDrawing;
		map<Layer, VIInspection> m_mapInspection;

	public:
		VIData() {
			Init();
		}
		virtual ~VIData() {
			Clear();
		}

		void Init() {
		}

		void Clear() {
			m_dLayer.Clear();
			m_dDrawing.Clear();
			m_mapInspection.clear();

			Init();
		}
	};
};