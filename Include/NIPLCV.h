#pragma once

#include "NIPL.h"
#include "NIPLCVParam.h"
#include "NIPLCVResult.h"

#ifdef _WINDOWS
#ifndef _BUILD_NIPL_CV
#ifdef _DEBUG
#pragma comment (lib, "NIPLCV_d.lib")
#else
#pragma comment (lib, "NIPLCV.lib")
#endif  // _DEBUG
#endif  // _BUILD_NIPL_CV 
#endif  // _WINDOWS

#define NIPL_CV_METHOD_IMPL(FuncName) NIPL_ERR NIPLCV::FuncName(NIPLInput *pInput, NIPLOutput *pOutput)

class AFX_EXT_CLASS NIPLCV : public NIPL {
public :
	static shared_ptr<NIPLCV> GetInstance(bool bNew = false);

	NIPLCV();
	virtual ~NIPLCV();

//	NIPL_METHOD_DECL(DetectDefect);
//	NIPL_METHOD_DECL(ApplyExcludingRegionToMask);
	NIPL_METHOD_DECL(DetectFixture);
	NIPL_METHOD_DECL(FindLine);
	NIPL_METHOD_DECL(FindCircle);
	NIPL_METHOD_DECL(FindEllipse);
	NIPL_METHOD_DECL(SetCircleROI);
	NIPL_METHOD_DECL(Circle2Rect);
	NIPL_METHOD_DECL(Rect2Circle);
	NIPL_METHOD_DECL(FitBackground);
	NIPL_METHOD_DECL(Binarize);
	NIPL_METHOD_DECL(Diff);
	NIPL_METHOD_DECL(EliminateNoise);
	NIPL_METHOD_DECL(FindBlob);
	NIPL_METHOD_DECL(FillHole);
	NIPL_METHOD_DECL(AnalyzeShape);
	NIPL_METHOD_DECL(FitToCorrectCircle);
	NIPL_METHOD_DECL(Histogram);
	//	NIPL_METHOD_DECL(CheckScrew);
//	NIPL_METHOD_DECL(CheckLogo);

private : 
	void _CheckLogoSimilarity(Mat dInputImg, Mat dTemplate, Mat dOutputImg, NIPLParam_CheckLogo &dParam);
};