#pragma once

#include "NIPLResult.h"

struct AFX_EXT_CLASS NIPLDefect_LDC : public NIPLDefect
{
	enum DEFECT_TYPE {
		DEFECT_TYPE_TERMINAL = 0,
		DEFECT_TYPE_TUBE_RED,
		DEFECT_TYPE_TUBE_BLACK,
		DEFECT_TYPE_GUIDE,
		DEFECT_TYPE_PAPER,
		DEFECT_TYPE_CLIP,
		DEFECT_TYPE_BOLT,
	};

	NIPLDefect_LDC(int nType, Rect rcBoundingBox, int nSetId = 0) : NIPLDefect(nType, rcBoundingBox, nSetId, 0)
	{
	}
};

class AFX_EXT_CLASS NIPLResult_Defect_LDC : public NIPLResult_Defect
{
public:
	NIPLResult_Defect_LDC() : NIPLResult_Defect() {
	}
	virtual ~NIPLResult_Defect_LDC() {
	}
};


//
//  MagCore
// 

struct AFX_EXT_CLASS NIPLDefect_MagCore : public NIPLDefect
{
	enum DEFECT_TYPE {
		DEFECT_TYPE_SCRETCH = 0,
		DEFECT_TYPE_BROKEN_CHARACTER,
		DEFECT_TYPE_EDGE,
		DEFECT_TYPE_SURFACE,
		DEFECT_TYPE_HOLE,
		DEFECT_TYPE_BLOCK = 100	// This is only for debugging to draw block reactangle
	};

	NIPLDefect_MagCore(int nType, Rect rcBoundingBox, int nSize) : NIPLDefect(nType, rcBoundingBox, 0, nSize)
	{
	}
};

class AFX_EXT_CLASS NIPLResult_Defect_MagCore : public NIPLResult_Defect
{
public:
	NIPLResult_Defect_MagCore() : NIPLResult_Defect() {
	}
	virtual ~NIPLResult_Defect_MagCore() {
	}
};
