#pragma once

#include <vector>
#include <list>
#include <xstring>
using namespace std;

#include "opencv2/opencv.hpp"
using namespace cv;

#include "NIPL_AutoRepair_Data.h"

#ifndef _BUILD_NIPL_AUTO_REPAIR
#ifdef _DEBUG
#pragma comment (lib, "NIPL_AutoRepair_d.lib")
#else
#pragma comment (lib, "NIPL_AutoRepair.lib")
#endif
#endif

// Define Macro for Err checking
#define ERR_SUCCESS(nErr) (nErr == NIPL_AutoRepair::Err::SUCCESS)
#define ERR_FAIL(nErr) (!ERR_SUCCESS(nErr))

//
// Time Profile 관련 매크로 및 inline 함수
//
#define TIME_PROFILE_DECL(proc) \
	DWORD m_nStartTime_##proc##; \
	DWORD m_nElapsedTime_##proc##; \
	void SetStartTime_##proc##() { m_nStartTime_##proc## = ::GetTickCount(); } \
	void SetElpasedTime_##proc##() { m_nElapsedTime_##proc##= ::GetTickCount() - m_nStartTime_##proc##; } 

#define TIME_PROFILE_CLEAR(proc) \
	m_nStartTime_##proc## = 0; \
	m_nElapsedTime_##proc## = 0;

#define TIME_PROFILE_STRING(proc) GetTimeProfileString(m_nElapsedTime_##proc##, m_nElapsedTime_Total).c_str()

inline wstring GetTimeProfileString(DWORD nTime, WORD nTotlaTime)
{
	wchar_t szText[256];
	if (nTotlaTime == nTime) {
		swprintf_s(szText, L"%d ms", nTime);
	}
	else {
		swprintf_s(szText, L"%d ms (%.2f%%)", nTime, ((float)nTime / nTotlaTime) * 100.f);
	}

	return szText;
}


namespace NIPL_AutoRepair {
	// 에러 코드
	enum class Err {
		SUCCESS = 0,
		FAIL = 1,
		DUPLICATED_ID,
		DIFFERENT_IMAGE_SIZE,

		FAIL_TO_LOAD_IMAGE_FILE = 100,
		FAIL_TO_LOAD_DATA_FILE,
		FAIL_TO_LOAD_MASK_FILE,
		FAIL_TO_SAVE_IMAGE_FILE,
		FAIL_TO_SAVE_DATA_FILE,
		FAIL_TO_DETECT,
		FAIL_TO_CLASSIFY,
		FAIL_TO_DRAW,

		INVALID_DATA_FILE = 200,
		INVALID_DATA_VALUE,

		NOT_FOUND_LAYER_DATA = 300,
		NOT_FOUND_TEMPLATE_IMAGE,
		NOT_FOUND_MASK_IMAGE,
		NOT_FOUND_DRAWING_MASK_IMAGE,
		NOT_FOUND_TARGET,
		NOT_FOUND_DEFECT
	};

	// 에러 코드를 문자열로 반환해주는 함수
	AFX_EXT_API wstring GetErrMessage(Err nErr);

	// 출력 이미지 Type
	enum class OutputImageType {
		DRAWING,
		DEFECT,
		DEFECT_WITH_MASK,
		DMD
	};

	// Time Profile
	struct TimeProfile {
		//
		// TIME_PROFILE_DECL 의 파라미터의 명칭에 따라 다음과 같은 변수 및 함수가 정의된다.
		// 예) 명칭 : Total
		// 	DWORD m_nStartTime_Total;
		//	DWORD m_nElapsedTime_Total;
		//	void SetStartTime_Total();
		//	void SetElpasedTime_Total();
		//
		// 처리 시간은 m_nElapsedTime_XXX 변수를 참고하면 됨. (ms 단위)

		TIME_PROFILE_DECL(Total);									// 전체 시간 

		TIME_PROFILE_DECL(FindTarget);								// Target 검색 시간
		TIME_PROFILE_DECL(FindTarget_Pos);								// Target 위치 검색 시간
		TIME_PROFILE_DECL(FindTarget_SetTargetInfoList);				// Target 정보 리스트 설정 시간

		TIME_PROFILE_DECL(DetectDefect);							// 불량 검출 시간
		TIME_PROFILE_DECL(DetectDefect_MakeTemplateDiffImage);			// Template 자체 차분 이미지 생성 시간
		TIME_PROFILE_DECL(DetectDefect_MakeDiffImage);					// 차분 이미지 생성 시간
		TIME_PROFILE_DECL(DetectDefect_MakeDiffImage_AdjustImageBackground);	// 차분 이미지 배경 평탄화 시간
		TIME_PROFILE_DECL(DetectDefect_Binarize);						// 이진화 시간
		TIME_PROFILE_DECL(DetectDefect_SetDefectTargetInfo);			// 불량 Target 정보 설정 시간

		TIME_PROFILE_DECL(CheckDefect);								// 불량 판별 시간
		TIME_PROFILE_DECL(CheckDefect_Target1);							// 첫번째 Target 에 대한 불량 판별 시간
		TIME_PROFILE_DECL(CheckDefect_Target2);							// 두번째 Target 에 대한 불량 판별 시간
		TIME_PROFILE_DECL(CheckDefect_MultiTarget);						// 2 Pixel 불량 판별 시간
		TIME_PROFILE_DECL(CheckDefect_DarkOrSeed);						// Dark 또는 Seed 불량 판별 시간

		TIME_PROFILE_DECL(DrawDefect);								// 불량 Drawing 시간

		void Clear() {
			TIME_PROFILE_CLEAR(Total);

			TIME_PROFILE_CLEAR(FindTarget);
			TIME_PROFILE_CLEAR(FindTarget_Pos);
			TIME_PROFILE_CLEAR(FindTarget_SetTargetInfoList);

			TIME_PROFILE_CLEAR(DetectDefect);
			TIME_PROFILE_CLEAR(DetectDefect_MakeTemplateDiffImage);
			TIME_PROFILE_CLEAR(DetectDefect_MakeDiffImage);
			TIME_PROFILE_CLEAR(DetectDefect_MakeDiffImage_AdjustImageBackground);
			TIME_PROFILE_CLEAR(DetectDefect_Binarize);
			TIME_PROFILE_CLEAR(DetectDefect_SetDefectTargetInfo);

			TIME_PROFILE_CLEAR(CheckDefect);
			TIME_PROFILE_CLEAR(CheckDefect_Target1);
			TIME_PROFILE_CLEAR(CheckDefect_Target2);
			TIME_PROFILE_CLEAR(CheckDefect_MultiTarget);
			TIME_PROFILE_CLEAR(CheckDefect_DarkOrSeed);

			TIME_PROFILE_CLEAR(DrawDefect);
		}

		void DebugPrint();
	};

	// 입력 파라미터
	class AFX_EXT_CLASS Param
	{
	public:
		Layer m_nLayer;							// 처리할 Layer (Active/Gate/SD)
		Mat m_dInputImage;						// 검사 대상 입력 이미지
		OutputImageType m_nOutputImageType;		// 결과로 받을 출력 이미지 타입

		Param() {
			Init();
		}
		virtual ~Param() {
			Clear();
		}
		void Clear() {
			m_dInputImage.release();

			Init();
		}
		void Init() {
			m_nLayer = Layer::ACTIVE;
			m_nOutputImageType = OutputImageType::DRAWING;
		}
	};

	// 출력 결과
	class AFX_EXT_CLASS Result
	{
	public:
		bool m_bFoundDefect;					// 불량 검출 여부
		wstring m_strDefectId;					// 불량 식별자 (데이터 파일에서 지정한 문자열)

		Mat m_dOutputImage;						// 결과 출력 이미지
		Mat m_dDMDImage;						// DMD 이미지 (출력 이미지 Type 이 DMD 일경우에만 저장됨)

		Rect m_rcDMDArea;						// DMD 영역의 위치 정보
		vector<Rect> m_listDefectPixelArea;		// 불량 영역이 검출된 Pixel 영역의 정보

		TimeProfile m_dTimeProfile;				// Time Profile

	public:
		Result() {
			Init();
		}
		virtual ~Result() {
			Clear();
		}
		void Clear() {
			m_strDefectId.clear();
			m_dDMDImage.release();
			m_dOutputImage.release();

			m_listDefectPixelArea.clear();

			m_dTimeProfile.Clear();

			Init();
		}
		void Init() {
			m_bFoundDefect = false;
			m_rcDMDArea = Rect(0, 0, 0, 0);
		}
	};

	// API Interface
	class AFX_EXT_CLASS Process {
	public:
		static Process &GetInstance();

		virtual Err LoadImage(const wstring strImagePath, Mat &dImage) = 0;
		virtual Err SaveImage(const wstring strImagePath, Mat dImage) =  0;
		virtual Err LoadData(const wstring strDataPath) = 0;
		virtual Err SaveData(const wstring strDataPath) = 0;
		virtual VIData &GetData() = 0;

		virtual Err Inspect(const Param &dParam, Result &dResult) = 0;

		virtual vector<wstring> GetMaskIdList() = 0;
		virtual Mat GetMaskImage(wstring strId) = 0;

		virtual Mat AdjustImageBackground(const Mat dImg, Layer nLayer, bool bGetFitBackground = false) = 0;
		virtual Mat FindTemplateTarget(const Mat dImg, Layer nLayer, Rect &rcBestFitArea, vector<Rect> &listTargetArea) = 0;
		virtual Mat RotateImage(const Mat dImage, Rotation nRotation, bool bHorizFlip = false, bool bVertFlip = false) = 0;
		virtual Mat ReverseRotateImage(const Mat dImage, Rotation nRotation, bool bHorizFlip = false, bool bVertFlip = false) = 0;
	};
};