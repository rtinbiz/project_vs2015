#pragma once

#include "NIPL_AutoRepair.h"
#include "NIPLCV.h"
#include "Xml/xml3.h"

using namespace XML3;
using namespace std;

//
// Error 관련 매크로
//
#define RETURN_SUCCESS() return Err::SUCCESS
#define RETURN_FAIL(err) return Err::err;
#define RETURN_ON_FAIL(Func) { Err nErr = Func; if (ERR_FAIL(nErr)) return nErr; }

//
// 데이터 로딩 관련 매크로
//
#define DEBUG_PRINT_XML_INVALID_VALUE(str) m_pCV->DebugPrint(L"              ! Warning. No or Invalid value : %s", str2wstr(str).c_str())
#define XML_TO_DATA_VALUE(var, str, func) try { var = func(pXMLChild->v(str)); } \
	catch (...) { DEBUG_PRINT_XML_INVALID_VALUE(str); }
#define XML_TO_DATA_STRING(var, str) XML_TO_DATA_VALUE(var, str, str2wstr)
#define XML_TO_DATA_BOOL(var, str) XML_TO_DATA_VALUE(var, str, STRING_TO_BOOL)
#define XML_TO_DATA_INT(var, str) XML_TO_DATA_VALUE(var, str, stoi)
#define XML_TO_DATA_FLOAT(var, str) XML_TO_DATA_VALUE(var, str, stof)
#define XML_TO_DATA_METHOD(method, str) try { method(str2wstr(pXMLChild->v(str))); } \
	catch (...) { DEBUG_PRINT_XML_INVALID_VALUE(str); }
#define XML_TO_DATA_COLOR(var, str) \
	try { \
		wstring strColor = str2wstr(pXMLChild->v(str)); \
		var = STRING_TO_COLOR(strColor); \
	} \
	catch (...) { DEBUG_PRINT_XML_INVALID_VALUE(str); }

//
// 데이터 저장 관련 매크로
//
#define DATA_TO_XML_VALUE(xml, str, var, func) (xml).AddVariable(str, func(var).c_str())
#define DATA_TO_XML_STRING(xml, str, var) \
	if(!wstring(var).empty()) DATA_TO_XML_VALUE(xml, str, var, wstr2str)
#define DATA_TO_XML_STRING_CHECK_DEFAULT(xml, str, var, def) if(!CHECK_STRING(var, def)) DATA_TO_XML_STRING(xml, str, var)
#define DATA_TO_XML_INT(xml, str, var) \
	if(var != 0) { \
		wchar_t szValue[16]; \
		wsprintf(szValue, L"%d", var); \
		DATA_TO_XML_STRING(xml, str, szValue); \
		}
#define DATA_TO_XML_INT_CHECK_DEFAULT(xml, str, var, def) if(var != def) DATA_TO_XML_INT(xml, str, var)
#define DATA_TO_XML_FLOAT(xml, str, var) \
	if(var != 0.f) { \
		wchar_t szValue[16]; \
		swprintf_s(szValue, L"%.3f", var); \
		DATA_TO_XML_STRING(xml, str, szValue); \
		}
#define DATA_TO_XML_FLOAT_CHECK_DEFAULT(xml, str, var, def) if(var != def) DATA_TO_XML_FLOAT(xml, str, var)
#define DATA_TO_XML_BOOL(xml, str, var) \
	if (var != false) {	\
		DATA_TO_XML_STRING(xml, str, BOOL_TO_WSTRING(var)); \
		}
#define DATA_TO_XML_COLOR(xml, str, var) \
	if (var[0] != 0 || var[1] != 0 || var[2] != 0) { \
		wstring strColor = COLOR_TO_WSTRING(var); \
		DATA_TO_XML_STRING(xml, str, strColor); \
	}

//
// Color 픽셀 관련 매크로
//
#define GET_COLOR_RED(color) color[2]
#define GET_COLOR_GREEN(color) color[1]
#define GET_COLOR_BLUE(color) color[0]

// 두 픽셀간의 거리 계산
#define CALC_DISTANCE(pt1, pt2) sqrt(pow(pt1.x - pt2.x, 2) + pow(pt1.y - pt2.y, 2))

//
// AutoRepair
// 
namespace NIPL_AutoRepair {
	//
	// Target 위치 정보
	//
	struct TargetPosInfo {
		int m_nPos;						// Target 영역 시작 좌표
		int m_nSize;					// Target 영역 크기
		int m_nTemplateSize;			// Target 에 적용되는 Template 이미지의 전체 크기

		TargetPosInfo() {
			Init();
		}

		TargetPosInfo(int nPos, int nSize, int nTemplateSize = 0) {
			m_nPos = nPos;
			m_nSize = nSize;
			if (nTemplateSize == 0) {
				m_nTemplateSize = nSize;
			}
			else {
				m_nTemplateSize = nTemplateSize;
			}
		}

		virtual ~TargetPosInfo() {
			Clear();
		}

		void Init() {
			m_nPos = -1;
			m_nSize = -1;
			m_nTemplateSize = -1;
		}

		void Clear() {
			Init();
		}
	};

	//
	// Target 정보
	//
	struct TargetInfo {
		int m_nIndexX;						// 2차원 Target 배열상의 가로 Index 
		int m_nIndexY;						// 2차원 Target 배열상의 세로 Index

		bool m_bSeed;						// Target 영역에 Seed 영역이 있는지 여부
		bool m_bDark;						// Target 영역에 Dark 영역이 있는지 여부

		Rect m_rcArea;						// 검사 이미지상의 Target 영역의 위치
		Size m_sizeTemplate;				// Target 영역에 적용되는 Template 이미지의 전체 크기
		Rect m_rcTemplatePixelArea;			// Target 영역과 대응되는 Template Pixel 영역의 위치
		int m_nTemplatePixelIndex;			// Target 영역과 대응되는 Template Pixel 의 Index

		int m_nDefectSize;					// Target 영역에 포함된 불량 영역의 크기

		TargetInfo() {
			Init();
		}

		virtual ~TargetInfo() {
			Clear();
		}

		void Init() {
			m_nIndexX = -1;
			m_nIndexY = -1;

			m_bDark = false;
			m_bSeed = false;

			m_rcArea = Rect(0, 0, 0, 0);
			m_sizeTemplate = Size(0, 0);
			m_rcTemplatePixelArea = Rect2f(0, 0, 0, 0);
			m_nTemplatePixelIndex = 0;

			m_nDefectSize = 0;
		}

		void Clear() {
			Init();
		}

		// Target 영역의 중심 위치 반환
		Point GetCenterPos() const
		{
			Point ptCenterPos;
			ptCenterPos.x = cvRound(m_rcArea.x + m_rcArea.width * 0.5f);
			ptCenterPos.y = cvRound(m_rcArea.y + m_rcArea.height * 0.5f);

			return ptCenterPos;
		}
	};

	//
	// 불량 판별용 Target 정보
	//
	struct DefectTargetInfo {
		bool m_bConnectedTarget;				// 인접한 불량 Target 이 있는지 여부 (2 Pixel 불량)
		bool m_bConnectedVertically;			// 세로 방향으로 인접해있는지 여부
		bool m_bDark;							// Target 영역에 Seed 영역이 있는지 여부
		bool m_bSeed;							// Target 영역에 Dark 영역이 있는지 여부

		TargetInfo m_dTargetInfo;				// 첫번째 불량 Target 정보
		TargetInfo m_dTargetInfo2;				// 두번째 불량 Target 정보

		DefectTargetInfo() {
			Init();
		}

		virtual ~DefectTargetInfo() {
			Clear();
		}

		void Init() {
			m_bConnectedTarget = false;
			m_bConnectedVertically = false;
			m_bDark = false;
			m_bSeed = false;
		}

		void Clear() {
			m_dTargetInfo.Clear();
			m_dTargetInfo2.Clear();

			Init();
		}

		// 전체 Target 영역의 중심 위치 반환
		Point GetCenterPos() const
		{
			if (!m_bConnectedTarget) {
				return m_dTargetInfo.GetCenterPos();
			}

			Point ptFirstTargetCenterPos = m_dTargetInfo.GetCenterPos();
			Point ptSecondTargetCenterPos = m_dTargetInfo2.GetCenterPos();

			Point ptCenterPos;
			ptCenterPos.x = cvRound((ptFirstTargetCenterPos.x + ptSecondTargetCenterPos.x) * 0.5f);
			ptCenterPos.y = cvRound((ptFirstTargetCenterPos.y + ptSecondTargetCenterPos.y) * 0.5f);

			return ptCenterPos;
		}

		// 전체 Target 영역에 포함된 불량 영역의 크기 반환
		int GetDefectSize() const
		{
			if (!m_bConnectedTarget) {
				return m_dTargetInfo.m_nDefectSize;
			}

			return m_dTargetInfo.m_nDefectSize + m_dTargetInfo2.m_nDefectSize;
		}
	};

	//
	// AutoRepair Core 알고리즘
	//
	class ProcessCore : public Process {
	public:
		static ProcessCore s_dThis;

	private :
		shared_ptr<NIPLCV> m_pCV;								// NIPL CV 라이브러리를 사용하기위한 객체 포인터
		VIData m_dVIData;							// AutoRepair 데이터를 담고 있는 객체

		Param m_dParam;								// 입력 파라미터
		Result m_dResult;							// 출력 결과

		Mat m_dDiffImg;								// 차분 이미지
		Mat m_dAdjustedBackgroundImg;				// 배경 밝기가 보정된 이미지
		Mat m_dDetectResultImg;						// 불량 영역이 포함된 이미지
		Mat m_dDrawingOverlapMaskImg;				// Drawing 용 OverlapMask 이미지
		Mat m_dDefectWidthMaskImg;					// 불량 영역과 접촉 Mask 가 함께 그려진 이미지
		Rect m_rcBestFitTarget;						// Template 이미지와 유사도가 가장 높은 Target 영역의 위치
		Size m_sizeTargetGrid;						// Target 영역에 대한 2차원 배열(Grid) 가로/세로 크기

		vector<TargetInfo> m_listTargetInfo;		// Target 정보 리스트
		vector<VIDraw> m_listDefectTargetDraw;		// 불량 그리기(Draw) 리스트
		DefectTargetInfo m_dDefectTargetInfo;		// 불량이 포함된 Target 정보

		Mat m_dDebugImg;							// only for debug, 디버그용 이미지를 생성하면 시뮬레이터에서 출력이 됨.

	public:
		ProcessCore();
		~ProcessCore();

		void Init();
		void Clear(bool bIncludeVIData = true);

		//
		// API 관련 함수
		//
		virtual Err LoadImage(const wstring strImagePath, Mat &dImage) override;
		virtual Err SaveImage(const wstring strImagePath, Mat dImage) override;
		virtual Err LoadData(const wstring strDataPath) override;
		virtual Err SaveData(const wstring strDataPath) override;
		virtual VIData &GetData() override;

		virtual Err Inspect(const Param &dParam, Result &dResult) override;

		virtual vector<wstring> GetMaskIdList() override;
		virtual Mat GetMaskImage(wstring strId) override;

		virtual Mat AdjustImageBackground(const Mat dImg, Layer nLayer, bool bGetFitBackground = false) override;
		virtual Mat FindTemplateTarget(const Mat dImg, Layer nLayer, Rect &rcBestFitArea, vector<Rect> &listTargetArea) override;
		virtual Mat RotateImage(const Mat dImage, Rotation nRotation, bool bHorizFlip = false, bool bVertFlip = false) override;
		virtual Mat ReverseRotateImage(const Mat dImage, Rotation nRotation, bool bHorizFlip = false, bool bVertFlip = false) override;

	private:
		//
		// 데이터 로딩 관련 함수
		//
		Err _LoadData(const wstring strDataPath);
		Err _LoadData_Layer(const shared_ptr<XMLElement> &pXML);
		Err _LoadData_Drawing(const shared_ptr<XMLElement> &pXML);
		Err _LoadData_Drawing_DrawSet(const shared_ptr<XMLElement> &pXML, VIDrawSet &dDrawSet);
		Err _LoadData_Inspection(const shared_ptr<XMLElement> &pXML);
		Err _LoadData_Inspection_Layer(const shared_ptr<XMLElement> &pXML, VIInspection &dInspection);
		Err _LoadData_Inspection_TemplatePixel(const shared_ptr<XMLElement> &pXML, VITemplate &dTemplate);
		Err _LoadData_Inspection_Binarize(const shared_ptr<XMLElement> &pXML, VIBinarize &dBinarize);
		Err _LoadData_Inspection_Defect(const shared_ptr<XMLElement> &pXML, VIDefect &dDefect);
		Err _LoadData_Inspection_Pixel(const shared_ptr<XMLElement> &pXML, VIPixel &dPixel);
		Err _LoadMask();

		//
		// 데이터 저장 관련 함수
		//
		Err _SaveData(const wstring strDataPath);
		Err _SaveData_Layer(XMLElement *pXML);
		Err _SaveData_Drawing(XMLElement *pXML);
		Err _SaveData_Inspection(XMLElement *pXML);
		Err _SaveData_Inspection_Layer(XMLElement *pXML, const VIInspection &dInspection);
		Err _SaveData_Inspection_Defect(XMLElement *pXML, const VIDefect &dDefect);

		//
		// 전처리 관련 함수
		//
		Err _Preprocess();
		Err _AdjustImageBackground(const Mat &dImg, int nTargetCountX, int nTargetCountY, const Mat &dBackgroundMask, Mat &dFitBackground);
		Err _SetTemplatePixelArea();
		Err _TransformTemplateImage();

		//
		// Target 검색 관련 함수
		//
		Err _FindTarget();
		Err _FindTarget(bool bReplace, float nMinSizeRatio, vector<TargetInfo> &listTargetInfo);
		Err _FindTarget_PosInfo(bool bVert, const Mat dImg, const Mat &dTemplateImg, vector<TargetPosInfo> &listTargetPosInfo);

		//
		// 불량 검출 관련 함수
		//
		Err _DetectDefect(bool &bFoundDefect);
		Err _DetectDefect_MakeDiffImage(bool bAdjustImageBackground, bool bGetFitBackground = false);
		Err _DetectDefect_ApplyMask(const TargetInfo &dTargetInfo, Mat &dResultImg);
		Err _DetectDefect_DarkOrSeed(const TargetInfo &dTargetInfo, const Mat &dTargetResultImg, Mat &dResultImg);
		Err _DetectDefect_SetDefectTarget(const DefectTargetInfo &dDefectTargetInfo);
		Err _DetectDefect_AddConnectedDefectTarget(int nIndexX, int nIndexY, vector<TargetInfo> &listDefectTargetInfo, Mat &dTargetGrid, Mat &dTargetBoundary);

		//
		// 불량 판별 관련 함수
		//
		Err _CheckDefect(bool &bFoundDefect);
		Err _CheckDefect_Target(const TargetInfo &dTargetInfo, vector<VIDefect> &listDefect_Target);
		Err _CheckDefect_MultiTarget(const vector<VIDefect> &listDefect_Target, const vector<VIDefect> &listDefect_Target2, wstring &strDefectId);
		Err _CheckDefect_DarkOrSeed(wstring &strDefectId);
		Err _CheckDefect_DrawDefectWithMask(const TargetInfo &dTargetInfo, const VIPixel &dPixel);
		Err _CheckDefect_Pixel(const vector<VIDefect> &listDefect_Target, const VIPixel &dPixel, VIDefect &dDefect, bool &bFoundDefect);
		Err _CheckDefect_Pixel(const TargetInfo &dTargetInfo, const VIPixel &dPixel, VIDefect::Connected nConnected, bool bFirstPixel, bool bCheckConnectedDefect, Mat &dConnectedDefectImg, bool &bFoundDefect);
		Err _CheckDefect_MultiPixel(const TargetInfo &dTargetInfo, const vector<VIDefect> &listDefect_Target, const VIPixel &dPixel,
			const TargetInfo &dTargetInfo2, const vector<VIDefect> &listDefect_Target2, const VIPixel &dPixel2, VIDefect::Connected nConnected, bool bCheckConnectedDefect, bool &bFoundDefect);
		Err _CheckDefect_Mask(const TargetInfo &dTargetInfo, const VICheck &dCheck, int nMinDefectSize, VIDefect::Connected nConnected, bool bFirstPixel, Mat &dConnectedDefectImg, bool &bFoundDefect);
		Err _CheckDefect_AddDefectTargetDraw(const VIDefect &dDefect, bool bClearFirst = false);

		//
		// 불량 Drawing 관련 함수
		//
		Err _DrawDefect();
		Err _DrawDefect_DrawSet(const wstring &strId);
		Err _DrawDefect_Draw(const VIDraw &dDraw);
		Err _DrawDefect_GetBoundingBoxMask(Mat &dBoundingBoxMaskImg, int nExtSize, bool bDMD = false);
		Err _DrawDefect_GetBoundingBox(Rect &rcDefectBox);
		Err _DrawDefect_DrawDMD(const VIDraw &dDraw);
		Err _DrawDefect_DrawDefectBox(const VIDraw &dDraw);
		Err _DrawDefect_DrawDefect(const VIDraw &dDraw);
		Err _DrawDefect_DrawMask(const VIDraw &dDraw);
		Err _DrawDefect_DrawArea(Mat &dImg, const VIDraw &dDraw, bool bFullImg = false);
		Err _DrawDefect_DrawOutput(Mat &dOutputImg, const Mat &dAreaImg, const VIDraw &dDraw);

		// 이미지 변환 관련 함수
		Err _GetTransformTemplateImage(wstring stdId, Mat &dTemplateImg);
		Err _GetTransformMaskImage(wstring stdId, Mat &dMaskImg);
		Err _RotateImage(Mat &dImg);
		Err _RotateImage(Mat &dImg, Rotation nRotation, bool bHorizFlip, bool bVertFlip);
		Err _ReverseRotateImage(Mat &dImg);
		Err _ReverseRotateImage(Mat &dImg, Rotation nRotation, bool bHorizFlip, bool bVertFlip);
	};
};