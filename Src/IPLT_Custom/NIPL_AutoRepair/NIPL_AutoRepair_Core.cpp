// Process.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NIPL_AutoRepair_Core.h"
#include "NIPL_AutoRepair.h"
#include <set>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define CLEAR_TIME_PROFILE() m_dResult.m_dTimeProfile.Clear();
#define SET_TIME_PROFILE_START(proc) m_dResult.m_dTimeProfile.SetStartTime_##proc##();
#define SET_TIME_PROFILE_ELAPSED(proc) m_dResult.m_dTimeProfile.SetElpasedTime_##proc##();
#define DEBUG_PRINT_TIME_PROFILE() m_dResult.m_dTimeProfile.DebugPrint();

namespace NIPL_AutoRepair {
	// 주어진 Rect 영역을 특정 크기만큼 확장
	inline Rect EXTEND_BOX_SIZE(Rect rcBoundingBox, int nExtSizeLeft, int nExtSizeRight, int nExtSizeTop, int nExtSizeBottom, int nImgSizeX, int nImgSizeY)
	{
		int nStartX = rcBoundingBox.x - nExtSizeLeft;
		int nStartY = rcBoundingBox.y - nExtSizeTop;
		int nEndX = rcBoundingBox.br().x - 1 + nExtSizeRight;
		int nEndY = rcBoundingBox.br().y - 1 + nExtSizeBottom;
		if (nStartX < 0) nStartX = 0;
		if (nStartY < 0) nStartY = 0;
		if (nEndX > nImgSizeX - 1) nEndX = nImgSizeX - 1;
		if (nEndY > nImgSizeY - 1) nEndY = nImgSizeY - 1;

		return Rect(nStartX, nStartY, nEndX - nStartX + 1, nEndY - nStartY + 1);
	}

	// 주어진 Rect 영역을 특정 크기만큼 확장
	inline Rect EXTEND_BOX_SIZE(Rect rcBoundingBox, int nExtendSize, int nImgSizeX, int nImgSizeY)
	{
		return EXTEND_BOX_SIZE(rcBoundingBox, nExtendSize, nExtendSize, nExtendSize, nExtendSize, nImgSizeX, nImgSizeY);
	}

	inline Rect MERGE_TARGET_AREA(Rect &rcTarget1, Rect &rcTarget2)
	{
		int nStartX = min(rcTarget1.x, rcTarget2.x);
		int nStartY = min(rcTarget1.y, rcTarget2.y);
		int nEndX = max(rcTarget1.br().x, rcTarget2.br().x) - 1;
		int nEndY = max(rcTarget1.br().y, rcTarget2.br().y) - 1;

		Rect rcMerge(nStartX, nStartY, nEndX - nStartX + 1, nEndY - nStartY + 1);

		rcTarget1.x -= rcMerge.x;
		rcTarget1.y -= rcMerge.y;
		rcTarget2.x -= rcMerge.x;
		rcTarget2.y -= rcMerge.y;

		return rcMerge;
	}

	ProcessCore ProcessCore::s_dThis;

	Process &Process::GetInstance()
	{
		return ProcessCore::s_dThis;
	}

	ProcessCore::ProcessCore()
	{
		Init();
	}

	ProcessCore::~ProcessCore()
	{
		Clear();
	}

	void ProcessCore::Init()
	{
		m_pCV = NIPLCV::GetInstance();
	}

	// 초기화
	void ProcessCore::Clear(bool bIncludeVIData)
	{
		m_dParam.Clear();
		m_dResult.Clear();

		m_dDiffImg.release();
		m_dAdjustedBackgroundImg.release();
		m_dDetectResultImg.release();
		m_dDrawingOverlapMaskImg.release();
		m_dDefectWidthMaskImg.release();
		m_sizeTargetGrid = Size(0, 0);

		m_listTargetInfo.clear();
		m_listDefectTargetDraw.clear();
		m_dDefectTargetInfo.Clear();

		m_dDebugImg.release();

		if (bIncludeVIData) {
			m_dVIData.Clear();
		}
	}

	// 이미지 로드
	Err ProcessCore::LoadImage(const wstring strImagePath, Mat &dImg)
	{
		if (NIPL_SUCCESS(m_pCV->LoadImage(strImagePath, dImg))) {
			m_pCV->DebugPrint(L"LoadImage : %s", strImagePath.c_str());
		}
		else {
			m_pCV->DebugPrint(L"LoadImage Fail : %s", strImagePath.c_str());
			RETURN_FAIL(FAIL_TO_LOAD_IMAGE_FILE);
		}

		RETURN_SUCCESS();
	}

	// 이미지 저장
	Err ProcessCore::SaveImage(const wstring strImagePath, Mat dImg)
	{
		if (NIPL_SUCCESS(m_pCV->SaveImage(strImagePath, dImg))) {
			m_pCV->DebugPrint(L"SaveImage : %s", strImagePath.c_str());
		}
		else {
			m_pCV->DebugPrint(L"SaveImage Fail : %s", strImagePath.c_str());
			RETURN_FAIL(FAIL_TO_SAVE_IMAGE_FILE);
		}

		RETURN_SUCCESS();
	}

	// AutoRepair 메인 이미지 검사 함수.
	Err ProcessCore::Inspect(const Param &dParam, /*out*/ Result &dResult)
	{
		Clear(false);

		SET_TIME_PROFILE_START(Total);

		m_dParam = dParam;

		// 1) 전처리
		RETURN_ON_FAIL(_Preprocess());

		//
		// 2) 불량 검출
		//
		// 2-1) Target 영역 추출
		SET_TIME_PROFILE_START(FindTarget);
		RETURN_ON_FAIL(_FindTarget());
		SET_TIME_PROFILE_ELAPSED(FindTarget);

		// 2-2) 불량 영역 검출
		bool bFoundDefect = false;
		SET_TIME_PROFILE_START(DetectDefect);
		RETURN_ON_FAIL(_DetectDefect(bFoundDefect));
		SET_TIME_PROFILE_ELAPSED(DetectDefect);

		if (bFoundDefect) {
			// 3) 불량 판별
			bFoundDefect = false;
			SET_TIME_PROFILE_START(CheckDefect);
			RETURN_ON_FAIL(_CheckDefect(bFoundDefect));
			SET_TIME_PROFILE_ELAPSED(CheckDefect);
			if (bFoundDefect) {
				if (m_dParam.m_nOutputImageType == OutputImageType::DRAWING || m_dParam.m_nOutputImageType == OutputImageType::DMD) {
					// 4) Drawing
					SET_TIME_PROFILE_START(DrawDefect);
					RETURN_ON_FAIL(_DrawDefect());
					SET_TIME_PROFILE_ELAPSED(DrawDefect);
				}
			}
		}

		// 디버그용 이미지가 생성되어 있을 경우, 결과 이미지에 저장하여 화면에 출력할 수 있도록 함.
		if (!CHECK_EMPTY_IMAGE(m_dDebugImg)) {
			m_dDebugImg.copyTo(m_dResult.m_dOutputImage);
		}

		dResult = m_dResult;

		SET_TIME_PROFILE_ELAPSED(Total);
		DEBUG_PRINT_TIME_PROFILE();

		RETURN_SUCCESS();
	}

	// 전처리
	Err ProcessCore::_Preprocess()
	{
		// Template 이미지 변형 (회전/타입변경)
		RETURN_ON_FAIL(_TransformTemplateImage());	// should do first

		// Template Pixel 의 크기 정보 확인
		RETURN_ON_FAIL(_SetTemplatePixelArea());

		RETURN_SUCCESS();
	}

	// 이미지 배경 밝기를 평탄하게 변경
	Err ProcessCore::_AdjustImageBackground(const Mat &dImg, int nTargetCountX, int nTargetCountY, const Mat &dBackgroundMask, Mat &dFitBackground)
	{
		// Block 수는 가로/세로 2개 이상은 되어야 한다.
		if (nTargetCountX < 2 && nTargetCountX < 2) {
			m_pCV->DebugPrint(L"[Adjust Image Background] Skip, Not Enough TargetCount (%d, %d) <2", nTargetCountX, nTargetCountY);

			dFitBackground = Mat::zeros(dImg.size(), CV_32FC1);
			RETURN_SUCCESS();
		}

		// Target 수가 8개 이하이면 2배를 함. 최대치는 16으로 제한
		int nBlockCountX = min(nTargetCountX <= 8 ? nTargetCountX * 2 : nTargetCountX, 16);
		int nBlockCountY = min(nTargetCountY <= 8 ? nTargetCountY * 2 : nTargetCountY, 16);

		// FitBackground
		NIPLParam_FitBackground dParam_FitBackground;
		dParam_FitBackground.m_nBlockCountX = nBlockCountX;
		dParam_FitBackground.m_nBlockCountY = nBlockCountY;
		dParam_FitBackground.m_nDegree = 2;
		dParam_FitBackground.m_nThreshold = 2.f;
		dParam_FitBackground.m_bSubtrackFromImage = false;

		m_pCV->DebugPrint(L"[Adjust Image Background] TargetCount : (%d, %d), BlockCount : (%d, %d), Degree : %d, Threshold : %.1f ====",
			nTargetCountX, nTargetCountY, nBlockCountX, nBlockCountY, dParam_FitBackground.m_nDegree, dParam_FitBackground.m_nThreshold);

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = dImg;
		dInput.m_dMask = dBackgroundMask;
		dInput.m_pParam = &dParam_FitBackground;
		NIPL_ERR nErr = m_pCV->FitBackground(&dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			dFitBackground = dOutput.m_dImg;
		}
		else {
			dFitBackground = Mat::zeros(dImg.size(), CV_32FC1);
		}

		RETURN_SUCCESS();
	}

	// Template Pixel 의 영역 위치/크기를 확인
	Err ProcessCore::_SetTemplatePixelArea()
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		VITemplate &dTemplate = dInspection.m_dTemplate;
		list<VITemplatePixel> &listTemplatePixel = dTemplate.m_listTemplatePixel;

		m_pCV->DebugPrint(L"==== [Set Template Pixel Area] Multi-Pixel : %s ====", BOOL_TO_WSTRING(dTemplate.m_bMultiPixel));

		if (dTemplate.m_bMultiPixel) {
			// 회전되지 않은 Template Pixel 이미지를 이용하여 Template Pixel Index 설정
			for (VITemplatePixel &dTemplatePixel : listTemplatePixel) {
				Mat dMaskImg = GetMaskImage(dTemplatePixel.m_strId);
				dTemplatePixel.m_rcPixelRatio = Rect2f(Point2f(0.f, 0.f), Size2f(1.f, 1.f));

				int nMaskImgSizeX = dMaskImg.cols;
				int nMaskImgSizeY = dMaskImg.rows;

				NIPLInput dInput;
				NIPLOutput dOutput;

				NIPLParam_FindBlob dParam_FindBlob;
				dParam_FindBlob.m_bFindInRange = true;

				dInput.m_dImg = dMaskImg;
				dInput.m_pParam = &dParam_FindBlob;
				NIPL_ERR nErr = m_pCV->FindBlob(&dInput, &dOutput);
				if (NIPL_SUCCESS(nErr)) {
					shared_ptr<NIPLResult_FindBlob> pResult_FindBlob = dOutput.GetResult<NIPLResult_FindBlob>();
					if (pResult_FindBlob != nullptr) {
						Rect rcBox;
						for (const auto &dBlob : pResult_FindBlob->m_listBlob) {
							rcBox = dBlob.m_rcBoundingBox;
							break;
						}

						// 위치 및 크기 비율 설정
						dTemplatePixel.m_rcPixelRatio.x = (float)rcBox.x / nMaskImgSizeX;
						dTemplatePixel.m_rcPixelRatio.y = (float)rcBox.y / nMaskImgSizeY;
						dTemplatePixel.m_rcPixelRatio.width = (float)rcBox.width / nMaskImgSizeX;
						dTemplatePixel.m_rcPixelRatio.height = (float)rcBox.height / nMaskImgSizeY;
					}
				}
			}

			// sort (시작 위치기준으로 상->좌->하->우 순으로 정렬)
			listTemplatePixel.sort([](const VITemplatePixel &left, const VITemplatePixel &right) {
				if (left.m_rcPixelRatio.y != right.m_rcPixelRatio.y) {
					return left.m_rcPixelRatio.y < right.m_rcPixelRatio.y;
				}

				return left.m_rcPixelRatio.x < right.m_rcPixelRatio.x;
			});

			// Template Pixel Index 설정.
			int nIndex = 0;
			for (VITemplatePixel &dTemplatePixel : listTemplatePixel) {
				dTemplatePixel.m_nIndex = nIndex;
				m_pCV->DebugPrint(L"TemplatePixel, Id : %s, Index : %d, Point(%.3f, %.3f), Size(%.3f, %.3f), Rotated Point(%.3f, %.3f), Size(%.3f, %.3f)",
					dTemplatePixel.m_strId.c_str(), dTemplatePixel.m_nIndex,
					dTemplatePixel.m_rcPixelRatio.x, dTemplatePixel.m_rcPixelRatio.y, dTemplatePixel.m_rcPixelRatio.width, dTemplatePixel.m_rcPixelRatio.height,
					dTemplatePixel.m_rcPixelRatio.x, dTemplatePixel.m_rcPixelRatio.y, dTemplatePixel.m_rcPixelRatio.width, dTemplatePixel.m_rcPixelRatio.height);

				nIndex++;
			}

			// 회전된 Template Pixel 이미지를 이용하여 Template Pixel 위치 및 크기 비율 설정
			for (VITemplatePixel &dTemplatePixel : listTemplatePixel) {
				Mat dMaskImg;
				RETURN_ON_FAIL(_GetTransformMaskImage(dTemplatePixel.m_strId, dMaskImg));

				dTemplatePixel.m_rcPixelRatio = Rect2f(Point2f(0.f, 0.f), Size2f(1.f, 1.f));

				int nMaskImgSizeX = dMaskImg.cols;
				int nMaskImgSizeY = dMaskImg.rows;

				NIPLInput dInput;
				NIPLOutput dOutput;

				NIPLParam_FindBlob dParam_FindBlob;

				dParam_FindBlob.m_bFindInRange = true;

				dInput.m_dImg = dMaskImg;
				dInput.m_pParam = &dParam_FindBlob;
				NIPL_ERR nErr = m_pCV->FindBlob(&dInput, &dOutput);
				if (NIPL_SUCCESS(nErr)) {
					shared_ptr<NIPLResult_FindBlob> pResult_FindBlob = dOutput.GetResult<NIPLResult_FindBlob>();
					if (pResult_FindBlob != nullptr) {
						Rect rcBox;
						for (const auto &dBlob : pResult_FindBlob->m_listBlob) {
							rcBox = dBlob.m_rcBoundingBox;
							break;
						}

						// 위치 및 크기 비율 설정
						dTemplatePixel.m_rcPixelRatio.x = (float)rcBox.x / nMaskImgSizeX;
						dTemplatePixel.m_rcPixelRatio.y = (float)rcBox.y / nMaskImgSizeY;
						dTemplatePixel.m_rcPixelRatio.width = (float)rcBox.width / nMaskImgSizeX;
						dTemplatePixel.m_rcPixelRatio.height = (float)rcBox.height / nMaskImgSizeY;
					}
				}
			}
		}

		RETURN_SUCCESS();
	}

	// 현재 설정된 Layer 의 Template 이미지 변형 (회전, 타입변경)
	Err ProcessCore::_TransformTemplateImage()
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		if (m_dVIData.m_mapInspection.find(nLayer) == m_dVIData.m_mapInspection.end()) {
			RETURN_FAIL(NOT_FOUND_LAYER_DATA);
		}

		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		VITemplate &dTemplate = dInspection.m_dTemplate;
		const wstring &strTemplateId = dTemplate.m_strId;

		m_pCV->DebugPrint(L"==== [Set Template] %s ====", strTemplateId.c_str());

		Mat &dTemplateImg = dInspection.m_dTemplate.GetTemplateImage();
		// Template 이미지 회전
		RETURN_ON_FAIL(_GetTransformTemplateImage(strTemplateId, dTemplateImg));
		if (CHECK_EMPTY_IMAGE(dTemplateImg)) {
			RETURN_FAIL(NOT_FOUND_TEMPLATE_IMAGE);
		}

		const Mat &dImg = m_dParam.m_dInputImage;
		// 검사 이미지와 Template 이미지의 채널수(칼라/흑백) 및 타입(8 bits Singed/Unsigned int or 32 bits float) 을 맞춰준다.
		if (dImg.channels() != dTemplateImg.channels()) {
			if (dImg.channels() == 3) {
				cvtColor(dTemplateImg, dTemplateImg, CV_GRAY2BGR);
			}
			else {
				cvtColor(dTemplateImg, dTemplateImg, CV_BGR2GRAY);
			}
		}
		if (dImg.type() != dTemplateImg.type()) {
			dTemplateImg.convertTo(dTemplateImg, dImg.type());
		}

		RETURN_SUCCESS();
	}

	// 주어진 이미지에서 Template 이미지와 유사한 Target 영역을 추출함
	Err ProcessCore::_FindTarget(bool bReplace, float nMinSizeRatio, vector<TargetInfo> &listTargetInfo)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		VITemplate &dTemplate = dInspection.m_dTemplate;
		Mat &dTemplateImg = dTemplate.GetTemplateImage();
		const Mat &dImg = m_dParam.m_dInputImage;

		if (CHECK_EMPTY_IMAGE(dTemplateImg)) {
			return Err::INVALID_DATA_VALUE;
		}

		m_pCV->DebugPrint(L"==== [Find Target] Replace : %s, nMinSizeRatio : %.2f", BOOL_TO_WSTRING(bReplace), nMinSizeRatio);

		vector<TargetPosInfo> listTargetPosInfoX;
		vector<TargetPosInfo> listTargetPosInfoY;

		// Template 이미지와 유사도가 제일 높은 최적 영역을 검색
		SET_TIME_PROFILE_START(FindTarget_Pos);
		// 최적 영역을 기준으로 가로 방향으로 Target 영역 위치 검색
		RETURN_ON_FAIL(_FindTarget_PosInfo(false, dImg, dTemplateImg, listTargetPosInfoX));
		// 최적 영역을 기준으로 세로 방향으로 Target 영역 위치 검색
		RETURN_ON_FAIL(_FindTarget_PosInfo(true, dImg, dTemplateImg, listTargetPosInfoY));
		SET_TIME_PROFILE_ELAPSED(FindTarget_Pos);

		//
		// 모든 Target 영역 검색
		// 
		int nImgSizeX = dImg.cols;
		int nImgSizeY = dImg.rows;

		int nPixelCount = (int)dTemplate.m_listTemplatePixel.size();
		int nPixelCountX = 1;
		int nPixelCountY = 1;
		if (dTemplate.m_bMultiPixel) {
			if (dTemplate.m_nRotation == Rotation::ROTATION_0 || dTemplate.m_nRotation == Rotation::ROTATION_90) {
				nPixelCountX = nPixelCount;
			}
			else {
				nPixelCountY = nPixelCount;
			}
		}

		float nBestFitMatch = -1.f;
		m_rcBestFitTarget = Rect(0, 0, 0, 0);
		set<int> listIneffectiveTargetIndexX;
		set<int> listIneffectiveTargetIndexY;

		listTargetInfo.clear();

		// 각 Pixel 영역의 평균 밝기를 Template 이미지의 평균 밝기와 같도록 하기 위해
		// Template 이미지의 평균 밝기를 구함
		Scalar dMean = mean(dTemplateImg);
		float nTemplateMean = (float)dMean[0];

		SET_TIME_PROFILE_START(FindTarget_SetTargetInfoList);
		// 검색된 가로/세로 방향의 Target 영역 정보를 이용하여 전체 Target 에 대한 정보 취득
		for (int i = 0; i < listTargetPosInfoY.size(); i++) {
			const TargetPosInfo &dTargetPosY = listTargetPosInfoY[i];
			int nPosY = dTargetPosY.m_nPos;
			int nTargetSizeY = dTargetPosY.m_nSize;
			int nTemplateSizeY = dTargetPosY.m_nTemplateSize;

			for (int j = 0; j < listTargetPosInfoX.size(); j++) {
				const TargetPosInfo &dTargetPosX = listTargetPosInfoX[j];
				int nPosX = dTargetPosX.m_nPos;
				int nTargetSizeX = dTargetPosX.m_nSize;
				int nTemplateSizeX = dTargetPosX.m_nTemplateSize;

				// Multi-Pixel 을 가진 Template 일 경우
				if (dTemplate.m_bMultiPixel) {
					for (const VITemplatePixel &dTemplatePixel : dTemplate.m_listTemplatePixel) {
						// Template Pixel 위치 및 크기 비율을 이용하여, 각 Pixel 의 실제 위치 와 크기를 구하여 Target 영역 으로 지정한다.
						int nPixelPosX = round(nPosX + dTemplatePixel.m_rcPixelRatio.x * nTemplateSizeX);
						int nPixelPosY = round(nPosY + dTemplatePixel.m_rcPixelRatio.y * nTemplateSizeY);
						int nPixelSizeX = round(dTemplatePixel.m_rcPixelRatio.width * nTemplateSizeX);
						int nPixelSizeY = round(dTemplatePixel.m_rcPixelRatio.height * nTemplateSizeY);
						int nPixelTargetSizeX = round(dTemplatePixel.m_rcPixelRatio.width * nTargetSizeX);
						int nPixelTargetSizeY = round(dTemplatePixel.m_rcPixelRatio.height * nTargetSizeY);

						m_pCV->DebugPrint(L"Find Target, MultiPixel, Target Size (%d, %d), TempalteSize (%d, %d), PixelSize(%.3f, %.3f)", nTargetSizeX, nTargetSizeY, nTemplateSizeX, nTemplateSizeY, dTemplatePixel.m_rcPixelRatio.width, dTemplatePixel.m_rcPixelRatio.height);

						int nTargetIndexX = j * nPixelCountX;
						int nTargetIndexY = i * nPixelCountY;
						switch (dTemplate.m_nRotation) {
						case Rotation::ROTATION_0: nTargetIndexX += dTemplatePixel.m_nIndex; break;
						case Rotation::ROTATION_90: nTargetIndexY += (nPixelCountY - (dTemplatePixel.m_nIndex + 1)); break;
						case Rotation::ROTATION_180: nTargetIndexX += (nPixelCountX - (dTemplatePixel.m_nIndex + 1)); break;
						case Rotation::ROTATION_270: nTargetIndexY += dTemplatePixel.m_nIndex; break;
						}

						// Target 크기 예외 조건 확인
						if (nPixelTargetSizeY < nPixelSizeY * nMinSizeRatio) {
							listIneffectiveTargetIndexY.insert(nTargetIndexY);
							m_pCV->DebugPrint(L"Find Target, MultiPixel, Too small height, Index (%d, %d), %d < %d (%.2f)", nTargetIndexY, nTargetIndexX, nTargetSizeY, (int)(nPixelSizeY * nMinSizeRatio), nMinSizeRatio);
							continue;
						}

						if (nPixelTargetSizeX < nPixelSizeX * nMinSizeRatio) {
							listIneffectiveTargetIndexX.insert(nTargetIndexX);
							m_pCV->DebugPrint(L"Find Target, MultiPixel, Too small width, Index (%d, %d), %d < %d (%.2f)", nTargetIndexY, nTargetIndexX, nTargetSizeX, (int)(nPixelSizeY * nMinSizeRatio), nMinSizeRatio);
							continue;
						}

						Rect rcTarget(nPixelPosX, nPixelPosY, nPixelSizeX, nPixelSizeY);

						if ((rcTarget.br().y - 1) < 0 || rcTarget.y > (nImgSizeY - 1)) {
							listIneffectiveTargetIndexY.insert(nTargetIndexY);
							m_pCV->DebugPrint(L"Find Target, MultiPixel, Not Shown Up/Down, Index (%d, %d), Pos(%d, %d), Size(%d, %d)", nTargetIndexY, nTargetIndexX, rcTarget.x, rcTarget.y, rcTarget.width, rcTarget.height);
							continue;
						}

						if ((rcTarget.br().x - 1) < 0 || rcTarget.x > (nImgSizeX - 1)) {
							listIneffectiveTargetIndexX.insert(nTargetIndexX);
							m_pCV->DebugPrint(L"Find Target, MultiPixel, Not Shown Left/Right, Index (%d, %d), Pos(%d, %d), Size(%d, %d)", nTargetIndexY, nTargetIndexX, rcTarget.x, rcTarget.y, rcTarget.width, rcTarget.height);
							continue;
						}

						if (rcTarget.x < 0) {
							rcTarget.width += rcTarget.x;
							rcTarget.x = 0;
						}
						if (rcTarget.y < 0) {
							rcTarget.height += rcTarget.y;
							rcTarget.y = 0;
						}
						if (rcTarget.br().x - 1 > (nImgSizeX - 1)) {
							rcTarget.width = nImgSizeX - rcTarget.x;
						}
						if (rcTarget.br().y - 1 > (nImgSizeY - 1)) {
							rcTarget.height = nImgSizeY - rcTarget.y;
						}

						TargetInfo dTargetInfo;
						dTargetInfo.m_rcArea = rcTarget;
						dTargetInfo.m_nIndexX = nTargetIndexX;
						dTargetInfo.m_nIndexY = nTargetIndexY;
						dTargetInfo.m_sizeTemplate = Size(nTemplateSizeX, nTemplateSizeY);
						dTargetInfo.m_rcTemplatePixelArea.x = dTemplatePixel.m_rcPixelRatio.x * nTemplateSizeX;
						dTargetInfo.m_rcTemplatePixelArea.y = dTemplatePixel.m_rcPixelRatio.y * nTemplateSizeY;
						dTargetInfo.m_rcTemplatePixelArea.width = rcTarget.width;
						dTargetInfo.m_rcTemplatePixelArea.height = rcTarget.height;
						if (rcTarget.width < nPixelSizeX && nTargetIndexX < nPixelCountX) {
							dTargetInfo.m_rcTemplatePixelArea.x = nTemplateSizeX - nTargetSizeX;
						}
						if (rcTarget.height < nPixelSizeY && nTargetIndexY < nPixelCountY) {
							dTargetInfo.m_rcTemplatePixelArea.y = nTemplateSizeY - nTargetSizeY;
						}
						dTargetInfo.m_nTemplatePixelIndex = dTemplatePixel.m_nIndex;

						listTargetInfo.push_back(dTargetInfo);

						m_pCV->DebugPrint(L"Find Target, MultiPixel, (%d, %d), Target (%d, %d) (%d, %d), TemplatePixel(%d, %d) (%d, %d), TemplatePixelIndex(%d)", 
							dTargetInfo.m_nIndexY, dTargetInfo.m_nIndexX,
							dTargetInfo.m_rcArea.x, dTargetInfo.m_rcArea.y, dTargetInfo.m_rcArea.width, dTargetInfo.m_rcArea.height,
							dTargetInfo.m_rcTemplatePixelArea.x, dTargetInfo.m_rcTemplatePixelArea.y, dTargetInfo.m_rcTemplatePixelArea.width, dTargetInfo.m_rcTemplatePixelArea.height,
							dTargetInfo.m_nTemplatePixelIndex);
					}
				}
				else {
					int nTargetIndexX = j;
					int nTargetIndexY = i;

					// Target 크기 예외 조건 확인
					if (nTargetSizeY < nTemplateSizeY * nMinSizeRatio) {
						listIneffectiveTargetIndexY.insert(nTargetIndexY);
						m_pCV->DebugPrint(L"Find Target, Too small height, Index (%d, %d), %d < %d (%.2f)", nTargetIndexY, nTargetIndexX, nTargetSizeY, (int)(nTemplateSizeY * nMinSizeRatio), nMinSizeRatio);
						continue;
					}

					if (nTargetSizeX < nTemplateSizeX * nMinSizeRatio) {
						listIneffectiveTargetIndexX.insert(nTargetIndexX);
						m_pCV->DebugPrint(L"Find Target, Too small width, Index (%d, %d), %d < %d (%.2f)", nTargetIndexY, nTargetIndexX, nTargetSizeX, (int)(nTemplateSizeX * nMinSizeRatio), nMinSizeRatio);
						continue;
					}

					Rect rcTarget(nPosX, nPosY, nTargetSizeX, nTargetSizeY);
					if (rcTarget.x < 0) rcTarget.x = 0;
					if (rcTarget.y < 0) rcTarget.y = 0;

					TargetInfo dTargetInfo;
					dTargetInfo.m_rcArea = rcTarget;
					dTargetInfo.m_nIndexX = nTargetIndexX;
					dTargetInfo.m_nIndexY = nTargetIndexY;
					dTargetInfo.m_sizeTemplate = Size(nTemplateSizeX, nTemplateSizeY);
					dTargetInfo.m_rcTemplatePixelArea.x = 0;
					dTargetInfo.m_rcTemplatePixelArea.y = 0;
					dTargetInfo.m_rcTemplatePixelArea.width = rcTarget.width;
					dTargetInfo.m_rcTemplatePixelArea.height = rcTarget.height;
					if (rcTarget.width < nTemplateSizeX && j == 0) {
						dTargetInfo.m_rcTemplatePixelArea.x = nTemplateSizeX - rcTarget.width;
					}
					if (rcTarget.height < nTemplateSizeY && i == 0) {
						dTargetInfo.m_rcTemplatePixelArea.y = nTemplateSizeY - rcTarget.height;
					}

					listTargetInfo.push_back(dTargetInfo);

					m_pCV->DebugPrint(L"Find Target, (%d, %d), Pos(%d, %d), Size(%d, %d)", i, j, rcTarget.x, rcTarget.y, rcTarget.width, rcTarget.height);
				}

				// Template 이미지 교체 옵션일 경우, 가장 유사도가 높은 Target 를 검색한다.
				if (bReplace) {
					// 정상 크기의 Target 일 경우, Template 과의 유사를 판단한다.
					if (nTargetSizeY == nTemplateSizeY && nTargetSizeX == nTemplateSizeX) {
						Rect rcTarget = Rect(nPosX, nPosY, nTargetSizeX, nTargetSizeY);
						Mat dTargetImg;
						Mat(dImg, rcTarget).copyTo(dTargetImg);
						dTemplate.ResizeTemplateImage(rcTarget.size());

						// 각 Pixel 영역의 평균 밝기를 Template 이미지의 평균 밝기와 같도록 만든다.
						// 평균 밝기가 다르면 유사도 평가시 오차가 심해짐.
						dMean = mean(dTargetImg);
						float nTargetMean = (float)dMean[0];
						dTargetImg = dTargetImg + (nTemplateMean - nTargetMean);

						// 정교한 유사도 평가를 위해, 2가지 방법을 적용하여 결합한다.
						Mat dMatch;
						matchTemplate(dTargetImg, dTemplateImg, dMatch, CV_TM_CCOEFF_NORMED);
						Mat dMatch2;
						matchTemplate(dTargetImg, dTemplateImg, dMatch2, CV_TM_SQDIFF_NORMED);

						float nMatch = dMatch.at<FLOAT>(0) * (1.f - dMatch2.at<FLOAT>(0));
						if (nBestFitMatch == -1.f || nMatch > nBestFitMatch) {
							m_rcBestFitTarget = rcTarget;

							m_pCV->DebugPrint(L"Find Target, BestFitArea Candidate Pos(%d, %d), Size(%d, %d), Match : %.3f > %.3f",
								m_rcBestFitTarget.x, m_rcBestFitTarget.y, m_rcBestFitTarget.width, m_rcBestFitTarget.height, nMatch, nBestFitMatch);

							nBestFitMatch = nMatch;
						}
					}
				}
			}
		}
		SET_TIME_PROFILE_ELAPSED(FindTarget_SetTargetInfoList);

		m_pCV->DebugPrint(L"Find Target, BestFitArea Pos(%d, %d) Size(%d, %d)",
			m_rcBestFitTarget.x, m_rcBestFitTarget.y, m_rcBestFitTarget.width, m_rcBestFitTarget.height);

		if (bReplace && m_rcBestFitTarget.width * m_rcBestFitTarget.height > 0) {
			// Replace 옵션이 있을 경우 Template 이미지를 최적 위치에 있는 이미지로 교체
			dImg(m_rcBestFitTarget).copyTo(dTemplateImg);
			m_pCV->DebugPrint(L"Find Target, Replaced Template Image");
		}

		// 추후 인접한 Target 영역 확인을 위한 2차원 배열(Grid)의 가로/세로 크기를 저장한다.
		m_sizeTargetGrid = Size((int)listTargetPosInfoX.size() * nPixelCountX, (int)listTargetPosInfoY.size() * nPixelCountY);

		m_pCV->DebugPrint(L"Find Target, Target Count X, Y : (%d, %d)", m_sizeTargetGrid.width, m_sizeTargetGrid.height);

		RETURN_SUCCESS();
	}

	Err ProcessCore::_FindTarget_PosInfo(bool bVert, const Mat dImg, const Mat &dTemplateImg, vector<TargetPosInfo> &listTargetPosInfo)
	{
		Mat dLineProfile;
		Mat dTemplateLineProfile;

		int nDim = bVert ? 1 : 0;
		reduce(dImg, dLineProfile, nDim, CV_REDUCE_AVG);
		reduce(dTemplateImg, dTemplateLineProfile, nDim, CV_REDUCE_AVG);

		if (bVert) {
			// 세로 방향 검색일 경우, 가로 방향으로 변환 (동일한 알고리즘 적용을 위해)
			transpose(dLineProfile, dLineProfile);
			transpose(dTemplateLineProfile, dTemplateLineProfile);
			m_pCV->DebugPrint(L"Find Target Pos Info, Find Y Pos");
		}
		else {
			m_pCV->DebugPrint(L"Find Target Pos Info, Find X Pos");
		}

		int nLineLength = dLineProfile.cols;
		int nTemplateLineLength = dTemplateLineProfile.cols;
		int nMatchLineLength = nLineLength - nTemplateLineLength + 1;

		Mat dMatchLine;
		matchTemplate(dLineProfile, dTemplateLineProfile, dMatchLine, CV_TM_SQDIFF_NORMED);


		// Matching 결과가 최소값인 곳을 찾는다.(유사도가 가장 높은 위치)
		Point ptMin;
		double nMin;
		minMaxLoc(dMatchLine, &nMin, 0, &ptMin, 0);

		int nBestFitPos = ptMin.x;
		int nBestFitSize = -1;

		int nTargetPos = nBestFitPos;
		int nTargetSize = nTemplateLineLength;

		//
		// 좌측 방향 검색
		// 
		int nTargetPosBefore = nTargetPos;
		nTargetPos -= nTargetSize;
		while (nTargetPos >= 0) {
			// 지정 위치의 좌/우 주변(Target 크기의 반)에서 Matching 결과가 최소값인 곳을 찾는다.
			int nFindSize = cvRound(nTargetSize * 0.25f);
			int nStartPos = nTargetPos - nFindSize;
			int nEndPos = nTargetPos + nFindSize;
			if (nStartPos < 0) nStartPos = 0;

			Mat dROILine(dMatchLine, Rect(nStartPos, 0, nEndPos - nStartPos + 1, 1));
			minMaxLoc(dROILine, &nMin, 0, &ptMin, 0);
			nTargetPos = nStartPos + ptMin.x;
			nTargetSize = nTargetPosBefore - nTargetPos;
			if (nBestFitSize < 0) nBestFitSize = nTargetSize;

			listTargetPosInfo.push_back(TargetPosInfo(nTargetPos, nTargetSize));

			nTargetPosBefore = nTargetPos;
			nTargetPos -= nTargetSize;
		}
		if (abs(nTargetPos) < nTargetSize) {
			listTargetPosInfo.push_back(TargetPosInfo(nTargetPos, nTargetPos + nTargetSize, nTargetSize));
		}
		if (nBestFitSize < 0) nBestFitSize = nTemplateLineLength;

		//
		// 최적 위치 및 우측 방향 위치 검색
		// 
		nTargetPos = nBestFitPos;
		nTargetSize = nBestFitSize;
		if (nTargetPos + nTargetSize >= nLineLength) {
			listTargetPosInfo.push_back(TargetPosInfo(nTargetPos, nLineLength - nTargetPos, nTargetSize));
		}
		else {
			listTargetPosInfo.push_back(TargetPosInfo(nTargetPos, nTargetSize));

			nTargetPosBefore = nTargetPos;
			nTargetPos += nTargetSize;
			while (nTargetPos <= nLineLength - 1) {
				if (nTargetPos + nTargetSize >= nLineLength) {
					listTargetPosInfo.push_back(TargetPosInfo(nTargetPos, nLineLength - nTargetPos, nTargetSize));
					break;
				}


				// 지정 위치의 좌/우 주변(Target 크기의 반)에서 Matching 결과가 최소값인 곳을 찾는다.
				int nFindSize = cvRound(nTargetSize * 0.25f);
				int nStartPos = nTargetPos - nFindSize;
				int nEndPos = nTargetPos + nFindSize;
				if (nEndPos > nMatchLineLength - 1) nEndPos = nMatchLineLength - 1;
				if (nStartPos <= nMatchLineLength - 1) {
					Mat dROILine(dMatchLine, Rect(nStartPos, 0, nEndPos - nStartPos + 1, 1));
					minMaxLoc(dROILine, &nMin, 0, &ptMin, 0);
					nTargetPos = nStartPos + ptMin.x;
					nTargetSize = nTargetPos - nTargetPosBefore;

					if (nTargetPos + nTargetSize >= nLineLength) {
						listTargetPosInfo.push_back(TargetPosInfo(nTargetPos, nLineLength - nTargetPos, nTargetSize));
						break;
					}
				}

				listTargetPosInfo.push_back(TargetPosInfo(nTargetPos, nTargetSize));

				nTargetPosBefore = nTargetPos;
				nTargetPos += nTargetSize;
			}
		}

		// sort
		sort(listTargetPosInfo.begin(), listTargetPosInfo.end(), [](const TargetPosInfo &left, const TargetPosInfo &right) {
			return left.m_nPos < right.m_nPos;
		});

		// Re-adjust Target Size correctly
		int nCount = (int)listTargetPosInfo.size();
		for (int i = 0; i < nCount; i++) {
			TargetPosInfo &left = listTargetPosInfo[i];

			// 정상 크기의 Target 들에 대해서만 크기 조정을 한다.
			if (i < nCount - 1 && left.m_nSize == left.m_nTemplateSize) {
				TargetPosInfo &right = listTargetPosInfo[i + 1];

				if (left.m_nSize == left.m_nTemplateSize) {
					nTargetSize = right.m_nPos - left.m_nPos;
					left.m_nSize = nTargetSize;
					left.m_nTemplateSize = nTargetSize;
				}
			}

			m_pCV->DebugPrint(L"  Pos (%d), Size (%d), TemplateSize(%d)%s", left.m_nPos, left.m_nSize, left.m_nTemplateSize,
				(left.m_nPos == nBestFitPos) ? L", BestFitPos" : L"");
		}

		RETURN_SUCCESS();
	}

	// 검사 이미지에서 Target 영역을 추출
	Err ProcessCore::_FindTarget()
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const bool &bReplace = dInspection.m_dTemplate.m_bReplace;
		const float &nMinSizeRatio = dInspection.m_dTemplate.m_nMinSizeRatio;

		// Template 과 유사한 Target 영역 추출
		RETURN_ON_FAIL(_FindTarget(bReplace, nMinSizeRatio, m_listTargetInfo));

		if (m_listTargetInfo.size() == 0) {
			RETURN_FAIL(NOT_FOUND_TARGET);
		}

		RETURN_SUCCESS();
	}

	// 불량 영역 검출
	Err ProcessCore::_DetectDefect(bool &bFoundDefect)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;
		VITemplate &dTemplate = dInspection.m_dTemplate;
		const Mat &dImg = m_dParam.m_dInputImage;
		const bool &bVertFlip = dInspection.m_dTemplate.m_bVerFlip;
		const bool &bHorizFlip = dInspection.m_dTemplate.m_bHorizFlip;
		Mat &dOutputImg = m_dResult.m_dOutputImage;

		const float &nDarkColorRatio = dDefaultParam.m_nDarkColorRatio;
		const Rotation &nRotation = dInspection.m_dTemplate.m_nRotation;
		const bool bAdjustImageBackground = dDefaultParam.m_bAdjustImageBackground;

		int nImgSizeX = dImg.cols;
		int nImgSizeY = dImg.rows;

		m_pCV->DebugPrint(L"==== [Detect Defect] ====");

		dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
		m_dDetectResultImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

		//
		// 차분 영상(Diff 이미지) 생성
		//
		// Template 이미지 자체 차분 이미지 생성
		SET_TIME_PROFILE_START(DetectDefect_MakeTemplateDiffImage);
		dTemplate.MakeTemplateDiffImage();
		SET_TIME_PROFILE_ELAPSED(DetectDefect_MakeTemplateDiffImage);
		// Template 이미지와 검사이미지와의 차분 이미지 생성
		SET_TIME_PROFILE_START(DetectDefect_MakeDiffImage);
		RETURN_ON_FAIL(_DetectDefect_MakeDiffImage(bAdjustImageBackground));
		SET_TIME_PROFILE_ELAPSED(DetectDefect_MakeDiffImage);

		// Target Grid 생성
		Mat dTargetGrid = Mat::zeros(m_sizeTargetGrid, CV_8UC1);
		vector<TargetInfo> listDefectTargetInfo;		// 불량 영역이 Target 을 저장할 리스트

		bFoundDefect = false;

		// 모든 Target 에 대해서 이진화를 수행하여 불량 영역을 검출한다.
		SET_TIME_PROFILE_START(DetectDefect_Binarize);
		for (auto &dTargetInfo : m_listTargetInfo) {
			const Rect &rcTarget = dTargetInfo.m_rcArea;
			const Rect &rcTemplatePixel = dTargetInfo.m_rcTemplatePixelArea;

			m_pCV->DebugPrint(L"[Detect Defect] (%d, %d), Target (%d, %d) (%d, %d), TemplatePixel(%d, %d) (%d, %d), TemplatePixelIndex(%d)",
				dTargetInfo.m_nIndexY, dTargetInfo.m_nIndexX, 
				rcTarget.x, rcTarget.y, rcTarget.width, rcTarget.height,
				rcTemplatePixel.x, rcTemplatePixel.y, rcTemplatePixel.width, rcTemplatePixel.height, dTargetInfo.m_nTemplatePixelIndex);

			// Template 이미지를 Target 크기에 맞게 확대/축소한다.
			dTemplate.ResizeTemplateImage(dTargetInfo.m_sizeTemplate, true);

			// Mask 를 적용하여 이진화를 수행하여 불량 영역을 검출
			Mat dTargetResultImg;
			RETURN_ON_FAIL(_DetectDefect_ApplyMask(dTargetInfo, dTargetResultImg));

			int nResultSize = countNonZero(dTargetResultImg);
			if (nResultSize > 0) { 	// 불량이 검출되었을 경우
				m_pCV->DebugPrint(L"  [Check Dark or Seed] Base Defect Size : %d", nResultSize);

				// Dark 영역 검출
				Mat dTargetDarkResultImg;
				RETURN_ON_FAIL(_DetectDefect_DarkOrSeed(dTargetInfo, dTargetResultImg, dTargetDarkResultImg));

				int nDarkResultSize = countNonZero(dTargetDarkResultImg);
				if (nDarkResultSize > 0) {	// Dark 영역이 검출되었을 경우
					dTargetResultImg |= dTargetDarkResultImg;
					nResultSize = countNonZero(dTargetResultImg);

					float nDarkPointRatio = (float)nDarkResultSize / nResultSize;
					if (nDarkPointRatio < nDarkColorRatio) {		// Dark 와 Seed 를 구분
						dTargetInfo.m_bSeed = true;
						m_pCV->DebugPrint(L"    [Seed Detect] (%d / %d) %.2f < %.2f", nDarkResultSize, nResultSize, nDarkPointRatio, nDarkColorRatio);
					}
					else {
						dTargetInfo.m_bDark = true;
						m_pCV->DebugPrint(L"    [Dark Detect] (%d / %d) %.2f >= %.2f", nDarkResultSize, nResultSize, nDarkPointRatio, nDarkColorRatio);
					}
				}
				dTargetInfo.m_nDefectSize = nResultSize;

				// 결과 이미지에 검출 결과를 복사
				Mat dOutputSubImg(dOutputImg, rcTarget);
				dTargetResultImg.copyTo(dOutputSubImg);
				// 불량 영역 이미지에 검출 결과를 복사
				Mat dDetectResultSubImg = Mat(m_dDetectResultImg, rcTarget);
				dTargetResultImg.copyTo(dDetectResultSubImg);

				bFoundDefect = true;

				// 불량 영역 Target 리스트에 추가
				listDefectTargetInfo.push_back(dTargetInfo);

				// Target Grid 에 '불량 있음' 을 표시
				dTargetGrid.at<UINT8>(dTargetInfo.m_nIndexY, dTargetInfo.m_nIndexX) = 255;
			}
		}
		SET_TIME_PROFILE_ELAPSED(DetectDefect_Binarize);

		if (!bFoundDefect) {
			RETURN_SUCCESS();
		}

		//
		// 불량 Target 정보 (DefectTargetInfo) 생성
		// 

		SET_TIME_PROFILE_START(DetectDefect_SetDefectTargetInfo);
		// 불량이 검출된 모든 Target 에서 불퍙 판별에 쓰일 1개 또는 상/하 또는 좌/우로 인접한 2개의 Target 를 찾아내어 불량 Target 정보로 설정한다.
		int nCount = (int)listDefectTargetInfo.size();
		for (int i = 0; i < nCount; i++) {
			TargetInfo &dTargetInfo = listDefectTargetInfo[i];

			DefectTargetInfo dDefectTargetInfo;
			dDefectTargetInfo.m_dTargetInfo = dTargetInfo;

			// 인접한 Target 검색
			for (int j = 0; j < nCount; j++) {
				TargetInfo &dTargetInfo2 = listDefectTargetInfo[j];
				if ((dTargetInfo2.m_nIndexY == dTargetInfo.m_nIndexY && dTargetInfo2.m_nIndexX == dTargetInfo.m_nIndexX - 1)
					|| (dTargetInfo2.m_nIndexX == dTargetInfo.m_nIndexX && dTargetInfo2.m_nIndexY == dTargetInfo.m_nIndexY - 1)) {
					dDefectTargetInfo.m_bConnectedTarget = true;
					continue;
				}
				else if ((dTargetInfo2.m_nIndexY == dTargetInfo.m_nIndexY && dTargetInfo2.m_nIndexX - 1 == dTargetInfo.m_nIndexX)) {
					dDefectTargetInfo.m_bConnectedTarget = true;
					if (nRotation == Rotation::ROTATION_90 || nRotation == Rotation::ROTATION_270) {
						dDefectTargetInfo.m_bConnectedVertically = true;
					}
					dDefectTargetInfo.m_dTargetInfo2 = dTargetInfo2;
				}
				else if (dTargetInfo2.m_nIndexX == dTargetInfo.m_nIndexX && dTargetInfo2.m_nIndexY - 1 == dTargetInfo.m_nIndexY) {
					dDefectTargetInfo.m_bConnectedTarget = true;
					if (nRotation == Rotation::ROTATION_0 || nRotation == Rotation::ROTATION_180) {
						dDefectTargetInfo.m_bConnectedVertically = true;
					}
					dDefectTargetInfo.m_dTargetInfo2 = dTargetInfo2;
				}
				else {
					continue;
				}

				// 불량 Target 정보로 설정. 아래 함수 내부에서 최적 Target 들인지 검사하여 맞을 경우 정보를 업데이트 함.
				RETURN_ON_FAIL(_DetectDefect_SetDefectTarget(dDefectTargetInfo));
			}

			if (!dDefectTargetInfo.m_bConnectedTarget) {	// 인접한 Target 이 없을 경우
				// 불량 Target 정보로 설정. 아래 함수 내부에서 최적 Target 들인지 검사하여 맞을 경우 정보를 업데이트 함.
				RETURN_ON_FAIL(_DetectDefect_SetDefectTarget(dDefectTargetInfo));
			}
		}

		// 회전/반전에 따라 선택된 Target 1번과 2번의 순서를 바꾸어준다.
		if (m_dDefectTargetInfo.m_bConnectedTarget) {
			bool bSwap = false;
			if (m_dDefectTargetInfo.m_bConnectedVertically) {
				if (nRotation == Rotation::ROTATION_180 || nRotation == Rotation::ROTATION_270) bSwap = true;
			}
			else {
				if (nRotation == Rotation::ROTATION_90 || nRotation == Rotation::ROTATION_180) bSwap = true;
			}
			if (bVertFlip) {
				bSwap = !bSwap;
			}
			if (bHorizFlip) {
				bSwap = !bSwap;
			}

			if (bSwap) {
				swap(m_dDefectTargetInfo.m_dTargetInfo, m_dDefectTargetInfo.m_dTargetInfo2);
			}
		}

		// 결과 구조체의 불량 Pixel 영역 정보를 설정한다.
		m_dResult.m_listDefectPixelArea.push_back(m_dDefectTargetInfo.m_dTargetInfo.m_rcArea);
		if (m_dDefectTargetInfo.m_bConnectedTarget) {
			m_dResult.m_listDefectPixelArea.push_back(m_dDefectTargetInfo.m_dTargetInfo2.m_rcArea);
		}

		//
		// 불량 판별용 Target 들과 인접해 있는 모든 Target 를 구한다. 이는 Drawing 에 이용한다.
		//
		// 불량 Target 영역에 대한 Bounding Box 를 그려넣을 이미지
		Mat dTargetBoundary = Mat::zeros(dOutputImg.size(), dOutputImg.type());

		// Target Grid 를 이용하여 불량 판별용 Target 들과 인접한 Target 들을 검색한다.
		// 검색하면서 m_listTargetInfo 를 재구성한다.
		m_listTargetInfo.clear();
		_DetectDefect_AddConnectedDefectTarget(m_dDefectTargetInfo.m_dTargetInfo.m_nIndexX, m_dDefectTargetInfo.m_dTargetInfo.m_nIndexY, listDefectTargetInfo, dTargetGrid, dTargetBoundary);

		// 검색 후에는 listDefectTargetInfo 에 인접하지 않은 Target 들만 남게됨.
		// 해당 Target 의 불량 영역을 결과 이미지에서 제거함.
		for (const auto &dTargetInfo : listDefectTargetInfo) {
			// clear non-target area
			Rect rcTarget = dTargetInfo.m_rcArea;
			Mat dOutputSubImg(dOutputImg, rcTarget);
			dOutputSubImg = 0;

			Mat dDetectResultSubImg(m_dDetectResultImg, rcTarget);
			dDetectResultSubImg = 0;
		}
		// 불량 Target 영역에 대한 Bounding Box 를 결과 이미지에 통합
		Mat(dOutputImg | dTargetBoundary).copyTo(dOutputImg);

		SET_TIME_PROFILE_ELAPSED(DetectDefect_SetDefectTargetInfo);

		RETURN_SUCCESS();
	}

	// 검사 이미지상의 모든 Target 에 대해서 Template 이미지와의 차분을 구해 전체 차분 영상을 구한다.
	Err ProcessCore::_DetectDefect_MakeDiffImage(bool bAdjustImageBackground, bool bGetFitBackground)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;
		VITemplate &dTemplate = dInspection.m_dTemplate;
		Mat &dImg = m_dParam.m_dInputImage;
		Mat &dDiffImg = m_dDiffImg;

		int nImgSizeX = dImg.cols;
		int nImgSizeY = dImg.rows;

		m_pCV->DebugPrint(L"MakeDiffImage");

		dDiffImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_32FC1);

		NIPLInput dInput;
		NIPLOutput dOutput;
		NIPLParam_Diff dParam_Diff;
		NIPL_ERR nErr;

		int nMinIndexX = -1;
		int nMinIndexY = -1;
		int nMaxIndexX = -1;
		int nMaxIndexY = -1;
		Rect rcBackground(0, 0, 0, 0);

		// 모든 Target 에 대해서 수행.
		for (auto &dTargetInfo : m_listTargetInfo) {
			const Rect &rcTarget = dTargetInfo.m_rcArea;
			const Rect &rcTemplatePixel = dTargetInfo.m_rcTemplatePixelArea;

			if (nMinIndexX == -1 || nMinIndexX > dTargetInfo.m_nIndexX) {
				nMinIndexX = dTargetInfo.m_nIndexX;
			}
			if (nMaxIndexX == -1 || nMaxIndexX < dTargetInfo.m_nIndexX) {
				nMaxIndexX = dTargetInfo.m_nIndexX;
			}
			if (nMinIndexY == -1 || nMinIndexY > dTargetInfo.m_nIndexY) {
				nMinIndexY = dTargetInfo.m_nIndexY;
			}
			if (nMaxIndexY == -1 || nMaxIndexY < dTargetInfo.m_nIndexY) {
				nMaxIndexY = dTargetInfo.m_nIndexY;
			}

			if (rcBackground.width == 0) {
				rcBackground = rcTarget;
			}
			else {
				if (rcTarget.x < rcBackground.x) rcBackground.x = rcTarget.x;
				if (rcTarget.y < rcBackground.y) rcBackground.y = rcTarget.y;
				if (rcTarget.br().x > rcBackground.br().x) rcBackground.width = rcTarget.br().x - rcBackground.x;
				if (rcTarget.br().y > rcBackground.br().y) rcBackground.height = rcTarget.br().y - rcBackground.y;
			}

			// Template 이미지를 Target 영역에 맞게 확대/축소함.
			dTemplate.ResizeTemplateImage(dTargetInfo.m_sizeTemplate);

			// Target 영역에 해당하는 이미지를 추출
			Mat dTargetImg(dImg, rcTarget);
			Mat dTargetTemplateImg = dTemplate.GetTemplatePixelImage(rcTemplatePixel);

			// Target 영역에 대한 차분 이미지를 구함
			dInput.Clear();
			dOutput.Clear();
			dParam_Diff.Clear();

			dParam_Diff.m_bSubtract = true;		// 배경 보정을 위해 '차감' 영상을 취득함.
			dParam_Diff.m_dTargetImg = dTargetTemplateImg;

			dInput.m_dImg = dTargetImg;
			dInput.m_pParam = &dParam_Diff;
			nErr = m_pCV->Diff(&dInput, &dOutput);
			if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
				RETURN_FAIL(FAIL_TO_DETECT);
			}

			Mat dTargetDiffImg(dDiffImg, rcTarget);
			dOutput.m_dImg.copyTo(dTargetDiffImg);
		}

//		dDiffImg.copyTo(m_dDebugImg);

		// 검사 이미지 및 차분 이미지에 대해서 배경 평탄화를 수행한다.
		if (bAdjustImageBackground) {
			Mat dBackgroundMask = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
			Mat(dBackgroundMask, rcBackground) = 255;
			Mat(dBackgroundMask, m_rcBestFitTarget) = 0;
			dBackgroundMask = Mat(dBackgroundMask, rcBackground);

			int nTargetCountX = nMaxIndexX - nMinIndexX + 1;
			int nTargetCountY = nMaxIndexY - nMinIndexY + 1;

			//
			// 차분이미지 배경 평탄화
			//
			Mat dDiffBackground(dDiffImg, rcBackground);

			SET_TIME_PROFILE_START(DetectDefect_MakeDiffImage_AdjustImageBackground);
			Mat dFitBackground;
			RETURN_ON_FAIL(_AdjustImageBackground(dDiffBackground, nTargetCountX, nTargetCountY, dBackgroundMask, dFitBackground));
			SET_TIME_PROFILE_ELAPSED(DetectDefect_MakeDiffImage_AdjustImageBackground);

			if (bGetFitBackground) {	// 배경 근사 이미지만 반환할 경우
				m_dAdjustedBackgroundImg = Mat::zeros(nImgSizeY, nImgSizeX, dFitBackground.type());
				dFitBackground.copyTo(Mat(m_dAdjustedBackgroundImg, rcBackground));
			}
			else {
				// 차분 이미지 배경 평탄화
				Scalar dMean = mean(dDiffBackground);
				float nDiffBackgroundMean = (float)dMean[0];
				dDiffBackground = abs(dDiffBackground - dFitBackground + nDiffBackgroundMean);	// 절대값을 취득하여 차분 영상으로 변환

				//
				// 원본 이미지(color) 배경 밝기 평탄화
				//
				m_dAdjustedBackgroundImg = Mat::zeros(nImgSizeY, nImgSizeX, dImg.type());
				Mat dBackground(m_dAdjustedBackgroundImg, rcBackground);
				Mat(dImg, rcBackground).copyTo(dBackground);

				auto itBackground = dBackground.begin<Vec3b>();
				auto itBackground_End = dBackground.end<Vec3b>();
				auto itFitBackground = dFitBackground.begin<FLOAT>();
				while (itBackground != itBackground_End) {
					Vec3b &dPixel = *itBackground;
					FLOAT &nFitBackground = *itFitBackground;
					for (int i = 0; i < 3; i++) {
						dPixel[i] = cvRound(dPixel[i] - nFitBackground);
					}

					itBackground++;
					itFitBackground++;
				}
			}
		}
		else {
			// 배경 보정을 하지 않을 경우,
			// 배경 보정 이미지는 원본 이미지를 그대로 사용하고
			// 차분 영상은 차감 영상에 절대값을 씌워 구한다.
			m_dAdjustedBackgroundImg = dImg;
			dDiffImg = abs(dDiffImg);
		}

		RETURN_SUCCESS();
	}

	// Mask 를 적용하여 이진화를 수행하여 불량 영역을 추출
	Err ProcessCore::_DetectDefect_ApplyMask(const TargetInfo &dTargetInfo, Mat &dResultImg)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;
		const VIBinarize &dBinarize = dInspection.m_dBinarize;
		VITemplate &dTemplate = dInspection.m_dTemplate;
		const auto &listApply = dBinarize.m_listApply;
		const bool &bApplyWholePixel = dBinarize.m_bApplyWholePixel;

		const Rect &rcTemplate = dTemplate.GetTemplatePixelArea(dTargetInfo.m_nTemplatePixelIndex);		// Template 상의 Template Pixel 영역 위치
		const Rect &rcTarget = dTargetInfo.m_rcArea;						// 검사 이미지상의 Target 영역 위치
		const Rect &rcTemplatePixel = dTargetInfo.m_rcTemplatePixelArea;	// Template Pixel 상의 Target 영역에 해당하는 영역의 위치

		Mat dTargetDiffImg(m_dDiffImg, rcTarget);							// Target 영역에 대한 차분 이미지
		Mat dTemplateDiffImg = dTemplate.GetTemplatePixelDiffImage(rcTemplate);				// Template Pixel 전체 크기에 대한 차분 이미지
		Mat dTargetTemplateDiffImg = dTemplate.GetTemplatePixelDiffImage(rcTemplatePixel);	// Target 영역에 대한 Template 자체 차분 이미지

		const int nTargetImgSizeX = dTargetTemplateDiffImg.cols;
		const int nTargetImgSizeY = dTargetTemplateDiffImg.rows;

		NIPLInput dInput;
		NIPLOutput dOutput;
		NIPL_ERR nErr;

		Scalar dMean;
		Scalar dStd;
		float nStd;

		if (bApplyWholePixel) { // 전체 Pixel 영역에 대해서 이진화를 수행하는 옵션이 설정되어 있을 경우
			const float &nBinarizeThreshold = dDefaultParam.m_nBinarizeThreshold;
			const int &nMinDefectSize = dDefaultParam.m_nMinDefectSize;
			const int &nHVMinLength = dDefaultParam.m_nHVMinLength;

			m_pCV->DebugPrint(L"  [ApplyMask] Whole Pixel, ColorMaskId : %s, BinarizeThreshold : %.1f, MinDefectSize : %d, HvMinLength : %d",
				dBinarize.m_strColorMaskId.c_str(), nBinarizeThreshold, nMinDefectSize, nHVMinLength);

			// Template 자체 차분 영상을 통해 표준편차(Std)를 구함
			if (!dBinarize.m_strColorMaskId.empty()) {	// Color Mask(색상 분포 정보 확인용)가 지정되어 있을 경우
				Mat dColorMaskImg;
				RETURN_ON_FAIL(_GetTransformMaskImage(dBinarize.m_strColorMaskId, dColorMaskImg));

				meanStdDev(dTemplateDiffImg, dMean, dStd, dColorMaskImg);
			}
			else {
				meanStdDev(dTemplateDiffImg, dMean, dStd);
			}

			nStd = (float)dStd[0];

			// 이진화 수행
			NIPLParam_Thresholding dParam_Thresholding;
			dParam_Thresholding.m_nMethod = NIPLParam_Thresholding::METHOD_UPPER;
			dParam_Thresholding.m_nThreshold = pow(nBinarizeThreshold, 2.f) * nStd;		// 임계치 설정

			dInput.m_dImg = dTargetDiffImg;
			dInput.m_pParam = &dParam_Thresholding;

			nErr = m_pCV->Thresholding(&dInput, &dOutput);
			if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
				RETURN_FAIL(NOT_FOUND_DEFECT);
			}
			Mat dBinImg = dOutput.m_dImg;

			//
			// 노이즈 제거
			//

			// 노이즈 제거 : 가는 선 제거 
			dInput.Clear();
			dOutput.Clear();

			NIPLParam_MorphologyOperate dParam_MorphologyOperate;
			dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_OPEN;
			dParam_MorphologyOperate.m_bCircleFilter = true;
			dParam_MorphologyOperate.m_nFilterSizeX = nHVMinLength;
			dParam_MorphologyOperate.m_nFilterSizeY = nHVMinLength;

			dInput.m_dImg = dBinImg;
			dInput.m_pParam = &dParam_MorphologyOperate;
			nErr = m_pCV->MorphologyOperate(&dInput, &dOutput);
			if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
				RETURN_FAIL(FAIL_TO_CLASSIFY);
			}
			dBinImg = dOutput.m_dImg;

			dInput.Clear();
			dOutput.Clear();

			// 노이즈 제거 : 크기가 작은 영역 제거 
			NIPLParam_FindBlob dParam_FindBlob;
			dParam_FindBlob.m_bFindInRange = true;
			dParam_FindBlob.m_nMinSize = nMinDefectSize;		// 최소 크기 지정

			dInput.m_dImg = dBinImg;
			dInput.m_pParam = &dParam_FindBlob;
			nErr = m_pCV->FindBlob(&dInput, &dOutput);
			if (NIPL_SUCCESS(nErr)) {
				dResultImg = dOutput.m_dImg;

				shared_ptr<NIPLResult_FindBlob> pResult_FindBlob = dOutput.GetResult<NIPLResult_FindBlob>();
				if (pResult_FindBlob != nullptr) {
					for (auto &dBlob : pResult_FindBlob->m_listBlob) {
						Rect &rcBox = dBlob.m_rcBoundingBox;
						m_pCV->DebugPrint(L"  >> Found Defect, Whole Pixel, Pos (%d, %d) Size(%d, %d)", rcTarget.x + rcBox.x, rcTarget.y + rcBox.y, rcBox.width, rcBox.height);
					}

				}
			}
		}
		else {
			dResultImg = Mat::zeros(nTargetImgSizeY, nTargetImgSizeX, CV_8UC1);
		}

		// Mask 로 지정된 영역에 대해 개별 이진화 수행
		for (const auto &dApply : listApply) {
			const float &nBinarizeThreshold = (dApply.m_nBinarizeThreshold != 0.f) ? dApply.m_nBinarizeThreshold : dDefaultParam.m_nBinarizeThreshold;
			const int &nMinDefectSize = (dApply.m_nMinDefectSize != 0) ? dApply.m_nMinDefectSize : dDefaultParam.m_nMinDefectSize;
			const int &nHVMinLength = (dApply.m_nHVMinLength != 0) ? dApply.m_nHVMinLength : dDefaultParam.m_nHVMinLength;
			const bool &bMergeResult = dApply.m_bMergeResult;
			const int &nMergeFillSize = dApply.m_nMergeFillSize;

			m_pCV->DebugPrint(L"  [ApplyMask] MaskId : %s, ColorMaskId : %s, BinarizeThreshold : %.1f, MinDefectSize : %d, HvMinLength : %d", 
				dApply.m_strMaskId.c_str(), dApply.m_strColorMaskId.c_str(), nBinarizeThreshold, nMinDefectSize, nHVMinLength);

			// Mask 복사
			Mat dMaskImg;
			RETURN_ON_FAIL(_GetTransformMaskImage(dApply.m_strMaskId, dMaskImg));

			// Color Mask(색상 분포 정보 확인용)가 지정되어 있을 경우 복사
			Mat dColorMaskImg;
			if (dApply.m_strColorMaskId.empty()) {
				dMaskImg.copyTo(dColorMaskImg);
			}
			else {
				RETURN_ON_FAIL(_GetTransformMaskImage(dApply.m_strColorMaskId, dColorMaskImg));
			}

			// Template 자체 차분 영상을 통해 표준편차(Std)를 구함
			meanStdDev(dTemplateDiffImg, dMean, dStd, dColorMaskImg);
			nStd = (float)dStd[0];

			Mat dTargetMaskImg(dMaskImg, rcTemplatePixel);

			// 이진화
			dInput.Clear();
			dOutput.Clear();

			NIPLParam_Thresholding dParam_Thresholding;
			dParam_Thresholding.m_nMethod = NIPLParam_Thresholding::METHOD_UPPER;
			dParam_Thresholding.m_nThreshold = pow(nBinarizeThreshold, 2.f) * nStd;		// 임계치 설정

			dInput.m_dImg = dTargetDiffImg;				
			dInput.m_dMask = dTargetMaskImg;			// 이진화를 수행할 Mask 영역 지정
			dInput.m_pParam = &dParam_Thresholding;

			nErr = m_pCV->Thresholding(&dInput, &dOutput);
			if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
				RETURN_FAIL(FAIL_TO_DETECT);
			}
			Mat dBinImg = dOutput.m_dImg;

			//
			// 노이즈 제거
			// 
			if (!bMergeResult) {		// 결과 이미지 통합에 대한 옵션이 설정되어 있지 않을 경우
				dInput.Clear();
				dOutput.Clear();

				// 노이즈 제거 : 가는 선 제거
				NIPLParam_MorphologyOperate dParam_MorphologyOperate;
				dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_OPEN;
				dParam_MorphologyOperate.m_bCircleFilter = true;
				dParam_MorphologyOperate.m_nFilterSizeX = nHVMinLength;
				dParam_MorphologyOperate.m_nFilterSizeY = nHVMinLength;

				dInput.m_dImg = dBinImg;
				dInput.m_pParam = &dParam_MorphologyOperate;
				nErr = m_pCV->MorphologyOperate(&dInput, &dOutput);
				if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
					RETURN_FAIL(FAIL_TO_CLASSIFY);
				}
				dBinImg = dOutput.m_dImg;
								
				dInput.Clear();
				dOutput.Clear();

				// 노이즈 제거 : 크기가 작은 영역 제거
				NIPLParam_FindBlob dParam_FindBlob;
				dParam_FindBlob.m_bFindInRange = true;
				dParam_FindBlob.m_nMinSize = nMinDefectSize;		// 최소 크기 지정

				dInput.m_dImg = dBinImg;
				dInput.m_pParam = &dParam_FindBlob;
				nErr = m_pCV->FindBlob(&dInput, &dOutput);
				if (NIPL_SUCCESS(nErr)) {
					shared_ptr<NIPLResult_FindBlob> pResult_FindBlob = dOutput.GetResult<NIPLResult_FindBlob>();
					if (pResult_FindBlob != nullptr) {
						dResultImg |= dOutput.m_dImg;

						for (auto &dBlob : pResult_FindBlob->m_listBlob) {
							Rect &rcBox = dBlob.m_rcBoundingBox;
							m_pCV->DebugPrint(L"  >> Found Defect, Pos (%d, %d) Size(%d, %d)", rcTarget.x + rcBox.x, rcTarget.y + rcBox.y, rcBox.width, rcBox.height);
						}
					}
				}
			}
			else {	// 결과 이미지 통합에 대한 옵션이 설정되어 있는 경우
				const int &nExtSize = max(nHVMinLength, nMergeFillSize) + 1;		// 검출된 개별 불량 영역을 확장시킬 크기 설정

				// 결과 이미지 통합의 경우 크기가 작은 영역을 제거하고 나머지 불량 영역에 대해서 개별 검사 수행.
				// 노이즈 제거 : 크기가 작은 영역 제거
				dInput.Clear();
				dOutput.Clear();

				NIPLParam_FindBlob dParam_FindBlob;
				dParam_FindBlob.m_bFindInRange = true;
				dParam_FindBlob.m_nMinSize = nMinDefectSize;		

				dInput.m_dImg = dBinImg;
				dInput.m_pParam = &dParam_FindBlob;
				nErr = m_pCV->FindBlob(&dInput, &dOutput);
				if (NIPL_SUCCESS(nErr)) {
					shared_ptr<NIPLResult_FindBlob> pResult_FindBlob = static_pointer_cast<NIPLResult_FindBlob>(dOutput.m_pResult);
					if (pResult_FindBlob != nullptr) {
						// 나머지(크기가 큰) 영역에 대해서 개별적으로 결과 통합 이미지를 생성하여 '가는 선 제거'를 수행
						for (auto &dBlob : pResult_FindBlob->m_listBlob) {
							Mat &dBlobImg = dBlob.m_dImg;
							Rect &rcBox = dBlob.m_rcBoundingBox;

							// 불량 영역보다 조금 큰 확장된 영역을 추출 (결과 이미지 전체를 대상으로 통합하는 것보다 처리 속도 향상)
							Rect rcExtBox = EXTEND_BOX_SIZE(rcBox, nExtSize, nTargetImgSizeX, nTargetImgSizeY);

							// 결과 이미지 영역 추출
							Mat dResultROIImg(dResultImg, rcExtBox);
							Mat dMergedResultImg;
							dResultROIImg.copyTo(dMergedResultImg);

							// 결과 이미지에 불량 영역을 통합시킴
							Mat dMergedResultBlobImg(dMergedResultImg, Rect(rcBox.tl() - rcExtBox.tl(), rcBox.size()));
							Mat(dMergedResultBlobImg | dBlobImg).copyTo(dMergedResultBlobImg);

							// 결과 통합 이미지상의 불량 영역 내부 채움.
							if (nMergeFillSize > 0) {
								// 영역과 영역사이의 틈을 채움
								int nFilterSize = nMergeFillSize * 2 + 1;

								NIPLParam_MorphologyOperate dParam_MorphologyOperate;
								dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_CLOSE;
								dParam_MorphologyOperate.m_nFilterSizeX = nFilterSize;
								dParam_MorphologyOperate.m_nFilterSizeY = nFilterSize;

								dInput.m_dImg = dMergedResultImg;
								dInput.m_pParam = &dParam_MorphologyOperate;
								nErr = m_pCV->MorphologyOperate(&dInput, &dOutput);
								if (NIPL_SUCCESS(nErr)) {
									dMergedResultImg = dOutput.m_dImg;
								}

								// 영역 내부의 구멍을 채움
								dInput.Clear();
								dOutput.Clear();

								NIPLParam_FillHole dParam_FillHole;
								dInput.m_dImg = dMergedResultImg;
								dInput.m_pParam = &dParam_FillHole;
								nErr = m_pCV->FillHole(&dInput, &dOutput);
								if (NIPL_SUCCESS(nErr)) {
									dMergedResultImg = dOutput.m_dImg;
								}
							}

							// 노이즈 제거 : 가는 선 제거 (결과 통합 이미지를 대상으로 함)
							dInput.Clear();
							dOutput.Clear();

							NIPLParam_MorphologyOperate dParam_MorphologyOperate;
							dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_OPEN;
							dParam_MorphologyOperate.m_bCircleFilter = true;
							dParam_MorphologyOperate.m_nFilterSizeX = nHVMinLength;
							dParam_MorphologyOperate.m_nFilterSizeY = nHVMinLength;

							dInput.m_dImg = dMergedResultImg;
							dInput.m_pParam = &dParam_MorphologyOperate;
							nErr = m_pCV->MorphologyOperate(&dInput, &dOutput);
							if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
								RETURN_FAIL(FAIL_TO_CLASSIFY);
							}
							dMergedResultImg = dOutput.m_dImg;

							int nDefectSize = countNonZero(dMergedResultImg);
							if (nDefectSize > 0) {
								m_pCV->DebugPrint(L"  >> Found Defect, Merged, Pos (%d, %d) Size(%d, %d)", rcTarget.x + rcBox.x, rcTarget.y + rcBox.y, rcBox.width, rcBox.height);

								// 원래 추출된 불량 영역만 결과 이미지에 통합해 넣음.
								Mat dResultBlobImg = Mat(dResultImg, rcBox);
								Mat(dBlobImg | dResultBlobImg).copyTo(dResultBlobImg);
							}
						}
					}
				}
			}
		}

		RETURN_SUCCESS();
	}

	// Dark 영역 검출
	Err ProcessCore::_DetectDefect_DarkOrSeed(const TargetInfo &dTargetInfo, const Mat &dTargetResultImg, Mat &dResultImg)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;
		VITemplate &dTemplate = dInspection.m_dTemplate;
		const Mat &dImg = m_dParam.m_dInputImage;

		const int &nColorTH_R = GET_COLOR_RED(dDefaultParam.m_dDarkColor);
		const int &nColorTH_G = GET_COLOR_GREEN(dDefaultParam.m_dDarkColor);
		const int &nColorTH_B = GET_COLOR_BLUE(dDefaultParam.m_dDarkColor);
		const float &nDarkColorExtendSizeRatio = dDefaultParam.m_nDarkColorExtendSizeRatio;
		const int &nMinSeedSize = dDefaultParam.m_nMinSeedSize;
		const int &nHVMinLength = dDefaultParam.m_nHVMinLength;

		const Rect &rcTarget = dTargetInfo.m_rcArea;
		Mat dTargetImg(m_dAdjustedBackgroundImg, rcTarget);			// 색상 비교를 할 때는 배경 보정이 된 이미지를 사용한다.

		NIPLInput dInput;
		NIPLOutput dOutput;
		NIPL_ERR nErr;

		// Dark 영역을 추출할 대상 영역을 Masking 함.
		Mat dTargetResultMaskImg = dTargetResultImg;

		if (nDarkColorExtendSizeRatio > 0.f && nDarkColorExtendSizeRatio < 1.f) {
			// Dark 영역 확장 옵션이 설정되어 있을 경우, Template 크기를 기준으로 해당 비율만큼 확장
			int nTemplateImgSizeX = dTargetInfo.m_sizeTemplate.width;
			int nTemplateImgSizeY = dTargetInfo.m_sizeTemplate.height;

			dInput.Clear();
			dOutput.Clear();

			NIPLParam_MorphologyOperate dParam_MorphologyOperate;
			dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_DILATE;
			dParam_MorphologyOperate.m_nFilterSizeX = nTemplateImgSizeX * nDarkColorExtendSizeRatio;
			dParam_MorphologyOperate.m_nFilterSizeY = nTemplateImgSizeY * nDarkColorExtendSizeRatio;

			dInput.m_dImg = dTargetResultMaskImg;
			dInput.m_pParam = &dParam_MorphologyOperate;
			nErr = m_pCV->MorphologyOperate(&dInput, &dOutput);
			if (NIPL_SUCCESS(nErr)) {
				dTargetResultMaskImg = dOutput.m_dImg;
			}
		}

		// 이진화 (Color 이미지를 대상으로 어두운 영역 검출)
		dInput.Clear();
		dOutput.Clear();

		NIPLParam_ColorThresholding dParam_ColorThresholding;

		dParam_ColorThresholding.m_nMethod_R = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_R = (float)nColorTH_R;
		dParam_ColorThresholding.m_nMethod_G = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_G = (float)nColorTH_G;
		dParam_ColorThresholding.m_nMethod_B = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_B = (float)nColorTH_B;

		dInput.m_dImg = dTargetImg;
		// 영역 확장 옵션이 1 이 아닐 경우, 검출 영역 Mask 지정. 1 일 경우, 전체 영역을 대상으로 검출
		if (nDarkColorExtendSizeRatio != 1.f) dInput.m_dMask = dTargetResultMaskImg;	
		dInput.m_pParam = &dParam_ColorThresholding;
		nErr = m_pCV->ColorThresholding(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			RETURN_FAIL(FAIL_TO_DETECT);
		}
		dResultImg = dOutput.m_dImg;
		
		//
		// 노이즈 제거
		//

		// 노이즈 제거 : 가는 선 제거
		dInput.Clear();
		dOutput.Clear();

		NIPLParam_MorphologyOperate dParam_MorphologyOperate;
		dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_OPEN;
		dParam_MorphologyOperate.m_bCircleFilter = true;
		dParam_MorphologyOperate.m_nFilterSizeX = nHVMinLength;
		dParam_MorphologyOperate.m_nFilterSizeY = nHVMinLength;

		dInput.m_dImg = dResultImg;
		dInput.m_pParam = &dParam_MorphologyOperate;
		nErr = m_pCV->MorphologyOperate(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			RETURN_FAIL(FAIL_TO_CLASSIFY);
		}
		dResultImg = dOutput.m_dImg;

		// 노이즈 제거 : 크기가 작은 영역 제거
		dInput.Clear();
		dOutput.Clear();

		NIPLParam_FindBlob dParam_FindBlob;
		dParam_FindBlob.m_bFindInRange = true;
		dParam_FindBlob.m_nMinSize = nMinSeedSize;

		dInput.m_dImg = dResultImg;
		dInput.m_pParam = &dParam_FindBlob;
		nErr = m_pCV->FindBlob(&dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			dResultImg = dOutput.m_dImg;

			shared_ptr<NIPLResult_FindBlob> pResult_FindBlob = dOutput.GetResult<NIPLResult_FindBlob>();
			if (pResult_FindBlob != nullptr) {
				for (auto &dBlob : pResult_FindBlob->m_listBlob) {
					Rect &rcBox = dBlob.m_rcBoundingBox;
					m_pCV->DebugPrint(L"  >> Found Defect, Dark, Pos (%d, %d) Size(%d, %d)", rcTarget.x + rcBox.x, rcTarget.y + rcBox.y, rcBox.width, rcBox.height);
				}
			}
		}

		RETURN_SUCCESS();
	}

	// 최적의 불량 Target 일 경우, 불량 Target 정보를 변경함
	Err ProcessCore::_DetectDefect_SetDefectTarget(const DefectTargetInfo &dDefectTargetInfo)
	{
		const Mat &dImg = m_dParam.m_dInputImage;
		int nImgSizeX = dImg.cols;
		int nImgSizeY = dImg.rows;

		Point ptDefectTargetCenterPos = m_dDefectTargetInfo.GetCenterPos();
		Point ptTargetCenterPos = dDefectTargetInfo.GetCenterPos();
		int nDefectSizeDefectTarget = m_dDefectTargetInfo.GetDefectSize();
		int nDefectSizeTarget = dDefectTargetInfo.GetDefectSize();

		// 최적 불량 Target 의 조건 : 이미지 중심에 가깝고, 불량 영역의 크기가 큰 경우
		float nDistDefectTargetCenterPos = CALC_DISTANCE(ptDefectTargetCenterPos, CPoint(nImgSizeX * 0.5f, nImgSizeY * 0.5f)) / sqrt(nDefectSizeDefectTarget);
		float nDistTargetCenterPos = CALC_DISTANCE(ptTargetCenterPos, CPoint(nImgSizeX * 0.5f, nImgSizeY * 0.5f)) / sqrt(nDefectSizeTarget);

		if (nDistTargetCenterPos < nDistDefectTargetCenterPos) {
			m_dDefectTargetInfo = dDefectTargetInfo;
		}

		RETURN_SUCCESS();
	}

	// 특정 Target 의 인접한 Target 에 불량이 있는지 검사한다.
	Err ProcessCore::_DetectDefect_AddConnectedDefectTarget(int nIndexX, int nIndexY, vector<TargetInfo> &listDefectTargetInfo, Mat &dTargetGrid, Mat &dTargetBoundary)
	{
		if (dTargetGrid.at<UINT8>(nIndexY, nIndexX) == 0) {
			RETURN_SUCCESS();
		}

		// 특정 Target Index 에 대응되는 Target 이 불량 Target 리스트에 있는지 확인함.
		auto it = find_if(listDefectTargetInfo.begin(), listDefectTargetInfo.end(), [nIndexX, nIndexY](const TargetInfo &dTargetInfo) {
			if (nIndexX == dTargetInfo.m_nIndexX && nIndexY == dTargetInfo.m_nIndexY) {
				return true;
			}

			return false;
		});

		if (it == listDefectTargetInfo.end()) {
			RETURN_SUCCESS();
		}

		TargetInfo &dTargetInfo = *it;

		// 불량 판별용 Target 1번, 2번과 그외 불량 Target 에 대한 Bounding Box 의 색상을 다르게 설정한다. (디버깅용 처리)
		int nColor = 24;
		if (nIndexY == m_dDefectTargetInfo.m_dTargetInfo.m_nIndexY && nIndexX == m_dDefectTargetInfo.m_dTargetInfo.m_nIndexX) {
			nColor = 96;
		}
		else {
			if (m_dDefectTargetInfo.m_bConnectedTarget) {
				if (nIndexY == m_dDefectTargetInfo.m_dTargetInfo2.m_nIndexY && nIndexX == m_dDefectTargetInfo.m_dTargetInfo2.m_nIndexX) {
					nColor = 48;
				}
			}
		}
		rectangle(dTargetBoundary, dTargetInfo.m_rcArea, CV_RGB(nColor, nColor, nColor), 1);

		m_pCV->DebugPrint(L"[Add Connected Target] (%d, %d), Seed : %s, Dark : %s", nIndexX, nIndexY,
			BOOL_TO_WSTRING(dTargetInfo.m_bSeed), BOOL_TO_WSTRING(dTargetInfo.m_bDark));

		// Dark or Seed 설정 : 인접 Target 들중에 하나라도 Dark/Seed 가 설정되어 있으면 전체에도 있은 것으로 설정
		m_dDefectTargetInfo.m_bSeed |= dTargetInfo.m_bSeed;
		m_dDefectTargetInfo.m_bDark |= dTargetInfo.m_bDark;
		if (m_dDefectTargetInfo.m_bDark) {	// Dark 와 Seed 는 공존할 수 없음. Dark 설정시 Seed 는 해제함.
			m_dDefectTargetInfo.m_bSeed = false;
		}
		m_pCV->DebugPrint(L"  > DefectTarget, Seed : %s, Dark : %s",
			BOOL_TO_WSTRING(m_dDefectTargetInfo.m_bSeed), BOOL_TO_WSTRING(m_dDefectTargetInfo.m_bDark));

		// 찾은 Target 을 Drawing 용 Target 리스트에 추가하고, 불량 Target 리스트 및 Target Grid 에 제거
		m_listTargetInfo.push_back(dTargetInfo);
		listDefectTargetInfo.erase(it);
		dTargetGrid.at<UINT8>(nIndexY, nIndexX) = 0;

		// 상/하, 좌/우 인접한 Target 에 대해서 다시 검색 (Recursively)
		int nTargetGridSizeX = dTargetGrid.cols;
		int nTargetGridSizeY = dTargetGrid.rows;
		if (nIndexY > 0 && dTargetGrid.at<UINT8>(nIndexY - 1, nIndexX) != 0) {
			_DetectDefect_AddConnectedDefectTarget(nIndexX, nIndexY - 1, listDefectTargetInfo, dTargetGrid, dTargetBoundary);
		}
		if (nIndexX > 0 && dTargetGrid.at<UINT8>(nIndexY, nIndexX - 1) != 0) {
			_DetectDefect_AddConnectedDefectTarget(nIndexX - 1, nIndexY, listDefectTargetInfo, dTargetGrid, dTargetBoundary);
		}
		if (nIndexY < nTargetGridSizeY - 1 && dTargetGrid.at<UINT8>(nIndexY + 1, nIndexX) != 0) {
			_DetectDefect_AddConnectedDefectTarget(nIndexX, nIndexY + 1, listDefectTargetInfo, dTargetGrid, dTargetBoundary);
		}
		if (nIndexX < nTargetGridSizeX - 1 && dTargetGrid.at<UINT8>(nIndexY, nIndexX + 1) != 0) {
			_DetectDefect_AddConnectedDefectTarget(nIndexX + 1, nIndexY, listDefectTargetInfo, dTargetGrid, dTargetBoundary);
		}

		RETURN_SUCCESS();
	}

	// 불량 규칙을 적용하여 불량을 판별함
	Err ProcessCore::_CheckDefect(bool &bFoundDefect)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const auto &listDefect = dInspection.m_listDefect;
		const bool &bSeed = m_dDefectTargetInfo.m_bSeed;
		const bool &bDark = m_dDefectTargetInfo.m_bDark;
		const bool &bConnectedTarget = m_dDefectTargetInfo.m_bConnectedTarget;

		const bool bDrawDefectWithMask = (m_dParam.m_nOutputImageType == OutputImageType::DEFECT_WITH_MASK);
		Mat &dOutputImg = m_dResult.m_dOutputImage;

		bFoundDefect = false;

		m_pCV->DebugPrint(L"==== [Check Defect] Target Count : %d ====", bConnectedTarget? 2 : 1);

		// 1번 Target 에 대해 불량 규칙에 부합하는지 검사
		const TargetInfo &dTargetInfo = m_dDefectTargetInfo.m_dTargetInfo;
		vector<VIDefect> listDefect_Target;
		m_pCV->DebugPrint(L"[Check Defect] ======== Target 1 ========");
		SET_TIME_PROFILE_START(CheckDefect_Target1);
		RETURN_ON_FAIL(_CheckDefect_Target(dTargetInfo, listDefect_Target));
		SET_TIME_PROFILE_ELAPSED(CheckDefect_Target1);

		// 2번 Target 이 있을 경우, 불량 규칙에 부합하는지 검사
		const TargetInfo &dTargetInfo2 = m_dDefectTargetInfo.m_dTargetInfo2;
		vector<VIDefect> listDefect_Target2;
		if (bConnectedTarget) {
			m_pCV->DebugPrint(L"[Check Defect] ======== Target 2 ========");
			SET_TIME_PROFILE_START(CheckDefect_Target2);
			RETURN_ON_FAIL(_CheckDefect_Target(dTargetInfo2, listDefect_Target2));
			SET_TIME_PROFILE_ELAPSED(CheckDefect_Target2);
		}

		if (!bConnectedTarget && listDefect_Target.empty() && listDefect_Target2.empty()) {
			m_pCV->DebugPrint(L"  >>>> Check Defect in Single Target : Not Found");
		}

		// 2 Pixel 불량 규칙에 부합하지는 검사
		wstring strDefectId;
		m_pCV->DebugPrint(L"[Check Defect] ======== MultiTarget ========");
		SET_TIME_PROFILE_START(CheckDefect_MultiTarget);
		RETURN_ON_FAIL(_CheckDefect_MultiTarget(listDefect_Target, listDefect_Target2, strDefectId));
		SET_TIME_PROFILE_ELAPSED(CheckDefect_MultiTarget);

		// 2 Pixel 불량이 아니지만, 1번 Target 검사가 불량으로 나왔을 경우
		if (strDefectId.empty() && !listDefect_Target.empty()) {
			const VIDefect &dDefect = listDefect_Target[0];		// 부합되는 불량 규칙중 첫번째 것을 채택함.
			const VIPixel &dPixel = dDefect.m_listPixel.front();
			strDefectId = dDefect.m_strId;

			// 불량 규칙에 설정된 Drawing 방법을 추가
			RETURN_ON_FAIL(_CheckDefect_AddDefectTargetDraw(dDefect));
			if (bDrawDefectWithMask) {
				RETURN_ON_FAIL(_CheckDefect_DrawDefectWithMask(dTargetInfo, dPixel));
			}
		}

		// 모든 Pixel 불량 규칙에 해당되지 않았으면 '불량 없음' 반환
		if (strDefectId.empty()) {
			m_pCV->DebugPrint(L"Check Defect Result : Not Found");
			bFoundDefect = false;
			RETURN_SUCCESS();
		}
		m_pCV->DebugPrint(L"Check Defect, Target Result : %s", strDefectId.c_str());

		// Dark/Seed 불량 검사를 수행하여, 최종적으로 불량 종류를 판별함.
		SET_TIME_PROFILE_START(CheckDefect_DarkOrSeed);
		RETURN_ON_FAIL(_CheckDefect_DarkOrSeed(strDefectId));
		SET_TIME_PROFILE_ELAPSED(CheckDefect_DarkOrSeed);
		m_pCV->DebugPrint(L"Check Defect, Final Result : %s", strDefectId.c_str());

		m_dResult.m_bFoundDefect = true;
		m_dResult.m_strDefectId = strDefectId;

		if (bDrawDefectWithMask) {
			m_dDefectWidthMaskImg.copyTo(dOutputImg);
		}

		bFoundDefect = true;
		RETURN_SUCCESS();
	}

	// 하나의 Target 이 1 Pixel 용 불량 규칙에 부합하는지 검사
	Err ProcessCore::_CheckDefect_Target(const TargetInfo &dTargetInfo, vector<VIDefect> &listDefect_Target)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		auto &listDefect = dInspection.m_listDefect;

		// 모든 1 Pixel 불량 규칙을 검사
		for (auto &dDefect : listDefect) {
			if (dDefect.m_nPixelCount > 1) {			// 1 Pixel 검사 조건이 아닐 경우 경우 skip
				continue;
			}
			if (dDefect.m_bSeed || dDefect.m_bDark) {	// Dark 또는 Seed 검사 조건일 경우 skip
				continue;
			}

			bool bFound = false;
			const VIPixel &dPixel = dDefect.m_listPixel.front();
			const bool bCheckConnectedDefect = dDefect.m_bConnectedDefect;
			m_pCV->DebugPrint(L"  Check Defect [%s], ConnectedDefect : %s", dDefect.m_strId.c_str(), BOOL_TO_WSTRING(bCheckConnectedDefect));

			Mat dConnectedDefectImg;
			RETURN_ON_FAIL(_CheckDefect_Pixel(dTargetInfo, dPixel, VIDefect::DONT_CARE, true, bCheckConnectedDefect, dConnectedDefectImg, bFound));

			if (bFound) {
				// 불량 규칙에 부합할 경우, 해당 불량 규칙을 리스트에 추가 
				dDefect.m_dConnectedDefectImg = dConnectedDefectImg;
				listDefect_Target.push_back(dDefect);

				m_pCV->DebugPrint(L"  >>>>>>>> Check Defect [%s] <<<<<<<<", dDefect.m_strId.c_str());
			}
		}

		RETURN_SUCCESS();
	}

	// 하나(이웃 Pixel 검사일경우에만) 또는 두개의 Target 이 2 Pixel 용 불량 규칙에 부합하는지 검사
	Err ProcessCore::_CheckDefect_MultiTarget(const vector<VIDefect> &listDefect_Target, const vector<VIDefect> &listDefect_Target2, wstring &strDefectId)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const auto &listDefect = dInspection.m_listDefect;
		const bool &bConnectedTarget = m_dDefectTargetInfo.m_bConnectedTarget;
		const bool &bConnectedVertically = m_dDefectTargetInfo.m_bConnectedVertically;
		const bool bDrawDefectWithMask = (m_dParam.m_nOutputImageType == OutputImageType::DEFECT_WITH_MASK);

		const TargetInfo &dTargetInfo = m_dDefectTargetInfo.m_dTargetInfo;
		const TargetInfo &dTargetInfo2 = m_dDefectTargetInfo.m_dTargetInfo2;

		bool bFoundDefect = false;

		// 모든 2 Pixel 불량 규칙을 검사
		for (auto &dDefect : listDefect) {
			if (dDefect.m_nPixelCount != 2) {			// 2 Pixel 검사 조건이 아닐 경우 경우 skip
				continue;
			}
			if (dDefect.m_bSeed || dDefect.m_bDark) {	// Dark 또는 Seed 검사 조건일 경우 skip
				continue;
			}

			const VIPixel &dPixel = dDefect.m_listPixel.front();
			const VIPixel &dPixel2 = *(++dDefect.m_listPixel.begin());
			bool bConnectedNeighbor = dPixel.m_bConnectedNeighbor || dPixel2.m_bConnectedNeighbor;
			bool bCheckConnectedDefect = dDefect.m_bConnectedDefect;

			if (dDefect.m_nConnected == VIDefect::VERT && !bConnectedVertically && !bConnectedNeighbor) {
				continue;
			}
			if (dDefect.m_nConnected == VIDefect::HORIZ && bConnectedVertically && !bConnectedNeighbor) {
				continue;
			}
			if (!bConnectedNeighbor && !bConnectedTarget) {
				continue;
			}

			// 2 Pixel 용 불량 규칙을 적용하여 불량 판단
			if (bConnectedNeighbor) {
				// 이웃 Pixel 검사 조건일 경우, 불량 영역이 있는 첫번째 Pixel 정보를 두번째 Pixel 에도 넣어준다.
				m_pCV->DebugPrint(L"  Check MultiTarget, Connected Neighbor [%s], Check Connected Defect : %s", dDefect.m_strId.c_str(), BOOL_TO_WSTRING(bCheckConnectedDefect));
				RETURN_ON_FAIL(_CheckDefect_MultiPixel(dTargetInfo, listDefect_Target, dPixel,
					dTargetInfo, listDefect_Target, dPixel2, dDefect.m_nConnected, bCheckConnectedDefect, bFoundDefect));
			}
			else {
				m_pCV->DebugPrint(L"  Check MultiTarget [%s], Check Connected Defect : %s", dDefect.m_strId.c_str(), BOOL_TO_WSTRING(bCheckConnectedDefect));
				RETURN_ON_FAIL(_CheckDefect_MultiPixel(dTargetInfo, listDefect_Target, dPixel,
					dTargetInfo2, listDefect_Target2, dPixel2, dDefect.m_nConnected, bCheckConnectedDefect, bFoundDefect));
			}

			// 불량 규칙에 부합할 경우, 최종 불량 후보로 결정 (추후 Dark/Seed 검사후 최종 결정)
			if (bFoundDefect) {
				strDefectId = dDefect.m_strId;

				// 불량 규칙에 설정된 Drawing 방법을 추가
				RETURN_ON_FAIL(_CheckDefect_AddDefectTargetDraw(dDefect));

				if (bDrawDefectWithMask) {
					RETURN_ON_FAIL(_CheckDefect_DrawDefectWithMask(dTargetInfo, dPixel));
					if (bConnectedTarget) RETURN_ON_FAIL(_CheckDefect_DrawDefectWithMask(dTargetInfo2, dPixel2));
				}
				m_pCV->DebugPrint(L"  >>>>>>>> Check MultiTarget [%s] <<<<<<<<", strDefectId.c_str());
				break;
			}

			// 불량 규칙에 부합하지 않았으나, 'Pixel 순서 지킴' 조건이 아닐 경우 Pixel 순서를 바꾸어 재검사
			if (!dDefect.m_bKeepPixelSeq) {
				// dPixel 과 dPixel2 를 바꾸어 적용

				if (bConnectedNeighbor) {
					m_pCV->DebugPrint(L"  Check MultiTarget, Connected Neighbor [%s], Check Connected Defect : %s, Reverse Order", dDefect.m_strId.c_str(), BOOL_TO_WSTRING(bCheckConnectedDefect));
					RETURN_ON_FAIL(_CheckDefect_MultiPixel(dTargetInfo, listDefect_Target, dPixel2,
						dTargetInfo, listDefect_Target, dPixel, dDefect.m_nConnected, bCheckConnectedDefect, bFoundDefect));
				}
				else {
					m_pCV->DebugPrint(L"  Check MultiTarget [%s], Check Connected Defect : %s, Reverse Order", dDefect.m_strId.c_str(), BOOL_TO_WSTRING(bCheckConnectedDefect));
					RETURN_ON_FAIL(_CheckDefect_MultiPixel(dTargetInfo, listDefect_Target, dPixel2,
						dTargetInfo2, listDefect_Target2, dPixel, dDefect.m_nConnected, bCheckConnectedDefect, bFoundDefect));
				}

				// 불량 규칙에 부합할 경우, 최종 불량 후보로 결정 (추후 Dark/Seed 검사후 최종 결정)
				if (bFoundDefect) {
					strDefectId = dDefect.m_strId;

					// 불량 규칙에 설정된 Drawing 방법을 추가
					RETURN_ON_FAIL(_CheckDefect_AddDefectTargetDraw(dDefect));

					if (bDrawDefectWithMask) {
						RETURN_ON_FAIL(_CheckDefect_DrawDefectWithMask(dTargetInfo, dPixel2));
						if (bConnectedTarget) RETURN_ON_FAIL(_CheckDefect_DrawDefectWithMask(dTargetInfo2, dPixel));
					}

					m_pCV->DebugPrint(L"  >>>>>>>> Check MultiTarget [%s] <<<<<<<<", strDefectId.c_str());
					break;
				}
			}
		}

		RETURN_SUCCESS();
	}

	// 판별된 불량이 Dark 또는 Seed 불량 규칙에 부합하는지 검사
	Err ProcessCore::_CheckDefect_DarkOrSeed(wstring &strDefectId)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const auto &listDefect = dInspection.m_listDefect;
		const bool &bSeed = m_dDefectTargetInfo.m_bSeed;
		const bool &bDark = m_dDefectTargetInfo.m_bDark;

		m_pCV->DebugPrint(L"Check Seed or Dark, Seed : %s, Dark : %s", BOOL_TO_WSTRING(bSeed), BOOL_TO_WSTRING(bDark));

		// Check Seed or Dark
		for (auto &dDefect : listDefect) {
			if (!dDefect.m_bSeed && !dDefect.m_bDark) {		// Dark 및 Seed 검사 조건 아닌 경우 skip
				continue;
			}

			m_pCV->DebugPrint(L"  Check Seed or Dark [%s], BaseDefectId : %s", dDefect.m_strId.c_str(), dDefect.m_strBaseDefectId.c_str());

			// 기판별 불량((BasedDefectId)가 맞으면서 Seed 또는 Dark 일 경우
			if (CHECK_STRING(strDefectId, dDefect.m_strBaseDefectId) && (dDefect.m_bSeed == bSeed) && (dDefect.m_bDark == bDark)) {
				strDefectId = dDefect.m_strId;

				// 불량 규칙에 설정된 Drawing 방법을 추가
				RETURN_ON_FAIL(_CheckDefect_AddDefectTargetDraw(dDefect, !dDefect.m_bBaseDefectDraw));

				m_pCV->DebugPrint(L"  > Check Seed or Dark Result : %s", strDefectId.c_str());
				break;
			}
		}

		RETURN_SUCCESS();
	}

	// 2 Pixel 용 불량 규칙을 적용하여 불량 판단
	Err ProcessCore::_CheckDefect_MultiPixel(const TargetInfo &dTargetInfo, const vector<VIDefect> &listDefect_Target, const VIPixel &dPixel,
		const TargetInfo &dTargetInfo2, const vector<VIDefect> &listDefect_Target2, const VIPixel &dPixel2, VIDefect::Connected nConnected, bool bCheckConnectedDefect, bool &bFoundDefect)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		const VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;
		const bool bDrawDefectWithMask = (m_dParam.m_nOutputImageType == OutputImageType::DEFECT_WITH_MASK);

		//
		// 첫번째 Pixel 규칙 검사
		//
		bool bFound = false;

		VIDefect dDefect;
		Mat dConnectedDefectImg;
		if (dPixel.m_bAcceptAll) {
			m_pCV->DebugPrint(L"    Check MultiPixel, Pixel, AcceptAll");
			bFound = true;
		}
		else if (!dPixel.m_strBaseDefectId.empty()) {
			m_pCV->DebugPrint(L"    Check MultiPixel, Pixel, BaseDefectId : %s", dPixel.m_strBaseDefectId.c_str());
			RETURN_ON_FAIL(_CheckDefect_Pixel(listDefect_Target, dPixel, dDefect, bFound));
			dConnectedDefectImg = dDefect.m_dConnectedDefectImg;
		}
		else {
			m_pCV->DebugPrint(L"    Check MultiPixel, Pixel");
			RETURN_ON_FAIL(_CheckDefect_Pixel(dTargetInfo, dPixel, nConnected, true, bCheckConnectedDefect, dConnectedDefectImg, bFound));
		}
		m_pCV->DebugPrint(L"    Check MultiPixel, Pixel Result : %s", BOOL_TO_WSTRING(bFound));

		if (!bFound) {
			bFoundDefect = false;
			RETURN_SUCCESS();
		}

		//
		// 두번째 Pixel 규칙 검사
		//
		bFound = false;
		VIDefect dDefect2;
		Mat dConnectedDefectImg2;
		if (dPixel2.m_bAcceptAll) {
			m_pCV->DebugPrint(L"    Check MultiPixel, Pixel2, AcceptAll");
			bFound = true;
		}
		else if (!dPixel2.m_strBaseDefectId.empty()) {
			m_pCV->DebugPrint(L"    Check MultiPixel, Pixel2, BaseDefectId : %s", dPixel2.m_strBaseDefectId.c_str());
			RETURN_ON_FAIL(_CheckDefect_Pixel(listDefect_Target2, dPixel2, dDefect2, bFound));
			dConnectedDefectImg2 = dDefect2.m_dConnectedDefectImg;
		}
		else {
			m_pCV->DebugPrint(L"    Check MultiPixel, Pixel2");
			RETURN_ON_FAIL(_CheckDefect_Pixel(dTargetInfo2, dPixel2, nConnected, false, bCheckConnectedDefect, dConnectedDefectImg2, bFound));
		}
		m_pCV->DebugPrint(L"    Check MultiPixel2, Pixel Result : %s", BOOL_TO_WSTRING(bFound));

		// 부합된 불량 규칙이 없으면 반환
		if (!bFound) {
			bFoundDefect = false;
			RETURN_SUCCESS();
		}

		if (bCheckConnectedDefect) {
			// 두 Pixel 에 대한 불량 연결 이미지가 모두 있을 때만 검사
			bool bExistConnectedDefectImg = !CHECK_EMPTY_IMAGE(dConnectedDefectImg);
			bool bExistConnectedDefectImg2 = !CHECK_EMPTY_IMAGE(dConnectedDefectImg2);
			if (bExistConnectedDefectImg && bExistConnectedDefectImg2) {
				Rect rcTarget = dTargetInfo.m_rcArea;
				Rect rcTarget2 = dTargetInfo2.m_rcArea;

				// 두 Target 의 크기를 합한 이미지를 생성하여, 각 영역에 Target 결과를 복사하여 겹치는 부분이 있는지(연결 여부) 확인한다.
				Rect rcMergeTarget = MERGE_TARGET_AREA(rcTarget, rcTarget2);

				Mat dExtConnectedDefectImg = Mat::zeros(rcMergeTarget.size(), dConnectedDefectImg.type());
				dConnectedDefectImg.copyTo(Mat(dExtConnectedDefectImg, rcTarget));

				Mat dExtConnectedDefectImg2 = Mat::zeros(rcMergeTarget.size(), dConnectedDefectImg2.type());
				dConnectedDefectImg2.copyTo(Mat(dExtConnectedDefectImg2, rcTarget2));


				{
					// 영역 확장 : Pixel 간에 서로 붙어 있는지를 확인하기 위해 첫번째 불량 영역의 크기를 조금 확장한다.
					NIPLInput dInput;
					NIPLOutput dOutput;

					NIPLParam_MorphologyOperate dParam_MorphologyOperate;
					dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_DILATE;
					dParam_MorphologyOperate.m_nFilterSizeX = 3;
					dParam_MorphologyOperate.m_nFilterSizeY = 3;

					dInput.m_dImg = dExtConnectedDefectImg;
					dInput.m_pParam = &dParam_MorphologyOperate;
					NIPL_ERR nErr = m_pCV->MorphologyOperate(&dInput, &dOutput);
					if (NIPL_SUCCESS(nErr)) {
						dExtConnectedDefectImg = dOutput.m_dImg;
					}
				}

/*
				// 불량 영역 겹침 여부 확인용 코드
				if (CHECK_EMPTY_IMAGE(m_dDebugImg)) m_dDebugImg = Mat::zeros(m_dDetectResultImg.size(), m_dDetectResultImg.type());
				Mat(dExtConnectedDefectImg * 0.6f + dExtConnectedDefectImg2 * 0.3f).copyTo(Mat(m_dDebugImg, rcMergeTarget));
*/
				Mat dIntersectImg = dExtConnectedDefectImg & dExtConnectedDefectImg2;
				if (countNonZero(dIntersectImg) == 0) {				// 불량이 연결되어 있지 않을 경우, 불량 판별 불일치 처리
					bFoundDefect = false;

					m_pCV->DebugPrint(L"    Check Connected Defect : false");
					RETURN_SUCCESS();
				}
				m_pCV->DebugPrint(L"    Check Connected Defect : true");
			}
			else {
				m_pCV->DebugPrint(L"    Check Connected Defect : pass, Image Exist : Pixel1 (%s), Pixel2 (%s)",
					BOOL_TO_WSTRING(bExistConnectedDefectImg), BOOL_TO_WSTRING(bExistConnectedDefectImg2));
			}
		}

		// 기판별 불량((BasedDefectId)의 Drawing 방법을 사용하는 옵션일 경우, 불량 규칙에 설정된 Drawing 방법을 추가
		bFoundDefect = true;
		if (!dPixel.m_strBaseDefectId.empty()) {
			if (dPixel.m_bBaseDefectDraw) {
				RETURN_ON_FAIL(_CheckDefect_AddDefectTargetDraw(dDefect));
			}
			if (bDrawDefectWithMask) {
				RETURN_ON_FAIL(_CheckDefect_DrawDefectWithMask(dTargetInfo, dDefect.m_listPixel.front()));
			}
		}
		if (!dPixel2.m_strBaseDefectId.empty()) {
			if (dPixel2.m_bBaseDefectDraw) {
				RETURN_ON_FAIL(_CheckDefect_AddDefectTargetDraw(dDefect2));
			}
			if (bDrawDefectWithMask) {
				RETURN_ON_FAIL(_CheckDefect_DrawDefectWithMask(dTargetInfo2, dDefect2.m_listPixel.front()));
			}
		}

		m_pCV->DebugPrint(L"    Check MultiPixel, Result : true");

		RETURN_SUCCESS();
	}

	// 이미 판별된 불량인지를 확인. (부합된 불량 규칙을 저장한 리스트에서 검색)
	Err ProcessCore::_CheckDefect_Pixel(const vector<VIDefect> &listDefect_Target, const VIPixel &dPixel, VIDefect &dDefect, bool &bFoundDefect)
	{
		bFoundDefect = false;
		for (const auto &dDefectTarget : listDefect_Target) {
			if (CHECK_STRING(dDefectTarget.m_strId, dPixel.m_strBaseDefectId)) {
				bFoundDefect = true;
				dDefect = dDefectTarget;
				break;
			}
		}

		RETURN_SUCCESS();
	}

	// Pixel 에 대한 불량 규칙을 적용하여 불량을 판단
	Err ProcessCore::_CheckDefect_Pixel(const TargetInfo &dTargetInfo, const VIPixel &dPixel, VIDefect::Connected nConnected, bool bFirstPixel, bool bCheckConnectedDefect, Mat &dConnectedDefectImg, bool &bFoundDefect)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		VITemplate &dTemplate = dInspection.m_dTemplate;
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;
		const auto &listCheck = dPixel.m_listCheck;

		// Template 을 해당 Target 의 크기에 맞게 확대/축소
		// Mask 복사시 Template 크기에 맞게 자동으로 크기 변환이 되므로, Mask 사용전에 Template 크기를 Target 크기에 맞춰주어야 함.
		dTemplate.ResizeTemplateImage(dTargetInfo.m_sizeTemplate);

		// Pixel 불량 판단을 위해 지정된 모든 Mask 와의 접촉여부를 검사
		bFoundDefect = false;
		for (const auto &dCheck : listCheck) {
			const int &nMinDefectSize = (dCheck.m_nMinDefectSize != 0) ? dCheck.m_nMinDefectSize : dDefaultParam.m_nMinDefectSize;
			m_pCV->DebugPrint(L"      Check Mask (%s, %d)", dCheck.GetStatus().c_str(), nMinDefectSize);

			// Mask 와 불량 영역간의 접촉여부 검사
			bool bFound = false;
			Mat dPixelConnectedDefectImg;
			RETURN_ON_FAIL(_CheckDefect_Mask(dTargetInfo, dCheck, nMinDefectSize, nConnected, bFirstPixel, dPixelConnectedDefectImg, bFound));
			m_pCV->DebugPrint(L"      Check Mask Result : %s", BOOL_TO_WSTRING(bFound));

			if (!bFound) {
				RETURN_SUCCESS();
			}

			if (!CHECK_EMPTY_IMAGE(dPixelConnectedDefectImg)) {
				// 기본값을 1 로 설정. 2 Pixel 불량일 경우 불량 영역간의 접촉 여부를 판단하기 위해서는 최소한의 확장 크기가 필요함.
				const int &nConnectedDefectExtendSize = (dDefaultParam.m_nConnectedDefectExtendSize != 0) ? dDefaultParam.m_nConnectedDefectExtendSize : 1;
				int nDilateFilterSize = nConnectedDefectExtendSize * 2 + 1;

				// 영역 확장
				NIPLInput dInput;
				NIPLOutput dOutput;

				NIPLParam_MorphologyOperate dParam_MorphologyOperate;
				dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_DILATE;
				dParam_MorphologyOperate.m_bCircleFilter = true;
				dParam_MorphologyOperate.m_nFilterSizeX = nDilateFilterSize;
				dParam_MorphologyOperate.m_nFilterSizeY = nDilateFilterSize;

				dInput.m_dImg = dPixelConnectedDefectImg;
				dInput.m_pParam = &dParam_MorphologyOperate;
				NIPL_ERR nErr = m_pCV->MorphologyOperate(&dInput, &dOutput);
				if (NIPL_SUCCESS(nErr)) {
					dPixelConnectedDefectImg = dOutput.m_dImg;
				}

				if (CHECK_EMPTY_IMAGE(dConnectedDefectImg)) {			// 첫번째 불량 영역일 경우 그대로 복사
					dConnectedDefectImg = dPixelConnectedDefectImg;

/*
					if (check_empty_image(m_ddebugimg)) m_ddebugimg = mat::zeros(m_ddetectresultimg.size(), m_ddetectresultimg.type());
					dconnecteddefectimg.copyto(mat(m_ddebugimg, dtargetinfo.m_rcarea));
*/
				}
				else {													
//					Mat(dConnectedDefectImg * 0.75f | dPixelConnectedDefectImg).copyTo(Mat(m_dDebugImg, dTargetInfo.m_rcArea));

					if (bCheckConnectedDefect) {						// 불량들이 서로 연결되어 있는지 확인
						Mat dIntersectImg = dConnectedDefectImg & dPixelConnectedDefectImg;
						if (countNonZero(dIntersectImg) == 0) {			// 연결되어 있지 않을 경우, 불량 판별 불일치 처리
							bFoundDefect = false;

							m_pCV->DebugPrint(L"      Check Connected Defect : false");
							RETURN_SUCCESS();
						}
					}
					m_pCV->DebugPrint(L"      Check Connected Defect : true");

					dConnectedDefectImg |= dPixelConnectedDefectImg;	// 불량 영역들을 합침
				}
			}
		}

		bFoundDefect = true;
		RETURN_SUCCESS();
	}

	// Mask 영역과 불량 영역의 접촉 관계를 확인하여 불량 여부를 판단
	Err ProcessCore::_CheckDefect_Mask(const TargetInfo &dTargetInfo, const VICheck &dCheck, int nMinDefectSize, VIDefect::Connected nConnected, bool bFirstPixel, Mat &dConnectedDefectImg, bool &bFoundDefect)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		VITemplate &dTemplate = dInspection.m_dTemplate;
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;

		const Mat &dImg = m_dParam.m_dInputImage;
		Rect rcTemplatePixel = dTargetInfo.m_rcTemplatePixelArea;
		const Mat dTargetImg(m_dAdjustedBackgroundImg, dTargetInfo.m_rcArea);		// 색상 비교를 할 때는 배경 보정이 된 이미지를 사용한다.
		const Mat dTargetResultImg(m_dDetectResultImg, dTargetInfo.m_rcArea);
		const bool bDarkOrSeed = dTargetInfo.m_bDark || dTargetInfo.m_bSeed;
		const int &nConnectedExtendSize = (dCheck.m_nConnectedExtendSize != 0) ? dCheck.m_nConnectedExtendSize :
			((dDefaultParam.m_nConnectedExtendSize != 0) ? dDefaultParam.m_nConnectedExtendSize : 0);
		int nDilateFilterSize = nConnectedExtendSize * 2 + 1;
		const float nColorSimilarityRed = (dCheck.m_nColorSimilarityRed != 0.f) ? dCheck.m_nColorSimilarityRed : dDefaultParam.m_nColorSimilarityRed;
		const float nColorSimilarityGreen = (dCheck.m_nColorSimilarityGreen != 0.f) ? dCheck.m_nColorSimilarityGreen : dDefaultParam.m_nColorSimilarityGreen;
		const float nColorSimilarityBlue = (dCheck.m_nColorSimilarityBlue != 0.f) ? dCheck.m_nColorSimilarityBlue : dDefaultParam.m_nColorSimilarityBlue;

		m_pCV->DebugPrint(L"        Check Mask, MaskId : %s, ColorMaskId : %s", dCheck.m_strMaskId.c_str(), dCheck.m_strColorMaskId.c_str());

		// Mask 이미지 복사 (확대/축소/회전 됨)
		Mat dMaskImg;
		RETURN_ON_FAIL(_GetTransformMaskImage(dCheck.m_strMaskId, dMaskImg));

		// Color Mask 가 있을 경우 이미지 복사 (확대/축소/회전 됨), 없으면 Mask 이미지 사용
		Mat dColorMaskImg;
		if (dCheck.m_strColorMaskId.empty()) {
			dMaskImg.copyTo(dColorMaskImg);
		}
		else {
			RETURN_ON_FAIL(_GetTransformMaskImage(dCheck.m_strColorMaskId, dColorMaskImg));
		}

		Mat dTargetTemplateImg; 
		Mat dTargetMaskImg;
		Mat dTargetColorMaskImg;

		Mat dExtResultImg;
		Mat dExtMaskImg;
		Rect rcExtResultPart;
		if (dCheck.m_nStatus == VICheck::CONNECTED_NEIGHBOR) {
			// 이웃 Pixel 검사(connected_neighbor) 옵션이 있을 경우
			// 1) Mask 및 불량 Target 을 전체 Template Pixel 크기로 복원하여 검사 (검사 이미지 가장 자리 Target 은 잘려져 있을 수 있으므로 복원함)
			// 2) Mask 와 불량 Target 을 표준 위치(Standard, 0도)로 복원하여 검사

			// 이웃 Pixel 의 전체 크기를 확인
			Rect rcFullTemplatePixel;
			bool bVert = (nConnected == VIDefect::VERT);
			if (bFirstPixel) rcFullTemplatePixel = dTemplate.GetPrevTemplatePixelArea(dTargetInfo.m_nTemplatePixelIndex, bVert);
			else rcFullTemplatePixel = dTemplate.GetNextTemplatePixelArea(dTargetInfo.m_nTemplatePixelIndex, bVert);

			// 불량 Target 이미지 변환
			Mat dStandardResultImg = Mat::zeros(dTargetInfo.m_sizeTemplate, dMaskImg.type());	// Template 전체 크기의 이미지 생성
			dTargetResultImg.copyTo(Mat(dStandardResultImg, rcTemplatePixel));					// 불량 영역 복사
			dStandardResultImg = Mat(dStandardResultImg, rcFullTemplatePixel);					// Template Pixel 영역 추출
			_ReverseRotateImage(dStandardResultImg);											// 회전 복원

			// Mask 이미지 변환
			Mat dStandardMaskImg;
			dTargetMaskImg = Mat(dMaskImg, rcFullTemplatePixel);			// Mask 에서 Template Pixel 영역에 해당하는 부분 추출
			dTargetMaskImg.copyTo(dStandardMaskImg);						// Mask 내의 Target 영역을 복사
			_ReverseRotateImage(dStandardMaskImg);							// 회전 복원

			int nStandardResultImgSizeX = dStandardResultImg.cols;
			int nStandardResultImgSizeY = dStandardResultImg.rows;
			int nStandardMaskImgSizeX = dStandardMaskImg.cols;
			int nStandardMaskImgSizeY = dStandardMaskImg.rows;

			// Mask 이미지와 불량 Target 의 접촉 비교를 위한 확장 이미지 생성
			Rect rcFirstPart;
			Rect rcSecondPart;
			if (bVert) { // 연결 상태(상/하)
				// 확장 이미지는 Mask 이미지와 불량 Target 을 붙여 놓은 크기를 가짐
				dExtResultImg = Mat::zeros(nStandardResultImgSizeY + nStandardMaskImgSizeY, nStandardMaskImgSizeX, dMaskImg.type());
				dExtMaskImg = Mat::zeros(nStandardResultImgSizeY + nStandardResultImgSizeY, nStandardMaskImgSizeX, dMaskImg.type());

				// Pixel 순서에 따라 복사해 넣을 위치를 설정
				if (bFirstPixel) {
					rcFirstPart = Rect(0, 0, nStandardMaskImgSizeX, nStandardMaskImgSizeY);
					rcSecondPart = Rect(0, nStandardMaskImgSizeY, nStandardResultImgSizeX, nStandardResultImgSizeY);
				}
				else {
					rcFirstPart = Rect(0, 0, nStandardResultImgSizeX, nStandardResultImgSizeY);
					rcSecondPart = Rect(0, nStandardResultImgSizeY, nStandardMaskImgSizeX, nStandardMaskImgSizeY);
				}
			}
			else {	// 연결 상태(좌/우)
				// 확장 이미지는 Mask 이미지와 불량 Target 을 붙여 놓은 크기를 가짐
				dExtResultImg = Mat::zeros(nStandardMaskImgSizeY, nStandardResultImgSizeX + nStandardMaskImgSizeX, dMaskImg.type());
				dExtMaskImg = Mat::zeros(nStandardMaskImgSizeY, nStandardResultImgSizeX + nStandardMaskImgSizeX, dMaskImg.type());

				// Pixel 순서에 따라 복사해 넣을 위치를 설정
				if (bFirstPixel) {
					rcFirstPart = Rect(0, 0, nStandardMaskImgSizeX, nStandardMaskImgSizeY);
					rcSecondPart = Rect(nStandardMaskImgSizeX, 0, nStandardResultImgSizeX, nStandardResultImgSizeY);
				}
				else {
					rcFirstPart = Rect(0, 0, nStandardResultImgSizeX, nStandardResultImgSizeY);
					rcSecondPart = Rect(nStandardResultImgSizeX, 0, nStandardMaskImgSizeX, nStandardMaskImgSizeY);
				}
			}

			// Mask 이미지와 불량 Target 을 이미지에 복사
			if (bFirstPixel) {
				dStandardMaskImg.copyTo(Mat(dExtMaskImg, rcFirstPart));
				dStandardResultImg.copyTo(Mat(dExtResultImg, rcSecondPart));
				rcExtResultPart = rcSecondPart;
			}
			else {
				dStandardResultImg.copyTo(Mat(dExtResultImg, rcFirstPart));
				dStandardMaskImg.copyTo(Mat(dExtMaskImg, rcSecondPart));
				rcExtResultPart = rcFirstPart;
			}

			// 접촉 테스트 진행을 위해 Template 및 ColorMask 도 전체 Template Pixel 크기로 변환. (회전은 안함)
			dTargetTemplateImg = dTemplate.GetTemplatePixelImage(rcFullTemplatePixel);
			dTargetColorMaskImg = Mat(dColorMaskImg, rcFullTemplatePixel);
		}
		else {
			dTargetTemplateImg = dTemplate.GetTemplatePixelImage(rcTemplatePixel);
			dTargetMaskImg = Mat(dMaskImg, rcTemplatePixel);
			dTargetColorMaskImg = Mat(dColorMaskImg, rcTemplatePixel);

			// 접촉 테스트시 동일한 변수를 사용하기위해, 이웃 Pixel 검사에 사용된 Ext 계열의 변수에 복사를 함. (영역 확장의 개념은 없음)
			dExtResultImg = dTargetResultImg;
			dExtMaskImg = dTargetMaskImg;
		}

		NIPLInput dInput;
		NIPLOutput dOutput;
		NIPL_ERR nErr;

		// 개별 불량 영역에 대해서 비교를 수행한다
		dInput.Clear();
		dOutput.Clear();

		NIPLParam_FindBlob dParam_FindBlob;
		dParam_FindBlob.m_bFindInRange = true;

		dInput.m_dImg = dExtResultImg;
		dInput.m_pParam = &dParam_FindBlob;

		nErr = m_pCV->FindBlob(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			RETURN_FAIL(FAIL_TO_CLASSIFY);
		}

		shared_ptr<NIPLResult_FindBlob> pResult_FindBlob = dOutput.GetResult<NIPLResult_FindBlob>();
		if (pResult_FindBlob == nullptr) {
			bFoundDefect = false;
			m_pCV->DebugPrint(L"        Check Mask, Not Found Defect");
			RETURN_SUCCESS();
		}

		for (auto &dBlob : pResult_FindBlob->m_listBlob) {
			Mat &dBlobImg = dBlob.m_dImg;					
			Rect &rcBoundingBox = dBlob.m_rcBoundingBox;
			Mat dDefectImg = Mat::zeros(dExtResultImg.size(), dExtResultImg.type());
			dBlobImg.copyTo(Mat(dDefectImg, rcBoundingBox));

			Mat dExtDefectImg = dDefectImg;
			if ((dCheck.m_nStatus == VICheck::CONNECTED || dCheck.m_nStatus == VICheck::CONNECTED_NEIGHBOR)) {
				if (nConnectedExtendSize > 0) {
					// 접촉(connected) 여부 테스트 일 경우, 불량 영역 확장 옵션을 적용
					NIPLInput dInput;
					NIPLOutput dOutput;

					NIPLParam_MorphologyOperate dParam_MorphologyOperate;
					dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_DILATE;
					dParam_MorphologyOperate.m_bCircleFilter = true;
					dParam_MorphologyOperate.m_nFilterSizeX = nDilateFilterSize;
					dParam_MorphologyOperate.m_nFilterSizeY = nDilateFilterSize;

					dInput.m_dImg = dExtDefectImg;
					dInput.m_pParam = &dParam_MorphologyOperate;
					nErr = m_pCV->MorphologyOperate(&dInput, &dOutput);
					if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
						RETURN_FAIL(FAIL_TO_CLASSIFY);
					}
					dExtDefectImg = dOutput.m_dImg;
				}

				// 접촉 테스트의 경우, 1 픽셀이라도 겹치면 접촉된 것으로 판단하기 위해 최소 크기를 1로 변경.
				nMinDefectSize = 1;
			}

			Mat dIntersectImg = dExtMaskImg & dExtDefectImg;
			int nDefectSize = countNonZero(dIntersectImg);
			if (nDefectSize > nMinDefectSize) {
				bFoundDefect = true;

				// 불량 영역 연결 테스트용 이미지 생성
				if (dCheck.m_nStatus == VICheck::CONNECTED || dCheck.m_nStatus == VICheck::CONNECTED_NEIGHBOR) {
					if (dCheck.m_nStatus == VICheck::CONNECTED_NEIGHBOR) {	// 불량 영역이 있었던 원래 Target 의 위치와 회전으로 복원한다.
						dDefectImg = Mat(dDefectImg, rcExtResultPart);
						_RotateImage(dDefectImg);
						dDefectImg = Mat(dDefectImg, rcTemplatePixel);
					}

					if (CHECK_EMPTY_IMAGE(dConnectedDefectImg)) {			// 첫번째 불량 영역일 경우, 복사
						dConnectedDefectImg = dDefectImg;
					}
					else {													// 두번째이후 불량 영역은 모두 합침.
						dConnectedDefectImg |= dDefectImg;
					}
				}
				else {			// 연결 여부 체크가 아닌 경우, 바로 빠져 나감
					break;
				}
			}
		}

		// 접촉이 되어 있을 경우, 색상을 비교하여 다를 경우 '판별 실패'로 변경
		if (bFoundDefect && (dCheck.m_nStatus == VICheck::CONNECTED || dCheck.m_nStatus == VICheck::CONNECTED_NEIGHBOR) && !bDarkOrSeed) {
			// Template 이미지에서 임계치에 사용한 색상 정보(평균, 표준편차)
			Scalar dMaskMean;
			Scalar dMaskStd;
			meanStdDev(dTargetTemplateImg, dMaskMean, dMaskStd, dTargetColorMaskImg);

			// Target 이미지상의 불량 영역에 대한 색상 정보 확인(평균)
			Scalar dMean;
			Scalar dStd;
			meanStdDev(dTargetImg, dMean, dStd, dConnectedDefectImg);

			for (int i = 2; i >= 0; i--) {
				float nMean = dMaskMean[i];
				float nStd = dMaskStd[i];
				float nValue = dMean[i];

				if (nStd == 0.f) {
					// Template 상의 밝기 표준 편차가 0 인 경우는 Mask 영역 지정이 없거나 잘못되었을 경우임
					// -> 동일한 색상이 아닌 것으로 처리
					m_pCV->DebugPrint(L"        Check Mask, Connected, Similar Color (%s) : false by Std == 0",
						(i == 0) ? L"Blue" : ((i == 1) ? L"Green" : L"Red"));

					bFoundDefect = false;
					break;
				}
				else {
					float nColorSimilarity = (i == 0) ? nColorSimilarityBlue : ((i == 1) ? nColorSimilarityGreen : nColorSimilarityRed);

					float nDistance = abs(nValue - nMean) / nStd;

					m_pCV->DebugPrint(L"        Check Mask, Connected, Similar Color (%s) : %s, Mean : %.2f, Std : %.2f, nValue : %.2f, Distance : %.2f < %.2f",
						(i == 0) ? L"Blue" : ((i == 1) ? L"Green" : L"Red"), BOOL_TO_WSTRING(nDistance < nColorSimilarity),
						nMean, nStd, nValue, nDistance, nColorSimilarity);

					// 불량 영역의 평균 밝기가 Template 상의 밝기 분포를 벗어날 경우 동일한 색상으로 간주하지 않음
					if (nDistance > nColorSimilarity) {
						bFoundDefect = false;
						break;
					}
				}
			}
		}

		// 테스트 조건이 비접촉(OFF) 일 경우, 접촉 테스트 결과의 반대를 반환함.
		if (dCheck.m_nStatus == VICheck::OFF) {
			bFoundDefect = !bFoundDefect;
		}

		RETURN_SUCCESS();
	}

	// 불량 규칙에 해당하는 Drawing 방법을 리스트에 추가
	Err ProcessCore::_CheckDefect_AddDefectTargetDraw(const VIDefect &dDefect, bool bClearFirst)
	{
		if (bClearFirst) {
			for (auto &dDraw : m_listDefectTargetDraw) {
				dDraw.Clear();
			}
			m_listDefectTargetDraw.clear();
		}

		// 불량 규칙에 설정된 모든 Draw 를 리스트에 추가
		for (const auto &dDraw : dDefect.m_listDraw) {
			m_listDefectTargetDraw.push_back(dDraw);
			m_pCV->DebugPrint(L"=> Add Draw, DrawSetId : %s, Type: %s, MaskId: %s, Color: (%.0f,%.0f,%.0f), Alpah: %.2f, Mode : %s",
				dDraw.m_strDrawSetId.c_str(), dDraw.GetType().c_str(), dDraw.m_strMaskId.c_str(),
				dDraw.m_dColor[2], dDraw.m_dColor[1], dDraw.m_dColor[0], dDraw.m_nAlpha, dDraw.GetMode().c_str());
		}

		RETURN_SUCCESS();
	}

	// 불량 영역을 접촉한 Mask 영역과 함께 그림
	Err ProcessCore::_CheckDefect_DrawDefectWithMask(const TargetInfo &dTargetInfo, const VIPixel &dPixel)
	{
		Mat &dOutputImg = m_dResult.m_dOutputImage;
		if (CHECK_EMPTY_IMAGE(m_dDefectWidthMaskImg)) {
			m_pCV->DebugPrint(L"    > DrawDefectWithMask, Copy from Output Image");

			dOutputImg.copyTo(m_dDefectWidthMaskImg);
		}

		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		VITemplate &dTemplate = dInspection.m_dTemplate;
		dTemplate.ResizeTemplateImage(dTargetInfo.m_sizeTemplate);

		const Rect &rcTarget = dTargetInfo.m_rcArea;
		const Rect &rcTemplatePixel = dTargetInfo.m_rcTemplatePixelArea;

		Mat dTargetImg(m_dDefectWidthMaskImg, rcTarget);
		for (auto &dCheck : dPixel.m_listCheck) {
			if (dCheck.m_nStatus != VICheck::OFF) {
				Mat dMaskImg;
				RETURN_ON_FAIL(_GetTransformMaskImage(dCheck.m_strMaskId, dMaskImg));

				Mat dTargetMaskImg(dMaskImg, rcTemplatePixel);
				Mat((dTargetMaskImg * 0.5f) | dTargetImg).copyTo(dTargetImg);

				m_pCV->DebugPrint(L"    > DrawDefectWithMask : %s", dCheck.m_strMaskId.c_str());
			}
		}

		RETURN_SUCCESS();
	}

	// 불량 Drawing
	Err ProcessCore::_DrawDefect()
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		const VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;
		const Mat &dImg = m_dParam.m_dInputImage;
		Mat &dOutputImg = m_dResult.m_dOutputImage;

		m_pCV->DebugPrint(L"==== [Draw Defect] ====");

		// 입력 이미지를 결과 이미지에 복사
		dImg.copyTo(dOutputImg);
		// 불량 영역을 OverlapMask 이미지에 복사
		m_dDetectResultImg.copyTo(m_dDrawingOverlapMaskImg);  

		// 불량 규칙에 따른 모든 Drawing 을 순서대로 그림.
		for (auto &dDraw : m_listDefectTargetDraw) {
			if (!dDraw.m_strDrawSetId.empty()) {
				// DrawSet 일 경우, 배치 작업 수행
				RETURN_ON_FAIL(_DrawDefect_DrawSet(dDraw.m_strDrawSetId));
			}
			else {
				// Draw 수행
				RETURN_ON_FAIL(_DrawDefect_Draw(dDraw));
			}
		}

		// DMD 이미지 복사
		Mat dDMDImg(dOutputImg, m_dResult.m_rcDMDArea);
		dDMDImg.copyTo(m_dResult.m_dDMDImage);
		if (m_dParam.m_nOutputImageType == OutputImageType::DMD) {
			dDMDImg.copyTo(dOutputImg);
		}

		RETURN_SUCCESS();
	}

	// DrawSet 으로 지정된 Drawing 배치 작업 수행
	Err ProcessCore::_DrawDefect_DrawSet(const wstring &strId)
	{
		if (!strId.empty()) {
			m_pCV->DebugPrint(L"Draw Defect, DrawSet : %s", strId.c_str());

			const list<VIDrawSet> &listDrawSet = m_dVIData.m_dDrawing.m_listDrawSet;

			// 지정한 DrawSet 이 있는지 검색
			auto it = find_if(listDrawSet.begin(), listDrawSet.end(), [&](const VIDrawSet &dDrawSet) {
				if(CHECK_STRING(strId, dDrawSet.m_strId)) {
					return true;
				}

				return false;
			});

			if (it != listDrawSet.end()) {
				const VIDrawSet &dDrawSet = *it;
				const list<VIDraw> &m_listDraw = dDrawSet.m_listDraw;

				// DrawSet 에 포함된 모든 Draw 수행
				for (const auto &dDraw : m_listDraw) {
					if (!dDraw.m_strDrawSetId.empty()) { 	// 내부에 DrawSet 이 또 있을 경우
						if (!CHECK_STRING(strId, dDraw.m_strDrawSetId)) {
							RETURN_ON_FAIL(_DrawDefect_DrawSet(dDraw.m_strDrawSetId));
						}
					}
					else {
						// Draw 수행
						RETURN_ON_FAIL(_DrawDefect_Draw(dDraw));
					}
				}
			}
		}

		RETURN_SUCCESS();
	}

	// Draw 으로 지정된 그리기 작업 수행
	Err ProcessCore::_DrawDefect_Draw(const VIDraw &dDraw)
	{
		m_pCV->DebugPrint(L"Draw Defect, Type: %s, MaskId: %s, Color: (%.0f,%.0f,%.0f), Alpah: %.2f",
			const_cast<VIDraw &>(dDraw).GetType().c_str(), dDraw.m_strMaskId.c_str(),
			dDraw.m_dColor[2], dDraw.m_dColor[1], dDraw.m_dColor[0], dDraw.m_nAlpha);

		switch (dDraw.m_nType) {
		case VIDraw::MASK:
		{
			// Mask 영역 그림
			RETURN_ON_FAIL(_DrawDefect_DrawMask(dDraw));
		}
		break;
		case VIDraw::DEFECT:
		{
			// 불량 영역 그림
			RETURN_ON_FAIL(_DrawDefect_DrawDefect(dDraw));
		}
		break;
		case VIDraw::DMD:
		{
			// DMD 영역 그림
			RETURN_ON_FAIL(_DrawDefect_DrawDMD(dDraw));
		}
		break;
		case VIDraw::DEFECT_BOX:
		{
			// DEFECT BOX 그림
			RETURN_ON_FAIL(_DrawDefect_DrawDefectBox(dDraw));
		}
		break;
		}

		RETURN_SUCCESS();
	}

	// 불량 영역에 대한 Bounding Box 를 그린 Mask 이미지를 생성
	Err ProcessCore::_DrawDefect_GetBoundingBoxMask(Mat &dBoundingBoxMaskImg, int nExtendSize, bool bDMD)
	{
		Mat &dOutputImg = m_dResult.m_dOutputImage;

		int nImgSizeX = dOutputImg.cols;
		int nImgSizeY = dOutputImg.rows;

		dBoundingBoxMaskImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

		// 불량 영역에 대한 Bounding Box 영역 확인
		Rect rcBoundingBox;
		RETURN_ON_FAIL(_DrawDefect_GetBoundingBox(rcBoundingBox));

		// 확장 크기만큼 Bounding Box 를 확장
		int nStartX = rcBoundingBox.x - nExtendSize;
		int nStartY = rcBoundingBox.y - nExtendSize;
		int nEndX = rcBoundingBox.br().x + nExtendSize;
		int nEndY = rcBoundingBox.br().y + nExtendSize;
		if (nStartX < 0) nStartX = 0;
		if (nStartY < 0) nStartY = 0;
		if (nEndX > nImgSizeX - 1) nEndX = nImgSizeX - 1;
		if (nEndY > nImgSizeY - 1) nEndY = nImgSizeY - 1;

		if (!bDMD) {
			int nTargetBoxStartX = -1;
			int nTargetBoxStartY = -1;
			int nTargetBoxEndX = -1;
			int nTargetBoxEndY = -1;
			for (const auto &dTargetInfo : m_listTargetInfo) {
				const Rect &rcTarget = dTargetInfo.m_rcArea;
				int nTargetStartX = rcTarget.x;
				int nTargetStartY = rcTarget.y;
				int nTargetEndX = rcTarget.br().x;
				int nTargetEndY = rcTarget.br().y;

				if (nTargetBoxStartX < 0) {
					nTargetBoxStartX = nTargetStartX;
					nTargetBoxStartY = nTargetStartY;
					nTargetBoxEndX = nTargetEndX;
					nTargetBoxEndY = nTargetEndY;
					continue;
				}

				if (nTargetStartX < nTargetBoxStartX) nTargetBoxStartX = nTargetStartX;
				if (nTargetStartY < nTargetBoxStartY) nTargetBoxStartY = nTargetStartY;
				if (nTargetEndX > nTargetBoxEndX) nTargetBoxEndX = nTargetEndX;
				if (nTargetEndY > nTargetBoxEndY) nTargetBoxEndY = nTargetEndY;
			}
			if (nStartX < nTargetBoxStartX) nStartX = nTargetBoxStartX;
			if (nStartY < nTargetBoxStartY) nStartY = nTargetBoxStartY;
			if (nEndX > nTargetBoxEndX) nEndX = nTargetBoxEndX;
			if (nEndY > nTargetBoxEndY) nEndY = nTargetBoxEndY;
		}

		Rect rcExtBoundingBox(nStartX, nStartY, nEndX - nStartX, nEndY - nStartY);
		Mat dBoxArea(dBoundingBoxMaskImg, rcExtBoundingBox);
		dBoxArea = 255;

		if (bDMD) {
			m_dResult.m_rcDMDArea = rcExtBoundingBox;
		}

		RETURN_SUCCESS();
	}

	// 불량 영역에 대한 Bounding Box 영역 확인
	Err ProcessCore::_DrawDefect_GetBoundingBox(Rect &rcBoundingBox)
	{
		Mat &dOutputImg = m_dResult.m_dOutputImage;

		int nImgSizeX = dOutputImg.cols;
		int nImgSizeY = dOutputImg.rows;

		NIPLInput dInput;
		NIPLOutput dOutput;

		NIPLParam_FindBlob dParam_FindBlob;
		dParam_FindBlob.m_bFindInRange = true;

		dInput.m_dImg = m_dDetectResultImg;
		dInput.m_pParam = &dParam_FindBlob;

		NIPL_ERR nErr = m_pCV->FindBlob(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			RETURN_SUCCESS();
		}

		shared_ptr<NIPLResult_FindBlob> pResult_FindBlob = dOutput.GetResult<NIPLResult_FindBlob>();
		if (pResult_FindBlob == nullptr) {
			RETURN_SUCCESS();
		}

		int nStartX = nImgSizeX;
		int nStartY = nImgSizeY;
		int nEndX = -1;
		int nEndY = -1;

		for (const auto &dBlob : pResult_FindBlob->m_listBlob) {
			Rect rcBox = dBlob.m_rcBoundingBox;

			if (rcBox.x < nStartX) {
				nStartX = rcBox.x;
			}
			if (rcBox.y < nStartY) {
				nStartY = rcBox.y;
			}
			if (rcBox.br().x > nEndX) {
				nEndX = rcBox.br().x;
			}
			if (rcBox.br().y > nEndY) {
				nEndY = rcBox.br().y;
			}
		}

		rcBoundingBox = Rect(nStartX, nStartY, nEndX - nStartX, nEndY - nStartY);
		RETURN_SUCCESS();
	}

	// Mask 영역을 그림
	Err ProcessCore::_DrawDefect_DrawMask(const VIDraw &dDraw)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		const VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;
		const float nExtendSize = dDraw.m_nExtendSize;

		m_pCV->DebugPrint(L"Draw Defect, DrawMask, MaskId : %s, ExtendSize : %.0f", dDraw.m_strMaskId, nExtendSize);

		Mat dMaskImg;
		RETURN_ON_FAIL(_GetTransformMaskImage(dDraw.m_strMaskId, dMaskImg));

		// 영역 확대/축소
		if (nExtendSize != 0) {
			NIPLInput dInput;
			NIPLOutput dOutput;
			NIPLParam_MorphologyOperate dParam_MorphologyOperate;
			NIPL_ERR nErr;

			int nFilterSize = abs(nExtendSize) * 2 + 1;

			dInput.Clear();
			dOutput.Clear();
			dParam_MorphologyOperate.Clear();

			if (nExtendSize > 0) dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_DILATE;
			else dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_ERODE;
			dParam_MorphologyOperate.m_nFilterSizeX = nFilterSize;
			dParam_MorphologyOperate.m_nFilterSizeY = nFilterSize;

			dInput.m_dImg = dMaskImg;
			dInput.m_pParam = &dParam_MorphologyOperate;
			nErr = m_pCV->MorphologyOperate(&dInput, &dOutput);
			if (NIPL_SUCCESS(nErr)) {
				dMaskImg = dOutput.m_dImg;
			}
		}

		// Mask 영역을 출력 이미지 또는 OverlapMask 에 그림
		RETURN_ON_FAIL(_DrawDefect_DrawArea(dMaskImg, dDraw));

		RETURN_SUCCESS();
	}

	// 불량 영역을 그림
	Err ProcessCore::_DrawDefect_DrawDefect(const VIDraw &dDraw)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		const VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;
		const float nFillSize = (dDraw.m_nFillSize == 0) ? dDefaultParam.m_nFillSize : (dDraw.m_nFillSize > 0) ? dDraw.m_nFillSize : 0;
		const float nExtendSize = (dDraw.m_nExtendSize == 0) ? dDefaultParam.m_nExtendSize : (dDraw.m_nExtendSize > 0) ? dDraw.m_nExtendSize : 0;

		m_pCV->DebugPrint(L"Draw Defect, DrawDefect, FillSize : %.0f, ExtendSize : %.0f", nFillSize, nExtendSize);

		NIPLInput dInput;
		NIPLOutput dOutput;
		NIPLParam_MorphologyOperate dParam_MorphologyOperate;
		NIPL_ERR nErr;

		// 영역 내부 채움
		if (nFillSize > 0) {
			int nFilterSize = nFillSize * 2 + 1;

			// 영역과 영역 사이의 틈을 채움
			NIPLParam_MorphologyOperate dParam_MorphologyOperate;
			dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_CLOSE;
			dParam_MorphologyOperate.m_nFilterSizeX = nFilterSize;
			dParam_MorphologyOperate.m_nFilterSizeY = nFilterSize;

			dInput.m_dImg = m_dDetectResultImg;
			dInput.m_pParam = &dParam_MorphologyOperate;
			nErr = m_pCV->MorphologyOperate(&dInput, &dOutput);
			if (NIPL_SUCCESS(nErr)) {
				m_dDetectResultImg = dOutput.m_dImg;
			}

			// 영역 내부의 구멍을 채움
			dInput.Clear();
			dOutput.Clear();

			NIPLParam_FillHole dParam_FillHole;
			dInput.m_dImg = m_dDetectResultImg;
			dInput.m_pParam = &dParam_FillHole;
			nErr = m_pCV->FillHole(&dInput, &dOutput);
			if (NIPL_SUCCESS(nErr)) {
				m_dDetectResultImg = dOutput.m_dImg;
			}
		}

		// 영역 확대/축소
		if (nExtendSize > 0) {
			int nDilateFilterSize = nExtendSize * 2 + 1;

			dInput.Clear();
			dOutput.Clear();
			dParam_MorphologyOperate.Clear();

			dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_DILATE;
			dParam_MorphologyOperate.m_nFilterSizeX = nDilateFilterSize;
			dParam_MorphologyOperate.m_nFilterSizeY = nDilateFilterSize;

			dInput.m_dImg = m_dDetectResultImg;
			dInput.m_pParam = &dParam_MorphologyOperate;
			nErr = m_pCV->MorphologyOperate(&dInput, &dOutput);
			if (NIPL_SUCCESS(nErr)) {
				m_dDetectResultImg = dOutput.m_dImg;
			}
		}

		// 불량 영역을 출력 이미지 또는 OverlapMask 에 그림
		RETURN_ON_FAIL(_DrawDefect_DrawArea(m_dDetectResultImg, dDraw, true));

		RETURN_SUCCESS();
	}

	// DMD 영역을 그림
	Err ProcessCore::_DrawDefect_DrawDMD(const VIDraw &dDraw)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		const VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;
		const float nExtendSize = dDraw.m_nExtendSize;

		m_pCV->DebugPrint(L"Draw Defect, DrawDMD, ExtendSize : %.0f", nExtendSize);

		// 불량 영역에 대한 Bounding Box 이미지 생성
		Mat dDMDImg;
		RETURN_ON_FAIL(_DrawDefect_GetBoundingBoxMask(dDMDImg, nExtendSize, true));

		// 출력 이미지에 바로 그림
		Mat &dOutputImg = m_dResult.m_dOutputImage;
		RETURN_ON_FAIL(_DrawDefect_DrawOutput(dOutputImg, dDMDImg, dDraw));

		RETURN_SUCCESS();
	}

	// Defect Box 를 그림
	Err ProcessCore::_DrawDefect_DrawDefectBox(const VIDraw &dDraw)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		const VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;
		const float nExtendSize = dDraw.m_nExtendSize;

		m_pCV->DebugPrint(L"Draw Defect, DrawDefectBox, ExtendSize : %.0f", nExtendSize);

		Mat dDefectBoxImg;
		// 불량 영역에 대한 Bounding Box 이미지 생성
		RETURN_ON_FAIL(_DrawDefect_GetBoundingBoxMask(dDefectBoxImg, nExtendSize));
		// Bounding Box 를 출력 이미지 또는 OverlapMask 에 그림
		RETURN_ON_FAIL(_DrawDefect_DrawArea(dDefectBoxImg, dDraw, true));

		RETURN_SUCCESS();
	}

	// Mask/불량/Box 등의 특정 영역을 출력 이미지 또는 OverlapMask 에 그림
	Err ProcessCore::_DrawDefect_DrawArea(Mat &dAreaImg, const VIDraw &dDraw, bool bFullImg)
	{
		Mat &dOutputImg = m_dResult.m_dOutputImage;
		const VIDraw::Type &nType = dDraw.m_nType;
		const VIDraw::Mode &nMode = dDraw.m_nMode;

		// 불량이 있는 모든 Target 영역에 그리기 수행
		for (const auto &dTargetInfo : m_listTargetInfo) {
			const Rect &rcTarget = dTargetInfo.m_rcArea;
			const Rect &rcTemplatePixel = dTargetInfo.m_rcTemplatePixelArea;

			// 출력 이미지의 Target 영역 추출
			Mat dTargetOutputImg(dOutputImg, rcTarget);

			// 그려야할 영역이 표시된 이미지의 Target 영역 추출
			Mat dTargetAreaImg;
			if (bFullImg) {		// 전체 크기의 결과 이미지에 바로 그릴 경우
				Mat(dAreaImg, rcTarget).copyTo(dTargetAreaImg);
			}
			else {				// Target 영역을 추출해서 그릴 경우
				// Template 이미지에 맞게 우선 확대/축소함
				if (dAreaImg.size() != dTargetInfo.m_sizeTemplate) {
					resize(dAreaImg, dAreaImg, dTargetInfo.m_sizeTemplate);
				}
				Mat(dAreaImg, rcTemplatePixel).copyTo(dTargetAreaImg);
			}

			// OverlapMask 이미지의 Target 영역 추출
			Mat dTargetDrawingOverlapMaskImg(m_dDrawingOverlapMaskImg, rcTarget);

			// Overlap/Not Overlap 모드로 그릴 경우, OverlapMask 와의 연산을 수행하여 그려야할 영역을 갱신한다.
			switch (nMode) {
			case VIDraw::OVERLAP:
				Mat(dTargetDrawingOverlapMaskImg & dTargetAreaImg).copyTo(dTargetAreaImg);
				break;
			case VIDraw::NOT_OVERLAP:
				Mat(~dTargetDrawingOverlapMaskImg & dTargetAreaImg).copyTo(dTargetAreaImg);
				break;
			}

			// 그려야할 영역이 없을 경우 skip
			if (countNonZero(dTargetAreaImg) == 0) {
				continue;
			}

			// 출력 이미지에 그리는 모드일 경우 그리기 수행
			if (nMode != VIDraw::OVERLAP_MASK && nMode != VIDraw::NOT_OVERLAP_MASK) {
				RETURN_ON_FAIL(_DrawDefect_DrawOutput(dTargetOutputImg, dTargetAreaImg, dDraw));
			}

			if (nMode == VIDraw::NOT_OVERLAP_MASK) {
				// OverlapMask 에서 해당 영역을 제외
				Mat(dTargetDrawingOverlapMaskImg & ~dTargetAreaImg).copyTo(dTargetDrawingOverlapMaskImg);
			}
			else {
				// OverlapMask 에서 해당 영역을 추가
				Mat(dTargetDrawingOverlapMaskImg | dTargetAreaImg).copyTo(dTargetDrawingOverlapMaskImg);
			}
		}

		RETURN_SUCCESS();
	}

	// 특정 영역을 출력 이미지에 그림
	Err ProcessCore::_DrawDefect_DrawOutput(Mat &dOutputImg, const Mat &dAreaImg, const VIDraw &dDraw)
	{
		if (dOutputImg.size() != dAreaImg.size()) {
			RETURN_FAIL(DIFFERENT_IMAGE_SIZE);
		}

		const CvColor &dColor = dDraw.m_dColor;
		const float &nAlpha = dDraw.m_nAlpha;

		Mat dColorData(1, 1, CV_8UC3, dColor);

		auto itOutputImg = dOutputImg.begin<Vec3b>();
		auto itOutputImg_End = dOutputImg.end<Vec3b>();
		auto itAreaImg = dAreaImg.begin<UINT8>();
		auto itColor = dColorData.begin<Vec3b>();

		while (itOutputImg != itOutputImg_End) {
			if (*itAreaImg != 0) {
				*itOutputImg = (*itOutputImg) * (1.f - nAlpha) + (*itColor) * (nAlpha);	 // 색상 및 투명 조건 반영
			}

			itOutputImg++;
			itAreaImg++;
		}

		RETURN_SUCCESS();
	}

	// Template 이미지 변형 (확대/축소/회전)
	Err ProcessCore::_GetTransformTemplateImage(wstring strId, Mat &dTemplateImg)
	{
		// Template 이미지 원본 복사
		dTemplateImg = GetMaskImage(strId);
		if (CHECK_EMPTY_IMAGE(dTemplateImg)) {
			RETURN_FAIL(NOT_FOUND_TEMPLATE_IMAGE);
		}

		// 이미지 회전
		RETURN_ON_FAIL(_RotateImage(dTemplateImg));

		RETURN_SUCCESS();
	}

	Err ProcessCore::_GetTransformMaskImage(wstring strId, Mat &dMaskImg)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		VITemplate &dTemplate = dInspection.m_dTemplate;
		Mat &dTemplateImg = dTemplate.GetTemplateImage();

		dMaskImg = GetMaskImage(strId);
		if (CHECK_EMPTY_IMAGE(dMaskImg)) {
			RETURN_FAIL(NOT_FOUND_MASK_IMAGE);
		}

		// Color Convert
		if (dMaskImg.channels() == 3) {
			cvtColor(dMaskImg, dMaskImg, CV_BGR2GRAY);
		}

		// Rotation
		RETURN_ON_FAIL(_RotateImage(dMaskImg));

		// Resize
		if (!CHECK_EMPTY_IMAGE(dTemplateImg) && dTemplateImg.size() != dMaskImg.size()) {
			resize(dMaskImg, dMaskImg, dTemplateImg.size());
		}

		RETURN_SUCCESS();
	}

	// 이미지 회전
	Err ProcessCore::_RotateImage(Mat &dImg)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		const VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const VITemplate &dTemplate = dInspection.m_dTemplate;
		const Rotation &nRotation = dTemplate.m_nRotation;
		const bool &bVertFlip = dTemplate.m_bVerFlip;
		const bool &bHorizFlip = dTemplate.m_bHorizFlip;

		return _RotateImage(dImg, nRotation, bHorizFlip, bVertFlip);
	}

	// 이미지 회전
	Err ProcessCore::_RotateImage(Mat &dImg, Rotation nRotation, bool bHorizFlip, bool bVertFlip)
	{
		switch (nRotation) {
		case Rotation::ROTATION_90:
			transpose(dImg, dImg);
			flip(dImg, dImg, 0);
			break;
		case Rotation::ROTATION_180:
			flip(dImg, dImg, 0);
			flip(dImg, dImg, 1);
			break;
		case Rotation::ROTATION_270:
			transpose(dImg, dImg);
			flip(dImg, dImg, 1);
			break;
		}

		if (bHorizFlip) {
			flip(dImg, dImg, 1);
		}
		if (bVertFlip) {
			flip(dImg, dImg, 0);
		}

		RETURN_SUCCESS();
	}

	// 이미지 역회전
	Err ProcessCore::_ReverseRotateImage(Mat &dImg)
	{
		const Layer &nLayer = m_dParam.m_nLayer;
		const VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		const VITemplate &dTemplate = dInspection.m_dTemplate;
		const Rotation &nRotation = dTemplate.m_nRotation;
		const bool &bVertFlip = dTemplate.m_bVerFlip;
		const bool &bHorizFlip = dTemplate.m_bHorizFlip;

		return _ReverseRotateImage(dImg, nRotation, bHorizFlip, bVertFlip);
	}

	// 이미지 역회전
	Err ProcessCore::_ReverseRotateImage(Mat &dImg, Rotation nRotation, bool bHorizFlip, bool bVertFlip)
	{
		if (bVertFlip) {
			flip(dImg, dImg, 0);
		}
		if (bHorizFlip) {
			flip(dImg, dImg, 1);
		}

		switch (nRotation) {
		case Rotation::ROTATION_90:
			transpose(dImg, dImg);
			flip(dImg, dImg, 1);
			break;
		case Rotation::ROTATION_180:
			flip(dImg, dImg, 0);
			flip(dImg, dImg, 1);
			break;
		case Rotation::ROTATION_270:
			transpose(dImg, dImg);
			flip(dImg, dImg, 0);
			break;
		}

		RETURN_SUCCESS();
	}

	// 로드된 Mask/Template 이미지들의 Id 리스트를 반환
	vector<wstring> ProcessCore::GetMaskIdList()
	{
		const list<VIMask> &listMask = m_dVIData.m_dLayer.m_listMask;

		vector<wstring> listMaskId;
		for (const VIMask &dMask : listMask) {
			listMaskId.push_back(dMask.m_strId);
		}

		return listMaskId;
	}

	// 특정 Id 의 Mask/Template 이미지를 반환
	Mat ProcessCore::GetMaskImage(wstring strId)
	{
		const list<VIMask> &listMask = m_dVIData.m_dLayer.m_listMask;

		Mat dImg;
		auto it = find_if(listMask.begin(), listMask.end(), [&](const VIMask &dMask) {
			if (CHECK_STRING(strId, dMask.m_strId)) {
				return true;
			}

			return false;
		});

		if (it != listMask.end()) {
			const VIMask &dMask = *it;
			dMask.m_dImg.copyTo(dImg);
		}

		return dImg;
	}

	// 이미지의 배경 밝기를 평탄화시킨 이미지를 반환함.
	Mat ProcessCore::AdjustImageBackground(const Mat dImg, Layer nLayer, bool bGetFitBackground)
	{
		CLEAR_TIME_PROFILE();
		SET_TIME_PROFILE_START(Total);

		m_dParam.m_dInputImage = dImg;
		m_dParam.m_nLayer = nLayer;

		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		VITemplate &dTemplate = dInspection.m_dTemplate;

		_Preprocess();
		SET_TIME_PROFILE_START(FindTarget);
		_FindTarget();
		SET_TIME_PROFILE_ELAPSED(FindTarget);
		SET_TIME_PROFILE_START(DetectDefect_MakeDiffImage);
		_DetectDefect_MakeDiffImage(true, bGetFitBackground);
		SET_TIME_PROFILE_ELAPSED(DetectDefect_MakeDiffImage);

		SET_TIME_PROFILE_ELAPSED(Total);
		DEBUG_PRINT_TIME_PROFILE();

		if (!CHECK_EMPTY_IMAGE(m_dDebugImg)) {
			return m_dDebugImg;
		}

		return m_dAdjustedBackgroundImg;
	}

	// 특정 Template 이미지와 유사한 Target 영역들을 검색함
	Mat ProcessCore::FindTemplateTarget(const Mat dImg, Layer nLayer, Rect &rcBestFitArea, vector<Rect> &listTargetArea)
	{
		CLEAR_TIME_PROFILE();
		SET_TIME_PROFILE_START(Total);

		m_dParam.m_dInputImage = dImg;
		m_dParam.m_nLayer = nLayer;

		VIInspection &dInspection = m_dVIData.m_mapInspection[nLayer];
		VITemplate &dTemplate = dInspection.m_dTemplate;

		_Preprocess();

		vector<TargetInfo> listTargetInfo;
		SET_TIME_PROFILE_START(FindTarget);
		_FindTarget(dTemplate.m_bReplace, dTemplate.m_nMinSizeRatio, listTargetInfo);
		SET_TIME_PROFILE_ELAPSED(FindTarget);

		rcBestFitArea = m_rcBestFitTarget;

		for (const TargetInfo &dTargetInfo : listTargetInfo) {
			listTargetArea.push_back(dTargetInfo.m_rcArea);
		}

		SET_TIME_PROFILE_ELAPSED(Total);
		DEBUG_PRINT_TIME_PROFILE();

		m_pCV->DebugPrint(L"==== [Find Target] BestFitArea : Pos(%d, %d) Size(%d, %d), Target Count : %d ====",
			rcBestFitArea.x, rcBestFitArea.y, rcBestFitArea.width, rcBestFitArea.height, listTargetInfo.size());

		if (!CHECK_EMPTY_IMAGE(m_dDebugImg)) {
			return m_dDebugImg;
		}

		return dTemplate.GetTemplateImage();
	}

	// 이미지를 회전/반전 시킴
	Mat ProcessCore::RotateImage(const Mat dImage, Rotation nRotation, bool bHorizFlip, bool bVertFlip)
	{
		Mat dImg = dImage.clone();
		_RotateImage(dImg, nRotation, bHorizFlip, bVertFlip);

		return dImg;
	}

	// 회전/반전된 이미지를 복원시킴
	Mat ProcessCore::ReverseRotateImage(const Mat dImage, Rotation nRotation, bool bHorizFlip, bool bVertFlip)
	{
		Mat dImg = dImage.clone();
		_ReverseRotateImage(dImg, nRotation, bHorizFlip, bVertFlip);

		return dImg;

	}
}
