// Process.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NIPL_AutoRepair_Core.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
// VI Data 편집 관련 매크로
//
#define VI_DATA_FIND(data) \
	auto it = find_if(m_list##data.begin(), m_list##data.end(), [&d##data](const VI##data &d##data##2) { \
		if (&d##data## == &d##data##2) return true; \
		return false; \
	});

#define VI_DATA_SORT(data, var) \
	m_list####data##.sort([](const VI##data## &left, const VI##data## &right) { \
		return wstricmp(left.##var##, right.##var##) < 0; \
	});

#define VI_DATA_ADD(data) \
	m_list##data##.push_back(VI##data##()); \
	auto it = m_list##data##.rbegin(); \
	return *it; \

#define VI_DATA_DELETE(data) \
	VI_DATA_FIND(data); \
	if (it != m_list##data##.end()) { \
		m_list##data##.erase(it); \
	}

#define VI_DATA_ORDER_UP(data) \
	VI_DATA_FIND(data); \
	if (it == m_list##data##.end()) return; \
	if (it == m_list##data##.begin()) return; \
	auto it2 = it--; \
	swap(*it, *it2); 

#define VI_DATA_ORDER_DOWN(data) \
	VI_DATA_FIND(data); \
	if (it == m_list##data##.end()) return; \
	if (it == --m_list##data##.end()) return; \
	auto it2 = it++; \
	swap(*it, *it2); 

namespace NIPL_AutoRepair {
	// 
	// Error Messages
	//
	wstring GetErrMessage(Err nErr) {
		wstring strErr = L"Undefined";
		switch (nErr) {
		case Err::SUCCESS: strErr = L"SUCCESS"; break;
		case Err::FAIL: strErr = L"FAIL"; break;
		case Err::DUPLICATED_ID: strErr = L"DUPLICATED_ID"; break;
		case Err::DIFFERENT_IMAGE_SIZE: strErr = L"DIFFERENT_IMAGE_SIZE"; break;

		case Err::FAIL_TO_LOAD_IMAGE_FILE: strErr = L"FAIL_TO_LOAD_IMAGE_FILE"; break;
		case Err::FAIL_TO_LOAD_DATA_FILE: strErr = L"FAIL_TO_LOAD_DATA_FILE"; break;
		case Err::FAIL_TO_LOAD_MASK_FILE: strErr = L"FAIL_TO_LOAD_MASK_FILE"; break;
		case Err::FAIL_TO_SAVE_IMAGE_FILE: strErr = L"FAIL_TO_SAVE_IMAGE_FILE"; break;
		case Err::FAIL_TO_DETECT: strErr = L"FAIL_TO_DETECT"; break;
		case Err::FAIL_TO_CLASSIFY: strErr = L"FAIL_TO_CLASSIFY"; break;
		case Err::FAIL_TO_DRAW: strErr = L"FAIL_TO_DRAW"; break;

		case Err::INVALID_DATA_FILE: strErr = L"INVALID_DATA_FILE"; break;
		case Err::INVALID_DATA_VALUE: strErr = L"INVALID_DATA_VALUE"; break;

		case Err::NOT_FOUND_LAYER_DATA: strErr = L"NOT_FOUND_LAYER_DATA"; break;
		case Err::NOT_FOUND_TEMPLATE_IMAGE: strErr = L"NOT_FOUND_TEMPLATE_IMAGE"; break;
		case Err::NOT_FOUND_MASK_IMAGE: strErr = L"NOT_FOUND_MASK_IMAGE"; break;
		case Err::NOT_FOUND_DRAWING_MASK_IMAGE: strErr = L"NOT_FOUND_DRAWING_MASK_IMAGE"; break;
		case Err::NOT_FOUND_TARGET: strErr = L"NOT_FOUND_TARGET"; break;
		case Err::NOT_FOUND_DEFECT: strErr = L"NOT_FOUND_DEFECT"; break;
		}

		return strErr;
	}

	//
	// VIMask
	//
	void VIMask::SetType(wstring strType)
	{
		if (CHECK_STRING(strType, _T(STR_XML_ELEMENT_DRAWING_MASK))) m_nType = DRAWING_MASK;
		else if (CHECK_STRING(strType, _T(STR_XML_ELEMENT_TEMPLATE))) m_nType = TEMPLATE;
		else if (CHECK_STRING(strType, _T(STR_XML_ELEMENT_TEMPLATE_PIXEL))) m_nType = TEMPLATE_PIXEL;
		else m_nType = MASK;
	}

	wstring VIMask::GetType(bool bEmptyByDefault) const
	{
		wstring strType;

		switch (m_nType) {
		case DRAWING_MASK: strType = _T(STR_XML_ELEMENT_DRAWING_MASK); break;
		case TEMPLATE: strType = _T(STR_XML_ELEMENT_TEMPLATE); break;
		case TEMPLATE_PIXEL: strType = _T(STR_XML_ELEMENT_TEMPLATE_PIXEL); break;
		default: if (!bEmptyByDefault) strType = _T(STR_XML_ELEMENT_MASK); break;
		}

		return strType;
	}

	vector<wstring> VIMask::GetTypeList()
	{
		vector<wstring> list;
		list.push_back(_T(STR_XML_ELEMENT_MASK));
		list.push_back(_T(STR_XML_ELEMENT_DRAWING_MASK));
		list.push_back(_T(STR_XML_ELEMENT_TEMPLATE));
		list.push_back(_T(STR_XML_ELEMENT_TEMPLATE_PIXEL));

		return list;
	}

	//
	// VITemplate
	//
	void VITemplate::SetRotation(wstring strRotation)
	{
		if (CHECK_STRING(strRotation, STR_XML_VALUE_ROTATION_90)) m_nRotation = Rotation::ROTATION_90;
		else if (CHECK_STRING(strRotation, STR_XML_VALUE_ROTATION_180)) m_nRotation = Rotation::ROTATION_180;
		else if (CHECK_STRING(strRotation, STR_XML_VALUE_ROTATION_270)) m_nRotation = Rotation::ROTATION_270;
		else m_nRotation = Rotation::ROTATION_0;
	}

	wstring VITemplate::GetRotation(bool bEmptyByDefault) const
	{
		wstring strRotation;

		switch (m_nRotation) {
		case Rotation::ROTATION_90: strRotation = STR_XML_VALUE_ROTATION_90; break;
		case Rotation::ROTATION_180: strRotation = STR_XML_VALUE_ROTATION_180; break;
		case Rotation::ROTATION_270: strRotation = STR_XML_VALUE_ROTATION_270; break;
		default: if (!bEmptyByDefault) strRotation = STR_XML_VALUE_ROTATION_0; break;
		}

		return strRotation;
	}

	vector<wstring> VITemplate::GetRotationList()
	{
		vector<wstring> list;
		list.push_back(STR_XML_VALUE_ROTATION_0);
		list.push_back(STR_XML_VALUE_ROTATION_90);
		list.push_back(STR_XML_VALUE_ROTATION_180);
		list.push_back(STR_XML_VALUE_ROTATION_270);

		return list;
	}

	void VITemplate::SortTemplatePixelList()
	{
		VI_DATA_SORT(TemplatePixel, m_strId);
	}

	VITemplatePixel &VITemplate::AddTemplatePixel()
	{
		VI_DATA_ADD(TemplatePixel);
	}

	void VITemplate::DeleteTemplatePixel(VITemplatePixel &dTemplatePixel)
	{
		VI_DATA_DELETE(TemplatePixel);
	}

	// Template 이미지 확대/축소
	void VITemplate::ResizeTemplateImage(Size size, bool bAlsoDiffImg)
	{
		if (m_dImg.size() != size) {
			resize(m_dImg, m_dImg, size);
		}

		if (bAlsoDiffImg) {
			if (m_dDiffImg.size() != size) {
				resize(m_dDiffImg, m_dDiffImg, size);
			}
		}
	}

	// Template 이미지 반환
	Mat &VITemplate::GetTemplateImage()
	{
		return m_dImg;
	}

	// Template 이미지에서 Template Pixel 영역에 해당하는 부분 추출
	Mat &VITemplate::GetTemplatePixelImage(Rect rcTemplatePixel)
	{
		m_dPixelImg = Mat(m_dImg, rcTemplatePixel);
		return m_dPixelImg;
	}

	// Template 이미지 자체 차분 이미지 생성
	void VITemplate::MakeTemplateDiffImage()
	{
		// Set TemplateDiff Image
		NIPLInput dInput;
		NIPLOutput dOutput;

		NIPLParam_Diff dParam_Diff;
		dParam_Diff.m_bSubtract = true;

		dInput.m_dImg = m_dImg;
		dInput.m_pParam = &dParam_Diff;

		shared_ptr<NIPLCV> pCV = NIPLCV::GetInstance();
		NIPL_ERR nErr = pCV->Diff(&dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			m_dDiffImg = dOutput.m_dImg;
		}
	}

	// 차분 이미지에서 Template Pixel 영역에 해당하는 부분 추출
	Mat &VITemplate::GetTemplatePixelDiffImage(Rect rcTemplatePixel)
	{
		m_dPixelDiffImg = Mat(m_dDiffImg, rcTemplatePixel);
		return m_dPixelDiffImg;
	}

	// 특정 Index 에 해당하는 Template Pixel 의 위치 반환
	Rect VITemplate::GetTemplatePixelArea(int nIndex)
	{
		int nCount = (int)m_listTemplatePixel.size();
		if (nIndex < 0 || nIndex >= nCount || m_bMultiPixel == false) {
			return Rect(0, 0, m_dImg.cols, m_dImg.rows);
		}

		auto it = find_if(m_listTemplatePixel.begin(), m_listTemplatePixel.end(), [&nIndex](const VITemplatePixel &dTemplatePixel) {
			if (dTemplatePixel.m_nIndex == nIndex) {
				return true;
			}
			return false;
		});

		if (it == m_listTemplatePixel.end()) {
			return Rect(0, 0, m_dImg.cols, m_dImg.rows);
		}

		VITemplatePixel &dTemplatePixel = *it;

		int nImgSizeX = m_dImg.cols;
		int nImgSizeY = m_dImg.rows;

		Rect rcTemplatePixel;
		rcTemplatePixel.x = dTemplatePixel.m_rcPixelRatio.x * nImgSizeX;
		rcTemplatePixel.y = dTemplatePixel.m_rcPixelRatio.y * nImgSizeY;
		rcTemplatePixel.width = dTemplatePixel.m_rcPixelRatio.width * nImgSizeX;
		rcTemplatePixel.height = dTemplatePixel.m_rcPixelRatio.height * nImgSizeY;

		return rcTemplatePixel;
	}

	// 현재 Template Pixel 의 이전 Template Pixel 에 대한 위치 반환
	Rect VITemplate::GetPrevTemplatePixelArea(int nIndex, bool bVert)
	{
		if (!bVert) {
			nIndex--;
			int nCount = (int)m_listTemplatePixel.size();
			if (nIndex < 0) nIndex = nCount - 1;
		}

		return GetTemplatePixelArea(nIndex);
	}

	// 현재 Template Pixel 의 다음 Template Pixel 에 대한 위치 반환
	Rect VITemplate::GetNextTemplatePixelArea(int nIndex, bool bVert)
	{
		if (!bVert) {
			nIndex++;
			int nCount = (int)m_listTemplatePixel.size();
			if (nIndex >= nCount) nIndex = 0;
		}

		return GetTemplatePixelArea(nIndex);
	}
	
	//
	// VICheck
	//
	void VICheck::SetStatus(wstring strStatus)
	{
		if (CHECK_STRING(strStatus, STR_XML_VALUE_CHECK_STATUS_OFF)) m_nStatus = OFF;
		else if (CHECK_STRING(strStatus, STR_XML_VALUE_CHECK_STATUS_CONNECTED)) m_nStatus = CONNECTED;
		else if (CHECK_STRING(strStatus, STR_XML_VALUE_CHECK_STATUS_CONNECTED_NEIGHBOR)) m_nStatus = CONNECTED_NEIGHBOR;
		else m_nStatus = ON;
	}

	wstring VICheck::GetStatus(bool bEmptyByDefault) const
	{
		wstring strStatus;

		switch (m_nStatus) {
		case OFF: strStatus = STR_XML_VALUE_CHECK_STATUS_OFF; break;
		case CONNECTED: strStatus = STR_XML_VALUE_CHECK_STATUS_CONNECTED; break;
		case CONNECTED_NEIGHBOR: strStatus = STR_XML_VALUE_CHECK_STATUS_CONNECTED_NEIGHBOR; break;
		default: if (!bEmptyByDefault) strStatus = STR_XML_VALUE_CHECK_STATUS_ON; break;
		}

		return strStatus;
	}

	vector<wstring> VICheck::GetStatusList()
	{
		vector<wstring> list;
		list.push_back(STR_XML_VALUE_CHECK_STATUS_ON);
		list.push_back(STR_XML_VALUE_CHECK_STATUS_OFF);
		list.push_back(STR_XML_VALUE_CHECK_STATUS_CONNECTED);
		list.push_back(STR_XML_VALUE_CHECK_STATUS_CONNECTED_NEIGHBOR);

		return list;
	}

	//
	// VIDraw
	//
	void VIDraw::SetType(wstring strType)
	{
		if (CHECK_STRING(strType, STR_XML_VALUE_DRAW_TYPE_MASK)) m_nType = MASK;
		else if (CHECK_STRING(strType, STR_XML_VALUE_DRAW_TYPE_DEFECT)) m_nType = DEFECT;
		else if (CHECK_STRING(strType, STR_XML_VALUE_DRAW_TYPE_DMD)) m_nType = DMD;
		else if (CHECK_STRING(strType, STR_XML_VALUE_DRAW_TYPE_DEFECT_BOX)) m_nType = DEFECT_BOX;
		else m_nType = DRAW_SET;
	}

	wstring VIDraw::GetType(bool bEmptyByDefault) const
	{
		wstring strType;

		switch (m_nType) {
		case MASK: strType = STR_XML_VALUE_DRAW_TYPE_MASK; break;
		case DEFECT: strType = STR_XML_VALUE_DRAW_TYPE_DEFECT; break;
		case DMD: strType = STR_XML_VALUE_DRAW_TYPE_DMD; break;
		case DEFECT_BOX: strType = STR_XML_VALUE_DRAW_TYPE_DEFECT_BOX; break;
		default: if (!bEmptyByDefault) strType = STR_XML_VALUE_DRAW_TYPE_DRAW_SET; break;
		}

		return strType;
	}

	vector<wstring> VIDraw::GetTypeList()
	{
		vector<wstring> list;
		list.push_back(STR_XML_VALUE_DRAW_TYPE_DRAW_SET);
		list.push_back(STR_XML_VALUE_DRAW_TYPE_MASK);
		list.push_back(STR_XML_VALUE_DRAW_TYPE_DEFECT);
		list.push_back(STR_XML_VALUE_DRAW_TYPE_DMD);
		list.push_back(STR_XML_VALUE_DRAW_TYPE_DEFECT_BOX);

		return list;
	}

	void VIDraw::SetMode(wstring strMode)
	{
		if (CHECK_STRING(strMode, STR_XML_VALUE_DRAW_MODE_OVERLAP)) m_nMode = OVERLAP;
		else if (CHECK_STRING(strMode, STR_XML_VALUE_DRAW_MODE_NOT_OVERLAP)) m_nMode = NOT_OVERLAP;
		else if (CHECK_STRING(strMode, STR_XML_VALUE_DRAW_MODE_OVERLAP_MASK)) m_nMode = OVERLAP_MASK;
		else if (CHECK_STRING(strMode, STR_XML_VALUE_DRAW_MODE_NOT_OVERLAP_MASK)) m_nMode = NOT_OVERLAP_MASK;
		else m_nMode = NORMAL;
	}

	wstring VIDraw::GetMode(bool bEmptyByDefault) const
	{
		wstring strMode;

		switch (m_nMode) {
		case OVERLAP: strMode = STR_XML_VALUE_DRAW_MODE_OVERLAP; break;
		case NOT_OVERLAP: strMode = STR_XML_VALUE_DRAW_MODE_NOT_OVERLAP; break;
		case OVERLAP_MASK: strMode = STR_XML_VALUE_DRAW_MODE_OVERLAP_MASK; break;
		case NOT_OVERLAP_MASK: strMode = STR_XML_VALUE_DRAW_MODE_NOT_OVERLAP_MASK; break;
		default: if (!bEmptyByDefault) strMode = STR_XML_VALUE_DRAW_MODE_NORMAL; break;
		}

		return strMode;
	}

	vector<wstring> VIDraw::GetModeList()
	{
		vector<wstring> list;
		list.push_back(STR_XML_VALUE_DRAW_MODE_NORMAL);
		list.push_back(STR_XML_VALUE_DRAW_MODE_OVERLAP);
		list.push_back(STR_XML_VALUE_DRAW_MODE_NOT_OVERLAP);
		list.push_back(STR_XML_VALUE_DRAW_MODE_OVERLAP_MASK);
		list.push_back(STR_XML_VALUE_DRAW_MODE_NOT_OVERLAP_MASK);

		return list;
	}

	//
	// VIDrawSet
	//
	VIDraw &VIDrawSet::AddDraw()
	{
		VI_DATA_ADD(Draw);
	}

	void VIDrawSet::DeleteDraw(VIDraw &dDraw)
	{
		VI_DATA_DELETE(Draw);
	}

	void VIDrawSet::OrderUpDraw(VIDraw &dDraw)
	{
		VI_DATA_ORDER_UP(Draw);
	}

	void VIDrawSet::OrderDownDraw(VIDraw &dDraw)
	{
		VI_DATA_ORDER_DOWN(Draw);
	}

	//
	// VIPixel
	//
	void VIPixel::SortCheckList()
	{
		VI_DATA_SORT(Check, m_strMaskId);
	}

	VICheck &VIPixel::AddCheck()
	{
		VI_DATA_ADD(Check);
	}

	void VIPixel::DeleteCheck(VICheck &dCheck)
	{
		VI_DATA_DELETE(Check);
	}

	//
	// VIDefect
	//
	void VIDefect::SetConnected(wstring strConnected)
	{
		if (CHECK_STRING(strConnected, STR_XML_VALUE_CONNECTED_HORIZ)) m_nConnected = HORIZ;
		else if (CHECK_STRING(strConnected, STR_XML_VALUE_CONNECTED_VERT)) m_nConnected = VERT;
		else m_nConnected = DONT_CARE;
	}

	wstring VIDefect::GetConnected(bool bEmptyByDefault) const
	{
		wstring strConnected;

		switch (m_nConnected) {
		case HORIZ: strConnected = STR_XML_VALUE_CONNECTED_HORIZ; break;
		case VERT: strConnected = STR_XML_VALUE_CONNECTED_VERT; break;
		default: if (!bEmptyByDefault) strConnected = STR_XML_VALUE_CONNECTED_DONT_CARE; break;
		}

		return strConnected;
	}

	vector<wstring> VIDefect::GetConnectedList()
	{
		vector<wstring> list;
		list.push_back(STR_XML_VALUE_CONNECTED_DONT_CARE);
		list.push_back(STR_XML_VALUE_CONNECTED_HORIZ);
		list.push_back(STR_XML_VALUE_CONNECTED_VERT);

		return list;
	}

	VIPixel &VIDefect::AddPixel()
	{
		VI_DATA_ADD(Pixel);
	}

	void VIDefect::DeletePixel(VIPixel &dPixel)
	{
		VI_DATA_DELETE(Pixel);
	}

	void VIDefect::OrderUpPixel(VIPixel &dPixel)
	{
		VI_DATA_ORDER_UP(Pixel);
	}

	void VIDefect::OrderDownPixel(VIPixel &dPixel)
	{
		VI_DATA_ORDER_DOWN(Pixel);
	}

	VIDraw &VIDefect::AddDraw()
	{
		VI_DATA_ADD(Draw);
	}

	void VIDefect::DeleteDraw(VIDraw &dDraw)
	{
		VI_DATA_DELETE(Draw);
	}

	void VIDefect::OrderUpDraw(VIDraw &dDraw)
	{
		VI_DATA_ORDER_UP(Draw);
	}

	void VIDefect::OrderDownDraw(VIDraw &dDraw)
	{
		VI_DATA_ORDER_DOWN(Draw);
	}

	//
	// VIBinarize
	//
	void VIBinarize::OrderUpApply(VIApply &dApply)
	{
		VI_DATA_ORDER_UP(Apply);
	}

	void VIBinarize::OrderDownApply(VIApply &dApply)
	{
		VI_DATA_ORDER_DOWN(Apply);
	}

	VIApply &VIBinarize::AddApply()
	{
		VI_DATA_ADD(Apply);
	}

	void VIBinarize::DeleteApply(VIApply &dApply)
	{
		VI_DATA_DELETE(Apply);
	}

	//
	// VILayer
	//
	void VILayer::SortMaskList()
	{
		m_listMask.sort([](const VIMask &left, const VIMask &right) {
			int nRet = left.m_nType - right.m_nType;
			if (nRet == 0) nRet = wstricmp(left.m_strId, right.m_strId);
			return nRet < 0;
		});
	}

	VIMask &VILayer::AddMask()
	{
		VI_DATA_ADD(Mask);
	}

	void VILayer::DeleteMask(VIMask &dMask)
	{
		VI_DATA_DELETE(Mask);
	}

	vector<wstring> VILayer::GetMaskIdList(VIMask::Type nType, bool bIncludeEmptyId)
	{
		vector<wstring> list;

		if (bIncludeEmptyId) {
			list.push_back(L"");
		}

		for (const VIMask &dMask : m_listMask) {
			if (nType == VIMask::Type::ALL || nType == dMask.m_nType) {
				list.push_back(dMask.m_strId);
			}
		}
		return list;
	}

	//
	// VIDrawing
	//
	void VIDrawing::SortDrawSetList()
	{
		VI_DATA_SORT(DrawSet, m_strId);
	}

	VIDrawSet &VIDrawing::AddDrawSet()
	{
		VI_DATA_ADD(DrawSet);
	}

	void VIDrawing::DeleteDrawSet(VIDrawSet &dDrawSet)
	{
		VI_DATA_DELETE(DrawSet);
	}

	vector<wstring> VIDrawing::GetDrawSetIdList(bool bIncludeEmptyId)
	{
		vector<wstring> list;

		if (bIncludeEmptyId) {
			list.push_back(L"");
		}

		for (const VIDrawSet &dDrawSet : m_listDrawSet) {
			list.push_back(dDrawSet.m_strId);
		}

		return list;
	}

	//
	// VIInspection
	//
	void VIInspection::SetLayer(wstring strLayer)
	{
		if (CHECK_STRING(strLayer, _T(STR_XML_ELEMENT_GATE))) m_nLayer = Layer::GATE;
		else if (CHECK_STRING(strLayer, _T(STR_XML_ELEMENT_SD))) m_nLayer = Layer::SD;
		else m_nLayer = Layer::ACTIVE;
	}

	wstring VIInspection::GetLayer(bool bEmptyByDefault) const
	{
		wstring strLayer;

		switch (m_nLayer) {
		case Layer::GATE: strLayer = _T(STR_XML_ELEMENT_GATE); break;
		case Layer::SD: strLayer = _T(STR_XML_ELEMENT_SD); break;
		default: if (!bEmptyByDefault) strLayer = _T(STR_XML_ELEMENT_ACTIVE); break;
		}

		return strLayer;
	}

	VIDefect &VIInspection::AddDefect()
	{
		VI_DATA_ADD(Defect);
	}

	void VIInspection::DeleteDefect(VIDefect &dDefect)
	{
		VI_DATA_DELETE(Defect);
	}

	void VIInspection::OrderUpDefect(VIDefect &dDefect)
	{
		VI_DATA_ORDER_UP(Defect);
	}

	void VIInspection::OrderDownDefect(VIDefect &dDefect)
	{
		VI_DATA_ORDER_DOWN(Defect);
	}

	vector<wstring> VIInspection::GetDefectIdList(bool bIncludeEmptyId)
	{
		vector<wstring> list;

		if (bIncludeEmptyId) {
			list.push_back(L"");
		}

		for (const VIDefect &dDefect : m_listDefect) {
			list.push_back(dDefect.m_strId);
		}
		return list;
	}

	//
	// Load/Save Data in ProcessCore
	//
	Err ProcessCore::LoadData(const wstring strDataPath)
	{
		RETURN_ON_FAIL(_LoadData(strDataPath));
		RETURN_ON_FAIL(_LoadMask());
		RETURN_SUCCESS();
	}

	Err ProcessCore::_LoadData(const wstring strDataPath)
	{
		m_dVIData.Clear();

		m_pCV->DebugPrint(L"==== LoadData, Start ==== ");
		m_pCV->DebugPrint(L"Data File : %s", strDataPath.c_str());

		XML dXML;
		auto nResult = dXML.Load(strDataPath.c_str());
		if (nResult != XML_PARSE::OK) {
			m_pCV->DebugPrint(L"Fail to Load Data Settings, XML Err : %d", nResult);
			RETURN_FAIL(FAIL_TO_LOAD_DATA_FILE);
		}

		auto dRoot = dXML.GetRootElement();
		wstring strVersion = str2wstr(dRoot.v(STR_XML_KEY_VERSION));
		wstring strId = str2wstr(dRoot.v(STR_XML_KEY_ID));
		wstring strElementName = str2wstr(dRoot.GetElementName());

		if (!CHECK_STRING(strElementName, _T(STR_XML_ELEMENT_VIDATA)) || !CHECK_STRING(strId, _T(STR_XML_ID_AUTO_REPAIR))) {
			m_pCV->DebugPrint(L"Fail to Load Data Settings, Invalid Data File for AutoRepair");
			RETURN_FAIL(INVALID_DATA_FILE);
		}

		if (!CHECK_STRING(strVersion, _T(STR_XML_VERSION_AUTO_REPAIR))) {
			m_pCV->DebugPrint(L"Warning!!! Invalid Data Version for AutoRepair. Current Version : %s, Data File Version : %s",
				_T(STR_XML_VERSION_AUTO_REPAIR), strVersion.c_str());
		}

		auto listXMLChild = dRoot.GetChildren();
		for (auto pXMLChild : listXMLChild) {
			strElementName = str2wstr(pXMLChild->GetElementName());
			if (CHECK_STRING(strElementName, _T(STR_XML_ELEMENT_LAYER))) {
				VILayer &dLayer = m_dVIData.m_dLayer;
				XML_TO_DATA_STRING(dLayer.m_strMaskRootPath, STR_XML_KEY_ROOT_PATH);
				m_pCV->DebugPrint(L"[Layer] Root Path : %s", dLayer.m_strMaskRootPath.c_str());

				RETURN_ON_FAIL(_LoadData_Layer(pXMLChild));
			}
			else if (CHECK_STRING(strElementName, _T(STR_XML_ELEMENT_DRAWING))) {
				m_pCV->DebugPrint(L"[Drawing]");

				RETURN_ON_FAIL(_LoadData_Drawing(pXMLChild));
			}
			else if (CHECK_STRING(strElementName, _T(STR_XML_ELEMENT_INSPECTION))) {
				m_pCV->DebugPrint(L"[Inspection]");

				RETURN_ON_FAIL(_LoadData_Inspection(pXMLChild));
			}
		}

		m_pCV->DebugPrint(L"==== LoadData, End ====");

		RETURN_SUCCESS();
	}

	Err ProcessCore::_LoadData_Layer(const shared_ptr<XMLElement> &pXML)
	{
		list<VIMask> &listMask = m_dVIData.m_dLayer.m_listMask;

		auto listXMLChild = pXML->GetChildren();
		for (auto pXMLChild : listXMLChild) {
			wstring strChildElementName = str2wstr(pXMLChild->GetElementName());

			VIMask dMask;
			dMask.SetType(strChildElementName);
			dMask.m_strId = str2wstr(pXMLChild->v(STR_XML_KEY_ID));
			dMask.m_strPath = str2wstr(pXMLChild->v(STR_XML_KEY_PATH));

			m_pCV->DebugPrint(L"  [%s] id : %s, path  : %s",
				dMask.GetType().c_str(), dMask.m_strId.c_str(), dMask.m_strPath.c_str());

			auto it = find_if(listMask.begin(), listMask.end(), [&dMask](const VIMask &dMask2) {
				if (CHECK_STRING(dMask.m_strId, dMask2.m_strId)) {
					return true;
				}
				return false;
			});

			if (it != listMask.end()) {
				m_pCV->DebugPrint(L"  ! Err. Duplicated id");
				RETURN_FAIL(DUPLICATED_ID);
			}

			listMask.push_back(dMask);
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_LoadData_Drawing(const shared_ptr<XMLElement> &pXML)
	{
		list<VIDrawSet> &listDrawSet = m_dVIData.m_dDrawing.m_listDrawSet;

		auto listXMLChild = pXML->GetChildren();
		for (auto pXMLChild : listXMLChild) {
			VIDrawSet dDrawSet;

			XML_TO_DATA_STRING(dDrawSet.m_strId, STR_XML_KEY_ID);

			m_pCV->DebugPrint(L"  [DrawSet] %s", dDrawSet.m_strId.c_str());

			auto it = find_if(listDrawSet.begin(), listDrawSet.end(), [&dDrawSet](const VIDrawSet &listDrawSet2) {
				if (CHECK_STRING(dDrawSet.m_strId, listDrawSet2.m_strId)) {
					return true;
				}
				return false;
			});

			if (it != listDrawSet.end()) {
				m_pCV->DebugPrint(L"  ! Err. Duplicated id");
				RETURN_FAIL(DUPLICATED_ID);
			}

			RETURN_ON_FAIL(_LoadData_Drawing_DrawSet(pXMLChild, dDrawSet));

			listDrawSet.push_back(dDrawSet);
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_LoadData_Drawing_DrawSet(const shared_ptr<XMLElement> &pXML, VIDrawSet &dDrawSet)
	{
		auto listXMLChild = pXML->GetChildren();
		for (auto pXMLChild : listXMLChild) {
			wstring strChildElementName = str2wstr(pXMLChild->GetElementName());
			if (CHECK_STRING(strChildElementName, _T(STR_XML_ELEMENT_DRAW))) {
				VIDraw dDraw;

				XML_TO_DATA_METHOD(dDraw.SetType, STR_XML_KEY_TYPE);
				XML_TO_DATA_STRING(dDraw.m_strMaskId, STR_XML_KEY_MASK_ID);
				XML_TO_DATA_COLOR(dDraw.m_dColor, STR_XML_KEY_COLOR);
				XML_TO_DATA_FLOAT(dDraw.m_nAlpha, STR_XML_KEY_ALPHA);
				XML_TO_DATA_INT(dDraw.m_nExtendSize, STR_XML_KEY_EXTEND_SIZE);
				XML_TO_DATA_INT(dDraw.m_nFillSize, STR_XML_KEY_FILL_SIZE);
				XML_TO_DATA_METHOD(dDraw.SetMode, STR_XML_KEY_MODE);

				m_pCV->DebugPrint(L"    [Draw] Type: %s, MaskId: %s, Color: (%.0f,%.0f,%.0f), Alpah: %.2f, ExtendSize : %d, FillSize : %d, Mode : %s",
					dDraw.GetType().c_str(), dDraw.m_strMaskId.c_str(),
					dDraw.m_dColor[2], dDraw.m_dColor[1], dDraw.m_dColor[0], dDraw.m_nAlpha, dDraw.m_nExtendSize, dDraw.m_nFillSize, dDraw.GetMode().c_str());

				dDrawSet.m_listDraw.push_back(dDraw);
			}
			else {
				continue;
			}
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_LoadData_Inspection(const shared_ptr<XMLElement> &pXML)
	{
		auto listXMLChild = pXML->GetChildren();
		for (auto pXMLChild : listXMLChild) {
			wstring strChildElementName = str2wstr(pXMLChild->GetElementName());

			VIInspection dInspection;
			dInspection.SetLayer(strChildElementName);

			m_pCV->DebugPrint(L"  [%s]", dInspection.GetLayer().c_str());

			RETURN_ON_FAIL(_LoadData_Inspection_Layer(pXMLChild, dInspection));

			m_dVIData.m_mapInspection[dInspection.m_nLayer] = dInspection;
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_LoadData_Inspection_Layer(const shared_ptr<XMLElement> &pXML, VIInspection &dInspection)
	{
		auto listXMLChild = pXML->GetChildren();
		for (auto pXMLChild : listXMLChild) {
			wstring strChildElementName = str2wstr(pXMLChild->GetElementName());
			if (CHECK_STRING(strChildElementName, _T(STR_XML_ELEMENT_TEMPLATE))) {
				VITemplate dTemplate;
				XML_TO_DATA_STRING(dTemplate.m_strId, STR_XML_KEY_ID);
				XML_TO_DATA_METHOD(dTemplate.SetRotation, STR_XML_KEY_ROTATION);
				XML_TO_DATA_BOOL(dTemplate.m_bVerFlip, STR_XML_KEY_VERT_FLIP);
				XML_TO_DATA_BOOL(dTemplate.m_bHorizFlip, STR_XML_KEY_HORIZ_FLIP);
				XML_TO_DATA_BOOL(dTemplate.m_bReplace, STR_XML_KEY_REPLACE);
				XML_TO_DATA_FLOAT(dTemplate.m_nMinSizeRatio, STR_XML_KEY_MIN_SIZE_RATIO);

				RETURN_ON_FAIL(_LoadData_Inspection_TemplatePixel(pXMLChild, dTemplate));

				if (dTemplate.m_listTemplatePixel.size() > 0) {
					dTemplate.m_bMultiPixel = true;
				}

				m_pCV->DebugPrint(L"    [Template] Id : %s, Rotation : %s, VertFlip : %s, HorizFlip : %s, Replace : %s MinSizeRatio : %.2f, MultiPixel : %s, PixelCount : %d",
					dTemplate.m_strId.c_str(), dTemplate.GetRotation().c_str(),
					BOOL_TO_WSTRING(dTemplate.m_bVerFlip), BOOL_TO_WSTRING(dTemplate.m_bHorizFlip), 
					BOOL_TO_WSTRING(dTemplate.m_bReplace), dTemplate.m_nMinSizeRatio,
					BOOL_TO_WSTRING(dTemplate.m_bMultiPixel), dTemplate.m_listTemplatePixel.size());

				dInspection.m_dTemplate = dTemplate;
			}
			else if (CHECK_STRING(strChildElementName, _T(STR_XML_ELEMENT_DEFAULT_PARAM))) {
				VIDefaultParam dDefaultParam;
				XML_TO_DATA_FLOAT(dDefaultParam.m_nBinarizeThreshold, STR_XML_KEY_BINARIZE_THRESHOLD);
				XML_TO_DATA_FLOAT(dDefaultParam.m_nColorSimilarityRed, STR_XML_KEY_COLOR_SIMILARITY_RED);
				XML_TO_DATA_FLOAT(dDefaultParam.m_nColorSimilarityGreen, STR_XML_KEY_COLOR_SIMILARITY_GREEN);
				XML_TO_DATA_FLOAT(dDefaultParam.m_nColorSimilarityBlue, STR_XML_KEY_COLOR_SIMILARITY_BLUE);
				XML_TO_DATA_INT(dDefaultParam.m_nMinDefectSize, STR_XML_KEY_DEFECT_MIN_SIZE);
				XML_TO_DATA_INT(dDefaultParam.m_nMinSeedSize, STR_XML_KEY_SEED_MIN_SIZE);
				XML_TO_DATA_COLOR(dDefaultParam.m_dDarkColor, STR_XML_KEY_DARK_COLOR);
				XML_TO_DATA_FLOAT(dDefaultParam.m_nDarkColorRatio, STR_XML_KEY_DARK_COLOR_RATIO);
				XML_TO_DATA_FLOAT(dDefaultParam.m_nDarkColorExtendSizeRatio, STR_XML_KEY_DARK_COLOR_EXTEND_SIZE_RATIO);
				XML_TO_DATA_INT(dDefaultParam.m_nHVMinLength, STR_XML_KEY_HV_MIN_LENGTH);
				XML_TO_DATA_INT(dDefaultParam.m_nConnectedExtendSize, STR_XML_KEY_CONNECTED_EXTEND_SIZE);
				XML_TO_DATA_INT(dDefaultParam.m_nConnectedDefectExtendSize, STR_XML_KEY_CONNECTED_DEFECT_EXTEND_SIZE);
				XML_TO_DATA_INT(dDefaultParam.m_nExtendSize, STR_XML_KEY_EXTEND_SIZE);
				XML_TO_DATA_INT(dDefaultParam.m_nFillSize, STR_XML_KEY_FILL_SIZE);
				XML_TO_DATA_BOOL(dDefaultParam.m_bAdjustImageBackground, STR_XML_KEY_ADJUST_IMAGE_BACKGROUND);

				m_pCV->DebugPrint(L"    [Param] BinarizeThreshold : %.2f, ColorSimilairty : (%.2f, %.2f, %.2f), MinDefectSize : %d, MinSeedSize : %d, DarkColor: (%.0f, %.0f, %.0f), DarkColorRatio : %.2f, DarkColorExtendSizeRatio : %.2f, HVMinLength : %d, ConnectedExtendSize : %d, , ConnectedDefectExtendSize : %dExtendSize : %d, FillSize : %d, AdjustImageBackground : %s",
					dDefaultParam.m_nBinarizeThreshold, dDefaultParam.m_nColorSimilarityRed, dDefaultParam.m_nColorSimilarityGreen, dDefaultParam.m_nColorSimilarityBlue, dDefaultParam.m_nMinDefectSize, dDefaultParam.m_nMinSeedSize,
					dDefaultParam.m_dDarkColor[2], dDefaultParam.m_dDarkColor[1], dDefaultParam.m_dDarkColor[0], dDefaultParam.m_nDarkColorRatio, dDefaultParam.m_nDarkColorExtendSizeRatio, dDefaultParam.m_nHVMinLength, dDefaultParam.m_nConnectedExtendSize, dDefaultParam.m_nConnectedDefectExtendSize,
					dDefaultParam.m_nExtendSize, dDefaultParam.m_nFillSize, BOOL_TO_WSTRING(dDefaultParam.m_bAdjustImageBackground));

				dInspection.m_dDefaultParam = dDefaultParam;
			}
			else if (CHECK_STRING(strChildElementName, _T(STR_XML_ELEMENT_BINARIZE))) {
				VIBinarize dBinarize;

				XML_TO_DATA_BOOL(dBinarize.m_bApplyWholePixel, STR_XML_KEY_APPLY_WHOLE_PIXEL);
				XML_TO_DATA_STRING(dBinarize.m_strColorMaskId, STR_XML_KEY_COLOR_MASK_ID);

				m_pCV->DebugPrint(L"    [Binarize] ApplyWholePixel : %s, ColorMaskId : %s",
					BOOL_TO_WSTRING(dBinarize.m_bApplyWholePixel), dBinarize.m_strColorMaskId.c_str());

				RETURN_ON_FAIL(_LoadData_Inspection_Binarize(pXMLChild, dBinarize));

				dInspection.m_dBinarize = dBinarize;
			}
			else if (CHECK_STRING(strChildElementName, _T(STR_XML_ELEMENT_DEFECT))) {
				VIDefect dDefect;

				XML_TO_DATA_STRING(dDefect.m_strId, STR_XML_KEY_ID);
				XML_TO_DATA_BOOL(dDefect.m_bDark, STR_XML_KEY_DARK);
				XML_TO_DATA_BOOL(dDefect.m_bSeed, STR_XML_KEY_SEED);
				XML_TO_DATA_METHOD(dDefect.SetConnected, STR_XML_KEY_CONNECTED);
				XML_TO_DATA_BOOL(dDefect.m_bConnectedDefect, STR_XML_KEY_CONNECTED_DEFECT);
				XML_TO_DATA_BOOL(dDefect.m_bKeepPixelSeq, STR_XML_KEY_KEEP_PIXEL_SEQ);
				XML_TO_DATA_STRING(dDefect.m_strBaseDefectId, STR_XML_KEY_BASE_DEFECT_ID);
				XML_TO_DATA_BOOL(dDefect.m_bBaseDefectDraw, STR_XML_KEY_BASE_DEFECT_DRAW);

				m_pCV->DebugPrint(L"    [Defect] Id : %s, BaseDefectId : %s, Dark : %s, Seed : %s, Connected : %s, DefectConnected : %s, KeepPixelSeq : %s, BaseDefectDraw : %s",
					dDefect.m_strId.c_str(), dDefect.m_strBaseDefectId.c_str(),
					BOOL_TO_WSTRING(dDefect.m_bDark), BOOL_TO_WSTRING(dDefect.m_bDark), dDefect.GetConnected().c_str(),
					BOOL_TO_WSTRING(dDefect.m_bConnectedDefect), BOOL_TO_WSTRING(dDefect.m_bBaseDefectDraw), BOOL_TO_WSTRING(dDefect.m_bKeepPixelSeq));

				RETURN_ON_FAIL(_LoadData_Inspection_Defect(pXMLChild, dDefect));

				dDefect.m_nPixelCount = (int)dDefect.m_listPixel.size();

				dInspection.m_listDefect.push_back(dDefect);
			}
			else {
				continue;
			}
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_LoadData_Inspection_TemplatePixel(const shared_ptr<XMLElement> &pXML, VITemplate &dTemplate)
	{
		auto listXMLChild = pXML->GetChildren();
		for (auto pXMLChild : listXMLChild) {
			wstring strChildElementName = str2wstr(pXMLChild->GetElementName());
			if (CHECK_STRING(strChildElementName, _T(STR_XML_ELEMENT_TEMPLATE_PIXEL))) {
				VITemplatePixel dTemplatePixel;

				XML_TO_DATA_STRING(dTemplatePixel.m_strId, STR_XML_KEY_ID);

				m_pCV->DebugPrint(L"      [TemplatePixel] MaskId: %s", dTemplatePixel.m_strId.c_str());

				dTemplate.m_listTemplatePixel.push_back(dTemplatePixel);
			}
			else {
				continue;
			}
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_LoadData_Inspection_Binarize(const shared_ptr<XMLElement> &pXML, VIBinarize &dBinarize)
	{
		auto listXMLChild = pXML->GetChildren();
		for (auto pXMLChild : listXMLChild) {
			wstring strChildElementName = str2wstr(pXMLChild->GetElementName());
			if (CHECK_STRING(strChildElementName, _T(STR_XML_ELEMENT_APPLY))) {
				VIApply dApply;

				XML_TO_DATA_STRING(dApply.m_strMaskId, STR_XML_KEY_MASK_ID);
				XML_TO_DATA_STRING(dApply.m_strColorMaskId, STR_XML_KEY_COLOR_MASK_ID);
				XML_TO_DATA_FLOAT(dApply.m_nBinarizeThreshold, STR_XML_KEY_BINARIZE_THRESHOLD);
				XML_TO_DATA_INT(dApply.m_nMinDefectSize, STR_XML_KEY_DEFECT_MIN_SIZE);
				XML_TO_DATA_INT(dApply.m_nHVMinLength, STR_XML_KEY_HV_MIN_LENGTH);
				XML_TO_DATA_BOOL(dApply.m_bMergeResult, STR_XML_KEY_MERGE_RESULT);
				XML_TO_DATA_INT(dApply.m_nMergeFillSize, STR_XML_KEY_MERGE_FILL_SIZE);

				m_pCV->DebugPrint(L"      [Apply] MaskId: %s, ColorMaskId : %s, BinarizeThreshold: %.2f, MinDefectSize: %d, HVMinLength : %d, MergeResult : %s, MergeFillSize : %d",
					dApply.m_strMaskId.c_str(), dApply.m_strColorMaskId.c_str(), dApply.m_nBinarizeThreshold, dApply.m_nMinDefectSize, dApply.m_nHVMinLength,
					BOOL_TO_WSTRING(dApply.m_bMergeResult), dApply.m_nMergeFillSize);

				dBinarize.m_listApply.push_back(dApply);
			}
			else {
				continue;
			}
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_LoadData_Inspection_Defect(const shared_ptr<XMLElement> &pXML, VIDefect &dDefect)
	{
		auto listXMLChild = pXML->GetChildren();
		for (auto pXMLChild : listXMLChild) {
			wstring strChildElementName = str2wstr(pXMLChild->GetElementName());

			if (CHECK_STRING(strChildElementName, _T(STR_XML_ELEMENT_PIXEL))) {
				VIPixel dPixel;
				XML_TO_DATA_STRING(dPixel.m_strBaseDefectId, STR_XML_KEY_BASE_DEFECT_ID);
				XML_TO_DATA_BOOL(dPixel.m_bBaseDefectDraw, STR_XML_KEY_BASE_DEFECT_DRAW);
				XML_TO_DATA_BOOL(dPixel.m_bAcceptAll, STR_XML_KEY_ACCPET_ALL);
				
				m_pCV->DebugPrint(L"      [Pixel] BaseDefectId : %s, BaseDefectDraw : %s, AcceptAll : %s",
					dPixel.m_strBaseDefectId.c_str(), BOOL_TO_WSTRING(dPixel.m_bBaseDefectDraw), BOOL_TO_WSTRING(dPixel.m_bAcceptAll));

				RETURN_ON_FAIL(_LoadData_Inspection_Pixel(pXMLChild, dPixel));

				dDefect.m_listPixel.push_back(dPixel);
			}
			else if (CHECK_STRING(strChildElementName, _T(STR_XML_ELEMENT_DRAW))) {
				VIDraw dDraw;

				XML_TO_DATA_STRING(dDraw.m_strDrawSetId, STR_XML_KEY_DRAW_SET_ID);
				XML_TO_DATA_METHOD(dDraw.SetType, STR_XML_KEY_TYPE);
				XML_TO_DATA_STRING(dDraw.m_strMaskId, STR_XML_KEY_MASK_ID);
				XML_TO_DATA_COLOR(dDraw.m_dColor, STR_XML_KEY_COLOR);
				XML_TO_DATA_FLOAT(dDraw.m_nAlpha, STR_XML_KEY_ALPHA);
				XML_TO_DATA_INT(dDraw.m_nExtendSize, STR_XML_KEY_EXTEND_SIZE);
				XML_TO_DATA_INT(dDraw.m_nFillSize, STR_XML_KEY_FILL_SIZE);
				XML_TO_DATA_METHOD(dDraw.SetMode, STR_XML_KEY_MODE);

				m_pCV->DebugPrint(L"      [Draw] DrawSetId : %s, Type: %s, MaskId: %s, Color: (%.0f,%.0f,%.0f), Alpah: %.2f, ExtendSize : %d, FillSize : %d, Mode : %s",
					dDraw.m_strDrawSetId.c_str(), dDraw.GetType().c_str(), dDraw.m_strMaskId.c_str(),
					dDraw.m_dColor[2], dDraw.m_dColor[1], dDraw.m_dColor[0], dDraw.m_nAlpha, dDraw.m_nExtendSize, dDraw.m_nFillSize, dDraw.GetMode().c_str());

				dDefect.m_listDraw.push_back(dDraw);
			}
			else {
				continue;
			}
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_LoadData_Inspection_Pixel(const shared_ptr<XMLElement> &pXML, VIPixel &dPixel)
	{
		auto listXMLChild = pXML->GetChildren();
		for (auto pXMLChild : listXMLChild) {
			wstring strChildElementName = str2wstr(pXMLChild->GetElementName());
			if (CHECK_STRING(strChildElementName, _T(STR_XML_ELEMENT_CHECK))) {
				VICheck dCheck;
				XML_TO_DATA_STRING(dCheck.m_strMaskId, STR_XML_KEY_MASK_ID);
				XML_TO_DATA_STRING(dCheck.m_strColorMaskId, STR_XML_KEY_COLOR_MASK_ID);
				XML_TO_DATA_METHOD(dCheck.SetStatus, STR_XML_KEY_STATUS);
				XML_TO_DATA_INT(dCheck.m_nMinDefectSize, STR_XML_KEY_DEFECT_MIN_SIZE);
				XML_TO_DATA_INT(dCheck.m_nConnectedExtendSize, STR_XML_KEY_CONNECTED_EXTEND_SIZE);
				XML_TO_DATA_FLOAT(dCheck.m_nColorSimilarityRed, STR_XML_KEY_COLOR_SIMILARITY_RED);
				XML_TO_DATA_FLOAT(dCheck.m_nColorSimilarityGreen, STR_XML_KEY_COLOR_SIMILARITY_GREEN);
				XML_TO_DATA_FLOAT(dCheck.m_nColorSimilarityBlue, STR_XML_KEY_COLOR_SIMILARITY_BLUE);

				m_pCV->DebugPrint(L"        [Check] MaskId : %s, ColorMaskId : %s, Status : %s, MinDefectSize : %d, ConnectedExtendSize : %d, ColorSimilarity : (%.2f, %.2f, %.2f)",
					dCheck.m_strMaskId.c_str(), dCheck.m_strColorMaskId.c_str(), dCheck.GetStatus().c_str(), dCheck.m_nMinDefectSize, dCheck.m_nConnectedExtendSize,
					dCheck.m_nColorSimilarityRed, dCheck.m_nColorSimilarityGreen, dCheck.m_nColorSimilarityBlue);

				dPixel.m_listCheck.push_back(dCheck);

				if (dCheck.m_nStatus == VICheck::CONNECTED_NEIGHBOR) {
					dPixel.m_bConnectedNeighbor = true;
				}
			}
			else {
				continue;
			}
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_LoadMask()
	{
		VILayer &dLayer = m_dVIData.m_dLayer;

		wstring strMaskRootPath = dLayer.m_strMaskRootPath;
		int nLength = (int)strMaskRootPath.size();
		if (nLength <= 0) {
			m_pCV->DebugPrint(L"  ! Warning. No Mask Root Path");
			RETURN_SUCCESS();
		}

		if (strMaskRootPath[nLength - 1] != '\\') strMaskRootPath += '\\';
		if (strMaskRootPath.find(':') == string::npos) {
			wchar_t szPath[MAX_PATH];
			GetModuleFileName(NULL, szPath, MAX_PATH - 1);
			PathRemoveFileSpec(szPath);

			wstring strTemp = szPath;
			if (strMaskRootPath[0] != '\\') strTemp += '\\';
			strTemp += strMaskRootPath;

			strMaskRootPath = strTemp;
		}
		m_pCV->DebugPrint(L"LoadMask, layer full path : %s", strMaskRootPath.c_str());

		for (VIMask &dMask : dLayer.m_listMask) {
			wstring strPath = dMask.m_strPath;
			if (strPath.find(':') == string::npos) {
				strPath = strMaskRootPath + dMask.m_strPath;
			}

			m_pCV->DebugPrint(L"LoadMask, id : %s, full path : %s", dMask.m_strId.c_str(), strPath.c_str());

			Mat dImg;
			NIPL_ERR nNIPLErr = m_pCV->LoadImage(strPath, dImg);
			if (NIPL_FAIL(nNIPLErr)) {
				m_pCV->DebugPrint(L"  ! Warning. Cannot load Mask.");
				continue;
			}

			if (dMask.m_nType != VIMask::TEMPLATE && dImg.channels() == 3) {
				cvtColor(dImg, dImg, CV_BGR2GRAY);
			}

			dMask.m_dImg = dImg;
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::SaveData(const wstring strDataPath)
	{
		RETURN_ON_FAIL(_SaveData(strDataPath));
		RETURN_SUCCESS();
	}

	Err ProcessCore::_SaveData(const wstring strDataPath)
	{
		m_pCV->DebugPrint(L"==== SaveData, Start ==== ");
		m_pCV->DebugPrint(L"Data File : %s", strDataPath.c_str());

		XML dXML;

		// Set Header
		XMLHeader dHeader;
		XMLVariable &version = dHeader.GetVersion();
		XMLVariable &encoding = dHeader.GetEncoding();
		XMLVariable &standalone = dHeader.GetStandalone();
		version.SetValue("1.0");
		encoding.SetValue("utf-8");
		standalone.SetValue("");
		dXML.SetHeader(dHeader);

		// Set Body
		XMLElement dRoot(STR_XML_ELEMENT_VIDATA);
		DATA_TO_XML_STRING(dRoot, STR_XML_KEY_VERSION, _T(STR_XML_VERSION_AUTO_REPAIR));
		DATA_TO_XML_STRING(dRoot, STR_XML_KEY_ID, _T(STR_XML_ID_AUTO_REPAIR));

		XMLElement dXMLLayer(STR_XML_ELEMENT_LAYER);
		DATA_TO_XML_STRING(dXMLLayer, STR_XML_KEY_ROOT_PATH, m_dVIData.m_dLayer.m_strMaskRootPath);
		RETURN_ON_FAIL(_SaveData_Layer(&dXMLLayer));
		dRoot.AddElement(dXMLLayer);

		XMLElement dXMLDrawing(STR_XML_ELEMENT_DRAWING);
		RETURN_ON_FAIL(_SaveData_Drawing(&dXMLDrawing));
		dRoot.AddElement(dXMLDrawing);

		XMLElement dXMLInspection(STR_XML_ELEMENT_INSPECTION);
		RETURN_ON_FAIL(_SaveData_Inspection(&dXMLInspection));
		dRoot.AddElement(dXMLInspection);

		dXML.SetRootElement(dRoot);

		auto nResult = dXML.Save(strDataPath.c_str());
		if (nResult != XML3::XML_ERROR::OK) {
			RETURN_FAIL(FAIL_TO_SAVE_DATA_FILE);
		}

		m_pCV->DebugPrint(L"==== SaveData, End ====");

		RETURN_SUCCESS();
	}

	Err ProcessCore::_SaveData_Layer(XMLElement *pXML)
	{
		for (const VIMask &dMask : m_dVIData.m_dLayer.m_listMask) {
			string strElement = wstr2str(dMask.GetType());
			XMLElement dXMLMask(strElement.c_str());
			DATA_TO_XML_STRING(dXMLMask, STR_XML_KEY_ID, dMask.m_strId);
			DATA_TO_XML_STRING(dXMLMask, STR_XML_KEY_PATH, dMask.m_strPath);

			pXML->AddElement(dXMLMask);
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_SaveData_Drawing(XMLElement *pXML)
	{
		for (const VIDrawSet &dDrawSet : m_dVIData.m_dDrawing.m_listDrawSet) {
			XMLElement dXMLDrawSet(STR_XML_ELEMENT_DRAW_SET);
			DATA_TO_XML_STRING(dXMLDrawSet, STR_XML_KEY_ID, dDrawSet.m_strId);

			for (const VIDraw &dDraw : dDrawSet.m_listDraw) {
				XMLElement dXMLDraw(STR_XML_ELEMENT_DRAW);
				if (dDraw.m_nType == VIDraw::DRAW_SET) {
					DATA_TO_XML_STRING(dXMLDraw, STR_XML_KEY_TYPE, dDraw.GetType());
					DATA_TO_XML_STRING(dXMLDraw, STR_XML_KEY_DRAW_SET_ID, dDraw.m_strDrawSetId);
				}
				else {
					DATA_TO_XML_STRING(dXMLDraw, STR_XML_KEY_TYPE, dDraw.GetType());
					DATA_TO_XML_STRING(dXMLDraw, STR_XML_KEY_MASK_ID, dDraw.m_strMaskId);
					DATA_TO_XML_COLOR(dXMLDraw, STR_XML_KEY_COLOR, dDraw.m_dColor);
					DATA_TO_XML_FLOAT_CHECK_DEFAULT(dXMLDraw, STR_XML_KEY_ALPHA, dDraw.m_nAlpha, 1.f);
					DATA_TO_XML_INT(dXMLDraw, STR_XML_KEY_EXTEND_SIZE, dDraw.m_nExtendSize);
					DATA_TO_XML_INT(dXMLDraw, STR_XML_KEY_FILL_SIZE, dDraw.m_nFillSize);
					DATA_TO_XML_STRING(dXMLDraw, STR_XML_KEY_MODE, dDraw.GetMode(true));
				}

				dXMLDrawSet.AddElement(dXMLDraw);
			}

			pXML->AddElement(dXMLDrawSet);
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_SaveData_Inspection(XMLElement *pXML)
	{
		for (const auto &dItem : m_dVIData.m_mapInspection) {
			const VIInspection &dInspection = dItem.second;

			string strElement = wstr2str(dInspection.GetLayer());
			XMLElement dXMLInspectionLayer(strElement.c_str());

			RETURN_ON_FAIL(_SaveData_Inspection_Layer(&dXMLInspectionLayer, dInspection));
			pXML->AddElement(dXMLInspectionLayer);
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_SaveData_Inspection_Layer(XMLElement *pXML, const VIInspection &dInspection)
	{
		XMLElement dXMLTemplate(STR_XML_ELEMENT_TEMPLATE);
		const VITemplate &dTemplate = dInspection.m_dTemplate;
		DATA_TO_XML_STRING(dXMLTemplate, STR_XML_KEY_ID, dTemplate.m_strId);
		DATA_TO_XML_STRING(dXMLTemplate, STR_XML_KEY_ROTATION, dTemplate.GetRotation(true));
		DATA_TO_XML_BOOL(dXMLTemplate, STR_XML_KEY_VERT_FLIP, dTemplate.m_bVerFlip);
		DATA_TO_XML_BOOL(dXMLTemplate, STR_XML_KEY_HORIZ_FLIP, dTemplate.m_bHorizFlip);
		DATA_TO_XML_BOOL(dXMLTemplate, STR_XML_KEY_REPLACE, dTemplate.m_bReplace);
		DATA_TO_XML_FLOAT(dXMLTemplate, STR_XML_KEY_MIN_SIZE_RATIO, dTemplate.m_nMinSizeRatio);
		if (dTemplate.m_listTemplatePixel.size() > 0) {
			for (const VITemplatePixel &dTemplatePixel : dTemplate.m_listTemplatePixel) {
				XMLElement dXMLPixel(STR_XML_ELEMENT_TEMPLATE_PIXEL);
				DATA_TO_XML_STRING(dXMLPixel, STR_XML_KEY_ID, dTemplatePixel.m_strId);

				dXMLTemplate.AddElement(dXMLPixel);
			}
		}
		pXML->AddElement(dXMLTemplate);

		XMLElement dXMLDefaultParam(STR_XML_ELEMENT_DEFAULT_PARAM);
		const VIDefaultParam &dDefaultParam = dInspection.m_dDefaultParam;
		DATA_TO_XML_FLOAT(dXMLDefaultParam, STR_XML_KEY_BINARIZE_THRESHOLD, dDefaultParam.m_nBinarizeThreshold);
		DATA_TO_XML_INT(dXMLDefaultParam, STR_XML_KEY_DEFECT_MIN_SIZE, dDefaultParam.m_nMinDefectSize);
		DATA_TO_XML_INT(dXMLDefaultParam, STR_XML_KEY_SEED_MIN_SIZE, dDefaultParam.m_nMinSeedSize);
		DATA_TO_XML_COLOR(dXMLDefaultParam, STR_XML_KEY_DARK_COLOR, dDefaultParam.m_dDarkColor);
		DATA_TO_XML_FLOAT(dXMLDefaultParam, STR_XML_KEY_DARK_COLOR_RATIO, dDefaultParam.m_nDarkColorRatio);
		DATA_TO_XML_FLOAT(dXMLDefaultParam, STR_XML_KEY_DARK_COLOR_EXTEND_SIZE_RATIO, dDefaultParam.m_nDarkColorExtendSizeRatio);
		DATA_TO_XML_INT(dXMLDefaultParam, STR_XML_KEY_HV_MIN_LENGTH, dDefaultParam.m_nHVMinLength);
		DATA_TO_XML_FLOAT(dXMLDefaultParam, STR_XML_KEY_COLOR_SIMILARITY_RED, dDefaultParam.m_nColorSimilarityRed);
		DATA_TO_XML_FLOAT(dXMLDefaultParam, STR_XML_KEY_COLOR_SIMILARITY_GREEN, dDefaultParam.m_nColorSimilarityGreen);
		DATA_TO_XML_FLOAT(dXMLDefaultParam, STR_XML_KEY_COLOR_SIMILARITY_BLUE, dDefaultParam.m_nColorSimilarityBlue);
		DATA_TO_XML_INT(dXMLDefaultParam, STR_XML_KEY_CONNECTED_EXTEND_SIZE, dDefaultParam.m_nConnectedExtendSize);
		DATA_TO_XML_INT(dXMLDefaultParam, STR_XML_KEY_CONNECTED_DEFECT_EXTEND_SIZE, dDefaultParam.m_nConnectedDefectExtendSize);
		DATA_TO_XML_INT(dXMLDefaultParam, STR_XML_KEY_EXTEND_SIZE, dDefaultParam.m_nExtendSize);
		DATA_TO_XML_INT(dXMLDefaultParam, STR_XML_KEY_FILL_SIZE, dDefaultParam.m_nFillSize);
		DATA_TO_XML_BOOL(dXMLDefaultParam, STR_XML_KEY_ADJUST_IMAGE_BACKGROUND, dDefaultParam.m_bAdjustImageBackground);
		pXML->AddElement(dXMLDefaultParam);

		XMLElement dXMLBinarize(STR_XML_ELEMENT_BINARIZE);
		const VIBinarize &dBinarize = dInspection.m_dBinarize;
		DATA_TO_XML_BOOL(dXMLBinarize, STR_XML_KEY_APPLY_WHOLE_PIXEL, dBinarize.m_bApplyWholePixel);
		DATA_TO_XML_STRING(dXMLBinarize, STR_XML_KEY_COLOR_MASK_ID, dBinarize.m_strColorMaskId);
		if (dBinarize.m_listApply.size() > 0) {
			for (const VIApply &dApply : dBinarize.m_listApply) {
				XMLElement dXMLApply(STR_XML_ELEMENT_APPLY);
				DATA_TO_XML_STRING(dXMLApply, STR_XML_KEY_MASK_ID, dApply.m_strMaskId);
				DATA_TO_XML_STRING(dXMLApply, STR_XML_KEY_COLOR_MASK_ID, dApply.m_strColorMaskId);
				DATA_TO_XML_FLOAT(dXMLApply, STR_XML_KEY_BINARIZE_THRESHOLD, dApply.m_nBinarizeThreshold);
				DATA_TO_XML_INT(dXMLApply, STR_XML_KEY_DEFECT_MIN_SIZE, dApply.m_nMinDefectSize);
				DATA_TO_XML_INT(dXMLApply, STR_XML_KEY_HV_MIN_LENGTH, dApply.m_nHVMinLength);
				DATA_TO_XML_BOOL(dXMLApply, STR_XML_KEY_MERGE_RESULT, dApply.m_bMergeResult);
				DATA_TO_XML_INT(dXMLApply, STR_XML_KEY_MERGE_FILL_SIZE, dApply.m_nMergeFillSize);

				dXMLBinarize.AddElement(dXMLApply);
			}
		}
		pXML->AddElement(dXMLBinarize);

		for (const VIDefect &dDefect : dInspection.m_listDefect) {
			XMLElement dXMLDefect(STR_XML_ELEMENT_DEFECT);
			DATA_TO_XML_STRING(dXMLDefect, STR_XML_KEY_ID, dDefect.m_strId);
			DATA_TO_XML_BOOL(dXMLDefect, STR_XML_KEY_DARK, dDefect.m_bDark);
			DATA_TO_XML_BOOL(dXMLDefect, STR_XML_KEY_SEED, dDefect.m_bSeed);
			DATA_TO_XML_STRING(dXMLDefect, STR_XML_KEY_CONNECTED, dDefect.GetConnected(true));
			DATA_TO_XML_BOOL(dXMLDefect, STR_XML_KEY_CONNECTED_DEFECT, dDefect.m_bConnectedDefect);
			DATA_TO_XML_BOOL(dXMLDefect, STR_XML_KEY_KEEP_PIXEL_SEQ, dDefect.m_bKeepPixelSeq);
			DATA_TO_XML_STRING(dXMLDefect, STR_XML_KEY_BASE_DEFECT_ID, dDefect.m_strBaseDefectId);
			DATA_TO_XML_BOOL(dXMLDefect, STR_XML_KEY_BASE_DEFECT_DRAW, dDefect.m_bBaseDefectDraw);

			RETURN_ON_FAIL(_SaveData_Inspection_Defect(&dXMLDefect, dDefect));
			pXML->AddElement(dXMLDefect);
		}

		RETURN_SUCCESS();
	}

	Err ProcessCore::_SaveData_Inspection_Defect(XMLElement *pXML, const VIDefect &dDefect)
	{
		for (const VIPixel &dPixel : dDefect.m_listPixel) {
			XMLElement dXMLPixel(STR_XML_ELEMENT_PIXEL);
			DATA_TO_XML_STRING(dXMLPixel, STR_XML_KEY_BASE_DEFECT_ID, dPixel.m_strBaseDefectId);
			DATA_TO_XML_BOOL(dXMLPixel, STR_XML_KEY_BASE_DEFECT_DRAW, dPixel.m_bBaseDefectDraw);
			DATA_TO_XML_BOOL(dXMLPixel, STR_XML_KEY_ACCPET_ALL, dPixel.m_bAcceptAll);

			for (const VICheck &dCheck : dPixel.m_listCheck) {
				XMLElement dXMLCheck(STR_XML_ELEMENT_CHECK);
				DATA_TO_XML_STRING(dXMLCheck, STR_XML_KEY_MASK_ID, dCheck.m_strMaskId);
				DATA_TO_XML_STRING(dXMLCheck, STR_XML_KEY_COLOR_MASK_ID, dCheck.m_strColorMaskId);
				DATA_TO_XML_STRING(dXMLCheck, STR_XML_KEY_STATUS, dCheck.GetStatus());
				DATA_TO_XML_INT(dXMLCheck, STR_XML_KEY_DEFECT_MIN_SIZE, dCheck.m_nMinDefectSize);
				DATA_TO_XML_INT(dXMLCheck, STR_XML_KEY_CONNECTED_EXTEND_SIZE, dCheck.m_nConnectedExtendSize);
				DATA_TO_XML_FLOAT(dXMLCheck, STR_XML_KEY_COLOR_SIMILARITY_RED, dCheck.m_nColorSimilarityRed);
				DATA_TO_XML_FLOAT(dXMLCheck, STR_XML_KEY_COLOR_SIMILARITY_GREEN, dCheck.m_nColorSimilarityGreen);
				DATA_TO_XML_FLOAT(dXMLCheck, STR_XML_KEY_COLOR_SIMILARITY_BLUE, dCheck.m_nColorSimilarityBlue);

				dXMLPixel.AddElement(dXMLCheck);
			}

			pXML->AddElement(dXMLPixel);
		}

		for (const VIDraw &dDraw : dDefect.m_listDraw) {
			XMLElement dXMLDraw(STR_XML_ELEMENT_DRAW);

			if (dDraw.m_nType == VIDraw::DRAW_SET) {
				DATA_TO_XML_STRING(dXMLDraw, STR_XML_KEY_TYPE, dDraw.GetType());
				DATA_TO_XML_STRING(dXMLDraw, STR_XML_KEY_DRAW_SET_ID, dDraw.m_strDrawSetId);
			}
			else {
				DATA_TO_XML_STRING(dXMLDraw, STR_XML_KEY_TYPE, dDraw.GetType());
				DATA_TO_XML_STRING(dXMLDraw, STR_XML_KEY_MASK_ID, dDraw.m_strMaskId);
				DATA_TO_XML_COLOR(dXMLDraw, STR_XML_KEY_COLOR, dDraw.m_dColor);
				DATA_TO_XML_FLOAT_CHECK_DEFAULT(dXMLDraw, STR_XML_KEY_ALPHA, dDraw.m_nAlpha, 1.f);
				DATA_TO_XML_INT(dXMLDraw, STR_XML_KEY_EXTEND_SIZE, dDraw.m_nExtendSize);
				DATA_TO_XML_INT(dXMLDraw, STR_XML_KEY_FILL_SIZE, dDraw.m_nFillSize);
				DATA_TO_XML_STRING(dXMLDraw, STR_XML_KEY_MODE, dDraw.GetMode(true));
			}

			pXML->AddElement(dXMLDraw);
		}

		RETURN_SUCCESS();
	}

	VIData &ProcessCore::GetData()
	{
		return m_dVIData;
	}

	//
	// Time Profile 관련 함수
	//
	void TimeProfile::DebugPrint()
	{
		shared_ptr<NIPLCV> pCV = NIPLCV::GetInstance();
		pCV->DebugPrint(L"======== Time Profile ========");
		pCV->DebugPrint(L"======== * FindTarget : %s", TIME_PROFILE_STRING(FindTarget));
		pCV->DebugPrint(L"========       - Pos : %s", TIME_PROFILE_STRING(FindTarget_Pos));
		pCV->DebugPrint(L"========       - SetTargetInfoList : %s", TIME_PROFILE_STRING(FindTarget_SetTargetInfoList));
		pCV->DebugPrint(L"======== * DetectDefect : %s", TIME_PROFILE_STRING(DetectDefect));
		pCV->DebugPrint(L"========       - MakeTemplateDiffImage : %s", TIME_PROFILE_STRING(DetectDefect_MakeTemplateDiffImage));
		pCV->DebugPrint(L"========       - MakeDiffImage : %s", TIME_PROFILE_STRING(DetectDefect_MakeDiffImage));
		pCV->DebugPrint(L"========             - AdjustImageBackground : %s", TIME_PROFILE_STRING(DetectDefect_MakeDiffImage_AdjustImageBackground));
		pCV->DebugPrint(L"========       - Binarize : %s", TIME_PROFILE_STRING(DetectDefect_Binarize));
		pCV->DebugPrint(L"========       - SetDefectTargetInfo : %s", TIME_PROFILE_STRING(DetectDefect_SetDefectTargetInfo));
		pCV->DebugPrint(L"======== * CheckDefect : %s", TIME_PROFILE_STRING(CheckDefect));
		pCV->DebugPrint(L"========       - Target1 : %s", TIME_PROFILE_STRING(CheckDefect_Target1));
		pCV->DebugPrint(L"========       - Target2 : %s", TIME_PROFILE_STRING(CheckDefect_Target2));
		pCV->DebugPrint(L"========       - MultiTarget : %s", TIME_PROFILE_STRING(CheckDefect_MultiTarget));
		pCV->DebugPrint(L"========       - DarkOrSeed : %s", TIME_PROFILE_STRING(CheckDefect_DarkOrSeed));
		pCV->DebugPrint(L"======== * DrawDefect : %s", TIME_PROFILE_STRING(DrawDefect));
		pCV->DebugPrint(L"======== >> Total : %s <<", TIME_PROFILE_STRING(Total));
		pCV->DebugPrint(L"======== Time Profile, End ========");
	}
}
