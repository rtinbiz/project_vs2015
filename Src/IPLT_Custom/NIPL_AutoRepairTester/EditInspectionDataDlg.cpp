// EditInspectionDataDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AutoRepairTester.h"
#include "EditInspectionDataDlg.h"
#include "afxdialogex.h"

// CEditInspectionDataDlg 대화 상자입니다.
#define DATA_TO_PROPERTY_VALUE(name, v) \
	pParamGrid = new CMFCPropertyGridProperty(_T(name), (_variant_t)v, L""); \
	m_ctlDataProperty.AddProperty(pParamGrid);
#define DATA_TO_PROPERTY_STRING(name, v) DATA_TO_PROPERTY_VALUE(name, v.c_str())
#define DATA_TO_PROPERTY_STRING_COMBO(name, v, list) \
	pParamGrid = new CMFCPropertyGridProperty(_T(name), (_variant_t)v.c_str(), L""); \
	for(const auto &str : list) pParamGrid->AddOption(str.c_str()); \
	pParamGrid->AllowEdit(FALSE); \
	m_ctlDataProperty.AddProperty(pParamGrid)
#define DATA_TO_PROPERTY_METHOD(name, method) \
	pParamGrid = new CMFCPropertyGridProperty(_T(name), (_variant_t)method().c_str(), L""); \
	for(const auto &str : ##method##List()) pParamGrid->AddOption(str.c_str()); \
	pParamGrid->AllowEdit(FALSE); \
	m_ctlDataProperty.AddProperty(pParamGrid)

#define DATA_TO_PROPERTY_BOOL(name, v) DATA_TO_PROPERTY_VALUE(name, v)
#define DATA_TO_PROPERTY_INT(name, v) DATA_TO_PROPERTY_VALUE(name, v)
#define DATA_TO_PROPERTY_FLOAT(name, v) DATA_TO_PROPERTY_VALUE(name, v)
#define DATA_TO_PROPERTY_COLOR(name, v) \
	{ \
		wstring str; \
		if(v[0] != 0 || v[1] != 0 || v[2] != 0) str = COLOR_TO_WSTRING(v); \
		DATA_TO_PROPERTY_STRING(name, str); \
	}

#define PROPERTY_TO_DATA_STRING(name, v) if (CHECK_STRING(strName, name)) v = wstring((wchar_t *)dValue.pbstrVal)
#define PROPERTY_TO_DATA_BOOL(name, v) if (CHECK_STRING(strName, name)) v = (dValue.boolVal != 0)
#define PROPERTY_TO_DATA_INT(name, v) if (CHECK_STRING(strName, name)) v = dValue.intVal
#define PROPERTY_TO_DATA_FLOAT(name, v) if (CHECK_STRING(strName, name)) v = dValue.fltVal
#define PROPERTY_TO_DATA_METHOD(name, method) if (CHECK_STRING(strName, name)) method(wstring((wchar_t *)dValue.pbstrVal))
#define PROPERTY_TO_DATA_COLOR(name, v) \
	if (CHECK_STRING(strName, name)) { \
		wstring str; \
		PROPERTY_TO_DATA_STRING(name, str); \
		v = STRING_TO_COLOR(str); \
	}

#define DATA_LIST_SHOW(parent, data) \
	const list<VI##data##> &list##data## = d##parent##.m_list##data##; \
	for (const VI##data## &d##data## : list##data##) { \
		SetListItem(nDataType, (DWORD_PTR)&d##data##); \
	}
#define DATA_SHOW(parent, data, type) \
	VI##data## &d##data## = d##parent##.m_d##data##; \
	SetListItem(type, (DWORD_PTR)&d##data##);
#define DATA_SORT(parent, data) d##parent##.Sort##data##List();
#define DATA_ORDER_UP(parent, data) d##parent##.OrderUp##data##(*(VI##data## *)pData)
#define DATA_ORDER_DOWN(parent, data) d##parent##.OrderDown##data##(*(VI##data## *)pData)
#define DATA_ADD(parent, data) \
	VI##data## &d##data## = d##parent##.Add##data##(); \
	pData = (DWORD_PTR)&d##data##;
#define DATA_DELETE(parent, data) \
	DWORD_PTR pData = pCtl->GetItemData(nIndex); \
	VI##data## &d##data## = *(VI##data## *)pData; \
	d##parent##.Delete##data##(d##data##); 
#define GET_SELECTED_LIST_ITEM_DATA(data) \
	int nSelectedIndex = m_ctl##data##List.GetCurSel(); \
	if (nSelectedIndex < 0) { \
		return; \
	} \
	VI##data## &d##data## = *(VI##data## *)m_ctl##data##List.GetItemData(nSelectedIndex)
#define GET_LAYER_DATA() \
	VILayer &dLayer = dData.m_dLayer
#define GET_DRAWING_DATA() \
	VIDrawing &dDrawing = dData.m_dDrawing
#define GET_INSPECTION_DATA() \
	VIInspection &dInspection = dData.m_mapInspection[(Layer)m_nLayerIndex]
#define GET_INSPECTION_BINARIZE_DATA() \
	GET_INSPECTION_DATA(); \
	VIBinarize &dBinarize = dInspection.m_dBinarize;
#define GET_INSPECTION_TEMPLATE_DATA() \
	GET_INSPECTION_DATA(); \
	VITemplate &dTemplate = dInspection.m_dTemplate;

IMPLEMENT_DYNAMIC(CEditInspectionDataDlg, CDialogEx)

CEditInspectionDataDlg::CEditInspectionDataDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CEditInspectionDataDlg::IDD, pParent)
	, m_strDataFilePath(_T(""))
	, m_strMaskRootPath(_T(""))
	, m_nLayerIndex(0)
{
	m_pMainDlg = nullptr;
}

CEditInspectionDataDlg::~CEditInspectionDataDlg()
{
}

void CEditInspectionDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOG_LIST, m_ctlLogList);
	DDX_Text(pDX, IDC_EDIT_DATA_FILE, m_strDataFilePath);
	DDX_Control(pDX, IDC_MASK_LIST, m_ctlMaskList);
	DDX_Control(pDX, IDC_DATA_PROPERTY, m_ctlDataProperty);
	DDX_Text(pDX, IDC_MASK_ROOT_PATH, m_strMaskRootPath);
	DDX_Control(pDX, IDC_DRAWSET_LIST, m_ctlDrawSetList);
	DDX_Control(pDX, IDC_DRAWSET_DRAW_LIST, m_ctlDrawSetDrawList);
	DDX_Control(pDX, IDC_PARAM_LIST, m_ctlParamList);
	DDX_Control(pDX, IDC_APPLY_LIST, m_ctlApplyList);
	DDX_Control(pDX, IDC_DEFCT_LIST, m_ctlDefectList);
	DDX_Control(pDX, IDC_PIXEL_LIST, m_ctlPixelList);
	DDX_Control(pDX, IDC_CHECK_LIST, m_ctlCheckList);
	DDX_Control(pDX, IDC_DEFECT_DRAW_LIST, m_ctlDefectDrawList);
	DDX_Radio(pDX, IDC_LAYER_ACTIVE, m_nLayerIndex);
	DDX_Control(pDX, IDC_TEMPLATE_PIXEL_LIST, m_ctlTemplatePixelList);
}


BEGIN_MESSAGE_MAP(CEditInspectionDataDlg, CDialogEx)
	ON_BN_CLICKED(IDC_SAVE_DATA, &CEditInspectionDataDlg::OnBnClickedSaveData)
	ON_BN_CLICKED(IDC_LOAD_DATA, &CEditInspectionDataDlg::OnBnClickedLoadData)
	ON_LBN_SELCHANGE(IDC_MASK_LIST, &CEditInspectionDataDlg::OnSelchangeMaskList)
	ON_REGISTERED_MESSAGE(AFX_WM_PROPERTY_CHANGED, &CEditInspectionDataDlg::OnDataPropertyChanged)
	ON_BN_CLICKED(IDC_MASK_LIST_SORT, &CEditInspectionDataDlg::OnBnClickedMaskListSort)
	ON_BN_CLICKED(IDC_MASK_ADD, &CEditInspectionDataDlg::OnBnClickedMaskAdd)
	ON_BN_CLICKED(IDC_MASK_DELETE, &CEditInspectionDataDlg::OnBnClickedMaskDelete)
	ON_BN_CLICKED(IDC_DRAWSET_LIST_SORT, &CEditInspectionDataDlg::OnBnClickedDrawsetListSort)
	ON_BN_CLICKED(IDC_DRAWSET_ADD, &CEditInspectionDataDlg::OnBnClickedDrawsetAdd)
	ON_BN_CLICKED(IDC_DRAWSET_DELETE, &CEditInspectionDataDlg::OnBnClickedDrawsetDelete)
	ON_BN_CLICKED(IDC_DRAWSET_DRAW_UP, &CEditInspectionDataDlg::OnBnClickedDrawsetDrawUp)
	ON_BN_CLICKED(IDC_DRAWSET_DRAW_DOWN, &CEditInspectionDataDlg::OnBnClickedDrawsetDrawDown)
	ON_BN_CLICKED(IDC_DRAWSET_DRAW_ADD, &CEditInspectionDataDlg::OnBnClickedDrawsetDrawAdd)
	ON_BN_CLICKED(IDC_DRAWSET_DRAW_DELETE, &CEditInspectionDataDlg::OnBnClickedDrawsetDrawDelete)
	ON_LBN_SELCHANGE(IDC_DRAWSET_DRAW_LIST, &CEditInspectionDataDlg::OnSelchangeDrawsetDrawList)
	ON_LBN_SELCHANGE(IDC_DRAWSET_LIST, &CEditInspectionDataDlg::OnSelchangeDrawsetList)
	ON_BN_CLICKED(IDC_CHECK_LIST_SORT, &CEditInspectionDataDlg::OnBnClickedCheckListSort)
	ON_BN_CLICKED(IDC_APPLY_ADD, &CEditInspectionDataDlg::OnBnClickedApplyAdd)
	ON_BN_CLICKED(IDC_DEFECT_ADD, &CEditInspectionDataDlg::OnBnClickedDefectAdd)
	ON_BN_CLICKED(IDC_PIXEL_ADD, &CEditInspectionDataDlg::OnBnClickedPixelAdd)
	ON_BN_CLICKED(IDC_CHECK_ADD, &CEditInspectionDataDlg::OnBnClickedCheckAdd)
	ON_BN_CLICKED(IDC_DEFECT_DRAW_ADD, &CEditInspectionDataDlg::OnBnClickedDefectDrawAdd)
	ON_BN_CLICKED(IDC_APPLY_DELETE, &CEditInspectionDataDlg::OnBnClickedApplyDelete)
	ON_BN_CLICKED(IDC_DEFECT_DELETE, &CEditInspectionDataDlg::OnBnClickedDefectDelete)
	ON_BN_CLICKED(IDC_PIXEL_DELETE, &CEditInspectionDataDlg::OnBnClickedPixelDelete)
	ON_BN_CLICKED(IDC_CHECK_DELETE, &CEditInspectionDataDlg::OnBnClickedCheckDelete)
	ON_BN_CLICKED(IDC_DEFECT_DRAW_DELETE, &CEditInspectionDataDlg::OnBnClickedDefectDrawDelete)
	ON_BN_CLICKED(IDC_DEFECT_UP, &CEditInspectionDataDlg::OnBnClickedDefectUp)
	ON_BN_CLICKED(IDC_DEFECT_DOWN, &CEditInspectionDataDlg::OnBnClickedDefectDown)
	ON_BN_CLICKED(IDC_PIXEL_UP, &CEditInspectionDataDlg::OnBnClickedPixelUp)
	ON_BN_CLICKED(IDC_DEFECT_DRAW_UP, &CEditInspectionDataDlg::OnBnClickedDefectDrawUp)
	ON_BN_CLICKED(IDC_PIXEL_DOWN, &CEditInspectionDataDlg::OnBnClickedPixelDown)
	ON_BN_CLICKED(IDC_DEFECT_DRAW_DOWN, &CEditInspectionDataDlg::OnBnClickedDefectDrawDown)
	ON_COMMAND_RANGE(IDC_LAYER_ACTIVE, IDC_LAYER_SD, OnClickedLayerRadio)
	ON_LBN_SELCHANGE(IDC_APPLY_LIST, &CEditInspectionDataDlg::OnSelchangeApplyList)
	ON_LBN_SELCHANGE(IDC_CHECK_LIST, &CEditInspectionDataDlg::OnSelchangeCheckList)
	ON_LBN_SELCHANGE(IDC_DEFCT_LIST, &CEditInspectionDataDlg::OnSelchangeDefctList)
	ON_LBN_SELCHANGE(IDC_DEFECT_DRAW_LIST, &CEditInspectionDataDlg::OnSelchangeDefectDrawList)
	ON_LBN_SELCHANGE(IDC_PARAM_LIST, &CEditInspectionDataDlg::OnSelchangeParamList)
	ON_LBN_SELCHANGE(IDC_PIXEL_LIST, &CEditInspectionDataDlg::OnSelchangePixelList)
	ON_BN_CLICKED(IDC_OPEN_DATA_FILE, &CEditInspectionDataDlg::OnBnClickedOpenDataFile)
	ON_BN_CLICKED(IDC_TEMPLATE_PIXEL_LIST_SORT, &CEditInspectionDataDlg::OnBnClickedTemplatePixelListSort)
	ON_BN_CLICKED(IDC_TEMPLATE_PIXEL_ADD, &CEditInspectionDataDlg::OnBnClickedTemplatePixelAdd)
	ON_BN_CLICKED(IDC_TEMPLATE_PIXEL_DELETE, &CEditInspectionDataDlg::OnBnClickedTemplatePixelDelete)
	ON_LBN_SELCHANGE(IDC_TEMPLATE_PIXEL_LIST, &CEditInspectionDataDlg::OnSelchangeTemplatePixelList)
	ON_BN_CLICKED(IDC_APPLY_UP, &CEditInspectionDataDlg::OnBnClickedApplyUp)
	ON_BN_CLICKED(IDC_APPLY_DOWN, &CEditInspectionDataDlg::OnBnClickedApplyDown)
	ON_BN_CLICKED(IDC_BACKUP_DATA, &CEditInspectionDataDlg::OnBnClickedBackupData)
	ON_BN_CLICKED(IDC_RESTORE_DATA, &CEditInspectionDataDlg::OnBnClickedRestoreData)
END_MESSAGE_MAP()

void CEditInspectionDataDlg::Create(CWnd *pMainDlg)
{
	m_pMainDlg = pMainDlg;
	CDialogEx::Create(IDD, GetDesktopWindow());
}

// CEditInspectionDataDlg 메시지 처리기입니다.
BOOL CEditInspectionDataDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_ctlMaskList.ModifyStyle(LBS_SORT, 0);

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_ctlDataProperty.SetVSDotNetLook();
	SetDataPropertyCtrlFont();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CEditInspectionDataDlg::SetDataPropertyCtrlFont()
{
	::DeleteObject(m_fntData.Detach());

	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);

	NONCLIENTMETRICS info;
	info.cbSize = sizeof(info);

	afxGlobalData.GetNonClientMetrics(info);

	lf.lfHeight = info.lfMenuFont.lfHeight;
	lf.lfWeight = info.lfMenuFont.lfWeight;
	lf.lfItalic = info.lfMenuFont.lfItalic;

	m_fntData.CreateFontIndirect(&lf);

	m_ctlDataProperty.SetFont(&m_fntData);
}


void CEditInspectionDataDlg::OnBnClickedLoadData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LoadData(m_strDataFilePath);
}

void CEditInspectionDataDlg::OnBnClickedSaveData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SaveData(m_strDataFilePath);
	if (m_pMainDlg) m_pMainDlg->SendMessage(WM_UPDATE_INSPECTION_DATA);
}

void CEditInspectionDataDlg::AddLogText(const wchar_t* szText, ...)
{
	wchar_t szBufferText[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szText);
	vswprintf_s(szBufferText, szText, (va_list)vargs);
	va_end(vargs);

	SYSTEMTIME dTime;
	GetLocalTime(&dTime);

	wchar_t szTime[256];
	swprintf_s(szTime, L"[%02d:%02d:%02d.%03d] ", dTime.wHour, dTime.wMinute, dTime.wSecond, dTime.wMilliseconds);

	wstring strFullText(szTime);
	strFullText += szBufferText;

	int nIndex = m_ctlLogList.GetCount();
	nIndex = m_ctlLogList.InsertString(nIndex, strFullText.c_str());
	m_ctlLogList.SetCurSel(nIndex);

	// Set horizontal scroll in Log list
	CDC *pDC = m_ctlLogList.GetDC();
	CFont *pFont = m_ctlLogList.GetFont();
	CFont *pOldFont = pDC->SelectObject(pFont);

	CSize sz = pDC->GetTextExtent(strFullText.c_str(), (int)strFullText.length());
	int nExt = sz.cx += 3*::GetSystemMetrics(SM_CXBORDER);

	if (m_ctlLogList.GetHorizontalExtent() < nExt) {
		m_ctlLogList.SetHorizontalExtent(nExt);
	}
	pDC->SelectObject(pOldFont);
	m_ctlLogList.ReleaseDC(pDC);
}

void CEditInspectionDataDlg::LoadData(CString strDataPath, bool bUpdateDataPath)
{
	UpdateData();

	if (bUpdateDataPath) {
		m_strDataFilePath = strDataPath;
	}

	Process &iProcess = Process::GetInstance();
	Err nErr = iProcess.LoadData(LPCTSTR(strDataPath));
	if (!ERR_SUCCESS(nErr)) {
		wstring strErr = GetErrMessage(nErr);
		AddLogText(L"Fail to load Data. Err : %s", strErr.c_str());
		return;
	}
	AddLogText(L"Success to load Data : %s", LPCTSTR(strDataPath));

	ClearList();

	ShowList(DPT_MASK);
	ShowList(DPT_DRAWSET);
	ChangeLayer();

	int nCount = m_ctlMaskList.GetCount();
	if (nCount > 0) {
		m_ctlMaskList.SetCurSel(0);
		OnSelectListItem(DPT_MASK);
	}
}

void CEditInspectionDataDlg::ShowList(DATA_PROPERTY_TYPE nDataType)
{
	Process &iProcess = Process::GetInstance();
	VIData &dData = iProcess.GetData();

	CListBox *pCtl = GetListCtl(nDataType);
	if (pCtl == nullptr) {
		return;
	}

	pCtl->ResetContent();

	switch (nDataType) {
	case DPT_MASK:
		{
			GET_LAYER_DATA();
			DATA_LIST_SHOW(Layer, Mask);
			m_strMaskRootPath = dLayer.m_strMaskRootPath.c_str();
		}
		break;
	case DPT_DRAWSET:
		{
			GET_DRAWING_DATA();
			DATA_LIST_SHOW(Drawing, DrawSet);
		}
		break;
	case DPT_DRAWSET_DRAW:
		{
			GET_SELECTED_LIST_ITEM_DATA(DrawSet);
			DATA_LIST_SHOW(DrawSet, Draw);
		}
		break;
	case DPT_PARAM:
		{
			GET_INSPECTION_DATA();
			DATA_SHOW(Inspection, Template, DPT_TEMPLATE);
			DATA_SHOW(Inspection, DefaultParam, DPT_DEFAULTPARAM);
			DATA_SHOW(Inspection, Binarize, DPT_BINARIZE);
		}
		break;
	case DPT_TEMPLATE_PIXEL:
		{
			GET_INSPECTION_TEMPLATE_DATA();
			DATA_LIST_SHOW(Template, TemplatePixel);
		}
		break;
	case DPT_APPLY:
		{
			GET_INSPECTION_BINARIZE_DATA();
			DATA_LIST_SHOW(Binarize, Apply);
		}
		break;
	case DPT_DEFECT:
		{
			GET_INSPECTION_DATA();
			DATA_LIST_SHOW(Inspection, Defect);
		}
		break;
	case DPT_PIXEL:
		{
			GET_SELECTED_LIST_ITEM_DATA(Defect);
			DATA_LIST_SHOW(Defect, Pixel);
		}
		break;
	case DPT_CHECK:
		{
			GET_SELECTED_LIST_ITEM_DATA(Pixel);
			DATA_LIST_SHOW(Pixel, Check);
		}
		break;
	case DPT_DEFECT_DRAW:
		{
			GET_SELECTED_LIST_ITEM_DATA(Defect);
			DATA_LIST_SHOW(Defect, Draw);
		}
		break;
	}

	UpdateData(FALSE);
}

void CEditInspectionDataDlg::SetListItem(DATA_PROPERTY_TYPE nDataType, DWORD_PTR pData, bool bUpdate)
{
	CListBox *pCtl = GetListCtl(nDataType);
	if (pCtl == nullptr) {
		return;
	}

	wchar_t szText[128];
	switch (nDataType) {
	case DPT_MASK:
		{
			VIMask &dMask = *(VIMask *)pData;
			swprintf_s(szText, L"[%s] %s", dMask.GetType().c_str(), dMask.m_strId.c_str());
		}
		break;
	case DPT_DRAWSET:
		{
			VIDrawSet &dDrawSet = *(VIDrawSet *)pData;
			swprintf_s(szText, L"%s", dDrawSet.m_strId.c_str());
		}
		break;
	case DPT_DRAWSET_DRAW:
	case DPT_DEFECT_DRAW:
	{
			VIDraw &dDraw = *(VIDraw *)pData;

			if (dDraw.m_nType == VIDraw::DRAW_SET) {
				swprintf_s(szText, L"[%s] %s", dDraw.GetType().c_str(), dDraw.m_strDrawSetId.c_str());
			}
			else if (!dDraw.m_strMaskId.empty()) {
				swprintf_s(szText, L"[%s] %s %s", dDraw.GetType().c_str(), dDraw.m_strMaskId.c_str(), dDraw.GetMode().c_str());
			}
			else {
				swprintf_s(szText, L"[%s] %s", dDraw.GetType().c_str(), dDraw.GetMode().c_str());
			}
		}
		break;
	case DPT_TEMPLATE:
		{
			VITemplate &dTemplate = *(VITemplate *)pData;
			swprintf_s(szText, L"[Template] %s", dTemplate.m_strId.c_str());
		}
		break;
	case DPT_BINARIZE:
		{
			VIBinarize &dBinarize = *(VIBinarize *)pData;
			swprintf_s(szText, L"[Binarize]");
		}
		break;
	case DPT_DEFAULTPARAM:
		{
			VIDefaultParam &dDefaultParam = *(VIDefaultParam *)pData;
			swprintf_s(szText, L"[DefaultParam]");
		}
		break;
	case DPT_TEMPLATE_PIXEL:
		{
			VITemplatePixel &dTemplatePixel = *(VITemplatePixel *)pData;
			swprintf_s(szText, L"%s", dTemplatePixel.m_strId.c_str());
		}
		break;
	case DPT_APPLY:
		{
			VIApply &dApply = *(VIApply *)pData;
			swprintf_s(szText, L"%s", dApply.m_strMaskId.c_str());
		}
		break;
	case DPT_DEFECT:
		{
			VIDefect &dDefect = *(VIDefect *)pData;
			swprintf_s(szText, L"%s", dDefect.m_strId.c_str());
		}
		break;
	case DPT_PIXEL:
		{
			VIPixel &dPixel = *(VIPixel *)pData;
			if (dPixel.m_bAcceptAll) {
				swprintf_s(szText, L"[Pixel] Accept All");
			}
			else if (!dPixel.m_strBaseDefectId.empty()) {
				swprintf_s(szText, L"[Pixel] %s", dPixel.m_strBaseDefectId.c_str());
			}
			else {
				swprintf_s(szText, L"[Pixel]");
			}
		}
		break;
	case DPT_CHECK:
		{
			VICheck &dCheck = *(VICheck *)pData;
			swprintf_s(szText, L"%s", dCheck.m_strMaskId.c_str());
		}
		break;
	default:
		return;
	}

	int nIndex = -1;
	if (bUpdate) {		// update text -> delete and add
		nIndex = pCtl->GetCurSel();
		if (nIndex >= 0) {
			pCtl->DeleteString(nIndex);
		}
	}

	nIndex = pCtl->InsertString(nIndex, szText);
	if (nIndex >= 0) {
		pCtl->SetItemData(nIndex, pData);
	}
}

void CEditInspectionDataDlg::SelectListItem(DATA_PROPERTY_TYPE nDataType, DWORD_PTR pData)
{
	CListBox *pCtl = GetListCtl(nDataType);
	if (pCtl == nullptr) {
		return;
	}

	int nCount = pCtl->GetCount();
	for (int i = 0; i < nCount; i++) {
		if (pData == pCtl->GetItemData(i)) {
			pCtl->SetCurSel(i);
			OnSelectListItem(nDataType);
			return;
		}
	}
}

void CEditInspectionDataDlg::OnSelchangeMaskList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnSelectListItem(DPT_MASK);
}

void CEditInspectionDataDlg::OnSelchangeDrawsetList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnSelectListItem(DPT_DRAWSET);
}

void CEditInspectionDataDlg::OnSelchangeDrawsetDrawList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnSelectListItem(DPT_DRAWSET_DRAW);
}

void CEditInspectionDataDlg::OnSelchangeParamList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnSelectListItem(DPT_PARAM);
}

void CEditInspectionDataDlg::OnSelchangeTemplatePixelList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnSelectListItem(DPT_TEMPLATE_PIXEL);
}

void CEditInspectionDataDlg::OnSelchangeApplyList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnSelectListItem(DPT_APPLY);
}

void CEditInspectionDataDlg::OnSelchangeDefctList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnSelectListItem(DPT_DEFECT);
}

void CEditInspectionDataDlg::OnSelchangePixelList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnSelectListItem(DPT_PIXEL);
}

void CEditInspectionDataDlg::OnSelchangeCheckList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnSelectListItem(DPT_CHECK);
}

void CEditInspectionDataDlg::OnSelchangeDefectDrawList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnSelectListItem(DPT_DEFECT_DRAW);
}


void CEditInspectionDataDlg::OnSelectListItem(DATA_PROPERTY_TYPE nDataType)
{
	UpdateData();

	CListBox *pCtl = GetListCtl(nDataType);
	if (pCtl == nullptr) {
		return;
	}

	int nIndex = pCtl->GetCurSel();
	if (nIndex >= 0) {
		DWORD_PTR pData = pCtl->GetItemData(nIndex);
		if (nDataType == DPT_PARAM) {
			DATA_PROPERTY_TYPE nItemDataType = DPT_NONE;
			switch (nIndex) {
			case 0: nItemDataType = DPT_TEMPLATE; break;
			case 1: nItemDataType = DPT_DEFAULTPARAM; break;
			case 2: nItemDataType = DPT_BINARIZE; break;
			}
			ShowDataProperty(nItemDataType, pData);
		}
		else {
			ShowDataProperty(nDataType, pData);
		}

		// Show Parent or Sub List
		switch (nDataType) {
		case DPT_DRAWSET:
			m_nSelectedIndex_DrawSet = nIndex;
			ShowList(DPT_DRAWSET_DRAW);
			break;
		case DPT_DRAWSET_DRAW:
			m_ctlDrawSetList.SetCurSel(m_nSelectedIndex_DrawSet);
			break;
		case DPT_DEFECT:
			m_nSelectedIndex_Defect = nIndex;
			ShowList(DPT_PIXEL);
			ShowList(DPT_DEFECT_DRAW);
			m_ctlCheckList.ResetContent();
			break;
		case DPT_PIXEL:
			m_ctlDefectList.SetCurSel(m_nSelectedIndex_Defect);
			m_nSelectedIndex_Pixel = nIndex;
			ShowList(DPT_CHECK);
			break;
		case DPT_CHECK:
			m_ctlDefectList.SetCurSel(m_nSelectedIndex_Defect);
			m_ctlPixelList.SetCurSel(m_nSelectedIndex_Pixel);
			break;
		case DPT_DEFECT_DRAW:
			m_ctlDefectList.SetCurSel(m_nSelectedIndex_Defect);
			break;
		}

		SelectCurrentListOnly(nDataType);
	}
	else {
		SelectCurrentListOnly(DPT_NONE);
	}
}

void CEditInspectionDataDlg::ClearList(bool bInspectionDataOnly)
{
	m_ctlParamList.ResetContent();
	m_ctlApplyList.ResetContent();
	m_ctlDefectList.ResetContent();
	m_ctlPixelList.ResetContent();
	m_ctlCheckList.ResetContent();
	m_ctlDefectDrawList.ResetContent();

	m_nSelectedIndex_Defect = -1;
	m_nSelectedIndex_Pixel = -1;

	if (bInspectionDataOnly) {
		return;
	}

	m_ctlMaskList.ResetContent();
	m_ctlDrawSetList.ResetContent();
	m_ctlDrawSetDrawList.ResetContent();

	m_nSelectedIndex_DrawSet = -1;
}

void CEditInspectionDataDlg::ClearDataProperty()
{
	m_ctlDataProperty.Clear();
	m_ctlDataProperty.Invalidate();
}

void CEditInspectionDataDlg::ShowDataProperty(DATA_PROPERTY_TYPE nDataType, DWORD_PTR pData)
{
	ClearDataProperty();

	if (pData == 0x00) {
		return;
	}

	Process &iProcess = Process::GetInstance();
	VIData &dData = iProcess.GetData();
	GET_INSPECTION_DATA();
	GET_LAYER_DATA();
	GET_DRAWING_DATA();

	wstring strValue;
	CMFCPropertyGridProperty* pParamGrid;

	switch (nDataType) {
	case DPT_MASK:
		{
			VIMask &dMask = *(VIMask *)pData;

			DATA_TO_PROPERTY_METHOD(STR_XML_KEY_TYPE, dMask.GetType);
			DATA_TO_PROPERTY_STRING(STR_XML_KEY_ID, dMask.m_strId);
			DATA_TO_PROPERTY_STRING(STR_XML_KEY_PATH, dMask.m_strPath);
		}
		break;
	case DPT_DRAWSET:
		{
			VIDrawSet &dDrawSet = *(VIDrawSet *)pData;
			DATA_TO_PROPERTY_STRING(STR_XML_KEY_ID, dDrawSet.m_strId);
		}
		break;
	case DPT_DRAWSET_DRAW:
	case DPT_DEFECT_DRAW:
	{
			VIDraw &dDraw = *(VIDraw *)pData;
			DATA_TO_PROPERTY_METHOD(STR_XML_KEY_TYPE, dDraw.GetType);
			DATA_TO_PROPERTY_STRING_COMBO(STR_XML_KEY_DRAW_SET_ID, dDraw.m_strDrawSetId, dDrawing.GetDrawSetIdList(true));
			DATA_TO_PROPERTY_STRING_COMBO(STR_XML_KEY_MASK_ID, dDraw.m_strMaskId, dLayer.GetMaskIdList(VIMask::MASK, true));
			DATA_TO_PROPERTY_COLOR(STR_XML_KEY_COLOR, dDraw.m_dColor);
			DATA_TO_PROPERTY_FLOAT(STR_XML_KEY_ALPHA, dDraw.m_nAlpha);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_EXTEND_SIZE, dDraw.m_nExtendSize);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_FILL_SIZE, dDraw.m_nFillSize);
			DATA_TO_PROPERTY_METHOD(STR_XML_KEY_MODE, dDraw.GetMode);
		}
		break;
	case DPT_TEMPLATE:
		{
			VITemplate &dTemplate = *(VITemplate *)pData;
			DATA_TO_PROPERTY_STRING_COMBO(STR_XML_KEY_ID, dTemplate.m_strId, dLayer.GetMaskIdList(VIMask::TEMPLATE));
			DATA_TO_PROPERTY_METHOD(STR_XML_KEY_ROTATION, dTemplate.GetRotation);
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_HORIZ_FLIP, dTemplate.m_bHorizFlip);
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_VERT_FLIP, dTemplate.m_bHorizFlip);
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_REPLACE, dTemplate.m_bReplace);
			DATA_TO_PROPERTY_FLOAT(STR_XML_KEY_MIN_SIZE_RATIO, dTemplate.m_nMinSizeRatio);
		}
		break;
	case DPT_BINARIZE :
		{
			VIBinarize &dBinarize = *(VIBinarize *)pData;
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_APPLY_WHOLE_PIXEL, dBinarize.m_bApplyWholePixel);
			DATA_TO_PROPERTY_STRING_COMBO(STR_XML_KEY_COLOR_MASK_ID, dBinarize.m_strColorMaskId, dLayer.GetMaskIdList(VIMask::MASK, true));
	}
		break;
	case DPT_DEFAULTPARAM:
		{
			VIDefaultParam &dDefaultParam = *(VIDefaultParam *)pData;
			DATA_TO_PROPERTY_FLOAT(STR_XML_KEY_BINARIZE_THRESHOLD, dDefaultParam.m_nBinarizeThreshold);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_DEFECT_MIN_SIZE, dDefaultParam.m_nMinDefectSize);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_SEED_MIN_SIZE, dDefaultParam.m_nMinSeedSize);
			DATA_TO_PROPERTY_COLOR(STR_XML_KEY_DARK_COLOR, dDefaultParam.m_dDarkColor);
			DATA_TO_PROPERTY_FLOAT(STR_XML_KEY_DARK_COLOR_RATIO, dDefaultParam.m_nDarkColorRatio);
			DATA_TO_PROPERTY_FLOAT(STR_XML_KEY_DARK_COLOR_EXTEND_SIZE_RATIO, dDefaultParam.m_nDarkColorExtendSizeRatio);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_HV_MIN_LENGTH, dDefaultParam.m_nHVMinLength);
			DATA_TO_PROPERTY_FLOAT(STR_XML_KEY_COLOR_SIMILARITY_RED, dDefaultParam.m_nColorSimilarityRed);
			DATA_TO_PROPERTY_FLOAT(STR_XML_KEY_COLOR_SIMILARITY_GREEN, dDefaultParam.m_nColorSimilarityGreen);
			DATA_TO_PROPERTY_FLOAT(STR_XML_KEY_COLOR_SIMILARITY_BLUE, dDefaultParam.m_nColorSimilarityBlue);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_CONNECTED_EXTEND_SIZE, dDefaultParam.m_nConnectedExtendSize);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_CONNECTED_DEFECT_EXTEND_SIZE, dDefaultParam.m_nConnectedDefectExtendSize);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_EXTEND_SIZE, dDefaultParam.m_nExtendSize);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_FILL_SIZE, dDefaultParam.m_nFillSize);
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_ADJUST_IMAGE_BACKGROUND, dDefaultParam.m_bAdjustImageBackground);
		}
		break;
	case DPT_TEMPLATE_PIXEL:
		{
			VITemplatePixel &dTemplatePixel = *(VITemplatePixel *)pData;
			DATA_TO_PROPERTY_STRING_COMBO(STR_XML_KEY_ID, dTemplatePixel.m_strId, dLayer.GetMaskIdList(VIMask::TEMPLATE_PIXEL));
		}
		break;

	case DPT_APPLY:
		{
			VIApply &dApply = *(VIApply *)pData;
			DATA_TO_PROPERTY_STRING_COMBO(STR_XML_KEY_MASK_ID, dApply.m_strMaskId, dLayer.GetMaskIdList(VIMask::MASK));
			DATA_TO_PROPERTY_STRING_COMBO(STR_XML_KEY_COLOR_MASK_ID, dApply.m_strColorMaskId, dLayer.GetMaskIdList(VIMask::MASK, true));
			DATA_TO_PROPERTY_FLOAT(STR_XML_KEY_BINARIZE_THRESHOLD, dApply.m_nBinarizeThreshold);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_DEFECT_MIN_SIZE, dApply.m_nMinDefectSize);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_HV_MIN_LENGTH, dApply.m_nHVMinLength);
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_MERGE_RESULT, dApply.m_bMergeResult);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_MERGE_FILL_SIZE, dApply.m_nMergeFillSize);
		}
		break;
	case DPT_DEFECT:
		{
			VIDefect &dDefect = *(VIDefect *)pData;
			DATA_TO_PROPERTY_STRING(STR_XML_KEY_ID, dDefect.m_strId);
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_DARK, dDefect.m_bDark);
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_SEED, dDefect.m_bSeed);
			DATA_TO_PROPERTY_METHOD(STR_XML_KEY_CONNECTED, dDefect.GetConnected);
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_CONNECTED_DEFECT, dDefect.m_bConnectedDefect);
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_KEEP_PIXEL_SEQ, dDefect.m_bKeepPixelSeq);
			DATA_TO_PROPERTY_STRING_COMBO(STR_XML_KEY_BASE_DEFECT_ID, dDefect.m_strBaseDefectId, dInspection.GetDefectIdList(true));
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_BASE_DEFECT_DRAW, dDefect.m_bBaseDefectDraw);
		}
		break;
	case DPT_PIXEL:
		{
			VIPixel &dPixel = *(VIPixel *)pData;
			DATA_TO_PROPERTY_STRING_COMBO(STR_XML_KEY_BASE_DEFECT_ID, dPixel.m_strBaseDefectId, dInspection.GetDefectIdList(true));
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_BASE_DEFECT_DRAW, dPixel.m_bBaseDefectDraw);
			DATA_TO_PROPERTY_BOOL(STR_XML_KEY_ACCPET_ALL, dPixel.m_bAcceptAll);
	}
		break;
	case DPT_CHECK:
		{
			VICheck &dCheck = *(VICheck *)pData;
			DATA_TO_PROPERTY_STRING_COMBO(STR_XML_KEY_MASK_ID, dCheck.m_strMaskId, dLayer.GetMaskIdList(VIMask::MASK));
			DATA_TO_PROPERTY_STRING_COMBO(STR_XML_KEY_COLOR_MASK_ID, dCheck.m_strColorMaskId, dLayer.GetMaskIdList(VIMask::MASK, true));
			DATA_TO_PROPERTY_METHOD(STR_XML_KEY_STATUS, dCheck.GetStatus);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_DEFECT_MIN_SIZE, dCheck.m_nMinDefectSize);
			DATA_TO_PROPERTY_INT(STR_XML_KEY_CONNECTED_EXTEND_SIZE, dCheck.m_nConnectedExtendSize);
			DATA_TO_PROPERTY_FLOAT(STR_XML_KEY_COLOR_SIMILARITY_RED, dCheck.m_nColorSimilarityRed);
			DATA_TO_PROPERTY_FLOAT(STR_XML_KEY_COLOR_SIMILARITY_GREEN, dCheck.m_nColorSimilarityGreen);
			DATA_TO_PROPERTY_FLOAT(STR_XML_KEY_COLOR_SIMILARITY_BLUE, dCheck.m_nColorSimilarityBlue);
	}
		break;
	default:
		return;
	}

	m_ctlDataProperty.SetData(nDataType, pData);
}

void CEditInspectionDataDlg::UpdateDataProperty()
{
	DATA_PROPERTY_TYPE nDataType;
	DWORD_PTR pData;
	m_ctlDataProperty.GetData(nDataType, pData);

	if (nDataType == DPT_NONE) {
		return;
	}

	CMFCPropertyGridProperty* pParamGrid = nullptr;
	int nCount = m_ctlDataProperty.GetPropertyCount();
	for (int i = 0; i < nCount; i++) {
		pParamGrid = m_ctlDataProperty.GetProperty(i);
		if (pParamGrid == nullptr) {
			continue;
		}

		const wstring strName = pParamGrid->GetName();
		const COleVariant &dValue = pParamGrid->GetValue();

		switch (nDataType) {
		case DPT_MASK:
			{
				VIMask &dMask = *(VIMask *)pData;
				PROPERTY_TO_DATA_METHOD(STR_XML_KEY_TYPE, dMask.SetType);
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_ID, dMask.m_strId);
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_PATH, dMask.m_strPath);
			}
			break;
		case DPT_DRAWSET:
			{
				VIDrawSet &dDrawSet = *(VIDrawSet *)pData;
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_ID, dDrawSet.m_strId);
			}
			break;
		case DPT_DRAWSET_DRAW:
		case DPT_DEFECT_DRAW:
		{
				VIDraw &dDraw = *(VIDraw *)pData;
				PROPERTY_TO_DATA_METHOD(STR_XML_KEY_TYPE, dDraw.SetType);
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_DRAW_SET_ID, dDraw.m_strDrawSetId);
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_MASK_ID, dDraw.m_strMaskId);
				PROPERTY_TO_DATA_COLOR(STR_XML_KEY_COLOR, dDraw.m_dColor);
				PROPERTY_TO_DATA_FLOAT(STR_XML_KEY_ALPHA, dDraw.m_nAlpha);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_EXTEND_SIZE, dDraw.m_nExtendSize);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_FILL_SIZE, dDraw.m_nFillSize);
				PROPERTY_TO_DATA_METHOD(STR_XML_KEY_MODE, dDraw.SetMode);
			}
			break;
		case DPT_TEMPLATE:
			{
				VITemplate &dTemplate = *(VITemplate *)pData;
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_ID, dTemplate.m_strId);
				PROPERTY_TO_DATA_METHOD(STR_XML_KEY_ROTATION, dTemplate.SetRotation);
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_HORIZ_FLIP, dTemplate.m_bHorizFlip);
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_VERT_FLIP, dTemplate.m_bHorizFlip);
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_REPLACE, dTemplate.m_bReplace);
				PROPERTY_TO_DATA_FLOAT(STR_XML_KEY_MIN_SIZE_RATIO, dTemplate.m_nMinSizeRatio);
			}
			break;
		case DPT_BINARIZE:
			{
				VIBinarize &dBinarize = *(VIBinarize *)pData;
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_APPLY_WHOLE_PIXEL, dBinarize.m_bApplyWholePixel);
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_COLOR_MASK_ID, dBinarize.m_strColorMaskId);
			}
			break;

		case DPT_DEFAULTPARAM:
			{
				VIDefaultParam &dDefaultParam = *(VIDefaultParam *)pData;
				PROPERTY_TO_DATA_FLOAT(STR_XML_KEY_BINARIZE_THRESHOLD, dDefaultParam.m_nBinarizeThreshold);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_DEFECT_MIN_SIZE, dDefaultParam.m_nMinDefectSize);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_SEED_MIN_SIZE, dDefaultParam.m_nMinSeedSize);
				PROPERTY_TO_DATA_COLOR(STR_XML_KEY_DARK_COLOR, dDefaultParam.m_dDarkColor);
				PROPERTY_TO_DATA_FLOAT(STR_XML_KEY_DARK_COLOR_RATIO, dDefaultParam.m_nDarkColorRatio);
				PROPERTY_TO_DATA_FLOAT(STR_XML_KEY_DARK_COLOR_EXTEND_SIZE_RATIO, dDefaultParam.m_nDarkColorExtendSizeRatio);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_HV_MIN_LENGTH, dDefaultParam.m_nHVMinLength);
				PROPERTY_TO_DATA_FLOAT(STR_XML_KEY_COLOR_SIMILARITY_RED, dDefaultParam.m_nColorSimilarityRed);
				PROPERTY_TO_DATA_FLOAT(STR_XML_KEY_COLOR_SIMILARITY_GREEN, dDefaultParam.m_nColorSimilarityGreen);
				PROPERTY_TO_DATA_FLOAT(STR_XML_KEY_COLOR_SIMILARITY_BLUE, dDefaultParam.m_nColorSimilarityBlue);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_CONNECTED_EXTEND_SIZE, dDefaultParam.m_nConnectedExtendSize);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_CONNECTED_DEFECT_EXTEND_SIZE, dDefaultParam.m_nConnectedDefectExtendSize);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_EXTEND_SIZE, dDefaultParam.m_nExtendSize);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_FILL_SIZE, dDefaultParam.m_nFillSize);
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_ADJUST_IMAGE_BACKGROUND, dDefaultParam.m_bAdjustImageBackground);
		}
			break;
		case DPT_TEMPLATE_PIXEL:
			{
				VITemplatePixel &dTemplatePixel = *(VITemplatePixel *)pData;
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_ID, dTemplatePixel.m_strId);
			}
			break;
		case DPT_APPLY:
			{
				VIApply &dApply = *(VIApply *)pData;
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_MASK_ID, dApply.m_strMaskId);
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_COLOR_MASK_ID, dApply.m_strColorMaskId);
				PROPERTY_TO_DATA_FLOAT(STR_XML_KEY_BINARIZE_THRESHOLD, dApply.m_nBinarizeThreshold);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_DEFECT_MIN_SIZE, dApply.m_nMinDefectSize);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_HV_MIN_LENGTH, dApply.m_nHVMinLength);
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_MERGE_RESULT, dApply.m_bMergeResult);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_MERGE_FILL_SIZE, dApply.m_nMergeFillSize);
		}
			break;
		case DPT_DEFECT:
			{
				VIDefect &dDefect = *(VIDefect *)pData;
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_ID, dDefect.m_strId);
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_DARK, dDefect.m_bDark);
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_SEED, dDefect.m_bSeed);
				PROPERTY_TO_DATA_METHOD(STR_XML_KEY_CONNECTED, dDefect.SetConnected);
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_CONNECTED_DEFECT, dDefect.m_bConnectedDefect);
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_KEEP_PIXEL_SEQ, dDefect.m_bKeepPixelSeq);
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_BASE_DEFECT_ID, dDefect.m_strBaseDefectId);
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_BASE_DEFECT_DRAW, dDefect.m_bBaseDefectDraw);
			}
			break;
		case DPT_PIXEL:
			{
				VIPixel &dPixel = *(VIPixel *)pData;
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_BASE_DEFECT_ID, dPixel.m_strBaseDefectId);
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_BASE_DEFECT_DRAW, dPixel.m_bBaseDefectDraw);
				PROPERTY_TO_DATA_BOOL(STR_XML_KEY_ACCPET_ALL, dPixel.m_bAcceptAll);
		}
			break;
		case DPT_CHECK:
			{
				VICheck &dCheck = *(VICheck *)pData;
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_MASK_ID, dCheck.m_strMaskId);
				PROPERTY_TO_DATA_STRING(STR_XML_KEY_COLOR_MASK_ID, dCheck.m_strColorMaskId);
				PROPERTY_TO_DATA_METHOD(STR_XML_KEY_STATUS, dCheck.SetStatus);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_DEFECT_MIN_SIZE, dCheck.m_nMinDefectSize);
				PROPERTY_TO_DATA_INT(STR_XML_KEY_CONNECTED_EXTEND_SIZE, dCheck.m_nConnectedExtendSize);
				PROPERTY_TO_DATA_FLOAT(STR_XML_KEY_COLOR_SIMILARITY_RED, dCheck.m_nColorSimilarityRed);
				PROPERTY_TO_DATA_FLOAT(STR_XML_KEY_COLOR_SIMILARITY_GREEN, dCheck.m_nColorSimilarityGreen);
				PROPERTY_TO_DATA_FLOAT(STR_XML_KEY_COLOR_SIMILARITY_BLUE, dCheck.m_nColorSimilarityBlue);
		}
			break;
		}
	}

	SetListItem(nDataType, pData, true);
	SelectListItem(nDataType, pData);
}

void CEditInspectionDataDlg::SaveData(CString strDataPath)
{
	UpdateData();

	Process &iProcess = Process::GetInstance();
	VIData &dData = iProcess.GetData();
	VILayer &dLayer = dData.m_dLayer;
	dLayer.m_strMaskRootPath = m_strMaskRootPath;

	Err nErr = iProcess.SaveData(LPCTSTR(strDataPath));
	if (!ERR_SUCCESS(nErr)) {
		wstring strErr = GetErrMessage(nErr);
		AddLogText(L"Fail to save Data. Err : %s", strErr.c_str());
		return;
	}
	AddLogText(L"Success to save Data : %s", LPCTSTR(strDataPath));
//	ShellExecute(NULL, L"open", LPCTSTR(m_strDataFilePath), NULL, NULL, SW_SHOW);
}

LRESULT CEditInspectionDataDlg::OnDataPropertyChanged(WPARAM wParam, LPARAM lParam)
{
	UpdateDataProperty();
	return 0;
}

void CEditInspectionDataDlg::OnBnClickedMaskListSort()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SortList(DPT_MASK);
}

void CEditInspectionDataDlg::OnBnClickedDrawsetListSort()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SortList(DPT_DRAWSET);
}

void CEditInspectionDataDlg::OnBnClickedTemplatePixelListSort()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SortList(DPT_TEMPLATE_PIXEL);
}

void CEditInspectionDataDlg::OnBnClickedCheckListSort()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SortList(DPT_CHECK);
}

void CEditInspectionDataDlg::SortList(DATA_PROPERTY_TYPE nDataType)
{
	Process &iProcess = Process::GetInstance();
	VIData &dData = iProcess.GetData();

	CListBox *pCtl = GetListCtl(nDataType);
	if (pCtl == nullptr) {
		return;
	}

	switch (nDataType) {
	case DPT_MASK:
		{
			GET_LAYER_DATA();
			DATA_SORT(Layer, Mask);
		}
		break;
	case DPT_DRAWSET:
		{
			GET_DRAWING_DATA();
			DATA_SORT(Drawing, DrawSet);
		}
		break;
	case DPT_TEMPLATE_PIXEL:
		{
			GET_INSPECTION_TEMPLATE_DATA();
			DATA_SORT(Template, TemplatePixel);
		}
		break;
	case DPT_CHECK:
		{
			GET_SELECTED_LIST_ITEM_DATA(Pixel);
			DATA_SORT(Pixel, Check);
		}
		break;
	default:
		return;
	}

	RefreshList(nDataType, pCtl->GetCurSel());
}

void CEditInspectionDataDlg::RefreshList(DATA_PROPERTY_TYPE nDataType, int nSelectedIndex)
{
	CListBox *pCtl = GetListCtl(nDataType);
	if (pCtl == nullptr) {
		return;
	}

	ShowList(nDataType);

	int nCount = pCtl->GetCount();
	if (nSelectedIndex < 0) nSelectedIndex = 0;
	else if (nSelectedIndex > nCount - 1) nSelectedIndex = nCount - 1;

	DWORD_PTR pData = pCtl->GetItemData(nSelectedIndex);
	SelectListItem(nDataType, pData);

	SelectCurrentListOnly(nDataType);
}

void CEditInspectionDataDlg::OnBnClickedMaskAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	AddListItem(DPT_MASK);
}

void CEditInspectionDataDlg::OnBnClickedDrawsetAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	AddListItem(DPT_DRAWSET);
}

void CEditInspectionDataDlg::OnBnClickedDrawsetDrawAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	AddListItem(DPT_DRAWSET_DRAW);
}

void CEditInspectionDataDlg::OnBnClickedTemplatePixelAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	AddListItem(DPT_TEMPLATE_PIXEL);
}

void CEditInspectionDataDlg::OnBnClickedApplyAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	AddListItem(DPT_APPLY);
}

void CEditInspectionDataDlg::OnBnClickedDefectAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	AddListItem(DPT_DEFECT);
}

void CEditInspectionDataDlg::OnBnClickedPixelAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	AddListItem(DPT_PIXEL);
}

void CEditInspectionDataDlg::OnBnClickedCheckAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	AddListItem(DPT_CHECK);
}

void CEditInspectionDataDlg::OnBnClickedDefectDrawAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	AddListItem(DPT_DEFECT_DRAW);
}

void CEditInspectionDataDlg::AddListItem(DATA_PROPERTY_TYPE nDataType)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Process &iProcess = Process::GetInstance();
	VIData &dData = iProcess.GetData();

	CListBox *pCtl = GetListCtl(nDataType);
	if (pCtl == nullptr) {
		return;
	}

	DWORD_PTR pData;
	switch (nDataType) {
	case DPT_MASK:
		{
			GET_LAYER_DATA();
			DATA_ADD(Layer, Mask);
			dMask.m_strId = L"MASK_NEW";
		}
		break;
	case DPT_DRAWSET:
		{
			GET_DRAWING_DATA();
			DATA_ADD(Drawing, DrawSet);
			dDrawSet.m_strId = L"DRAW_SET_NEW";
		}
		break;
	case DPT_DRAWSET_DRAW:
		{
			if (m_nSelectedIndex_DrawSet < 0) {
				return;
			}
			m_ctlDrawSetList.SetCurSel(m_nSelectedIndex_DrawSet);
			GET_SELECTED_LIST_ITEM_DATA(DrawSet);
			DATA_ADD(DrawSet, Draw);
		}
		break;
	case DPT_TEMPLATE_PIXEL:
		{
			GET_INSPECTION_TEMPLATE_DATA();
			DATA_ADD(Template, TemplatePixel);
			dTemplatePixel.m_strId = L"TEMPLATE_PIXEL_NEW";
		}
		break;
	case DPT_APPLY:
		{
			GET_INSPECTION_BINARIZE_DATA();
			DATA_ADD(Binarize, Apply);
			dApply.m_strMaskId = L"MASK_NEW";
		}
		break;
	case DPT_DEFECT:
		{
			GET_INSPECTION_DATA();
			DATA_ADD(Inspection, Defect);
			dDefect.m_strId = L"DEFECT_NEW";
		}
		break;
	case DPT_PIXEL:
		{
			if (pCtl->GetCount() >= 2) {	// maximum count of pixels is 2
				return;
			}
			if (m_nSelectedIndex_Defect < 0) {
				return;
			}
			m_ctlDefectList.SetCurSel(m_nSelectedIndex_Defect);
			GET_SELECTED_LIST_ITEM_DATA(Defect);
			DATA_ADD(Defect, Pixel);
		}
		break;
	case DPT_CHECK:
		{
			if (m_nSelectedIndex_Defect < 0 || m_nSelectedIndex_Pixel < 0) {
				return;
			}
			m_ctlDefectList.SetCurSel(m_nSelectedIndex_Defect);
			m_ctlPixelList.SetCurSel(m_nSelectedIndex_Pixel);
			GET_SELECTED_LIST_ITEM_DATA(Pixel);
			DATA_ADD(Pixel, Check);
			dCheck.m_strMaskId = L"MASK_NEW";
		}
		break;
	case DPT_DEFECT_DRAW:
		{
			if (m_nSelectedIndex_Defect < 0) {
				return;
			}
			m_ctlDefectList.SetCurSel(m_nSelectedIndex_Defect);
			GET_SELECTED_LIST_ITEM_DATA(Defect);
			DATA_ADD(Defect, Draw);
		}
		break;
	default:
		return;
	}

	SetListItem(nDataType, pData);
	SelectListItem(nDataType, pData);
}

void CEditInspectionDataDlg::OnBnClickedMaskDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	DeleteListItem(DPT_MASK);
}

void CEditInspectionDataDlg::OnBnClickedDrawsetDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	DeleteListItem(DPT_DRAWSET);
}

void CEditInspectionDataDlg::OnBnClickedDrawsetDrawDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	DeleteListItem(DPT_DRAWSET_DRAW);
}

void CEditInspectionDataDlg::OnBnClickedTemplatePixelDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	DeleteListItem(DPT_TEMPLATE_PIXEL);
}

void CEditInspectionDataDlg::OnBnClickedApplyDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	DeleteListItem(DPT_APPLY);
}

void CEditInspectionDataDlg::OnBnClickedDefectDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	DeleteListItem(DPT_DEFECT);
}

void CEditInspectionDataDlg::OnBnClickedPixelDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	DeleteListItem(DPT_PIXEL);
}

void CEditInspectionDataDlg::OnBnClickedCheckDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	DeleteListItem(DPT_CHECK);
}

void CEditInspectionDataDlg::OnBnClickedDefectDrawDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	DeleteListItem(DPT_DEFECT_DRAW);
}

void CEditInspectionDataDlg::DeleteListItem(DATA_PROPERTY_TYPE nDataType)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Process &iProcess = Process::GetInstance();
	VIData &dData = iProcess.GetData();

	CListBox *pCtl = GetListCtl(nDataType);
	if (pCtl == nullptr) {
		return;
	}

	int nIndex = pCtl->GetCurSel();
	if (nIndex < 0) {
		return;
	}

	switch (nDataType) {
	case DPT_MASK:
		{
			GET_LAYER_DATA();
			DATA_DELETE(Layer, Mask);
		}
		break;
	case DPT_DRAWSET:
		{
			GET_DRAWING_DATA();
			DATA_DELETE(Drawing, DrawSet);
		}
		break;
	case DPT_DRAWSET_DRAW:
		{
			GET_SELECTED_LIST_ITEM_DATA(DrawSet);
			DATA_DELETE(DrawSet, Draw);
		}
		break;
	case DPT_TEMPLATE_PIXEL:
		{
			GET_INSPECTION_TEMPLATE_DATA();
			DATA_DELETE(Template, TemplatePixel);
		}
		break;
	case DPT_APPLY:
		{
			GET_INSPECTION_BINARIZE_DATA();
			DATA_DELETE(Binarize, Apply);
		}
		break;
	case DPT_DEFECT:
		{
			GET_INSPECTION_DATA();
			DATA_DELETE(Inspection, Defect);
		}
		break;
	case DPT_PIXEL:
		{
			GET_SELECTED_LIST_ITEM_DATA(Defect);
			DATA_DELETE(Defect, Pixel);
		}
		break;
	case DPT_CHECK:
		{
			GET_SELECTED_LIST_ITEM_DATA(Pixel);
			DATA_DELETE(Pixel, Check);
		}
		break;
	case DPT_DEFECT_DRAW:
		{
			GET_SELECTED_LIST_ITEM_DATA(Defect);
			DATA_DELETE(Defect, Draw);
		}
		break;
	}

	int nCount = pCtl->DeleteString(nIndex);
	if (nCount > 0) {
		if (nIndex >= nCount) nIndex = nCount - 1;
		pCtl->SetCurSel(nIndex);
		OnSelectListItem(nDataType);
	}
	else {
		ClearDataProperty();
	}
}

void CEditInspectionDataDlg::OnBnClickedDrawsetDrawUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OrderUpListItem(DPT_DRAWSET_DRAW);
}

void CEditInspectionDataDlg::OnBnClickedApplyUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OrderUpListItem(DPT_APPLY);
}

void CEditInspectionDataDlg::OnBnClickedDefectUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OrderUpListItem(DPT_DEFECT);
}

void CEditInspectionDataDlg::OnBnClickedPixelUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OrderUpListItem(DPT_PIXEL);
}

void CEditInspectionDataDlg::OnBnClickedDefectDrawUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OrderUpListItem(DPT_DEFECT_DRAW);
}

void CEditInspectionDataDlg::OrderUpListItem(DATA_PROPERTY_TYPE nDataType)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Process &iProcess = Process::GetInstance();
	VIData &dData = iProcess.GetData();

	CListBox *pCtl = GetListCtl(nDataType);
	if (pCtl == nullptr) {
		return;
	}

	int nIndex = pCtl->GetCurSel();
	if (nIndex < 0) {
		return;
	}
	DWORD_PTR pData = pCtl->GetItemData(nIndex);

	switch (nDataType) {
	case DPT_DRAWSET_DRAW:
		{
			GET_SELECTED_LIST_ITEM_DATA(DrawSet);
			DATA_ORDER_UP(DrawSet, Draw);
		}
		break;
	case DPT_APPLY:
		{
			GET_INSPECTION_BINARIZE_DATA();
			DATA_ORDER_UP(Binarize, Apply);
		}
		break;
	case DPT_DEFECT:
		{
			GET_INSPECTION_DATA();
			DATA_ORDER_UP(Inspection, Defect);
		}
		break;
	case DPT_PIXEL:
		{
			GET_SELECTED_LIST_ITEM_DATA(Defect);
			DATA_ORDER_UP(Defect, Pixel);
		}
		break;
	case DPT_DEFECT_DRAW:
		{
			GET_SELECTED_LIST_ITEM_DATA(Defect);
			DATA_ORDER_UP(Defect, Draw);
		}
		break;
	}

	RefreshList(nDataType, nIndex - 1);
}

void CEditInspectionDataDlg::OnBnClickedDrawsetDrawDown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OrderDownListItem(DPT_DRAWSET_DRAW);
}

void CEditInspectionDataDlg::OnBnClickedApplyDown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OrderDownListItem(DPT_APPLY);
}

void CEditInspectionDataDlg::OnBnClickedDefectDown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OrderDownListItem(DPT_DEFECT);
}

void CEditInspectionDataDlg::OnBnClickedPixelDown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OrderDownListItem(DPT_PIXEL);
}

void CEditInspectionDataDlg::OnBnClickedDefectDrawDown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OrderDownListItem(DPT_DEFECT_DRAW);
}

void CEditInspectionDataDlg::OrderDownListItem(DATA_PROPERTY_TYPE nDataType)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Process &iProcess = Process::GetInstance();
	VIData &dData = iProcess.GetData();

	CListBox *pCtl = GetListCtl(nDataType);
	if (pCtl == nullptr) {
		return;
	}

	int nIndex = pCtl->GetCurSel();
	if (nIndex < 0) {
		return;
	}
	DWORD_PTR pData = pCtl->GetItemData(nIndex);

	switch (nDataType) {
	case DPT_DRAWSET_DRAW:
		{
			GET_SELECTED_LIST_ITEM_DATA(DrawSet);
			DATA_ORDER_DOWN(DrawSet, Draw);
		}
		break;
	case DPT_APPLY:
		{
			GET_INSPECTION_BINARIZE_DATA();
			DATA_ORDER_DOWN(Binarize, Apply);
		}
		break;
	case DPT_DEFECT:
		{
			GET_INSPECTION_DATA();
			DATA_ORDER_DOWN(Inspection, Defect);
		}
		break;
	case DPT_PIXEL:
		{
			GET_SELECTED_LIST_ITEM_DATA(Defect);
			DATA_ORDER_DOWN(Defect, Pixel);
		}
		break;
	case DPT_DEFECT_DRAW:
		{
			GET_SELECTED_LIST_ITEM_DATA(Defect);
			DATA_ORDER_DOWN(Defect, Draw);
		}
		break;
	}

	RefreshList(nDataType, nIndex + 1);
}

void CEditInspectionDataDlg::OnClickedLayerRadio(UINT nID)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeLayer();
}

void CEditInspectionDataDlg::ChangeLayer()
{
	UpdateData();
	SelectCurrentListOnly(DPT_NONE);
	ClearList(true);

	ShowList(DPT_PARAM);
	ShowList(DPT_TEMPLATE_PIXEL);
	ShowList(DPT_APPLY);
	ShowList(DPT_DEFECT);
}

void CEditInspectionDataDlg::SelectCurrentListOnly(DATA_PROPERTY_TYPE nDataType)
{
	if (nDataType != DPT_MASK) m_ctlMaskList.SetCurSel(-1);
	if (nDataType != DPT_DRAWSET && nDataType != DPT_DRAWSET_DRAW) m_ctlDrawSetList.SetCurSel(-1);
	if (nDataType != DPT_DRAWSET_DRAW) m_ctlDrawSetDrawList.SetCurSel(-1);
	if (nDataType != DPT_PARAM && nDataType != DPT_TEMPLATE && nDataType != DPT_DEFAULTPARAM) m_ctlParamList.SetCurSel(-1);
	if (nDataType != DPT_TEMPLATE_PIXEL) m_ctlTemplatePixelList.SetCurSel(-1);
	if (nDataType != DPT_APPLY) m_ctlApplyList.SetCurSel(-1);
	if (nDataType != DPT_DEFECT && nDataType != DPT_PIXEL && nDataType != DPT_CHECK && nDataType != DPT_DEFECT_DRAW) m_ctlDefectList.SetCurSel(-1);
	if (nDataType != DPT_PIXEL && nDataType != DPT_CHECK) m_ctlPixelList.SetCurSel(-1);
	if (nDataType != DPT_CHECK) m_ctlCheckList.SetCurSel(-1);
	if (nDataType != DPT_DEFECT_DRAW) m_ctlDefectDrawList.SetCurSel(-1);

	if (nDataType == DPT_NONE) {
		ClearDataProperty();
	}
}

CListBox *CEditInspectionDataDlg::GetListCtl(DATA_PROPERTY_TYPE nDataType)
{
	CListBox *pCtl = nullptr;
	switch (nDataType) {
	case DPT_MASK: pCtl = &m_ctlMaskList; break;
	case DPT_DRAWSET: pCtl = &m_ctlDrawSetList; break;
	case DPT_DRAWSET_DRAW: pCtl = &m_ctlDrawSetDrawList; break;
	case DPT_PARAM: 
	case DPT_TEMPLATE:
	case DPT_DEFAULTPARAM: 
	case DPT_BINARIZE: pCtl = &m_ctlParamList; break;
	case DPT_TEMPLATE_PIXEL: pCtl = &m_ctlTemplatePixelList; break;
	case DPT_APPLY: pCtl = &m_ctlApplyList; break;
	case DPT_DEFECT: pCtl = &m_ctlDefectList; break;
	case DPT_PIXEL: pCtl = &m_ctlPixelList; break;
	case DPT_CHECK: pCtl = &m_ctlCheckList; break;
	case DPT_DEFECT_DRAW: pCtl = &m_ctlDefectDrawList; break;
	}

	return pCtl;
}

void CEditInspectionDataDlg::OnBnClickedOpenDataFile()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ShellExecute(NULL, L"open", LPCTSTR(m_strDataFilePath), NULL, NULL, SW_SHOW);
}

void CEditInspectionDataDlg::OnBnClickedBackupData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strBackupDataFilePath = m_strDataFilePath + L".backup";
	SaveData(strBackupDataFilePath);

	AddLogText(L"Finish to Backup Data");
}


void CEditInspectionDataDlg::OnBnClickedRestoreData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strBackupDataFilePath = m_strDataFilePath + L".backup";
	LoadData(strBackupDataFilePath);

	AddLogText(L"Finish to Restore Data");
}
