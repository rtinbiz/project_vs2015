#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "afxpropertygridctrl.h"

#define WM_UPDATE_INSPECTION_DATA (WM_USER+705)

enum DATA_PROPERTY_TYPE {
	DPT_NONE = 0,
	DPT_MASK,
	DPT_DRAWSET,
	DPT_DRAWSET_DRAW,
	DPT_PARAM,
	DPT_TEMPLATE,
	DPT_TEMPLATE_PIXEL,
	DPT_BINARIZE,
	DPT_DEFAULTPARAM,
	DPT_APPLY,
	DPT_DEFECT,
	DPT_PIXEL,
	DPT_CHECK,
	DPT_DEFECT_DRAW,
	DPT_COUNT
};

class CMFCPropertyGridCtrlEx : public CMFCPropertyGridCtrl
{
protected :
	DATA_PROPERTY_TYPE m_nDataType;
	DWORD_PTR m_pData;

public:
	CMFCPropertyGridCtrlEx() {
		m_nLeftColumnWidth = 250;

		m_nDataType = DPT_NONE;
		m_pData = 0x00;
	}

	void SetData(DATA_PROPERTY_TYPE nType, DWORD_PTR pData) {
		m_nDataType = nType;
		m_pData = pData;
	}

	void GetData(DATA_PROPERTY_TYPE &nType, DWORD_PTR &pData) {
		nType = m_nDataType;
		pData = m_pData;
	}

	void Clear() {
		RemoveAll();

		m_nDataType = DPT_NONE;
		m_pData = 0x00;
	}

};


class CMFCPropertyGridPropertyEx : public CMFCPropertyGridProperty
{
public:
	int GetMinValue() { return m_nMinValue; }
	int GetMaxValue() { return m_nMaxValue; }
};


// CEditInspectionDataDlg 대화 상자입니다.

class CEditInspectionDataDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CEditInspectionDataDlg)

private:
	CFont m_fntData;
	int m_nSelectedIndex_DrawSet;
	int m_nSelectedIndex_Defect;
	int m_nSelectedIndex_Pixel;

	Layer m_nLayer;
	
	CWnd *m_pMainDlg;

public:
	CEditInspectionDataDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CEditInspectionDataDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_EDIT_INSPECTION_DATA };

	void Create(CWnd *pMainDlg);

	void SetDataPropertyCtrlFont();
	void AddLogText(const wchar_t* szText, ...);

	void LoadData(CString strDataPath, bool bUpdateDataPath = false);
	void SaveData(CString strDataPath);

	void ClearList(bool bInspectionDataOnly = false);
	void ShowList(DATA_PROPERTY_TYPE nDataType);
	void SelectListItem(DATA_PROPERTY_TYPE nDataType, DWORD_PTR pData);
	void OnSelectListItem(DATA_PROPERTY_TYPE nDataType);

	void ClearDataProperty();
	void ShowDataProperty(DATA_PROPERTY_TYPE nDataType, DWORD_PTR pData = 0x00);

	void UpdateDataProperty();
	void SetListItem(DATA_PROPERTY_TYPE nDatatType, DWORD_PTR pData, bool bUpdate = false);

	void AddListItem(DATA_PROPERTY_TYPE nDataType);
	void DeleteListItem(DATA_PROPERTY_TYPE nDataType);
	void OrderUpListItem(DATA_PROPERTY_TYPE nDataType);
	void OrderDownListItem(DATA_PROPERTY_TYPE nDataType);

	void SortList(DATA_PROPERTY_TYPE nDataType);
	void SelectCurrentListOnly(DATA_PROPERTY_TYPE nDataType);

	void ChangeLayer();

	void RefreshList(DATA_PROPERTY_TYPE nDataType, int nSelectedIndex);
	CListBox *GetListCtl(DATA_PROPERTY_TYPE nDataType);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_ctlLogList;
	CString m_strDataFilePath;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedSaveData();
	afx_msg void OnBnClickedLoadData();
	CListBox m_ctlMaskList;
	afx_msg void OnSelchangeMaskList();
	CMFCPropertyGridCtrlEx m_ctlDataProperty;
	CString m_strMaskRootPath;
	afx_msg LRESULT OnDataPropertyChanged(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedMaskListSort();
	afx_msg void OnBnClickedMaskAdd();
	afx_msg void OnBnClickedMaskDelete();
	afx_msg void OnBnClickedDrawsetListSort();
	afx_msg void OnBnClickedDrawsetAdd();
	afx_msg void OnBnClickedDrawsetDelete();
	afx_msg void OnBnClickedDrawsetDrawUp();
	afx_msg void OnBnClickedDrawsetDrawDown();
	afx_msg void OnBnClickedDrawsetDrawAdd();
	afx_msg void OnBnClickedDrawsetDrawDelete();
	CListBox m_ctlDrawSetList;
	CListBox m_ctlDrawSetDrawList;
	afx_msg void OnSelchangeDrawsetDrawList();
	afx_msg void OnSelchangeDrawsetList();
	CListBox m_ctlParamList;
	CListBox m_ctlApplyList;
	CListBox m_ctlDefectList;
	CListBox m_ctlPixelList;
	CListBox m_ctlCheckList;
	CListBox m_ctlDefectDrawList;
	afx_msg void OnBnClickedCheckListSort();
	afx_msg void OnBnClickedApplyAdd();
	afx_msg void OnBnClickedDefectAdd();
	afx_msg void OnBnClickedPixelAdd();
	afx_msg void OnBnClickedCheckAdd();
	afx_msg void OnBnClickedDefectDrawAdd();
	afx_msg void OnBnClickedApplyDelete();
	afx_msg void OnBnClickedDefectDelete();
	afx_msg void OnBnClickedPixelDelete();
	afx_msg void OnBnClickedCheckDelete();
	afx_msg void OnBnClickedDefectDrawDelete();
	afx_msg void OnBnClickedDefectUp();
	afx_msg void OnBnClickedDefectDown();
	afx_msg void OnBnClickedPixelUp();
	afx_msg void OnBnClickedDefectDrawUp();
	afx_msg void OnBnClickedPixelDown();
	afx_msg void OnBnClickedDefectDrawDown();
	afx_msg void OnClickedLayerRadio(UINT nID);
	int m_nLayerIndex;
	afx_msg void OnSelchangeApplyList();
	afx_msg void OnSelchangeCheckList();
	afx_msg void OnSelchangeDefctList();
	afx_msg void OnSelchangeDefectDrawList();
	afx_msg void OnSelchangeParamList();
	afx_msg void OnSelchangePixelList();
	afx_msg void OnBnClickedOpenDataFile();
	CListBox m_ctlTemplatePixelList;
	afx_msg void OnBnClickedTemplatePixelListSort();
	afx_msg void OnBnClickedTemplatePixelAdd();
	afx_msg void OnBnClickedTemplatePixelDelete();
	afx_msg void OnSelchangeTemplatePixelList();
	afx_msg void OnBnClickedApplyUp();
	afx_msg void OnBnClickedApplyDown();
	afx_msg void OnBnClickedBackupData();
	afx_msg void OnBnClickedRestoreData();
};
