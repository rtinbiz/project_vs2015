#include "stdafx.h"
#include "NIPLUnitTest.h"

class NIPL_Test : public Test {
protected:
	virtual void SetUp() {
		wchar_t szModulePath[MAX_PATH];
		GetModuleFileName(NULL, szModulePath, MAX_PATH - 1);
		PathRemoveFileSpec(szModulePath);

		_strDataPath = szModulePath;
		_strDataPath += TEST_DATA_PATH;

		_strInputImagePath = _strDataPath + L"Input.bmp";
		_strInvalidInputImagePath = _strDataPath + L"Invalid_Input.bmp";
		_strOutputImagePath = _strDataPath + L"Output.bmp";

		_pNIPL = NIPL::GetInstance();
	}

	wstring _strDataPath;
	wstring _strInputImagePath;
	wstring _strInvalidInputImagePath;
	wstring _strOutputImagePath;

	shared_ptr<NIPL> _pNIPL;
};

TEST_F(NIPL_Test, CreateImage) {
	NIPLInput dInput;
	NIPLOutput dOutput;
	NIPL_ERR nErr;

	// Success Cases
	NIPLParam_CreateImage dParam_CreateImage;
	dInput.m_pParam = &dParam_CreateImage;

	int nWidth = 100;
	int nHeight = 100;
	dParam_CreateImage.m_nType = NIPLParam_CreateImage::TYPE_GRAY_UINT8;
	dParam_CreateImage.m_nWidth = nWidth;
	dParam_CreateImage.m_nHeight = nHeight;
	dParam_CreateImage.m_nValue = 0;

	nErr = _pNIPL->CreateImage(&dInput, &dOutput);
	int nWidthOutputImg = dOutput.m_dImg.cols;
	int nHeightOutputImg = dOutput.m_dImg.rows;
	EXPECT_TRUE(NIPL_SUCCESS(nErr) && nWidth == nWidthOutputImg && nHeight == nHeightOutputImg);

	float nValue = 100.f;
	dParam_CreateImage.m_nValue = nValue;
	nErr = _pNIPL->CreateImage(&dInput, &dOutput);
	float nMean = static_cast<float>(mean(dOutput.m_dImg)[0]);
	EXPECT_TRUE(NIPL_SUCCESS(nErr) && nMean == nValue);

	//
	// Fail Case
	//
	// no input argument
	nErr = _pNIPL->CreateImage(nullptr, &dOutput);
	EXPECT_TRUE(nErr == NIPL_ERR_NO_PARAMS);

	// no output argument
	nErr = _pNIPL->CreateImage(&dInput, nullptr);
	EXPECT_TRUE(nErr == NIPL_ERR_NO_PARAMS);

	// no parameter set in input argument
	dInput.m_pParam = nullptr;
	nErr = _pNIPL->CreateImage(&dInput, &dOutput);
	EXPECT_TRUE(nErr == NIPL_ERR_NO_PARAMS);

	// Invalid Case
	dInput.m_pParam = &dParam_CreateImage;
	dParam_CreateImage.m_nType = NIPLParam_CreateImage::TYPE_GRAY_UINT8;
	dParam_CreateImage.m_nWidth = 0;
	dParam_CreateImage.m_nHeight = 100;
	nErr = _pNIPL->CreateImage(&dInput, &dOutput);
	EXPECT_TRUE(nErr == NIPL_ERR_INVALID_PARAM_VALUE);

	dParam_CreateImage.m_nWidth = 100;
	dParam_CreateImage.m_nHeight = 0;
	nErr = _pNIPL->CreateImage(&dInput, &dOutput);
	EXPECT_TRUE(nErr == NIPL_ERR_INVALID_PARAM_VALUE);

	// Extreme Case
	dParam_CreateImage.m_nWidth = 1000000;
	dParam_CreateImage.m_nHeight = 1000000;
	nErr = _pNIPL->CreateImage(&dInput, &dOutput);
	EXPECT_TRUE(nErr == NIPL_ERR_INVALID_PARAM_VALUE);
}

TEST_F(NIPL_Test, LoadImage) {
	NIPLInput dInput;
	NIPLOutput dOutput;
	NIPL_ERR nErr;

	Mat dInputImg;

	// 
	// Success Case
	//
	nErr = _pNIPL->LoadImage(_strInputImagePath, dInputImg);
	EXPECT_TRUE(NIPL_SUCCESS(nErr));

	// 
	// Fail Case
	//
	nErr = _pNIPL->LoadImage(_strInvalidInputImagePath, dInputImg);
	EXPECT_TRUE(nErr == NIPL_ERR_FAIL_TO_LOAD_IMAGE);
}

TEST_F(NIPL_Test, SaveImage) {
	NIPLInput dInput;
	NIPLOutput dOutput;
	NIPL_ERR nErr;

	Mat dInputImg;
	nErr = _pNIPL->LoadImage(_strInputImagePath, dInputImg);
	ASSERT_TRUE(NIPL_SUCCESS(nErr));

	// 
	// Success Case
	//

	nErr = _pNIPL->SaveImage(_strOutputImagePath, dInputImg);
	EXPECT_TRUE(NIPL_SUCCESS(nErr));
}

/*
TEST_F(NIPL_Test, Color2Gray) {
	NIPLInput dInput;
	NIPLOutput dOutput;
	NIPL_ERR nErr;

	Mat dInputImg;
	nErr = _pNIPL->LoadImage(_strInputImagePath, dInputImg);
	ASSERT_TRUE(NIPL_SUCCESS(nErr));


	// Success Cases
	NIPLParam_Color2Gray dParam_CreateImage;
	dInput.m_pParam = &dParam_CreateImage;

	int nWidth = 100;
	int nHeight = 100;
	dParam_CreateImage.m_nType = NIPLParam_CreateImage::TYPE_GRAY_UINT8;
	dParam_CreateImage.m_nWidth = nWidth;
	dParam_CreateImage.m_nHeight = nHeight;
	dParam_CreateImage.m_nValue = 0;

	nErr = _pNIPL->SaveImage(_strOutputImagePath, dInputImg);
	EXPECT_TRUE(NIPL_SUCCESS(nErr));
}
*/