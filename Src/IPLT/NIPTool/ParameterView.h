
#pragma once

#include "NIPToolDoc.h"

class CMFCPropertyGridPropertyEx : public CMFCPropertyGridProperty
{
public :
	int GetMinValue() { return m_nMinValue; }
	int GetMaxValue() { return m_nMaxValue; }
};

class CParameterToolBar : public CMFCToolBar
{
public:
	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}

	virtual BOOL AllowShowOnList() const { return FALSE; }
};

class CParameterView : public CDockablePane
{
// Construction
public:
	CParameterView();

	void AdjustLayout();

// Attributes
public:
	void SetVSDotNetLook(BOOL bSet)
	{
		m_wndParamList.SetVSDotNetLook(bSet);
		m_wndParamList.SetGroupNameFullWidth(bSet);
	}

protected:
	CFont m_fntParamList;
	CStatic m_wndName;
	CParameterToolBar m_wndToolBar;
	CMFCPropertyGridCtrl m_wndParamList;
	NIPJobProcess m_dProcess;
	bool m_bMenuProcess;


// Implementation
public:
	virtual ~CParameterView();
	void UpdateParameter(NIPJobProcess *pProcess, bool bMenu = false);
	void Clear();

protected:
	CNIPToolDoc *GetDocument();
	void AddPropGroup(NIPJobProcess *pProcess, CMFCPropertyGridProperty *pParentPropGroup = nullptr);
	void AddPropGroup(NIPJobParam *pGroup, CMFCPropertyGridProperty *pParentPropGroup = nullptr);
	void AddProp(NIPJobParam *pParam, CMFCPropertyGridProperty *pGroup = nullptr);

	void SetPropListToProcess();
	void SetPropGroupToSubProcess(CMFCPropertyGridProperty *pPropGroup, NIPJobProcess *pProcess);
	void SetPropGroupToParamGroup(CMFCPropertyGridProperty *pPropGroup, void *pParent, bool bProcess);
	void SetPropToParam(CMFCPropertyGridProperty *pParamGrid, void *pParent, bool bProcess);

	void ChangeProcess();

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnExpandAllParameter();
	afx_msg void OnUpdateExpandAllParameter(CCmdUI* pCmdUI);
//	afx_msg void OnSortParameter();
//	afx_msg void OnUpdateSortParameter(CCmdUI* pCmdUI);
	afx_msg void OnRunProcess();
	afx_msg void OnUpdateRunProcess(CCmdUI* pCmdUI);
	afx_msg void OnAddProcess();
	afx_msg void OnUpdateAddProcess(CCmdUI* pCmdUI);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
	afx_msg LRESULT OnPropertyChanged(WPARAM wp, LPARAM lp);

	DECLARE_MESSAGE_MAP()

	void InitParamList();
	void SetParamListFont();


//	int m_nComboHeight;
public:
//	virtual void PostNcDestroy();
	virtual void PostNcDestroy();
};

