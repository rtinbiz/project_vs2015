// This MFC Samples source code demonstrates using MFC Microsoft Office Fluent User Interface 
// (the "Fluent UI") and is provided only as referential material to supplement the 
// Microsoft Foundation Classes Reference and related electronic documentation 
// included with the MFC C++ library software.  
// License terms to copy, use or distribute the Fluent UI are available separately.  
// To learn more about our Fluent UI licensing program, please visit 
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// Copyright (C) Microsoft Corporation
// All rights reserved.

#pragma once

/////////////////////////////////////////////////////////////////////////////
// CLogList window

class CLogList : public CListBox
{
// Construction
public:
	CLogList();

// Implementation
public:
	virtual ~CLogList();

protected:
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnEditCopy();
	afx_msg void OnEditClear();
	afx_msg void OnViewLog();

	DECLARE_MESSAGE_MAP()
};

class CLogView : public CDockablePane
{
// Construction
public:
	CLogView();

	void UpdateFonts();
	void ProcessNotify(wstring strName, NIPL_ERR nErr, DWORD nTime);
	void AddLog(wstring strText);

// Attributes
protected:
	CMFCTabCtrl	m_wndTabs;
	CLogList m_wndLogOutput;

protected:

	void AdjustHorzScroll(CListBox& wndListBox);

// Implementation
public:
	virtual ~CLogView();

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
};

