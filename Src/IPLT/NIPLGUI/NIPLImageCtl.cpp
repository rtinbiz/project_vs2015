// NIPLImageCtl.cpp : implementation file
//

#include "stdafx.h"
#include "NIPLGUI.h"
#include "NIPLImageCtl.h"

#define DEFAULT_VIEW_WIDTH 1600
#define DEFAULT_VIEW_HEIGHT 800

// NIPLImageCtl

vector<NIPLImageCtl *> NIPLImageCtl::listSyncImageTarget;
SYNC_IMAGE_DATA NIPLImageCtl::dSyncImage;

IMPLEMENT_DYNAMIC(NIPLImageCtl, CWnd)

NIPLImageCtl::NIPLImageCtl()
{
	m_nScrollSizeX = 0;
	m_nScrollPageSizeX = 0;
	m_nScrollMaxPosX = 0;
	m_nScrollSizeY = 0;
	m_nScrollPageSizeY = 0;
	m_nScrollMaxPosY = 0;
	m_nZoom = 1.0f;
	m_nZoomBefore = 1.0f;
	m_nMinZoom = 0.1f;
	m_nMaxZoom = 50.f;
	m_nImgSizeX = 0;
	m_nImgSizeY = 0;
	m_bGrab = false;
	m_bDrag = false;
	m_bSyncImageTarget = false;
	m_bSyncImageHost = false;
	m_bDoingSyncImage = false;
	m_bKeepPosAndZoom = false;
	m_bShowImagePos = false;
	m_bShowMask = false;

	m_bSendEventMessage = true;
}

NIPLImageCtl::~NIPLImageCtl()
{
}

BEGIN_MESSAGE_MAP(NIPLImageCtl, CWnd)
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_SHOWWINDOW()
	ON_WM_NCHITTEST()
	ON_WM_MOUSEHWHEEL()
	ON_WM_MOUSEWHEEL()
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_SETCURSOR()
	ON_WM_KEYUP()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()

// NIPLImageCtl message handlers
void NIPLImageCtl::PostNcDestroy()
{
	CWnd::PostNcDestroy();

	SetSyncImageTarget(false);

	Clear();
}

BOOL NIPLImageCtl::CreateCtl(CWnd* pParentWnd, UINT nID)
{
	CRect rc(0, 0, DEFAULT_VIEW_WIDTH,DEFAULT_VIEW_HEIGHT);
	if (pParentWnd) {
		CWnd *pCtl = pParentWnd->GetDlgItem(nID);
		if (pCtl) {
			pCtl->GetWindowRect(rc);
			pParentWnd->ScreenToClient(rc);
			pCtl->DestroyWindow();
		}
	}

	return CWnd::CreateEx(WS_EX_STATICEDGE/*WS_EX_CLIENTEDGE*/, NULL, NULL, WS_CHILD | WS_VISIBLE, rc, pParentWnd, nID);
}

void NIPLImageCtl::Clear()
{
	m_dViewImage.Destroy();
}

void NIPLImageCtl::ShowBackground()
{
	Mat dImg = Mat::zeros(1, 1, CV_8UC1);
	Show(dImg);
}

void NIPLImageCtl::Show(Mat dImg, bool m_bKeepPosAndZoom)
{
	if (CHECK_EMPTY_IMAGE(dImg)) {
		ShowBackground();
		return;
	}

	m_dImg = Mat::zeros(dImg.size(), CV_8UC3);

	int nChannel = dImg.channels();
	int nDepth = dImg.depth();
	if (nChannel == 1) {
		// convert to color image
		if (nDepth == CV_32F) {	// if float values, change color spectrum.
			double nMin, nMax;
			minMaxLoc(dImg, &nMin, &nMax);

			if (nMin != 0 || nMax != 0) {
				float nMaxValue = (float)max(abs(nMin), nMax);

				auto itSrc = dImg.begin<FLOAT>();
				auto itSrc_End = dImg.end<FLOAT>();
				auto itDst = m_dImg.begin<Vec3b>();

				UINT8 nValue;
				while (itSrc != itSrc_End) {
					FLOAT &nSrcValue = *itSrc;
					Vec3b &dDstPixel = *itDst;
					if (nSrcValue > 0) {
						nValue = cvRound((nSrcValue / nMaxValue) * 255.f);
						dDstPixel[2] = nValue;
					}
					else if (nSrcValue < 0) {
						nValue = cvRound((-nSrcValue / nMaxValue) * 255.f);
						dDstPixel[0] = nValue;
					}
					itSrc++;
					itDst++;
				}
			}
		}
		else {
			if (nDepth != CV_8U) {
				dImg.convertTo(dImg, CV_8U);
			}
			cvtColor(dImg, m_dImg, CV_GRAY2BGR);
		}
	}
	else if (nChannel == 3) {
		if (nDepth == CV_8U) {
			dImg.copyTo(m_dImg);
		}
		else {
			dImg.convertTo(m_dImg, CV_8U);
		}
	}

	Clear();
	m_dViewImage.CopyOf(&(IplImage(m_dImg)));

	m_nImgSizeX = m_dImg.cols;
	m_nImgSizeY = m_dImg.rows;

	CRect rcClient;
	GetClientRect(&rcClient);
	m_nMinZoomX = (float)rcClient.Width() / m_nImgSizeX;
	m_nMinZoomY = (float)rcClient.Height() / m_nImgSizeY;
	m_nMinZoom = min(m_nMinZoomX, m_nMinZoomY);

	if (m_bSyncImageTarget) {
		SyncImage(true);
	}
	else if (m_bKeepPosAndZoom || m_bKeepPosAndZoom) {
		if (m_nZoom < m_nMinZoom) {
			ChangeZoom(m_nMinZoom, true);
			ResetScrollSize(TRUE);
		}
		else {
			ResetScrollSize();
		}
	}
	else {
		ChangeZoom(m_nMinZoom, true);
		ResetScrollSize(FALSE, TRUE);
	}

	ShowWindow(SW_SHOW);
	Invalidate(FALSE);
}

void NIPLImageCtl::Close()
{
	Clear();
	ShowWindow(SW_HIDE);
}

void NIPLImageCtl::OnClose()
{
	// TODO: Add your message handler code here and/or call default
	ShowWindow(SW_HIDE);
}

void NIPLImageCtl::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CWnd::OnPaint() for painting messages

	DrawImage();
}

void NIPLImageCtl::DrawImage()
{
	if(GetSafeHwnd() == NULL) {
		return;
	}

	CDC *pDC = GetDC();
	if (pDC == NULL) {
		return;
	}

	if (m_bShowMask) {
		if (m_dImg.size == m_dMask.size && m_dImg.channels() == m_dMask.channels()) {
			Mat dImg = (m_dImg * 0.4) + (m_dMask * 0.6);
			m_dViewImage.CopyOf(&(IplImage(dImg)));
		}
	}

	UpdateShowROI();

	CRect rcClient;
	GetClientRect(&rcClient);
	m_dViewImage.ShowROI(pDC->m_hDC, &CvRectToRect(m_rcShowROI), rcClient);

	ReleaseDC(pDC);

	DrawImagePos();
	DrawImageMark();
}

void NIPLImageCtl::DrawImagePos(bool bUpdateImagePos)
{
	if (!m_bShowImagePos) {
		return;
	}

	if (GetSafeHwnd() == NULL) {
		return;
	}

	CPoint ptStart;
	if (!GetScreenPos(CPoint(m_nImgPosX, m_nImgPosY), ptStart)) {
		return;
	}
	CPoint ptEnd;
	GetScreenPos(CPoint(m_nImgPosX + 1, m_nImgPosY + 1), ptEnd);

/*
	CRect rcClient;
	GetClientRect(&rcClient);
	int nPixelPerPosX = cvRound(((float)rcClient.Width()) / m_rcShowROI.width);
	int nPixelPerPosY = cvRound(((float)rcClient.Height()) / m_rcShowROI.height);


	CPoint ptEnd(ptStart.x + nPixelPerPosX, ptStart.y + nPixelPerPosY);
*/
	// the lenth should be longer than 1
	if ((ptEnd.x - ptStart.x) < 1 || (ptEnd.y - ptStart.y) < 1) {
		return;
	}

	CDC *pDC = GetDC();
	if (pDC == NULL) {
		return;
	}

	CPen *pPen = new CPen(PS_SOLID, 2, COLOR_POS);
	if (pPen == NULL) {
		return;
	}
	CPen *pOldPen = pDC->SelectObject(pPen);
	CBrush *pOldBrush = (CBrush*)pDC->SelectStockObject(NULL_BRUSH);

	pDC->Rectangle(ptStart.x, ptStart.y, ptEnd.x, ptEnd.y);

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	delete pPen;

	ReleaseDC(pDC);
}

void NIPLImageCtl::DrawImageMark()
{
	if (GetSafeHwnd() == NULL) {
		return;
	}

	CDC *pDC = GetDC();
	if (pDC == NULL) {
		return;
	}

	CPen dPen(PS_SOLID, 2, COLOR_BOX);
	CBrush dSolidBrush(COLOR_BOX);

	CPen *pOldPen = pDC->SelectObject(&dPen);
	CBrush *pOldBrush = NULL;
	for (auto &dMark : m_listImageMark) {
		Rect rc = dMark.m_rcBoundingBox;
		CPoint ptStart;
		GetScreenPos(CPoint(rc.x, rc.y), ptStart);
		CPoint ptEnd;
		GetScreenPos(CPoint(rc.x + rc.width, rc.y + rc.height), ptEnd);

		// the lenth should be longer than 1
		if ((ptEnd.x - ptStart.x) < 1 || (ptEnd.y - ptStart.y) < 1) {
			continue;
		}

		if (dMark.m_bFill) {
			pOldBrush = (CBrush*)pDC->SelectObject(&dSolidBrush);
		}
		else {
			pOldBrush = (CBrush*)pDC->SelectStockObject(NULL_BRUSH);
		}

		if (dMark.m_bCircle) {
			pDC->Ellipse(ptStart.x, ptStart.y, ptEnd.x, ptEnd.y);
		}
		else {
			pDC->Rectangle(ptStart.x, ptStart.y, ptEnd.x, ptEnd.y);
		}
	}

	if (pOldPen) pDC->SelectObject(pOldPen);
	if (pOldBrush) pDC->SelectObject(pOldBrush);

	ReleaseDC(pDC);
}


void NIPLImageCtl::DrawDragBox(CPoint ptStart, CPoint ptEnd, bool bBegin)
{
	CDC *pDC = GetDC();
	if (pDC == NULL) {
		return;
	}

	CRect rcBox;
	rcBox.left = min(ptStart.x, ptEnd.x);
	rcBox.top = min(ptStart.y, ptEnd.y);
	rcBox.right = max(ptStart.x, ptEnd.x);
	rcBox.bottom = max(ptStart.y, ptEnd.y);

	CSize size(rcBox.Width(), rcBox.Height());
	CSize sizePrev(m_rcPrevDragBox.Width(), m_rcPrevDragBox.Height());

	if (bBegin) {
		pDC->DrawDragRect(rcBox, size, NULL, sizePrev);
	}
	else {
		pDC->DrawDragRect(rcBox, size, m_rcPrevDragBox, sizePrev);
	}

	m_rcPrevDragBox = rcBox;
}


/*
void NIPLImageCtl::Show(vector<Rect> listRect)
{
	for (int i = 0; i < (int)listRect.size(); i++) {
		Rect rc = listRect[i];
		cvRectangle(m_dViewImage.GetImage(), cvPoint(rc.x, rc.y), cvPoint(rc.x + rc.width, rc.y + rc.height), CV_RGB(255, 255, 255), 3);
	}
}
*/

void NIPLImageCtl::AddImageMark(ImageMark dMark)
{
	m_listImageMark.push_back(dMark);
}

void NIPLImageCtl::ShowImageMark()
{
	DrawImage();
	SendSyncImage(false, true);
}

void NIPLImageCtl::ShowImageMark(ImageMark dMark, bool bGotoImagePos)
{
	m_listImageMark.push_back(dMark);
	if (bGotoImagePos) {
		Rect rc = dMark.m_rcBoundingBox;
		GotoImagePos(CPoint(rc.x + rc.width / 2, rc.y + rc.height / 2));
	}
	else {
// 		DrawImage();
		SendSyncImage(false, true);
	}
}

void NIPLImageCtl::ClearImageMark()
{
	m_listImageMark.clear();
}

bool NIPLImageCtl::HasImageMark()
{
	return (m_listImageMark.size() > 0);
}

void NIPLImageCtl::UpdateShowROI()
{
	CRect rcClient;
	GetClientRect(&rcClient);

	m_rcShowROI.x = cvRound(GetScrollPos(SB_HORZ) / m_nZoom);
	m_rcShowROI.y = cvRound(GetScrollPos(SB_VERT) / m_nZoom);
	m_rcShowROI.width = max(min(cvRound(rcClient.Width() / m_nZoom), (m_nImgSizeX - m_rcShowROI.x)), 1);
	m_rcShowROI.height = max(min(cvRound(rcClient.Height() / m_nZoom), (m_nImgSizeY - m_rcShowROI.y)), 1);
}

void NIPLImageCtl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	int nCurPos = GetScrollPos(SB_HORZ);
	switch(nSBCode) {
	case SB_THUMBPOSITION :
	case SB_THUMBTRACK :
		if ((int)nPos < 0) nPos &= 0x0000FFFF;		// adjust carry over
		break;
	case SB_PAGELEFT :
		nPos = max(0, nCurPos - m_nScrollPageSizeX);
		break;
	case SB_PAGERIGHT :
		nPos = min(m_nScrollMaxPosX, nCurPos + m_nScrollPageSizeX);
		break;
	case SB_LINELEFT :
		nPos = max(0, nCurPos - m_nScrollSizeX);
		break;
	case SB_LINERIGHT :
		nPos = min(m_nScrollMaxPosX, nCurPos + m_nScrollSizeX);
		break;

	default : 
		return;
	}

	SetScrollPos(SB_HORZ, nPos);

	DrawImage();
	SendSyncImage();

	CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
}

void NIPLImageCtl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	int nCurPos = GetScrollPos(SB_VERT);
	switch(nSBCode) {
	case SB_THUMBPOSITION :
	case SB_THUMBTRACK :
		if ((int)nPos < 0) nPos &= 0x0000FFFF;		// adjust carry over
		break;
	case SB_PAGEUP :
		nPos = max(0, nCurPos - m_nScrollPageSizeY);
		break;
	case SB_PAGEDOWN :
		nPos = min(m_nScrollMaxPosY, nCurPos + m_nScrollPageSizeY);
		break;
	case SB_LINEUP :
		nPos = max(0, nCurPos - m_nScrollSizeY);
		break;
	case SB_LINEDOWN :
		nPos = min(m_nScrollMaxPosY, nCurPos + m_nScrollSizeY);
		break;

	default : 
		return;
	}

	SetScrollPos(SB_VERT, nPos);
	
	DrawImage();
	SendSyncImage();

	CWnd::OnVScroll(nSBCode, nPos, pScrollBar);
}

void NIPLImageCtl::OnSize(UINT nType, int cx, int cy)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	m_nMinZoomX = (float)rcClient.Width() / m_nImgSizeX;
	m_nMinZoomY = (float)rcClient.Height() / m_nImgSizeY;
	m_nMinZoom = min(m_nMinZoomX, m_nMinZoomY);

	ResetScrollSize();
	SendSyncImage();

	CWnd::OnSize(nType, cx, cy);
}

void NIPLImageCtl::ResetScrollSize(BOOL bZoom, BOOL bResetPosition)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	int nViewSizeX = rcClient.Width();
	int nViewSizeY = rcClient.Height();

	int nSizeX = (int)(m_nImgSizeX * m_nZoom);
	int nSizeY = (int)(m_nImgSizeY * m_nZoom);

	int nMouseX = 0;
	int nMouseY = 0;
	int nCenterPosX = 0;
	int nCenterPosY = 0;
	if (bZoom) {
		CPoint pt;
		GetCursorPos(&pt);
		ScreenToClient(&pt);
		nMouseX = pt.x;
		nMouseY = pt.y;

		nCenterPosX = (int)(((GetScrollPos(SB_HORZ) + nMouseX) / m_nZoomBefore) * m_nZoom);
		nCenterPosY = (int)(((GetScrollPos(SB_VERT) + nMouseY) / m_nZoomBefore) * m_nZoom);
	}

	if (m_nZoom <= m_nMinZoomX) {
		SetScrollPos(SB_HORZ, 0);
		ShowScrollBar(SB_HORZ, FALSE);
	}
	else {
		ShowScrollBar(SB_HORZ, TRUE);
		m_nScrollMaxPosX = nSizeX - nViewSizeX;
		m_nScrollSizeX = max(1, m_nScrollMaxPosX / 20);
		m_nScrollPageSizeX = max(1, m_nScrollMaxPosX / 10);
		SetScrollRange(SB_HORZ, 0, m_nScrollMaxPosX);

		if(bResetPosition) {
			SetScrollPos(SB_HORZ, 0);
		}
		else if (bZoom) {
			SetScrollPos(SB_HORZ, nCenterPosX - nMouseX);
		}
	}

	if (m_nZoom <= m_nMinZoomY) {
		SetScrollPos(SB_VERT, 0);
		ShowScrollBar(SB_VERT, FALSE);
	}
	else {
		ShowScrollBar(SB_VERT, TRUE);
		m_nScrollMaxPosY = nSizeY - nViewSizeY;
		m_nScrollSizeY = max(1, m_nScrollMaxPosY / 20);
		m_nScrollPageSizeY = max(1, m_nScrollMaxPosY / 10);
		SetScrollRange(SB_VERT, 0, m_nScrollMaxPosY);

		if(bResetPosition) {
			SetScrollPos(SB_VERT, 0);
		}
		else if (bZoom) {
			SetScrollPos(SB_VERT, nCenterPosY - nMouseY);
		}
	}

	UpdateImagePos();
	DrawImage();
}

void NIPLImageCtl::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: Add your message handler code here and/or call default
	lpMMI->ptMinTrackSize.x = DEFAULT_VIEW_WIDTH / 4;
	lpMMI->ptMinTrackSize.y = DEFAULT_VIEW_HEIGHT / 4;

	CWnd::OnGetMinMaxInfo(lpMMI);
}

void NIPLImageCtl::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	// TODO: Add your message handler code here
	ResetScrollSize();
	SyncImage(true);
}

BOOL NIPLImageCtl::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: Add your message handler code here and/or call default
	ZoomImage((float)zDelta/1000);

	return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}

void NIPLImageCtl::ZoomImage(float nDelta)
{
	float nZoom = m_nZoom * (1.f + nDelta);

	if (nZoom < m_nMinZoom) nZoom = m_nMinZoom;
	if (nZoom > m_nMaxZoom) nZoom = m_nMaxZoom;

	if(nZoom != m_nZoom) {
		ChangeZoom(nZoom);

		ResetScrollSize(TRUE);
		SendSyncImage();
	}
}

BOOL NIPLImageCtl::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default
	return TRUE;
}

HBRUSH NIPLImageCtl::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	// TODO:  Change any attributes of the DC here
	pDC->SetBkMode(TRANSPARENT);
	return (HBRUSH)GetStockObject(NULL_BRUSH);
}

void NIPLImageCtl::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	SetFocus();
	ChangeCursor();

	if ((nFlags & MK_CONTROL) && m_bShowMask) {
		m_bDrag = true;
		m_ptDragStart = point;

		DrawDragBox(m_ptDragStart, point, true);
	}
	else {
		m_bGrab = true;
		m_ptGrabStart = point;
	}

	SetCapture();

	CWnd::OnLButtonDown(nFlags, point);
}

void NIPLImageCtl::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	ChangeCursor();

	if (m_bDrag) {
		m_bDrag = false;

		DrawDragBox(m_ptDragStart, point);
		UpdateMask(m_ptDragStart, point, true);

		ReleaseCapture();
	}

	if (m_bGrab) {
		m_bGrab = false;
		MoveImage(point.x - m_ptGrabStart.x, point.y - m_ptGrabStart.y);

		ReleaseCapture();
	}

	CWnd::OnLButtonUp(nFlags, point);
}

void NIPLImageCtl::OnRButtonDown(UINT nFlags, CPoint point)
{
	SetFocus();
	ChangeCursor();

	// TODO: Add your message handler code here and/or call default
	if ((nFlags & MK_CONTROL) && m_bShowMask) {
		m_bDrag = true;
		m_ptDragStart = point;

		DrawDragBox(m_ptDragStart, point, true);

		SetCapture();
	}

	CWnd::OnRButtonDown(nFlags, point);
}

void NIPLImageCtl::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	ChangeCursor();

	if (m_bDrag) {
		m_bDrag = false;

		DrawDragBox(m_ptDragStart, point, true);
		UpdateMask(m_ptDragStart, point, false);

		ReleaseCapture();
	}

	CWnd::OnRButtonUp(nFlags, point);
}

void NIPLImageCtl::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	ChangeCursor();

	if (m_bGrab) {
		MoveImage(point.x - m_ptGrabStart.x, point.y - m_ptGrabStart.y);
		m_ptGrabStart = point;
	}

	UpdateImagePos();
	if (m_bShowImagePos) {
		DrawImage();
	}

	// drag box should be draw after drawing image pos box when its flag is on.
	if (m_bDrag) {
		DrawDragBox(m_ptDragStart, point, m_bShowImagePos);
	}

	// send message to parent
	CWnd *pParent = GetParentWnd();
	if (pParent && m_bSendEventMessage) {
		pParent->SendMessage(WM_NIPL_IMAGE_POS_VALUE, m_nImgPosX, m_nImgPosY);
	}
	SendSyncImage();

	CWnd::OnMouseMove(nFlags, point);
}

void NIPLImageCtl::MoveImage(int nDeltaX, int nDeltaY)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	int nViewSizeX = rcClient.Width();
	int nViewSizeY = rcClient.Height();

	int nSizeX = (int)(m_nImgSizeX * m_nZoom);
	int nSizeY = (int)(m_nImgSizeY * m_nZoom);

	BOOL bEnableMove = FALSE;
	if (nSizeX > nViewSizeX) {
		bEnableMove = TRUE;
		int nPosX = (GetScrollPos(SB_HORZ) - nDeltaX);
		SetScrollPos(SB_HORZ, nPosX);
	}
	if (nSizeY > nViewSizeY) {
		bEnableMove = TRUE;
		int nPosY = (GetScrollPos(SB_VERT) - nDeltaY);
		SetScrollPos(SB_VERT, nPosY);
	}

	if (bEnableMove) {
		DrawImage();
	}
}

void NIPLImageCtl::SetSyncImageTarget(bool bSet)
{
	m_bSyncImageTarget = bSet;

	auto dPos = find(listSyncImageTarget.begin(), listSyncImageTarget.end(), this);
	if (bSet) {
		if (dPos == listSyncImageTarget.end()) {
			listSyncImageTarget.push_back(this);
		}

		if (listSyncImageTarget.size() == 1) {
			SendSyncImage(true, true);		// If it's first target, set SyncImageData for later target
		}
		else {
			SyncImage(true);
		}
	}
	else {
		if (dPos != listSyncImageTarget.end()) {
			listSyncImageTarget.erase(dPos);
		}

		if (listSyncImageTarget.size() == 0) {
			dSyncImage.Clear();
		}

		// clear Mask and restore image
		m_bShowMask = false;
//		m_dMask.release();
		m_listImageMark.clear();
		m_dViewImage.CopyOf(&(IplImage(m_dImg)));

		CWnd *pParent = GetParentWnd();
		if (pParent && m_bSendEventMessage) {
			pParent->SendMessage(WM_NIPL_IMAGE_MARK_UPDATE);
			pParent->SendMessage(WM_NIPL_MASK_UPDATE, m_bShowMask);
		}

		DrawImage();
	}
}

void NIPLImageCtl::SendSyncImage(bool bUpdateMask, bool bUpdateMark)
{
	if (!m_bSyncImageTarget) {
		return;
	}

	if (m_bDoingSyncImage) {
		return;
	}

	dSyncImage.m_bValid = true;
	dSyncImage.m_nZoom = m_nZoom;
	dSyncImage.m_nPosX = GetScrollPos(SB_HORZ);
	dSyncImage.m_nPosY = GetScrollPos(SB_VERT);
	dSyncImage.m_nImagePosX = m_nImgPosX;
	dSyncImage.m_nImagePosY = m_nImgPosY;
	dSyncImage.m_bUpdateMark = bUpdateMark;
	if (bUpdateMark) {
		dSyncImage.m_listImageMark = m_listImageMark;
	}
	dSyncImage.m_bUpdateMask = bUpdateMask;
	if (bUpdateMask) {
		dSyncImage.m_bShowMask = m_bShowMask;
		dSyncImage.m_dMask = m_dMask;
	}

	vector<NIPLImageCtl *> listRemoveTarget;

	for (auto pCtl : listSyncImageTarget) {
		if (pCtl == this) {
			continue;
		}

		HWND hWnd = pCtl->GetSafeHwnd();
		if (hWnd != NULL && ::IsWindow(hWnd)) {
			pCtl->SyncImage();
		}
		else {
			listRemoveTarget.push_back(pCtl);
		}
	}

	for (auto pCtl : listRemoveTarget) {
		pCtl->SetSyncImageTarget(false);
	}
}

void NIPLImageCtl::SyncImage(bool bUpdateMask)
{
	if (!m_bSyncImageTarget) {
		return;
	}

	if (!dSyncImage.m_bValid) {
		return;
	}

	if (m_bDoingSyncImage) {
		return;
	}

	m_bDoingSyncImage = true;

	int nPosX = dSyncImage.m_nPosX;
	int nPosY = dSyncImage.m_nPosY;
	m_nImgPosX = max(min(dSyncImage.m_nImagePosX, m_nImgSizeX - 1), 0);
	m_nImgPosY = max(min(dSyncImage.m_nImagePosY, m_nImgSizeY - 1), 0);

	bool bUpdateMark = dSyncImage.m_bUpdateMark;
	if (bUpdateMark) {
		m_listImageMark = dSyncImage.m_listImageMark;
	}
	if (!bUpdateMask) {
		bUpdateMask = dSyncImage.m_bUpdateMask;
	}
	if (bUpdateMask) {
		m_dMask = dSyncImage.m_dMask;
		VERIFY_MASK(m_dImg, m_dMask);
		SetShowMask(dSyncImage.m_bShowMask);
	}

	ChangeZoom(dSyncImage.m_nZoom, true);

	CRect rcClient;
	GetClientRect(&rcClient);
	int nViewSizeX = rcClient.Width();
	int nViewSizeY = rcClient.Height();

	int nSizeX = (int)(m_nImgSizeX * m_nZoom);
	int nSizeY = (int)(m_nImgSizeY * m_nZoom);

	if (m_nZoom <= m_nMinZoomX) {
		SetScrollPos(SB_HORZ, 0);
		ShowScrollBar(SB_HORZ, FALSE);
	}
	else {
		ShowScrollBar(SB_HORZ, TRUE);
		m_nScrollMaxPosX = nSizeX - nViewSizeX;
		m_nScrollSizeX = max(1, m_nScrollMaxPosX / 20);
		m_nScrollPageSizeX = max(1, m_nScrollMaxPosX / 10);
		SetScrollRange(SB_HORZ, 0, m_nScrollMaxPosX);

		if (nPosX > m_nScrollMaxPosX) {
			nPosX = m_nScrollMaxPosX;
		}

		SetScrollPos(SB_HORZ, nPosX);
	}

	if (m_nZoom <= m_nMinZoomY) {
		SetScrollPos(SB_VERT, 0);
		ShowScrollBar(SB_VERT, FALSE);
	}
	else {
		ShowScrollBar(SB_VERT, TRUE);
		m_nScrollMaxPosY = nSizeY - nViewSizeY;
		m_nScrollSizeY = max(1, m_nScrollMaxPosY / 20);
		m_nScrollPageSizeY = max(1, m_nScrollMaxPosY / 10);
		SetScrollRange(SB_VERT, 0, m_nScrollMaxPosY);

		if (nPosY > m_nScrollMaxPosY) {
			nPosY = m_nScrollMaxPosY;
		}

		SetScrollPos(SB_VERT, nPosY);
	}

	DrawImage();

	// send message to parent
	CWnd *pParent = GetParentWnd();
	if (pParent && m_bSendEventMessage) {
		pParent->SendMessage(WM_NIPL_IMAGE_POS_VALUE, m_nImgPosX, m_nImgPosY);
		if (bUpdateMark) {
			pParent->SendMessage(WM_NIPL_IMAGE_MARK_UPDATE);
		}

		if (bUpdateMask) {
			pParent->SendMessage(WM_NIPL_MASK_UPDATE, m_bShowMask);
		}
	}

	m_bDoingSyncImage = false;
}

void NIPLImageCtl::ChangeZoom(float nZoom, bool bResetZoomBefore)
{
	m_nZoomBefore = m_nZoom;
	m_nZoom = nZoom;

	if (bResetZoomBefore) {
		m_nZoomBefore = m_nZoom;
	}

	if (!m_bDoingSyncImage) {
		UpdateImagePos();
	}

	// send message to parent
	CWnd *pParent = GetParentWnd();
	if (pParent && m_bSendEventMessage) {
		pParent->SendMessage(WM_NIPL_IMAGE_ZOOM);
		pParent->SendMessage(WM_NIPL_IMAGE_POS_VALUE, m_nImgPosX, m_nImgPosY);
	}
}

void NIPLImageCtl::GetZoom(float &nZoom, float &nMinZoom, float &nMaxZoom)
{
	nZoom = m_nZoom;
	nMinZoom = m_nMinZoom;
	nMaxZoom = m_nMaxZoom;
}

void NIPLImageCtl::UpdateImagePos()
{
	CPoint pt;
	GetCursorPos(&pt);
	ScreenToClient(&pt);

	CPoint ptImagePos;
	GetImagePos(pt, ptImagePos);

	m_nImgPosX = ptImagePos.x;
	m_nImgPosY = ptImagePos.y;
}

void NIPLImageCtl::GetImagePos(CPoint pt, CPoint &ptImagePos)
{
	UpdateShowROI();

	CRect rcClient;
	GetClientRect(&rcClient);

	float nRatioX = ((float)pt.x) / rcClient.Width();
	float nRatioY = ((float)pt.y) / rcClient.Height();

	int nImagePosX = m_rcShowROI.x + int(m_rcShowROI.width * nRatioX);
	int nImagePosY = m_rcShowROI.y + int(m_rcShowROI.height * nRatioY);

	ptImagePos.x = max(min(nImagePosX, m_nImgSizeX - 1), 0);
	ptImagePos.y = max(min(nImagePosY, m_nImgSizeY - 1), 0);
}

bool NIPLImageCtl::GetScreenPos(CPoint ptImagePos, CPoint &pt)
{
	UpdateShowROI();

	int nPosX = ptImagePos.x - m_rcShowROI.x;
	int nPosY = ptImagePos.y - m_rcShowROI.y;

	float nRatioX = ((float)nPosX) / m_rcShowROI.width;
	float nRatioY = ((float)nPosY) / m_rcShowROI.height;

	CRect rcClient;
	GetClientRect(&rcClient);

	pt.x = cvRound(rcClient.Width() * nRatioX);
	pt.y = cvRound(rcClient.Height() * nRatioY);

//	pt.x = max(min(nPosX, rcClient.Width() - 1), 0);
//	pt.y = max(min(nPosY, rcClient.Height() - 1), 0);

	// out of screen
	if (nRatioX < 0 || nRatioX >= 1 || nRatioY < 0 || nRatioY >= 1) {
		return false;
	}

	return true;
}

void NIPLImageCtl::SetShowImagePos(bool bSet) 
{
	m_bShowImagePos = bSet; 

	UpdateImagePos();
	DrawImage();
}

void NIPLImageCtl::SetShowMask(bool bSet)
{
	if (CHECK_EMPTY_IMAGE(m_dMask)) {
		return;
	}

	m_bShowMask = bSet;
	if (!m_bShowMask) {
		// restore image
		m_dViewImage.CopyOf(&(IplImage(m_dImg)));
	}

	SendSyncImage();
}

void NIPLImageCtl::SetMask(const Mat &dMask)
{
	dMask.copyTo(m_dMask);
	if (m_dMask.channels() == 1) {
		cvtColor(m_dMask, m_dMask, CV_GRAY2BGR);
	}

	double nMin, nMax;
	minMaxLoc(m_dMask, &nMin, &nMax);
	// adjust value to 256 level if it's between 0 and 1.
	if (nMin >= 0 && nMax <= 1) {
		m_dMask = m_dMask * 255;
	}

	SendSyncImage(true);
}

void NIPLImageCtl::UpdateMask(CPoint ptStart, CPoint ptEnd, bool bSet)
{
	CPoint ptStartImagePos;
	GetImagePos(ptStart, ptStartImagePos);

	CPoint ptEndImagePos;
	GetImagePos(ptEnd, ptEndImagePos);

	Rect rcROI;
	rcROI.x = min(ptStartImagePos.x, ptEndImagePos.x);
	rcROI.y = min(ptStartImagePos.y, ptEndImagePos.y);
	rcROI.width = abs(ptStartImagePos.x - ptEndImagePos.x) + 1;
	rcROI.height = abs(ptStartImagePos.y - ptEndImagePos.y) + 1;

	Mat dMaskROI(m_dMask, rcROI);
	dMaskROI = bSet ? 255 : 0;

	DrawImage();
	SendSyncImage(true);

	CWnd *pParent = GetParentWnd();
	if (pParent && m_bSendEventMessage) {
		pParent->SendMessage(WM_NIPL_MASK_UPDATE, m_bShowMask);
	}
}

void NIPLImageCtl::GotoImagePos(CPoint ptImagePos)
{
	if (m_nZoom > m_nMinZoomX) {
		int nPosX = (int)((ptImagePos.x - (m_rcShowROI.width / 2)) * m_nZoom);
		nPosX = max(min(nPosX, m_nScrollMaxPosX), 0);
		SetScrollPos(SB_HORZ, nPosX);
	}
	if (m_nZoom > m_nMinZoomY) {
		int nPosY = (int)((ptImagePos.y - (m_rcShowROI.height / 2)) * m_nZoom);
		nPosY = max(min(nPosY, m_nScrollMaxPosY), 0);
		SetScrollPos(SB_VERT, nPosY);
	}

	DrawImage();
	SendSyncImage();
}

void NIPLImageCtl::Refresh()
{
	m_listImageMark.clear();

	DrawImage();
	SendSyncImage();
}

void NIPLImageCtl::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	UpdateImagePos();
	CWnd *pParent = GetParentWnd();
	if (pParent && m_bSendEventMessage) {
		pParent->SendMessage(WM_NIPL_IMAGE_POS_DBLCLICK, m_nImgPosX, m_nImgPosY);
	}

	CWnd::OnLButtonDblClk(nFlags, point);
}


BOOL NIPLImageCtl::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	// TODO: Add your message handler code here and/or call default
	if (ChangeCursor()) {
		return TRUE;
	}

	return CWnd::OnSetCursor(pWnd, nHitTest, message);
}

bool NIPLImageCtl::ChangeCursor()
{
	if (m_bShowMask && ((GetAsyncKeyState(VK_CONTROL) & 0x8000) || m_bDrag)) {
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
		return true;
	}
	else if (m_bGrab) {
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
		return true;
	}

	::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	return true;
}

void NIPLImageCtl::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default
	ChangeCursor();

	CWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}


void NIPLImageCtl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default
	ChangeCursor();

	CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

CWnd *NIPLImageCtl::GetParentWnd()
{
	if (GetSafeHwnd()) {
		return GetParent();
	}

	return nullptr;
}

void NIPLImageCtl::SetSendEventMessage(bool bSet)
{
	m_bSendEventMessage = bSet;
}