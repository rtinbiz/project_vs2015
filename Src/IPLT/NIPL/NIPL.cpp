// NIPL.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"

#include <codecvt>

#include <clocale>
#include <locale>

#include <stdarg.h>

#ifdef _LINUX
#include <dirent.h>
#endif

#ifdef _WINDOWS
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#endif

string _wstr2str(wstring ws)
{
	if (!ws.empty()) {
		setlocale(LC_ALL, "");
		const locale locale("");
		typedef std::codecvt<wchar_t, char, std::mbstate_t> converter_type;
		const converter_type& converter = std::use_facet<converter_type>(locale);
		std::vector<char> to(ws.length() * converter.max_length());
		std::mbstate_t state;
		const wchar_t* from_next;
		char* to_next;
		const converter_type::result result = converter.out(state, ws.data(), ws.data() + ws.length(), from_next, &to[0], &to[0] + to.size(), to_next);
		if (result == converter_type::ok || result == converter_type::noconv) {
			const string s(&to[0], to_next);
			return s;
		}
	}

	return "";
}

shared_ptr<NIPL> NIPL::GetInstance(bool bNew)
{
	if (bNew) {
		return shared_ptr<NIPL>(new NIPL);
	}

	static shared_ptr<NIPL> pThis(new NIPL);

	return pThis;
}

NIPL::NIPL()
{
	m_pOption = nullptr;
}

NIPL::~NIPL()
{
}

void NIPL::SetOption(NIPLOption *pOption)
{
	m_pOption = pOption;
}

NIPL_ERR NIPL::LoadImage(wstring strPath, Mat &dImg, int nFlags)
{
	return LoadImage(_wstr2str(strPath), dImg, nFlags);
}

NIPL_ERR NIPL::LoadImage(string strPath, Mat &dImg, int nFlags)
{
	dImg = imread(strPath, nFlags);

	if(CHECK_EMPTY_IMAGE(dImg)) {
		return NIPL_ERR_FAIL_TO_LOAD_IMAGE;
	} 

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPL::SaveImage(wstring strPath, Mat dImg)
{
	return SaveImage(_wstr2str(strPath), dImg);
}

NIPL_ERR NIPL::SaveImage(string strPath, Mat dImg)
{
	if (!imwrite(strPath, dImg)) {
		return NIPL_ERR_FAIL_TO_SAVE_IMAGE;
	}

	return NIPL_ERR_SUCCESS;
}

void NIPL::DebugPrint(const wchar_t* szDebugString, ...)
{
	const int BUFFER_SIZE = 2048;
	wchar_t szBufferString[BUFFER_SIZE] = { 0, };

	va_list vargs;
	va_start(vargs, szDebugString);
#ifdef _WINDOWS    
	vswprintf_s(szBufferString, szDebugString, (va_list)vargs);
#endif
#ifdef _LINUX
	vswprintf(szBufferString, BUFFER_SIZE, szDebugString, vargs);
#endif    
	va_end(vargs);

	wchar_t szBuffer[BUFFER_SIZE] = L"\n[NIPL] ";
	wcscat(szBuffer, szBufferString);

#ifdef _WINDOWS    
	OutputDebugStringW(szBuffer);
#endif
#ifdef _LINUX
	wprintf(szBuffer);
#endif    
}

void NIPL::DebugPrint(const char* szDebugString, ...)
{
	const int BUFFER_SIZE = 2048;
	char szBufferString[BUFFER_SIZE] = { 0, };

	va_list vargs;
	va_start(vargs, szDebugString);
#ifdef _WINDOWS    
	vsprintf_s(szBufferString, szDebugString, (va_list)vargs);
#endif
#ifdef _LINUX
	vsprintf(szBufferString, szDebugString, vargs);
#endif     
	va_end(vargs);

	char szBuffer[BUFFER_SIZE] = "\n[NIPL] ";
	strcat(szBuffer, szBufferString);

#ifdef _WINDOWS    
	OutputDebugStringA(szBuffer);
#endif
#ifdef _LINUX
	printf("%s", szBuffer);
#endif    
}

void NIPL::DebugPrintImageProperty(wstring strTitle, Mat dImg)
{
	DebugPrintImageProperty(wstr2str(strTitle), dImg);
}

void NIPL::DebugPrintImageProperty(string strTitle, Mat dImg)
{
	int depth = dImg.depth();
	int channels = dImg.channels();
	double nMin, nMax;
	minMaxLoc(dImg, &nMin, &nMax);
	char szText[256];
	
#ifdef _WINDOWS    
	sprintf_s(szText, "\n[NIPL] %s - depth : %d, channels : %d, nMin : %.3f, nMax : %.3f ", strTitle.c_str(), depth, channels, nMin, nMax);
#endif
#ifdef _LINUX    
	sprintf(szText, "\n[NIPL] %s - depth : %d, channels : %d, nMin : %.3f, nMax : %.3f ", strTitle.c_str(), depth, channels, nMin, nMax);
#endif

#ifdef _WINDOWS    
	OutputDebugStringA(szText);
#endif
#ifdef _LINUX
	printf("%s", szText);
#endif  
}

void NIPL::ShowErrorDesc(wstring strFunc, wstring strMsg)
{
	ShowErrorDesc(wstr2str(strFunc), wstr2str(strMsg));
}

void NIPL::ShowErrorDesc(string strFunc, string strMsg)
{
	string strErrDesc = "\n[NIPL] Exception - " + strFunc + " : " + strMsg;
	
#ifdef _WINDOWS    
	OutputDebugStringA(strErrDesc.c_str());
#endif
#ifdef _LINUX
	printf("%s", strErrDesc.c_str());
#endif  
}

NIPL_METHOD_IMPL(CreateImage)
{
	VERIFY_PARAMS();
	NIPLParam_CreateImage *pParam_CreateImage = (NIPLParam_CreateImage *)pInput->m_pParam;

	int nType = pParam_CreateImage->m_nType;
	int nWidth = pParam_CreateImage->m_nWidth;
	int nHeight = pParam_CreateImage->m_nHeight;
	float nValue = pParam_CreateImage->m_nValue;

	if (nWidth <= 0 || nHeight <= 0) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}

	Mat dOutputImg;
	try {
		switch (nType) {
		case NIPLParam_CreateImage::TYPE_GRAY_UINT8:
			dOutputImg = Mat(nHeight, nWidth, CV_8UC1, cvScalar(nValue));
			break;
		case NIPLParam_CreateImage::TYPE_GRAY_FLOAT:
			dOutputImg = Mat(nHeight, nWidth, CV_32FC1, cvScalar(nValue));
			break;
		case NIPLParam_CreateImage::TYPE_COLOR_UINT8:
			dOutputImg = Mat(nHeight, nWidth, CV_8UC3, cvScalar(nValue, nValue, nValue));
			break;
		}
	}
	catch(Exception e) {
		ShowErrorDesc(e.func.c_str(), e.err.c_str());
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(LoadTemplateImage)
{
	VERIFY_PARAMS();
	NIPLParam_LoadTemplateImage *pParam_LoadTemplateImage = (NIPLParam_LoadTemplateImage *)pInput->m_pParam;

	wstring strTemplateImagePath = pParam_LoadTemplateImage->m_strTemplateImagePath;
	Mat dOutputImg;

	NIPL_ERR nErr = LoadImage(strTemplateImagePath, dOutputImg, CV_LOAD_IMAGE_GRAYSCALE);
	if (NIPL_SUCCESS(nErr)) {
		pOutput->m_dImg = dOutputImg;
		return NIPL_ERR_SUCCESS;
	}

	int nFileCount = 0;

#ifdef _WINDOWS
	WIN32_FIND_DATA dFindData;
	wstring strFormat = strTemplateImagePath + L"\\*.bmp";
	HANDLE hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
	if (hFile != INVALID_HANDLE_VALUE) {
		do {
			nFileCount++;
		} while (::FindNextFile(hFile, &dFindData));
		::FindClose(hFile);

		Mat dImg;
		bool bFirstFile = true;
		int nIndex = 0;
		hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
		do {
			wstring strFilePath = strTemplateImagePath + L"\\" + dFindData.cFileName;
			nErr = LoadImage(strFilePath, dImg, CV_LOAD_IMAGE_GRAYSCALE);
			if (NIPL_SUCCESS(nErr)) {
				int nImgSizeY = dImg.rows;
				int nImgSizeX = dImg.cols;

				if (bFirstFile) {
					bFirstFile = false;
					dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX * nFileCount, dImg.type());
				}

				Rect rcSubImage;
				rcSubImage.x = nIndex * nImgSizeX;
				rcSubImage.y = 0;
				rcSubImage.width = nImgSizeX;
				rcSubImage.height = nImgSizeY;

				Mat dSubImage(dOutputImg, rcSubImage);
				dImg.copyTo(dSubImage);

				nIndex++;
			}
		} while (::FindNextFile(hFile, &dFindData));

		::FindClose(hFile);

		pOutput->m_dImg = dOutputImg;
	}
	else {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}
 
#endif
#ifdef _LINUX
	string strDir = wstr2str(strTemplateImagePath);
	DIR *pDir = opendir(strDir.c_str());
	if (pDir != nullptr) {
		dirent *pEntry;
		while (pEntry = readdir(pDir)) {
			string strName = pEntry->d_name;
			
			auto dPos = strName.rfind('.');
			if(dPos == string::npos) continue;
			
			string strExt = strName.substr(dPos);
			if (!CHECK_STRING(strExt, ".bmp")) continue;
				
			nFileCount++;
		}
		closedir(pDir);

		Mat dImg;
		bool bFirstFile = true;
		int nIndex = 0;
		pDir = opendir(strDir.c_str());
		while (pEntry = readdir(pDir)) {
			string strName = pEntry->d_name;
			
			auto dPos = strName.rfind('.');
			if(dPos == string::npos) continue;
			
			string strExt = strName.substr(dPos);
			if (!CHECK_STRING(strExt, ".bmp")) continue;
			
			string strFilePath = strDir + "\\" + strName;
			nErr = LoadImage(strFilePath, dImg, CV_LOAD_IMAGE_GRAYSCALE);
			if (NIPL_SUCCESS(nErr)) {
				int nImgSizeY = dImg.rows;
				int nImgSizeX = dImg.cols;

				if (bFirstFile) {
					bFirstFile = false;
					dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX * nFileCount, dImg.type());
				}

				Rect rcSubImage;
				rcSubImage.x = nIndex * nImgSizeX;
				rcSubImage.y = 0;
				rcSubImage.width = nImgSizeX;
				rcSubImage.height = nImgSizeY;

				Mat dSubImage(dOutputImg, rcSubImage);
				dImg.copyTo(dSubImage);

				nIndex++;
			}
		}
		closedir(pDir);

		pOutput->m_dImg = dOutputImg;
	}
	else {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}    
#endif    

	return NIPL_ERR_SUCCESS;
}


NIPL_METHOD_IMPL(Color2Gray)
{
	VERIFY_PARAMS();
	NIPLParam_Color2Gray *pParam_Color2Gray = (NIPLParam_Color2Gray *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nGrayLevel = pParam_Color2Gray->m_nGrayLevel;
	int nChannel = pParam_Color2Gray->m_nChannel;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	// assume that a color image has CV_8U depth(256 level) and 3 channels
	if (dImg.depth() != CV_8U || dImg.channels() != 3) {
		return NIPL_ERR_PASS;
	}

	if (nChannel == NIPLParam_Color2Gray::CHANNEL_ALL) {
		Mat dOutputImg;
		if (nGrayLevel == NIPLParam_Color2Gray::GRAYLEVEL_256) {
			cvtColor(dImg, dOutputImg, CV_BGR2GRAY);
			pOutput->m_dImg = dOutputImg;
		}
		else if (nGrayLevel == NIPLParam_Color2Gray::GRAYLEVEL_FLOAT) {
			dImg.copyTo(dOutputImg);
			dOutputImg.convertTo(dOutputImg, CV_32F);
			dOutputImg *= (1.f / 255.f);
			cvtColor(dOutputImg, dOutputImg, CV_BGR2GRAY);

			pOutput->m_dImg = dOutputImg;
		}
	}
	else {
		Mat dChannel[3];
		split(dImg, dChannel);

		if (nChannel == NIPLParam_Color2Gray::CHANNEL_R) {
			dImg = dChannel[2];
		}
		else if (nChannel == NIPLParam_Color2Gray::CHANNEL_G) {
			dImg = dChannel[1];
		}
		else if (nChannel == NIPLParam_Color2Gray::CHANNEL_B) {
			dImg = dChannel[0];
		}

		if (nGrayLevel == NIPLParam_Color2Gray::GRAYLEVEL_256) {
			pOutput->m_dImg = dImg;
		}
		else if (nGrayLevel == NIPLParam_Color2Gray::GRAYLEVEL_FLOAT) {
			Mat dOutputImg;
			dImg.copyTo(dOutputImg);
			dOutputImg.convertTo(dOutputImg, CV_32F);
			dOutputImg *= (1.f / 255.f);

			pOutput->m_dImg = dOutputImg;
		}
	}

	if (CHECK_EMPTY_IMAGE(pOutput->m_dImg)) {
		return NIPL_ERR_PASS;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(Copy)
{
	VERIFY_PARAMS();
	NIPLParam_Copy *pParam_Copy = (NIPLParam_Copy *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;

	Mat dOutputImg;
	dImg.copyTo(dOutputImg);

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(Reduce)
{
	VERIFY_PARAMS();
	NIPLParam_Reduce *pParam_Reduce = (NIPLParam_Reduce *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nMethod = pParam_Reduce->m_nMethod;
	bool bVert = pParam_Reduce->m_bVert;
	bool bGraph = pParam_Reduce->m_bGraph;

	Mat dOutputImg;

	int nDim = bVert ? 1 : 0;
	int nType;
	switch (nMethod) {
	case NIPLParam_Reduce::METHOD_AVERAGE: nType = CV_REDUCE_AVG; break;
	case NIPLParam_Reduce::METHOD_MIN: nType = CV_REDUCE_MIN; break;
	case NIPLParam_Reduce::METHOD_MAX: nType = CV_REDUCE_MAX; break;
	}

	reduce(dImg, dOutputImg, nDim, nType);

	if (bGraph) {
		Mat dLine = dOutputImg;
		if (dLine.channels() == 3) {
			cvtColor(dLine, dLine, CV_BGR2GRAY);
		}

		if (bVert) {
			transpose(dLine, dLine);
		}

		double nMin;
		double nMax;
		minMaxLoc(dLine, &nMin, &nMax);

		int nGraphImgSizeX = dLine.cols;
		int nGraphImgSizeY = cvRound(nMax);
		Mat dGraphImg = Mat::zeros(nGraphImgSizeY, nGraphImgSizeX, CV_8UC1);

		for (int i = 0; i < (nGraphImgSizeX - 1); i++)	{
			line(dGraphImg,
				Point(i, nGraphImgSizeY - cvRound(dLine.at<UINT8>(i))),
				Point(i + 1, nGraphImgSizeY - cvRound(dLine.at<UINT8>(i + 1))),
				Scalar(255, 255, 255), 1);
		}

		if (bVert) {
			transpose(dGraphImg, dGraphImg);
			flip(dGraphImg, dGraphImg, 1);
		}

		dOutputImg = dGraphImg;
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}


NIPL_METHOD_IMPL(Invert)
{
	VERIFY_PARAMS();
	NIPLParam_Invert *pParam_Invert = (NIPLParam_Invert *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	Mat dOutputImg;
	bitwise_not(dImg, dOutputImg);

	pOutput->m_dImg = dOutputImg;

	if (CHECK_EMPTY_IMAGE(pOutput->m_dImg)) {
		return NIPL_ERR_PASS;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(Operate)
{
	VERIFY_PARAMS();
	NIPLParam_Operate *pParam_Operate = (NIPLParam_Operate *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nMethod = pParam_Operate->m_nMethod;
	Mat dTargetImg = pParam_Operate->m_dTargetImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (dImg.size != dTargetImg.size) {
		return NIPL_ERR_FAIL_NOT_MATCH_IMAGE_SIZE;
	}

	Mat dImg2 = dTargetImg;
	if (dImg.type() != dImg2.type()) {
		CHECK_EXCEPTION(dImg2.convertTo(dImg2, dImg.type()));
	}

	Mat dOutputImg;
	if (nMethod == NIPLParam_Operate::METHOD_ADD) {
		dOutputImg = dImg + dImg2;
	}
	else if (nMethod == NIPLParam_Operate::METHOD_SUBTRACT) {
		dOutputImg = dImg - dImg2;
	}
	else if (nMethod == NIPLParam_Operate::METHOD_AND) {
		dOutputImg = (dImg & dImg2);
	}
	else if (nMethod == NIPLParam_Operate::METHOD_OR) {
		dOutputImg = (dImg | dImg2);
	}
	else if (nMethod == NIPLParam_Operate::METHOD_XOR) {
		dOutputImg = (dImg != dImg2);
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(MorphologyOperate)
{
	VERIFY_PARAMS();
	NIPLParam_MorphologyOperate *pParam_MorphologyOperate = (NIPLParam_MorphologyOperate *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nMethod = pParam_MorphologyOperate->m_nMethod;
	int nFilterSizeX = pParam_MorphologyOperate->m_nFilterSizeX;
	int nFilterSizeY = pParam_MorphologyOperate->m_nFilterSizeY;
	bool bCircleFilter = pParam_MorphologyOperate->m_bCircleFilter;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (nFilterSizeX <= 0) nFilterSizeX = 1;
	if (nFilterSizeY <= 0) nFilterSizeY = 1;

	Mat dFilter;
	if (bCircleFilter) {
		dFilter = Mat::zeros(nFilterSizeY, nFilterSizeX, CV_8UC1);
		RotatedRect dRotatedRect;
		dRotatedRect.center = Point2f((nFilterSizeX - 1) * 0.5f, (nFilterSizeY - 1) * 0.5f);
		dRotatedRect.size = Size2f(nFilterSizeX - 1, nFilterSizeY - 1);

		ellipse(dFilter, dRotatedRect, Scalar(255, 255, 255), FILLED);
	}
	else {
		dFilter = Mat::ones(nFilterSizeY, nFilterSizeX, dImg.type());
	}

	int nOperate;
	if (nMethod == NIPLParam_MorphologyOperate::METHOD_ERODE) nOperate = MORPH_ERODE;
	else if (nMethod == NIPLParam_MorphologyOperate::METHOD_DILATE) nOperate = MORPH_DILATE;
	else if (nMethod == NIPLParam_MorphologyOperate::METHOD_OPEN) nOperate = MORPH_OPEN;
	else if (nMethod == NIPLParam_MorphologyOperate::METHOD_CLOSE) nOperate = MORPH_CLOSE;

	// extend image size to deal with boundary area when the filter value is big
	Mat dExtImg = Mat::zeros(nImgSizeY + 2 * (nFilterSizeY + 1), nImgSizeX + 2 * (nFilterSizeX + 1), dImg.type());
	Rect rcCenter(nFilterSizeX + 1, nFilterSizeY + 1, nImgSizeX, nImgSizeY);
	Mat dCenter(dExtImg, rcCenter);
	dImg.copyTo(dCenter);

	morphologyEx(dExtImg, dExtImg, nOperate, dFilter);

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, dImg.type());
	dCenter.copyTo(dOutputImg);

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(Move)
{
	VERIFY_PARAMS();
	NIPLParam_Move *pParam_Move = (NIPLParam_Move *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	float nOffsetX = pParam_Move->m_nOffsetX;
	float nOffsetY = pParam_Move->m_nOffsetY;
	float nBorder = pParam_Move->m_nBorder;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nBorderMode = BORDER_CONSTANT;
	Scalar dColor = CV_RGB(0, 0, 0);
	
	if (nBorder == NIPLParam_Move::FILL_WHITE) {
		dColor = CV_RGB(255, 255, 255);
	}
	else if(nBorder == NIPLParam_Move::ROTATE) {
		nBorderMode = BORDER_WRAP;
	}

	Mat dTranslate = (Mat_<double>(2, 3) << 1, 0, nOffsetX, 0, 1, nOffsetY);

	Mat dOutputImg;
	warpAffine(dImg, dOutputImg, dTranslate, Size(nImgSizeX, nImgSizeY), INTER_LINEAR, nBorderMode, dColor);

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(Rotate)
{
	VERIFY_PARAMS();
	NIPLParam_Rotate *pParam_Rotate = (NIPLParam_Rotate *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	float nCenterPosX = pParam_Rotate->m_nCenterPosX;
	float nCenterPosY = pParam_Rotate->m_nCenterPosY;
	float nAngle = pParam_Rotate->m_nAngle;
	float nScale = pParam_Rotate->m_nScale;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	Point2f ptCenter(nCenterPosX, nCenterPosY);
	Mat dRot = getRotationMatrix2D(ptCenter, nAngle, nScale);

	Mat dOutputImg;
	warpAffine(dImg, dOutputImg, dRot, Size(nImgSizeX, nImgSizeY), INTER_LINEAR, BORDER_REPLICATE);

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}


NIPL_METHOD_IMPL(CopySubImageFrom)
{
	VERIFY_PARAMS();
	NIPLParam_CopySubImageFrom *pParam_CopySubImageFrom = (NIPLParam_CopySubImageFrom *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nStartPosX = pParam_CopySubImageFrom->m_nStartPosX;
	int nStartPosY = pParam_CopySubImageFrom->m_nStartPosY;
	int nEndPosX = pParam_CopySubImageFrom->m_nEndPosX;
	int nEndPosY = pParam_CopySubImageFrom->m_nEndPosY;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (nStartPosX < 0 || nEndPosX >= nImgSizeX || nStartPosY < 0 || nEndPosY >= nImgSizeY || nStartPosX > nEndPosX || nStartPosY > nEndPosY) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}

//	DebugPrintImageProperty(L"CopySubImageFrom Input", dImg);

	int nSubImageWidth = nEndPosX - nStartPosX + 1;
	int nSubImageHeight = nEndPosY - nStartPosY + 1;
	Mat dOutputImg(dImg, Rect(nStartPosX, nStartPosY, nSubImageWidth, nSubImageHeight));

//	DebugPrintImageProperty(L"CopySubImageFrom Output", dOutputImg);

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(CopySubImageTo)
{
	VERIFY_PARAMS();
	NIPLParam_CopySubImageTo *pParam_CopySubImageTo = (NIPLParam_CopySubImageTo *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	Mat dTargetImg = pParam_CopySubImageTo->m_dTargetImg;
	int nStartPosX = pParam_CopySubImageTo->m_nStartPosX;
	int nStartPosY = pParam_CopySubImageTo->m_nStartPosY;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nEndPosX = nStartPosX + nImgSizeX - 1;
	int nEndPosY = nStartPosY + nImgSizeY - 1;

	int nTargetImgSizeY = dTargetImg.rows;
	int nTargetImgSizeX = dTargetImg.cols;

	if (nStartPosX < 0 || nEndPosX >= nTargetImgSizeX || nStartPosY < 0 || nEndPosY >= nTargetImgSizeY || nStartPosX > nEndPosX || nStartPosY > nEndPosY) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}

	Mat dOutputImg;
	dTargetImg.copyTo(dOutputImg);

	Mat dSubImg = dImg;
	if (dSubImg.type() != dOutputImg.type()) {
		CHECK_EXCEPTION(dSubImg.convertTo(dSubImg, dOutputImg.type()));
	}

	Mat dROI(dOutputImg, Rect(nStartPosX, nStartPosY, nImgSizeX, nImgSizeY));
	dSubImg.copyTo(dROI);

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(Thresholding)
{
	VERIFY_PARAMS();
	NIPLParam_Thresholding *pParam_Thresholding = (NIPLParam_Thresholding *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	Mat dMask = pInput->m_dMask;
	int nMethod = pParam_Thresholding->m_nMethod;
	float nThreshold = pParam_Thresholding->m_nThreshold;
	bool bApplyMaskOnlyToResult = pParam_Thresholding->m_bApplyMaskOnlyToResult;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if ((dImg.depth() != CV_8U && dImg.depth() != CV_32F) || dImg.channels() != 1) {
		return NIPL_ERR_INVALID_IMAGE_TYPE;
	}

	int nType = CV_THRESH_BINARY;	// set Upper as default
	if (nMethod & NIPLParam_Thresholding::METHOD_LOWER) {
		nType = CV_THRESH_BINARY_INV;
	}
	if (nMethod & NIPLParam_Thresholding::METHOD_OTSU) {
		nType |= CV_THRESH_OTSU;
	}

//	DebugPrintImageProperty(L"Thresholding Input", dImg);

	Mat dTempImg;
	dImg.copyTo(dTempImg);

	// change first to 8 bit image.
/*
	if (dTempImg.depth() == CV_32F) {
		dTempImg *= 255.f;
		dTempImg.convertTo(dTempImg, CV_8U);
	}
*/

	bool bExistMask = !CHECK_EMPTY_IMAGE(dMask);
	if (bExistMask && !bApplyMaskOnlyToResult) {
		// Set non-ROI to the value not to be selected
		Scalar dMean;
		Scalar dStd;
		meanStdDev(dTempImg, dMean, dStd, dMask);

		if (dTempImg.depth() == CV_8U) {
			UINT8 nMean = (UINT8)dMean[0];

			auto itTempImg = dTempImg.begin<UINT8>();
			auto itTempImg_End = dTempImg.end<UINT8>();
			auto itMask = dMask.begin<UINT8>();

			while (itTempImg != itTempImg_End) {
				if (*itMask == 0) {
					*itTempImg = nMean;
				}

				itTempImg++;
				itMask++;
			}
		}
		else {
			FLOAT nMean = (UINT8)dMean[0];

			auto itTempImg = dTempImg.begin<FLOAT>();
			auto itTempImg_End = dTempImg.end<FLOAT>();
			auto itMask = dMask.begin<UINT8>();

			while (itTempImg != itTempImg_End) {
				if (*itMask == 0) {
					*itTempImg = nMean;
				}

				itTempImg++;
				itMask++;
			}
		}
	}

//	DebugPrintImageProperty(L"Thresholding Converted", dTempImg);
	Mat dOutputImg;
	if (nMethod & NIPLParam_Thresholding::METHOD_OTSU && nThreshold > 0.f) {
		double nThresholdOtsu;
		CHECK_EXCEPTION(nThresholdOtsu = threshold(dTempImg, dOutputImg, 0, 255, nType));

		nType &= ~CV_THRESH_OTSU;
		CHECK_EXCEPTION(threshold(dTempImg, dOutputImg, nThresholdOtsu * nThreshold, 255, nType));
	}
	else {
		CHECK_EXCEPTION(threshold(dTempImg, dOutputImg, nThreshold, 255, nType));
	}
	
	if (dOutputImg.depth() == CV_32F) {
		dOutputImg.convertTo(dOutputImg, CV_8U);
	}

	if (bExistMask) {
		dOutputImg = dOutputImg.mul(dMask);
	}

//	DebugPrintImageProperty(L"Thresholding Output", dOutputImg);

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(ColorThresholding)
{
	VERIFY_PARAMS();
	NIPLParam_ColorThresholding *pParam_Thresholding = (NIPLParam_ColorThresholding *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	Mat dMask = pInput->m_dMask;
	int nMethod_R = pParam_Thresholding->m_nMethod_R;
	int nMethod_G = pParam_Thresholding->m_nMethod_G;
	int nMethod_B = pParam_Thresholding->m_nMethod_B;
	float nLowerValue_R = pParam_Thresholding->m_nLowerValue_R;
	float nLowerValue_G = pParam_Thresholding->m_nLowerValue_G;
	float nLowerValue_B = pParam_Thresholding->m_nLowerValue_B;
	float nUpperValue_R = pParam_Thresholding->m_nUpperValue_R;
	float nUpperValue_G = pParam_Thresholding->m_nUpperValue_G;
	float nUpperValue_B = pParam_Thresholding->m_nUpperValue_B;
	float nLowerRatio_R_G = pParam_Thresholding->m_nLowerRatio_R_G;
	float nLowerRatio_R_B = pParam_Thresholding->m_nLowerRatio_R_B;
	float nLowerRatio_G_B = pParam_Thresholding->m_nLowerRatio_G_B;
	float nUpperRatio_R_G = pParam_Thresholding->m_nUpperRatio_R_G;
	float nUpperRatio_R_B = pParam_Thresholding->m_nUpperRatio_R_B;
	float nUpperRatio_G_B = pParam_Thresholding->m_nUpperRatio_G_B;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if ((dImg.depth() != CV_8U && dImg.depth() != CV_32F) || dImg.channels() != 3) {
		return NIPL_ERR_INVALID_IMAGE_TYPE;
	}

	Mat dTempImg;
	dImg.copyTo(dTempImg);

	// change first to 8 bit image.
/*
	if (dTempImg.depth() == CV_32F) {
		nLowerValue_R *= 255.f;
		nLowerValue_G *= 255.f;
		nLowerValue_B *= 255.f;
		nUpperValue_R *= 255.f;
		nUpperValue_G *= 255.f;
		nUpperValue_B *= 255.f;

		dTempImg *= 255.f;
		dTempImg.convertTo(dTempImg, CV_8U);
	}
*/

	int nMin_R = 0;
	int nMin_G = 0;
	int nMin_B = 0;
	int nMax_R = 255;
	int nMax_G = 255;
	int nMax_B = 255;

	ColorThresholding_SetThreshold(nMethod_R, nLowerValue_R, nUpperValue_R, nMin_R, nMax_R);
	ColorThresholding_SetThreshold(nMethod_G, nLowerValue_G, nUpperValue_G, nMin_G, nMax_G);
	ColorThresholding_SetThreshold(nMethod_B, nLowerValue_B, nUpperValue_B, nMin_B, nMax_B);

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

	Vec3b dPixel;
	UINT8 nR, nG, nB;
	for (int nY = 0; nY < nImgSizeY; nY++) {
		for (int nX = 0; nX < nImgSizeX; nX++) {
			dPixel = dTempImg.at<Vec3b>(nY, nX);
			nR = dPixel[2];
			nG = dPixel[1];
			nB = dPixel[0];

			if (nR < nMin_R || nR > nMax_R) {
				continue;
			}
			if (nG < nMin_G || nG > nMax_G) {
				continue;
			}
			if (nB < nMin_B || nB > nMax_B) {
				continue;
			}

			if (nLowerRatio_R_G != 0.f && nR < nG * nLowerRatio_R_G) {
				continue;
			}
			if (nUpperRatio_R_G != 0.f && nR > nG * nUpperRatio_R_G) {
				continue;
			}
			if (nLowerRatio_R_B != 0.f && nR < nB * nLowerRatio_R_B) {
				continue;
			}
			if (nUpperRatio_R_B != 0.f && nR > nB * nUpperRatio_R_B) {
				continue;
			}
			if (nLowerRatio_G_B != 0.f && nG < nB * nLowerRatio_G_B) {
				continue;
			}
			if (nUpperRatio_G_B != 0.f && nG > nB * nUpperRatio_G_B) {
				continue;
			}

			dOutputImg.at<UINT8>(nY, nX) = 255;
		}
	}

	if (dOutputImg.depth() == CV_32F) {
		dOutputImg.convertTo(dOutputImg, CV_8U);
	}

	bool bExistMask = !CHECK_EMPTY_IMAGE(dMask);
	if (bExistMask) {
		dOutputImg = dOutputImg.mul(dMask);
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

void NIPL::ColorThresholding_SetThreshold(int nMethod, float nLowerValue, float nUpperValue, int &nMin, int &nMax)
{
	switch (nMethod) {
	case NIPLParam_ColorThresholding::METHOD_LOWER:
		nMin = 0;
		nMax = (int)nLowerValue;
		break;
	case NIPLParam_ColorThresholding::METHOD_UPPER:
		nMin = (int)nUpperValue;
		nMax = 255;
		break;
	case NIPLParam_ColorThresholding::METHOD_IN_RANGE:
		nMin = (int)nLowerValue;
		nMax = (int)nUpperValue;
		break;
	}
}

NIPL_METHOD_IMPL(Smoothing)
{
	VERIFY_PARAMS();
	NIPLParam_Smoothing *pParam_Smoothing = (NIPLParam_Smoothing *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nMethod = pParam_Smoothing->m_nMethod;
	int nFilterSize = pParam_Smoothing->m_nFilterSize;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	Mat dOutputImg;
	if (nMethod & NIPLParam_Smoothing::METHOD_FILTER) {
		if (nFilterSize > 0) {
			blur(dImg, dOutputImg, Size(nFilterSize, nFilterSize));
			pOutput->m_dImg = dOutputImg;
		}
	}

	if (CHECK_EMPTY_IMAGE(pOutput->m_dImg)) {
		return NIPL_ERR_PASS;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(EdgeDetecting)
{
	VERIFY_PARAMS();
	NIPLParam_EdgeDetecting *pParam_EdgeDetecting = (NIPLParam_EdgeDetecting *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nMethod = pParam_EdgeDetecting->m_nMethod;
	float nCannyLowerThreshold = pParam_EdgeDetecting->m_nCannyLowerThreshold;
	float nCannyUpperThreshold = pParam_EdgeDetecting->m_nCannyUpperThreshold;
	int nFilterSize = pParam_EdgeDetecting->m_nFilterSize;
	int nLineThickness = pParam_EdgeDetecting->m_nLineThickness;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (dImg.channels() == 3) {
		cvtColor(dImg, dImg, CV_BGR2GRAY);
	}

	Mat dOutputImg;
	if (nMethod == NIPLParam_EdgeDetecting::METHOD_CANNY) {
		if (nFilterSize > 0) {
			if (nCannyLowerThreshold == 0.f && nCannyUpperThreshold == 0.f) {
				Mat dTemp;
				nCannyUpperThreshold = (float)threshold(dImg, dTemp, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
				nCannyLowerThreshold = nCannyUpperThreshold * 0.5f;
			}

			Canny(dImg, dOutputImg, nCannyLowerThreshold, nCannyUpperThreshold, nFilterSize);
			if (nLineThickness > 1) {
				Mat dFilter = Mat::ones(nLineThickness, nLineThickness, dImg.type());
				morphologyEx(dOutputImg, dOutputImg, MORPH_DILATE, dFilter);
			}
			pOutput->m_dImg = dOutputImg;
		}
	}
	else if (nMethod == NIPLParam_EdgeDetecting::METHOD_CONTOUR) {
		Mat dTempImg;
		dImg.copyTo(dTempImg);
		vector<vector<Point>> dContours;
		CHECK_EXCEPTION(findContours(dTempImg, dContours, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE));

		dOutputImg = Mat::zeros(dTempImg.size(), CV_8UC3);
		for (int i = 0; i < dContours.size(); i++)
		{
			drawContours(dOutputImg, dContours, i, Scalar(255, 255, 255), nLineThickness);
		}

		pOutput->m_dImg = dOutputImg;
	}

	if (CHECK_EMPTY_IMAGE(pOutput->m_dImg)) {
		return NIPL_ERR_PASS;
	}

	return NIPL_ERR_SUCCESS;
}


NIPL_METHOD_IMPL(Thinning)
{
	VERIFY_PARAMS();
	NIPLParam_Thinning *pParam_Thinning = (NIPLParam_Thinning *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	Mat dOutputImg;
	dImg.copyTo(dOutputImg);

	double nMin, nMax;
	minMaxLoc(dOutputImg, &nMin, &nMax);
	if (nMax > 1) {
		dOutputImg /= nMax;
	}

	Mat dPrev = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	Mat dDiff;

	int nCount = 0;
	do {
		Thinning_Iteration(dOutputImg, 0);
		Thinning_Iteration(dOutputImg, 1);
		absdiff(dOutputImg, dPrev, dDiff);
		dOutputImg.copyTo(dPrev);
		
		nCount = countNonZero(dDiff);
	} while (nCount > 0);

	if (nMax > 1) {
		dOutputImg *= nMax;
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

void NIPL::Thinning_Iteration(Mat &dImg, int nIter)
{
	cv::Mat dMarker = cv::Mat::zeros(dImg.size(), CV_8UC1);

	for (int i = 1; i < dImg.rows - 1; i++)
	{
		for (int j = 1; j < dImg.cols - 1; j++)
		{
			uchar p2 = dImg.at<uchar>(i - 1, j);
			uchar p3 = dImg.at<uchar>(i - 1, j + 1);
			uchar p4 = dImg.at<uchar>(i, j + 1);
			uchar p5 = dImg.at<uchar>(i + 1, j + 1);
			uchar p6 = dImg.at<uchar>(i + 1, j);
			uchar p7 = dImg.at<uchar>(i + 1, j - 1);
			uchar p8 = dImg.at<uchar>(i, j - 1);
			uchar p9 = dImg.at<uchar>(i - 1, j - 1);

			int A = (p2 == 0 && p3 == 1) + (p3 == 0 && p4 == 1) +
				(p4 == 0 && p5 == 1) + (p5 == 0 && p6 == 1) +
				(p6 == 0 && p7 == 1) + (p7 == 0 && p8 == 1) +
				(p8 == 0 && p9 == 1) + (p9 == 0 && p2 == 1);
			int B = p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
			int m1 = nIter == 0 ? (p2 * p4 * p6) : (p2 * p4 * p8);
			int m2 = nIter == 0 ? (p4 * p6 * p8) : (p2 * p6 * p8);

			if (A == 1 && (B >= 2 && B <= 6) && m1 == 0 && m2 == 0)
				dMarker.at<uchar>(i, j) = 1;
		}
	}

	dImg &= ~dMarker;
}

NIPL_METHOD_IMPL(ConvexHull)
{
	VERIFY_PARAMS();
	NIPLParam_ConvexHull *pParam_ConvexHull = (NIPLParam_ConvexHull *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	bool m_bFill = pParam_ConvexHull->m_bFill;
	int nLineThickness = pParam_ConvexHull->m_nLineThickness;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (m_bFill) {
		nLineThickness = FILLED;
	}

	Mat dTempImg;
	dImg.copyTo(dTempImg);
	vector<vector<Point>> dContours;
	CHECK_EXCEPTION(findContours(dTempImg, dContours, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE));

	vector<vector<Point>> dHull(dContours.size());
	for (int i = 0; i < dContours.size(); i++)
	{
		convexHull(Mat(dContours[i]), dHull[i], false);
	}

	Mat dOutputImg = Mat::zeros(dTempImg.size(), CV_8UC3);
	for (int i = 0; i < dContours.size(); i++)
	{
		drawContours(dOutputImg, dContours, i, Scalar(255, 255, 255), nLineThickness);
		drawContours(dOutputImg, dHull, i, Scalar(255, 255, 255), nLineThickness);
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}


