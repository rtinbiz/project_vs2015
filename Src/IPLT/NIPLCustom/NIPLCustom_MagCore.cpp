// NIPLCustom.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NIPLCustom.h"
#include "Xml/xml3.h"
using namespace XML3;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#define SKIP_INNER_CAMERA_DEFECT

#define RADIUS_EDGE_SPOT_POS_UPPER 0x01
#define RADIUS_EDGE_SPOT_POS_LOWER 0x02
#define RADIUS_EDGE_SPOT_POS_BOTH (RADIUS_EDGE_SPOT_POS_UPPER | RADIUS_EDGE_SPOT_POS_LOWER);

#define RADIUS_EDGE_HOLE 0x01
#define RADIUS_EDGE_PERFECT_HOLE 0x02

inline float calc_angle(const Point2f &ptLeft, const Point2f &ptCenter, const Point2f &ptRight)
{
	Point2f pt1(ptLeft.x - ptCenter.x, ptLeft.y - ptCenter.y);
	Point2f pt2(ptRight.x - ptCenter.x, ptRight.y - ptCenter.y);;

	float nDenom = (sqrtf(powf(pt1.x, 2) + powf(pt1.y, 2))*sqrtf(powf(pt2.x, 2) + powf(pt2.y, 2)));
	if (nDenom == 0.f) {
		return 0.f;
	}

	float nCos = (pt1.x*pt2.x + pt1.y*pt2.y) / nDenom;
	if (nCos < -1.f) nCos = -1.f;
	else if (nCos > 1.f) nCos = 1.f;
	float nAngle = acosf(nCos) * (180.f / (float)CV_PI);

	return nAngle;
}


bool NIPLCustom::MagCore_LoadData_Calibration(NIPLParam_MagCore_Calibration *pParam)
{
	wstring strDataPath = pParam->m_strDataPath;
	wstring strComp = pParam->m_strComp;

	XML dXML;
	auto nResult = dXML.Load(strDataPath.c_str());
	if (nResult != XML_PARSE::OK) {
		return false;
	}

	auto dRoot = dXML.GetRootElement();
	auto listCategory = dRoot.GetChildren();
	for (auto pCategory : listCategory) {
		wstring strElementName = str2wstr(pCategory->GetElementName());
		if (CHECK_STRING(strElementName, L"Calibration")) {
			auto listData = pCategory->GetChildren();
			for (auto pData : listData) {
				strElementName = str2wstr(pData->GetElementName());
				if (CHECK_STRING(strElementName, strComp)) {
					try { pParam->m_nColor2GrayChannel = stoi(pData->v("color_to_gray_channel")); }
					catch (...) { pParam->m_nColor2GrayChannel = 0; }
					try { pParam->m_nBrightnessEnhancementLevel = stoi(pData->v("brightness_enhancement_level")); }
					catch (...) { pParam->m_nBrightnessEnhancementLevel = 0; }

					try { pParam->m_nCheckCoreThreshold = stoi(pData->v("check_core_threshold")); }
					catch (...) { pParam->m_nCheckCoreThreshold = 0.f; }

					pParam->m_nCenterPosX = stoi(pData->v("center_pos_x"));
					pParam->m_nCenterPosY = stoi(pData->v("center_pos_y"));
					pParam->m_nMinRadiusPosX = stoi(pData->v("min_radius_pos_x"));
					pParam->m_nMaxRadiusPosX = stoi(pData->v("max_radius_pos_x"));
					pParam->m_nMinRadiusRange = stoi(pData->v("min_radius_range"));
					pParam->m_nMaxRadiusRange = stoi(pData->v("max_radius_range"));
					pParam->m_nMinRadiusMargin = stoi(pData->v("min_radius_margin"));
					pParam->m_nMaxRadiusMargin = stoi(pData->v("max_radius_margin"));

					pParam->m_bMinRadiusReverseEdgeDetect = (stoi(pData->v("min_radius_reverse_edge_detect")) == 1) ? true : false;
					pParam->m_bMaxRadiusReverseEdgeDetect = (stoi(pData->v("max_radius_reverse_edge_detect")) == 1) ? true : false;
					try { pParam->m_bMinRadiusBinarize = (stoi(pData->v("min_radius_binarize")) == 1) ? true : false; }
					catch (...) { pParam->m_bMinRadiusBinarize = false; }
					try { pParam->m_bMaxRadiusBinarize = (stoi(pData->v("max_radius_binarize")) == 1) ? true : false; }
					catch (...) { pParam->m_bMaxRadiusBinarize = false; }
					try { pParam->m_bMinRadiusBlack = (stoi(pData->v("min_radius_black")) == 1) ? true : false; }
					catch (...) { pParam->m_bMinRadiusBlack = false; }
					try { pParam->m_bMaxRadiusBlack = (stoi(pData->v("max_radius_black")) == 1) ? true : false; }
					catch (...) { pParam->m_bMaxRadiusBlack = false; }
				}
			}
		}
	}

	return true;
}

bool NIPLCustom::MagCore_LoadData_Inspection(NIPLParam_MagCore *pParam)
{
	wstring strDataPath = pParam->m_strDataPath;
	wstring strComp = pParam->m_strComp;

	XML dXML;
	auto nResult = dXML.Load(strDataPath.c_str());
	if (nResult != XML_PARSE::OK) {
		return false;
	}

	auto dRoot = dXML.GetRootElement();
	auto listCategory = dRoot.GetChildren();
	for (auto pCategory : listCategory) {
		wstring strElementName = str2wstr(pCategory->GetElementName());
		if (CHECK_STRING(strElementName, L"Inspection")) {
			auto listData = pCategory->GetChildren();
			for (auto pData : listData) {
				strElementName = str2wstr(pData->GetElementName());
				if (CHECK_STRING(strElementName, strComp)) {
					try { pParam->m_bRadiusEdgeDoubleLine = (stoi(pData->v("radius_edge_double_line")) == 1) ? true : false; }
					catch (...) { pParam->m_bRadiusEdgeDoubleLine = false; }
					try { pParam->m_bMinRadiusEdgeInsideCheck = (stoi(pData->v("min_radius_edge_inside_check")) == 1) ? true : false; }
					catch (...) { pParam->m_bMinRadiusEdgeInsideCheck = false; }
					try { pParam->m_bMinRadiusEdgeCheck = (stoi(pData->v("min_radius_edge_check")) == 1) ? true : false; }
					catch (...) { pParam->m_bMinRadiusEdgeCheck = false; }
					try { pParam->m_bMaxRadiusEdgeCheck = (stoi(pData->v("max_radius_edge_check")) == 1) ? true : false; }
					catch (...) { pParam->m_bMaxRadiusEdgeCheck = false; }
					try { pParam->m_bMinRadiusEdgeBlack = (stoi(pData->v("min_radius_edge_black")) == 1) ? true : false; }
					catch (...) { pParam->m_bMinRadiusEdgeBlack = false; }
					try { pParam->m_bMaxRadiusEdgeBlack = (stoi(pData->v("max_radius_edge_black")) == 1) ? true : false; }
					catch (...) { pParam->m_bMaxRadiusEdgeBlack = false; }
					try { pParam->m_nMinRadiusEdgeInsideAreaHeight = stoi(pData->v("min_radius_edge_inside_area_height")); }
					catch (...) {}
					try { pParam->m_nMinRadiusEdgeAreaHeight = stoi(pData->v("min_radius_edge_area_height")); }
					catch (...) {}
					try { pParam->m_nMaxRadiusEdgeAreaHeight = stoi(pData->v("max_radius_edge_area_height")); }
					catch (...) {}
					try { pParam->m_nMinRadiusEdgeInsideThreshold = stof(pData->v("min_radius_edge_inside_threshold")); }
					catch (...) {}
					try { pParam->m_nMinRadiusEdgeSpotBinarizeThreshold = stof(pData->v("min_radius_edge_spot_binarize_threshold")); }
					catch (...) {}
					try { pParam->m_nMaxRadiusEdgeSpotBinarizeThreshold = stof(pData->v("max_radius_edge_spot_binarize_threshold")); }
					catch (...) {}
					try { pParam->m_nMinRadiusEdgeSpotProfileThreshold = stof(pData->v("min_radius_edge_spot_profile_threshold")); }
					catch (...) {}
					try { pParam->m_nMaxRadiusEdgeSpotProfileThreshold = stof(pData->v("max_radius_edge_spot_profile_threshold")); }
					catch (...) {}
					try { pParam->m_nMinRadiusEdgeHoleBinarizeThreshold = stof(pData->v("min_radius_edge_hole_binarize_threshold")); }
					catch (...) {}
					try { pParam->m_nMinRadiusEdgeHoleBinarizeThreshold2 = stof(pData->v("min_radius_edge_hole_binarize_threshold_2")); }
					catch (...) {}
					try { pParam->m_nMaxRadiusEdgeHoleBinarizeThreshold = stof(pData->v("max_radius_edge_hole_binarize_threshold")); }
					catch (...) {}
					try { pParam->m_nMaxRadiusEdgeHoleBinarizeThreshold2 = stof(pData->v("max_radius_edge_hole_binarize_threshold_2")); }
					catch (...) {}
					try { pParam->m_nMinRadiusEdgeBinarizeThreshold = stof(pData->v("min_radius_edge_binarize_threshold")); }
					catch (...) {}
					try { pParam->m_nMinRadiusEdgeBinarizeThreshold2 = stof(pData->v("min_radius_edge_binarize_threshold_2")); }
					catch (...) {}
					try { pParam->m_nMaxRadiusEdgeBinarizeThreshold = stof(pData->v("max_radius_edge_binarize_threshold")); }
					catch (...) {}
					try { pParam->m_nMaxRadiusEdgeBinarizeThreshold2 = stof(pData->v("max_radius_edge_binarize_threshold_2")); }
					catch (...) {}
					try { pParam->m_nMinRadiusEdgeProfileThreshold = stof(pData->v("min_radius_edge_profile_threshold")); }
					catch (...) {}
					try { pParam->m_nMinRadiusEdgeProfileThreshold2 = stof(pData->v("min_radius_edge_profile_threshold_2")); }
					catch (...) {}
					try { pParam->m_nMaxRadiusEdgeProfileThreshold = stof(pData->v("max_radius_edge_profile_threshold")); }
					catch (...) {}
					try { pParam->m_nMaxRadiusEdgeProfileThreshold2 = stof(pData->v("max_radius_edge_profile_threshold_2")); }
					catch (...) {}

					try { pParam->m_nSurfaceMinRadiusEdgeMargin = stoi(pData->v("surface_min_radius_edge_margin")); }
					catch (...) {}
					try { pParam->m_nSurfaceMaxRadiusEdgeMargin = stoi(pData->v("surface_max_radius_edge_margin")); }
					catch (...) {}
					try { pParam->m_nSurfaceBlockCount = stoi(pData->v("surface_block_count")); }
					catch (...) {}
					try { pParam->m_nSurfaceBlockBinarizeThreshold = stof(pData->v("surface_block_binarize_threshold")); }
					catch (...) {}
					try { pParam->m_nSurfaceBlockVarianceRatio = stof(pData->v("surface_block_variance_ratio")); }
					catch (...) {}
					try { pParam->m_nSurfaceBlockVarianceMax = stof(pData->v("surface_block_variance_max")); }
					catch (...) {}
					try { pParam->m_nSurfaceBlockVarianceThreshold = stof(pData->v("surface_block_variance_threshold")); }
					catch (...) {}

					try { pParam->m_nHoleMinRadiusEdgeMargin = stoi(pData->v("hole_min_radius_edge_margin")); }
					catch (...) {}
					try { pParam->m_nHoleMaxRadiusEdgeMargin = stoi(pData->v("hole_max_radius_edge_margin")); }
					catch (...) {}
					try { pParam->m_nHoleBlockCountX = stoi(pData->v("hole_block_count_x")); }
					catch (...) {}
					try { pParam->m_nHoleBlockCountY = stoi(pData->v("hole_block_count_y")); }
					catch (...) {}
					try { pParam->m_nHoleBinarizeThreshold = stof(pData->v("hole_binarize_threshold")); }
					catch (...) {}
					try { pParam->m_nHoleMinSize = stof(pData->v("hole_min_size")); }
					catch (...) {}
					try { pParam->m_nHoleSizeRatio = stof(pData->v("hole_size_ratio")); }
					catch (...) {}
					try { pParam->m_nHoleSmallSize = stof(pData->v("hole_small_size")); }
					catch (...) {}
					try { pParam->m_nHoleSmallSizeRatio = stof(pData->v("hole_small_size_ratio")); }
					catch (...) {}
					try { pParam->m_nHoleBigSize = stof(pData->v("hole_big_size")); }
					catch (...) {}
					try { pParam->m_nHoleBigSizeRatio = stof(pData->v("hole_big_size_ratio")); }
					catch (...) {}

					try { pParam->m_bCharCheck = (stoi(pData->v("char_check")) == 1) ? true : false; }
					catch (...) { pParam->m_bCharCheck = false; }
					try { pParam->m_bCharBlack = (stoi(pData->v("char_black")) == 1) ? true : false; }
					catch (...) { pParam->m_bCharBlack = false; }
					try { pParam->m_nCharMinRadiusEdgeMargin = stoi(pData->v("char_min_radius_edge_margin")); }
					catch (...) {}
					try { pParam->m_nCharMaxRadiusEdgeMargin = stoi(pData->v("char_max_radius_edge_margin")); }
					catch (...) {}
					try { pParam->m_nCharConnectDistance = stoi(pData->v("char_connect_distance")); }
					catch (...) {}
				}
			}
		}
	}

	return true;
}

NIPL_ERR NIPLCustom::MagCore_Calibration_FindCircle(Mat dImg, Mat &dOutputImg, NIPLParam_MagCore_Calibration *pParam, NIPLEllipse &dFoundCircle, bool bMaxCircle)
{
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	wstring strComp = pParam->m_strComp;
	wstring strShowImage = pParam->m_strShowImage;
	float nCheckCoreThreshold = pParam->m_nCheckCoreThreshold;
	int nCenterPosX = pParam->m_nCenterPosX;
	int nCenterPosY = pParam->m_nCenterPosY;
	int nMinRadius = pParam->m_nMinRadiusPosX - nCenterPosX;
	int nMaxRadius = pParam->m_nMaxRadiusPosX - nCenterPosX;
	int nMinRadiusRange = pParam->m_nMinRadiusRange;
	int nMaxRadiusRange = pParam->m_nMaxRadiusRange;
	int nMinRadiusMargin = pParam->m_nMinRadiusMargin;
	int nMaxRadiusMargin = pParam->m_nMaxRadiusMargin;
	float nSamplingAngleStep = pParam->m_nSamplingAngleStep;
	bool bMinRadiusReverseEdgeDetect = pParam->m_bMinRadiusReverseEdgeDetect;
	bool bMaxRadiusReverseEdgeDetect = pParam->m_bMaxRadiusReverseEdgeDetect;
	bool bMinRadiusBinarize = pParam->m_bMinRadiusBinarize;
	bool bMaxRadiusBinarize = pParam->m_bMaxRadiusBinarize;
	bool bMinRadiusBlack = pParam->m_bMinRadiusBlack;
	bool bMaxRadiusBlack = pParam->m_bMaxRadiusBlack;

	bool bBinarize = (bMaxCircle && bMaxRadiusBinarize) || (!bMaxCircle && bMinRadiusBinarize);
	bool bBlack = (bMaxCircle && bMaxRadiusBlack) || (!bMaxCircle && bMinRadiusBlack);

	NIPLInput dInput;
	NIPLOutput dOutput;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	// Circle2Rect
	NIPLParam_Circle2Rect dParam_Circle2Rect;
	dParam_Circle2Rect.m_nCenterPosX = nCenterPosX;
	dParam_Circle2Rect.m_nCenterPosY = nCenterPosY;
	if (bMaxCircle) {
		dParam_Circle2Rect.m_nMinRadius = nMaxRadius - nMaxRadiusRange;
		dParam_Circle2Rect.m_nMaxRadius = nMaxRadius + nMaxRadiusRange;
	}
	else {
		dParam_Circle2Rect.m_nMinRadius = nMinRadius - nMinRadiusRange;
		dParam_Circle2Rect.m_nMaxRadius = nMinRadius + nMinRadiusRange;
	}
	dParam_Circle2Rect.m_nAngleStep = nSamplingAngleStep;

	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_Circle2Rect;
	nErr = Circle2Rect(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dRectImg = dOutput.m_dImg;

	if (!bMaxCircle) {
		Scalar dMean;
		Scalar dStd;
		meanStdDev(dRectImg, dMean, dStd);
		float nStd = (float)dStd[0];

		DebugPrint("Calibration, Check Core, MinCircle Std : %.1f >= %.1f", nStd, nCheckCoreThreshold);
		if (nStd < nCheckCoreThreshold) {
			return NIPL_ERR_NO_OBJECT;
		}
	}

	if ((!bMaxCircle && CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MIN_CIRCLE)) || (bMaxCircle && CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MAX_CIRCLE))) {
		NIPLParam_Rect2Circle dParam_Rect2Circle;
		dParam_Rect2Circle.m_nOutputImgSizeX = nImgSizeX;
		dParam_Rect2Circle.m_nOutputImgSizeY = nImgSizeY;
		dParam_Rect2Circle.m_nCenterPosX = dParam_Circle2Rect.m_nCenterPosX;
		dParam_Rect2Circle.m_nCenterPosY = dParam_Circle2Rect.m_nCenterPosY;
		dParam_Rect2Circle.m_nMinRadius = dParam_Circle2Rect.m_nMinRadius;
		dParam_Rect2Circle.m_nAngleStep = dParam_Circle2Rect.m_nAngleStep;

		dInput.m_dImg = dRectImg;
		dInput.m_pParam = &dParam_Rect2Circle;
		nErr = Rect2Circle(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		dOutputImg = dOutput.m_dImg;
		return NIPL_ERR_SUCCESS;
	}

	Mat dMask;
	if (CHECK_STRING(strComp, STR_MAGCORE_COMP_OUTER) && bBinarize) {
		// Make a mask to avoid the area of characters on thresholding
		dInput.Clear();
		dOutput.Clear();

		NIPLParam_Binarize dParam_Binarize_Char;
		dParam_Binarize_Char.m_nMethod = NIPLParam_Binarize::METHOD_BOTH;
		dParam_Binarize_Char.m_nThreshold = 5.f;

		dInput.m_dImg = dRectImg;
		dInput.m_pParam = &dParam_Binarize_Char;
		nErr = Binarize(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		dMask = ~dOutput.m_dImg;
	}

	// Thresholding
	int nRectImgSizeY = dRectImg.rows;
	int nRectImgSizeX = dRectImg.cols;

	int nDefectSize = nRectImgSizeX * 2;

	Mat dBinImg;
	bool bDefect = false;
	int nTry = 3;
	do {
		dInput.Clear();
		dOutput.Clear();

		if (bBinarize) {
			NIPLParam_Binarize dParam_Binarize;
			if (bBlack) dParam_Binarize.m_nMethod = NIPLParam_Binarize::METHOD_LOWER;
			else dParam_Binarize.m_nMethod = NIPLParam_Binarize::METHOD_UPPER;
			dParam_Binarize.m_nThreshold = 0.5f;

			dInput.m_dImg = dRectImg;
			dInput.m_dMask = dMask;
			dInput.m_pParam = &dParam_Binarize;
			nErr = Binarize(&dInput, &dOutput);
			if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
				return nErr;
			}
			dBinImg = dOutput.m_dImg;
		}
		else {
			NIPLParam_Thresholding dParam_Thresholding;
			if (bBlack) dParam_Thresholding.m_nMethod = NIPLParam_Thresholding::METHOD_LOWER_OTSU;
			else dParam_Thresholding.m_nMethod = NIPLParam_Thresholding::METHOD_UPPER_OTSU;

			dInput.m_dImg = dRectImg;
			dInput.m_dMask = dMask;
			dInput.m_pParam = &dParam_Thresholding;
			nErr = Thresholding(&dInput, &dOutput);
			if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
				return nErr;
			}
			dBinImg = dOutput.m_dImg;
		}

		int nCount = countNonZero(dBinImg);
		if (nCount < nDefectSize) {	// check if the region is one of defects
			bDefect = true;
		}
		else {
			bDefect = false;
		}

		if (bDefect) {
			if (CHECK_EMPTY_IMAGE(dMask)) {
				dMask = Mat::zeros(nRectImgSizeY, nRectImgSizeX, CV_8UC1);
			}
			dMask = dMask | ~dBinImg;

			nTry--;
		}
	} while (bDefect && nTry > 0);

	if ((!bMaxCircle && CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MIN_CIRCLE_BIN)) || (bMaxCircle && CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MAX_CIRCLE_BIN))) {
		NIPLParam_Rect2Circle dParam_Rect2Circle;
		dParam_Rect2Circle.m_nOutputImgSizeX = nImgSizeX;
		dParam_Rect2Circle.m_nOutputImgSizeY = nImgSizeY;
		dParam_Rect2Circle.m_nCenterPosX = dParam_Circle2Rect.m_nCenterPosX;
		dParam_Rect2Circle.m_nCenterPosY = dParam_Circle2Rect.m_nCenterPosY;
		dParam_Rect2Circle.m_nMinRadius = dParam_Circle2Rect.m_nMinRadius;
		dParam_Rect2Circle.m_nAngleStep = dParam_Circle2Rect.m_nAngleStep;

		dInput.m_dImg = dBinImg;
		dInput.m_pParam = &dParam_Rect2Circle;
		nErr = Rect2Circle(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		dOutputImg = dOutput.m_dImg;
		return NIPL_ERR_SUCCESS;
	}

	// Edge Detecting
	Mat dEdgeImage;

	bool bReverse = bMaxCircle? bMaxRadiusReverseEdgeDetect : bMinRadiusReverseEdgeDetect;
	MagCore_Calibration_EdgeDetecting(dBinImg, dEdgeImage, bReverse);

	// Rect2Circle
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_Rect2Circle dParam_Rect2Circle;
	dParam_Rect2Circle.m_nOutputImgSizeX = nImgSizeX;
	dParam_Rect2Circle.m_nOutputImgSizeY = nImgSizeY;
	dParam_Rect2Circle.m_nCenterPosX = dParam_Circle2Rect.m_nCenterPosX;
	dParam_Rect2Circle.m_nCenterPosY = dParam_Circle2Rect.m_nCenterPosY;
	dParam_Rect2Circle.m_nMinRadius = dParam_Circle2Rect.m_nMinRadius;
	dParam_Rect2Circle.m_nAngleStep = dParam_Circle2Rect.m_nAngleStep;

	dInput.m_dImg = dEdgeImage;
	dInput.m_pParam = &dParam_Rect2Circle;
	nErr = Rect2Circle(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	Mat dCircleROIImg = dOutput.m_dImg;

	if ((!bMaxCircle && CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MIN_CIRCLE_EDGE)) || (bMaxCircle && CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MAX_CIRCLE_EDGE))) {
		dOutputImg = dCircleROIImg;
		return NIPL_ERR_SUCCESS;
	}

	// initialize output image
	dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

	// Find Ellipse
	NIPLParam_FindEllipse dParam_FindEllipse;
	dParam_FindEllipse.m_bFindOnlyOne = true;

	dParam_FindEllipse.m_bCheckCenterPos = true;
	dParam_FindEllipse.m_nCenterPosX = nCenterPosX;
	dParam_FindEllipse.m_nCenterPosY = nCenterPosY;
	if (bMaxCircle) {
		dParam_FindEllipse.m_nCenterPosRange = nMaxRadiusRange;
	}
	else {
		dParam_FindEllipse.m_nCenterPosRange = nMinRadiusRange;
	}

	dInput.m_dImg = dCircleROIImg;
	dInput.m_pParam = &dParam_FindEllipse;
	nErr = FindEllipse(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dEllipseImg = dOutput.m_dImg;

	if (dOutput.m_pResult == nullptr) {
		return NIPL_ERR_FAIL_INVALID_DATA;
	}

	NIPLResult_FindEllipse *pResult = (NIPLResult_FindEllipse *)dOutput.m_pResult;
	dFoundCircle = pResult->m_listEllipse[0];

	if (bMaxCircle) {
		dFoundCircle.m_rcEllipse.size.width -= (2 * nMaxRadiusMargin);
		dFoundCircle.m_rcEllipse.size.height -= (2 * nMaxRadiusMargin);
	}
	else {
		dFoundCircle.m_rcEllipse.size.width += (2 * nMinRadiusMargin);
		dFoundCircle.m_rcEllipse.size.height += (2 * nMinRadiusMargin);
	}

	// Adjust ellipse size because it is drawn by pen thickness
	int nThickness = 1;
	RotatedRect dDrawEllipse = dFoundCircle.m_rcEllipse;
	if (bMaxCircle) {
		dDrawEllipse.size.width += (nThickness * 0.5f);
		dDrawEllipse.size.height += (nThickness * 0.5f);
	}
	else {
		dDrawEllipse.size.width -= (nThickness * 0.5f);
		dDrawEllipse.size.height -= (nThickness * 0.5f);
	}
	ellipse(dOutputImg, dDrawEllipse, Scalar(255, 255, 255), nThickness, LINE_AA);

	delete pResult;

	return NIPL_ERR_SUCCESS;
}

void NIPLCustom::MagCore_Calibration_EdgeDetecting(Mat dImg, Mat &dOutputImg, bool bReverse)
{
	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

	for (int nX = 0; nX < nImgSizeX; nX++) {
		if (bReverse) {
			for (int nY = nImgSizeY - 1; nY >= 0; nY--) {
				if (dImg.at<UINT8>(nY, nX) != 0) {
					dOutputImg.at<UINT8>(nY, nX) = 255;
					break;
				}
			}
		}
		else {
			for (int nY = 0; nY < nImgSizeY; nY++) {
				if(dImg.at<UINT8>(nY, nX) != 0) {
					dOutputImg.at<UINT8>(nY, nX) = 255;
					break;
				}
			}
		}
	}
}


NIPL_ERR NIPLCustom::MagCore_Calibration_AdjustCircle(Mat dImg, Mat dCircleImg, Mat &dOutputImg, NIPLParam_MagCore_Calibration *pParam, const NIPLCircle &dMinCircle, const NIPLCircle &dMaxCircle)
{
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	wstring strShowImage = pParam->m_strShowImage;
	int nCenterPosX = dMinCircle.m_ptCenter.x;
	int nCenterPosY = dMinCircle.m_ptCenter.y;
	int nMinRadius = dMinCircle.m_nRadius;
	int nMaxRadius = dMaxCircle.m_nRadius + 1;	// give a margin as 1 pixel
	float nCircle2RectAngleStep = pParam->m_nCircle2RectAngleStep;
	float nRect2CircleAngleStep = pParam->m_nRect2CircleAngleStep;

	NIPLInput dInput;
	NIPLOutput dOutput;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	vector<UINT8> listValue;
	int nSamplingCount = nMaxRadius - nMinRadius + 1;

	// Make rect image first
	int nOutputImgSizeY = nSamplingCount;
	int nOutputImgSizeX = cvCeil(360.f / nCircle2RectAngleStep);
	dOutputImg = Mat::zeros(nOutputImgSizeY, nOutputImgSizeX, CV_8UC1);

	int nImgPosX = 0;
	int nImgPosY = 0;
	for (float nAngle = 0; nAngle < 360.f; nAngle += nCircle2RectAngleStep) {
		float nStartX = (float)(nMinRadius * cos(-nAngle * CV_PI / 180));
		float nStartY = (float)(nMinRadius * sin(-nAngle * CV_PI / 180));
		float nEndX = (float)(nMaxRadius * cos(-nAngle * CV_PI / 180));
		float nEndY = (float)(nMaxRadius * sin(-nAngle * CV_PI / 180));
		float nMidX = (nStartX + nEndX) * 0.5f;
		float nMidY = (nStartY + nEndY) * 0.5f;

		float nDeltaX = (nEndX - nStartX) / (nSamplingCount - 1);
		float nDeltaY = (nEndY - nStartY) / (nSamplingCount - 1);

		MagCore_Calibration_GetCircleValues(dImg, dCircleImg, nCenterPosX, nCenterPosY, nMidX, nMidY, nDeltaX, nDeltaY, nSamplingCount, listValue);

		for (int i = 0; i < nSamplingCount; i++) {
			nImgPosY = i;

			dOutputImg.at<UINT8>(nImgPosY, nImgPosX) = listValue[i];
		}
		nImgPosX++;
	}

	// Rect to Circle
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_Rect2Circle dParam_Rect2Circle;
	dParam_Rect2Circle.m_nOutputImgSizeX = nImgSizeX;
	dParam_Rect2Circle.m_nOutputImgSizeY = nImgSizeY;
	dParam_Rect2Circle.m_nCenterPosX = dMinCircle.m_ptCenter.x;
	dParam_Rect2Circle.m_nCenterPosY = dMinCircle.m_ptCenter.y;
	dParam_Rect2Circle.m_nMinRadius = dMinCircle.m_nRadius;
	dParam_Rect2Circle.m_nCenterPosX = dMinCircle.m_ptCenter.x;
	dParam_Rect2Circle.m_nAngleStep = nRect2CircleAngleStep;

	dInput.m_dImg = dOutputImg;
	dInput.m_pParam = &dParam_Rect2Circle;
	nErr = Rect2Circle(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dOutputImg = dOutput.m_dImg;

	return NIPL_ERR_SUCCESS;
}

void NIPLCustom::MagCore_Calibration_GetCircleValues(Mat dImg, Mat dCircleImg, int nCenterPosX, int nCenterPosY, float nMidX, float nMidY, float nDeltaX, float nDeltaY, int nSamplingCount, vector<UINT8> &listValue)
{
	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	// Find Start Position
	float nStartX = 0;
	float nStartY = 0;

	bool bFound = false;
	int nStep = 0;
	float nPortionStart = 0.f;
	do {
		float nX = nMidX - nDeltaX * nStep;
		float nY = nMidY - nDeltaY * nStep;

		int nImgPosX = cvFloor(nCenterPosX + nX);
		int nImgPosY = cvFloor(nCenterPosY + nY);

		if (nImgPosX < 0 || nImgPosX >(nImgSizeX - 1) || nImgPosY < 0 || nImgPosY > (nImgSizeY - 1)) { 
			bFound = true;
		}
		else {
			UINT8 nValue = dCircleImg.at<UINT8>(nImgPosY, nImgPosX);
			if (nValue > 0) {
				// the line was drawn by LINE_AA, adjust the position depending on the brightness
				nPortionStart = (1.f - (float)nValue / 255);

				bFound = true;
			}
			else {
				nStartX = nX;
				nStartY = nY;
			}
		}

		nStep++;
	} while (!bFound);

	// Find End Position
	float nEndX = 0;
	float nEndY = 0;

	bFound = false;
	nStep = 0;
	float nPortionEnd = 0.f;
	do {
		float nX = nMidX + nDeltaX * nStep;
		float nY = nMidY + nDeltaY * nStep;

		int nImgPosX = cvFloor(nCenterPosX + nX);
		int nImgPosY = cvFloor(nCenterPosY + nY);

		if (nImgPosX < 0 || nImgPosX >(nImgSizeX - 1) || nImgPosY < 0 || nImgPosY > (nImgSizeY - 1)) {
			bFound = true;
		}
		else {
			UINT8 nValue = dCircleImg.at<UINT8>(nImgPosY, nImgPosX);
			if (nValue > 0) {
				// the line was drawn by LINE_AA, adjust the position depending on the brightness
				nPortionEnd = (1.f - (float)nValue / 255);

				bFound = true;
			}
			else {
				nEndX = nX;
				nEndY = nY;
			}
		}

		nStep++;
	} while (!bFound);

	// Get Values
	nDeltaX = (nEndX - nStartX) / (nSamplingCount - 1);
	nDeltaY = (nEndY - nStartY) / (nSamplingCount - 1);

	// the line was drawn by LINE_AA, adjust the position depending on the brightness
	nStartX -= nDeltaX * nPortionStart;
	nStartY -= nDeltaY * nPortionStart;
	nEndX += nDeltaX * nPortionEnd;
	nEndY += nDeltaY * nPortionEnd;

	// Recalculate DeltaX with adjusted Positions
 	nDeltaX = (nEndX - nStartX) / (nSamplingCount - 1);
 	nDeltaY = (nEndY - nStartY) / (nSamplingCount - 1);

	listValue.clear();
	for (int i = 0; i < nSamplingCount; i++) {
		float nX = nStartX + nDeltaX * i;
		float nY = nStartY + nDeltaY * i;
		float nNextX = nStartX + nDeltaX * (i + 1);
		float nNextY = nStartY + nDeltaY * (i + 1);

		float nPortionX = (nNextX - nX) - cvFloor(nNextX - nX);
		float nPortionY = (nNextY - nY) - cvFloor(nNextY - nY);

		int nImgPosX = cvFloor(nCenterPosX + nX);
		int nImgPosY = cvFloor(nCenterPosY + nY);
		int nImgPosNextX = cvFloor(nCenterPosX + nNextX);
		int nImgPosNextY = cvFloor(nCenterPosY + nNextY);

		float nValueX = (UINT8)(dImg.at<UINT8>(nImgPosY, nImgPosX) * (1.f - nPortionX) + dImg.at<UINT8>(nImgPosY, nImgPosNextX) * nPortionX);
		float nValueXOfNextY = (UINT8)(dImg.at<UINT8>(nImgPosNextY, nImgPosX) * (1.f - nPortionX) + dImg.at<UINT8>(nImgPosNextY, nImgPosNextX) * nPortionX);

		UINT8 nValue = (UINT8)(nValueX * (1.f - nPortionY) + nValueXOfNextY * nPortionY);
		listValue.push_back(nValue);
	}
}

NIPL_CUSTOM_METHOD_IMPL(MagCore_Calibration)
{
	VERIFY_PARAMS();

	dTimeProfile_MagCore.Clear_Calibration();
	dTimeProfile_MagCore.SetStartTime_Calibration();

	NIPLParam_MagCore_Calibration *pParam = (NIPLParam_MagCore_Calibration*)pInput->m_pParam;

	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	wstring strShowImage = pParam->m_strShowImage;

	dTimeProfile_MagCore.SetStartTime();
	MagCore_LoadData_Calibration(pParam);
	dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nCalibration_LoadData);

	NIPLInput dInput;
	NIPLOutput dOutput;
	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	// Color2Gray
	int nColor2GrayChannel = pParam->m_nColor2GrayChannel;

	NIPLParam_Color2Gray dParam_Color2Gray;
	dParam_Color2Gray.m_nGrayLevel = NIPLParam_Color2Gray::GRAYLEVEL_256;
	dParam_Color2Gray.m_nChannel = nColor2GrayChannel;
	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_Color2Gray;
	dTimeProfile_MagCore.SetStartTime();
	nErr = Color2Gray(&dInput, &dOutput);
	dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nCalibration_Color2Gray);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dImg = dOutput.m_dImg;

	// Brightness Enhancement
	int nBrightnessEnhancementLevel = pParam->m_nBrightnessEnhancementLevel;

	if (nBrightnessEnhancementLevel > 0) {
		dTimeProfile_MagCore.SetStartTime();
		dInput.Clear();
		dOutput.Clear();

		// Get Invert Image
		NIPLParam_Invert dParam_Invert;
		dInput.m_dImg = dImg;
		dInput.m_pParam = &dParam_Invert;
		nErr = Invert(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}
		Mat dInvertImg = dOutput.m_dImg;

		dInput.Clear();
		dOutput.Clear();
		NIPLParam_Diff dParam_Diff;
		dParam_Diff.m_dTargetImg = dImg;
		dInput.m_dImg = dImg;
		dInput.m_pParam = &dParam_Diff;

		for (int i = 0; i < nBrightnessEnhancementLevel; i++) {
			dParam_Diff.m_dTargetImg = dInvertImg;

			nErr = Diff(&dInput, &dOutput);
			if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
				return nErr;
			}
			dInvertImg = dOutput.m_dImg;
		}

		dInput.Clear();
		dOutput.Clear();
		dParam_Invert.Clear();

		// Invert Image Again
		dInput.m_dImg = dInvertImg;
		dInput.m_pParam = &dParam_Invert;
		nErr = Invert(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}
		dImg = dOutput.m_dImg;
		dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nCalibration_BrightnessEnhancement);
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_COLOR_CONVERT)) {
		pOutput->m_dImg = dImg;

		dTimeProfile_MagCore.Print_Calibration(this);
		return NIPL_ERR_SUCCESS;
	}

	NIPLEllipse dMinEllipse;
	NIPLEllipse dMaxEllipse;

	Mat dMinCircleImg;
	dTimeProfile_MagCore.SetStartTime();
	nErr = MagCore_Calibration_FindCircle(dImg, dMinCircleImg, pParam, dMinEllipse, false);
	dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nCalibration_FindCircle);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		DebugPrint("Calibration Error, MinCircle, Cannot Find Circle (%d).", nErr);
		return nErr;
	}
	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MIN_CIRCLE) || CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MIN_CIRCLE_BIN)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MIN_CIRCLE_EDGE) || CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MIN_CIRCLE_OUTPUT)) {
		pOutput->m_dImg = dMinCircleImg;

		dTimeProfile_MagCore.Print_Calibration(this);
		return NIPL_ERR_SUCCESS;
	}

	DebugPrint("Calibration, MinCircle : Center(%.1f, %.1f) Radius(%.1f, %.1f)", 
		dMinEllipse.m_rcEllipse.center.x, dMinEllipse.m_rcEllipse.center.y,
		dMinEllipse.m_rcEllipse.size.width, dMinEllipse.m_rcEllipse.size.height);

	// Adjust CenterPos and MaxRadiusPosX with based on the found MinCircle
	int nMaxRadiusPosLength = pParam->m_nMaxRadiusPosX - pParam->m_nCenterPosX;
	pParam->m_nCenterPosX = dMinEllipse.m_rcEllipse.center.x;
	pParam->m_nCenterPosY = dMinEllipse.m_rcEllipse.center.y;
	pParam->m_nMaxRadiusPosX = pParam->m_nCenterPosX + nMaxRadiusPosLength;

	Mat dMaxCircleImg;
	nErr = MagCore_Calibration_FindCircle(dImg, dMaxCircleImg, pParam, dMaxEllipse, true);
	dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nCalibration_FindCircle);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		DebugPrint("Calibration Error, MaxCircle, Cannot Find Circle (%d).", nErr);
		return nErr;
	}

	DebugPrint("Calibration, MaxCircle : Center(%.1f, %.1f) Radius(%.1f, %.1f)",
		dMaxEllipse.m_rcEllipse.center.x, dMaxEllipse.m_rcEllipse.center.y,
		dMaxEllipse.m_rcEllipse.size.width, dMaxEllipse.m_rcEllipse.size.height);

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MAX_CIRCLE) || CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MAX_CIRCLE_BIN)
		| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MAX_CIRCLE_EDGE) || CHECK_STRING(strShowImage, STR_SHOW_IMAGE_MAX_CIRCLE_OUTPUT)) {
		pOutput->m_dImg = dMaxCircleImg;

		dTimeProfile_MagCore.Print_Calibration(this);
		return NIPL_ERR_SUCCESS;
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_CIRCLE_OUTPUT)) {
		NIPLResult_FindEllipse *pResult = new NIPLResult_FindEllipse();
		if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

		int nMinRadiusMargin = pParam->m_nMinRadiusMargin;
		int nMaxRadiusMargin = pParam->m_nMaxRadiusMargin;
		dMaxEllipse.m_rcEllipse.size.width += (2 * nMaxRadiusMargin);
		dMaxEllipse.m_rcEllipse.size.height += (2 * nMaxRadiusMargin);
		dMinEllipse.m_rcEllipse.size.width -= (2 * nMinRadiusMargin);
		dMinEllipse.m_rcEllipse.size.height -= (2 * nMinRadiusMargin);

		pResult->m_listEllipse.push_back(dMinEllipse);
		pResult->m_listEllipse.push_back(dMaxEllipse);

		pOutput->m_dImg = dImg;
		pOutput->m_pResult = pResult;

		dTimeProfile_MagCore.Print_Calibration(this);
		return NIPL_ERR_SUCCESS;
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_CIRCLE_MARGIN_OUTPUT)) {
		NIPLResult_FindEllipse *pResult = new NIPLResult_FindEllipse();
		if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

		pResult->m_listEllipse.push_back(dMinEllipse);
		pResult->m_listEllipse.push_back(dMaxEllipse);

		pOutput->m_dImg = dImg;
		pOutput->m_pResult = pResult;

		dTimeProfile_MagCore.Print_Calibration(this);
		return NIPL_ERR_SUCCESS;
	}

	if (dMinEllipse.m_rcEllipse.size.width >= dMaxEllipse.m_rcEllipse.size.width) {
		return NIPL_ERR_FAIL;
	}

	// Adjust Ellipse to Circle
	NIPLCircle dMinCircle;
	NIPLCircle dMaxCircle;

	dMinCircle.m_ptCenter = dMinEllipse.GetCenterPos();
	dMinCircle.m_nRadius = cvRound(dMinEllipse.m_rcEllipse.size.width / 2.f);
	dMaxCircle.m_ptCenter = dMinCircle.m_ptCenter;
	dMaxCircle.m_nRadius = cvRound(dMaxEllipse.m_rcEllipse.size.width / 2.f);

	Mat dCircleImg = dMinCircleImg | dMaxCircleImg;
	Mat dOutputImg;
	dTimeProfile_MagCore.SetStartTime();
	nErr = MagCore_Calibration_AdjustCircle(dImg, dCircleImg, dOutputImg, pParam, dMinCircle, dMaxCircle);
	dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nCalibration_AdjustCircle);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	NIPLResult_FindCircle *pResult = new NIPLResult_FindCircle();
	if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

	pResult->m_listCircle.push_back(dMinCircle);
	pResult->m_listCircle.push_back(dMaxCircle);

	pOutput->m_dImg = dOutputImg;
	pOutput->m_pResult = pResult;

	dTimeProfile_MagCore.Print_Calibration(this);

	return NIPL_ERR_SUCCESS;
}

NIPL_CUSTOM_METHOD_IMPL(MagCore_Inspection)
{
	VERIFY_PARAMS();

	dTimeProfile_MagCore.Clear_Inspection();
	dTimeProfile_MagCore.SetStartTime_Inspection();

	NIPLParam_MagCore *pParam = (NIPLParam_MagCore *)pInput->m_pParam;
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	wstring strComp = pParam->m_strComp;
	wstring strShowImage = pParam->m_strShowImage;
	int nCenterPosX = pParam->m_dParam_Circle2Rect.m_nCenterPosX;
	int nCenterPosY = pParam->m_dParam_Circle2Rect.m_nCenterPosY;
	int nMinRadius = pParam->m_dParam_Circle2Rect.m_nMinRadius;
	int nMaxRadius = pParam->m_dParam_Circle2Rect.m_nMaxRadius;

	dTimeProfile_MagCore.SetStartTime();
	MagCore_LoadData_Inspection(pParam);
	dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nInspection_LoadData);

	bool bCharCheck = pParam->m_bCharCheck;
	bool bCharBlack = pParam->m_bCharBlack;

	NIPLInput dInput;
	NIPLOutput dOutput;
	Mat dImg = pInput->m_dImg;
	Mat dRadiusEdgeImg;
	Mat dRadiusEdgeBinImg;
	Mat dOutputImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	// Circle2Rect
	NIPLParam_Circle2Rect &dParam_Circle2Rect = pParam->m_dParam_Circle2Rect;

	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_Circle2Rect;
	dTimeProfile_MagCore.SetStartTime();
	nErr = Circle2Rect(&dInput, &dOutput);
	dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nInspection_Circle2Rect);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dRectImg = dOutput.m_dImg;

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT)) {
		pOutput->m_dImg = dRectImg;

		dTimeProfile_MagCore.Print_Inspection(this);
		return NIPL_ERR_SUCCESS;
	}

	int nRectImgSizeY = dRectImg.rows;
	int nRectImgSizeX = dRectImg.cols;

	NIPLResult_Defect_MagCore *pResult = new NIPLResult_Defect_MagCore();
	if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

	//
	// Check Radius Edge
	// 
	bool bFoundDefect = false;
	dTimeProfile_MagCore.SetStartTime();
	nErr = MagCore_Inspection_RadiusEdge(dRectImg, dRadiusEdgeImg, pParam, pResult, bFoundDefect, false);
	dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nInspection_EdgeCheck_MinRadius);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		delete pResult;
		return nErr;
	}

	if (bFoundDefect) {
		pOutput->m_pResult = pResult;
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_INSIDE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_BIN)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_BIN_2)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_SPOT)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE_2)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE_2)) {
		pOutput->m_dImg = dRadiusEdgeImg;

		pOutput->m_pResult = pResult;

		dTimeProfile_MagCore.Print_Inspection(this);
		return NIPL_ERR_SUCCESS;
	}

	bFoundDefect = false;
	dTimeProfile_MagCore.SetStartTime();
	nErr = MagCore_Inspection_RadiusEdge(dRectImg, dRadiusEdgeImg, pParam, pResult, bFoundDefect, true);
	dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nInspection_EdgeCheck_MaxRadius);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		delete pResult;
		return nErr;
	}

	if (bFoundDefect) {
		pOutput->m_pResult = pResult;
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN_2)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_SPOT)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE_2)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE_2)) {
		pOutput->m_dImg = dRadiusEdgeImg;

		pOutput->m_pResult = pResult;

		dTimeProfile_MagCore.Print_Inspection(this);
		return NIPL_ERR_SUCCESS;
	}

	// Convert Edge Defect Position
	if (pOutput->m_pResult != nullptr) {
		NIPLResult_Defect_MagCore *pResult = (NIPLResult_Defect_MagCore *)pOutput->m_pResult;
		for (auto &dDefect : pResult->m_listDefect) {
			if (dDefect.m_nType == NIPLDefect_MagCore::DEFECT_TYPE_EDGE) {
				MagCore_Inspection_Convert_Rect(dDefect.m_rcBoundingBox, &dParam_Circle2Rect);
			}
		}
	}

	//
	// Check Character
	//
	Mat dMask;
	if (bCharCheck) {
		Mat dCharImg;
		bool bFoundBrokenChar = false;
		dTimeProfile_MagCore.SetStartTime();
		nErr = MagCore_Inspection_Character(dRectImg, dCharImg, pParam, pResult, bFoundBrokenChar);
		dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nInspection_CharCheck_Inspection);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			delete pResult;
			return nErr;
		}

		if (bFoundBrokenChar) {
			pOutput->m_pResult = pResult;
		}

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_EXT_CHAR)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_BIN_CHAR)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_BIN_CHAR_REGION)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_CHAR)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_BROKEN_CHAR)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MATCHED_CHAR_BIN)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MATCHED_CHAR)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MATCHED_CHAR_TEMPLATE)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_CHAR_LOT_NUMBER)) {
			pOutput->m_dImg = dCharImg;

			if (pOutput->m_pResult == nullptr) delete pResult;

			dTimeProfile_MagCore.Print_Inspection(this);
			return NIPL_ERR_SUCCESS;
		}

		// Convert Broken Character Position
		if (bFoundBrokenChar) {
			NIPLResult_Defect_MagCore *pResult = (NIPLResult_Defect_MagCore *)pOutput->m_pResult;
			for (auto &dDefect : pResult->m_listDefect) {
				if (dDefect.m_nType == NIPLDefect_MagCore::DEFECT_TYPE_BROKEN_CHARACTER) {
					MagCore_Inspection_Convert_Rect(dDefect.m_rcBoundingBox, &dParam_Circle2Rect);
				}
			}
		}

		// Extend size to erase the particle of character boundary
		int nFilterSize = 3;
		morphologyEx(dCharImg, dCharImg, MORPH_DILATE, Mat::ones(nFilterSize, nFilterSize, dCharImg.type()));
		dMask = ~dCharImg;
	}

	//
	// Check Surface
	//
	dTimeProfile_MagCore.SetStartTime();
	Mat dSurfaceImg;
	bFoundDefect = false;
	nErr = MagCore_Inspection_Surface(dRectImg, dSurfaceImg, dMask, pParam, pResult, bFoundDefect);
	dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nInspection_Surface);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		delete pResult;
		return nErr;
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE_BLOCK)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE_BLOCK_BIN)) {
		pOutput->m_dImg = dSurfaceImg;
		pOutput->m_pResult = pResult;

		dTimeProfile_MagCore.Print_Inspection(this);
		return NIPL_ERR_SUCCESS;
	}

	if (bFoundDefect) {
		pOutput->m_pResult = pResult;

		NIPLResult_Defect_MagCore *pResult = (NIPLResult_Defect_MagCore *)pOutput->m_pResult;
		for (auto &dDefect : pResult->m_listDefect) {
			if (dDefect.m_nType == NIPLDefect_MagCore::DEFECT_TYPE_SURFACE) {
				MagCore_Inspection_Convert_Rect(dDefect.m_rcBoundingBox, &dParam_Circle2Rect);
			}
		}
	}

	//
	// Check Hole
	//
	dTimeProfile_MagCore.SetStartTime();
	Mat dHoleImg;
	bFoundDefect = false;
	nErr = MagCore_Inspection_Hole(dRectImg, dHoleImg, dMask, pParam, pResult, bFoundDefect);
	dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nInspection_Hole);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		delete pResult;
		return nErr;
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN_CIRCLE)) {
		pOutput->m_dImg = dHoleImg;
		pOutput->m_pResult = pResult;

		dTimeProfile_MagCore.Print_Inspection(this);
		return NIPL_ERR_SUCCESS;
	}

	if (bFoundDefect) {
		pOutput->m_pResult = pResult;

		NIPLResult_Defect_MagCore *pResult = (NIPLResult_Defect_MagCore *)pOutput->m_pResult;
		for (auto &dDefect : pResult->m_listDefect) {
			if (dDefect.m_nType == NIPLDefect_MagCore::DEFECT_TYPE_HOLE) {
				MagCore_Inspection_Convert_Rect(dDefect.m_rcBoundingBox, &dParam_Circle2Rect);
			}
		}
	}

	pOutput->m_dImg = dImg;
	if (pResult->m_listDefect.size() > 0) {
		pOutput->m_pResult = pResult;
	}
	else {
		delete pResult;
	}

	dTimeProfile_MagCore.Print_Inspection(this);

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::MagCore_Inspection_RadiusEdge(Mat dImg, Mat &dRadiusEdgeImg, NIPLParam_MagCore *pParam, NIPLResult_Defect_MagCore *pResult, bool &bFoundDefect, bool bMaxRadius)
{
	bool bRadiusEdgeDoubleLine = pParam->m_bRadiusEdgeDoubleLine;
	bool bRadiusEdgeInsideCheck;
	if (bMaxRadius) {
		bRadiusEdgeInsideCheck = false;
	}
	else {
		bRadiusEdgeInsideCheck = pParam->m_bMinRadiusEdgeInsideCheck;
	}

	bool bRadiusEdgeCheck;
	if (bMaxRadius) {
		bRadiusEdgeCheck = pParam->m_bMaxRadiusEdgeCheck;
	}
	else {
		bRadiusEdgeCheck = pParam->m_bMinRadiusEdgeCheck;
	}

	if (!bRadiusEdgeCheck) {
		return NIPL_ERR_SUCCESS;
	}

	int nImgSizeX = dImg.cols;
	int nImgSizeY = dImg.rows;

	wstring strShowImage = pParam->m_strShowImage;
	bool bRadiusEdgeLineBlack;
	if (bMaxRadius) {
		bRadiusEdgeLineBlack = pParam->m_bMaxRadiusEdgeBlack;
	}
	else {
		bRadiusEdgeLineBlack = pParam->m_bMinRadiusEdgeBlack;
	}
	int nRadiusEdgeInsideAreaHeight;
	if (bMaxRadius) {
		nRadiusEdgeInsideAreaHeight = 0;
	}
	else {
		nRadiusEdgeInsideAreaHeight = pParam->m_nMinRadiusEdgeInsideAreaHeight;
	}

	int nRadiusEdgeAreaHeight;
	if (bMaxRadius) {
		nRadiusEdgeAreaHeight = pParam->m_nMaxRadiusEdgeAreaHeight;
	}
	else {
		nRadiusEdgeAreaHeight = pParam->m_nMinRadiusEdgeAreaHeight;
	}

	int nRadiusEdgeMaxSize = pParam->m_nRadiusEdgeMaxSize;

	if (nRadiusEdgeAreaHeight <= 0) {
		return NIPL_ERR_SUCCESS;
	}

	NIPLInput dInput;
	NIPLOutput dOutput;
	NIPL_ERR nErr;

	//
	// Check Radius Edge Inside
	// 
	Rect dROI;
	if (bRadiusEdgeInsideCheck && nRadiusEdgeInsideAreaHeight > 0) {
		dROI = Rect(0, 0, nImgSizeX, nRadiusEdgeInsideAreaHeight);
		Mat(dImg, dROI).copyTo(dRadiusEdgeImg);

		nErr = MagCore_Inspection_RadiusEdge_Inside(dRadiusEdgeImg, pParam, pResult, bFoundDefect);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_INSIDE)) {
			return NIPL_ERR_SUCCESS;
		}
	}

	//
	// Check Radius Edge Line
	//
	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_BIN)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_SPOT)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_SPOT)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE)) {

		pResult->Clear();
	}

	if (bMaxRadius) {
		dROI = Rect(0, nImgSizeY - nRadiusEdgeAreaHeight, nImgSizeX, nRadiusEdgeAreaHeight);
		Mat(dImg, dROI).copyTo(dRadiusEdgeImg);
		flip(dRadiusEdgeImg, dRadiusEdgeImg, 0);
	}
	else {
		dROI = Rect(0, 0, nImgSizeX, nRadiusEdgeAreaHeight);
		Mat(dImg, dROI).copyTo(dRadiusEdgeImg);
	}

	// Smoothing
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_Smoothing dParam_Smoothing;
	dParam_Smoothing.m_nMethod = NIPLParam_Smoothing::METHOD_FILTER;
	dParam_Smoothing.m_nFilterSize = 3;

	dInput.m_dImg = dRadiusEdgeImg;
	dInput.m_pParam = &dParam_Smoothing;
	nErr = Smoothing(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dRadiusEdgeImg = dOutput.m_dImg;

	// Extend the width to check the defect on the right side as min size
	MagCore_Inspection_ExtendImage(dRadiusEdgeImg, dRadiusEdgeImg, nRadiusEdgeMaxSize);

	int nRadiusEdgeImgSizeX = dRadiusEdgeImg.cols;
	int nRadiusEdgeImgSizeY = dRadiusEdgeImg.rows;

	int nMarginY = 0;
	Mat dRadiusEdgeSpotImg;
	Mat dRadiusEdgeSpotMask;
	Mat dRadiusEdgeHoleImg;
	Mat dRadiusEdgeHoleMask;
	int nEndY;
	if (bRadiusEdgeDoubleLine) {
		DebugPrint("Radius Edge ==== Double Line, First Line, %s", bMaxRadius ? "MaxRadius" : "MinRadius");

		float nPartRatio = 0.5f;
		int nEstEndY = nRadiusEdgeAreaHeight * nPartRatio;

		// find first line
		Rect rcUpperPart = Rect(0, 0, nRadiusEdgeImgSizeX, nEstEndY);
		Mat dUpperPart(dRadiusEdgeImg, rcUpperPart);

		Mat dUpperPartBin;
		nErr = MagCore_Inspection_RadiusEdge_Binarize(dUpperPart, dUpperPartBin, dRadiusEdgeSpotImg, dRadiusEdgeSpotMask, pParam, bMaxRadius, nEndY);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		nErr = MagCore_Inspection_RadiusEdge_CheckHole(dUpperPart, dUpperPartBin, dRadiusEdgeHoleImg, dRadiusEdgeHoleMask, pParam, bMaxRadius);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		DebugPrint("Radius Edge, Check First Line, %s, EndY : %d (%d)", bMaxRadius ? "MaxRadius" : "MinRadius", nEndY, nEstEndY);

		Mat dUpperImg;
		dRadiusEdgeImg.copyTo(dUpperImg);
		nErr = MagCore_Inspection_RadiusEdge_Line(dUpperPartBin, dUpperImg, dRadiusEdgeSpotMask, dRadiusEdgeHoleMask, pParam, pResult, nImgSizeY, nMarginY, bFoundDefect, bMaxRadius);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_BIN)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE)) {

			dRadiusEdgeImg = dUpperImg;
			return NIPL_ERR_SUCCESS;
		}

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE)) {

			dRadiusEdgeImg = dRadiusEdgeHoleImg;
			return NIPL_ERR_SUCCESS;
		}

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_SPOT)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_SPOT)) {

			dRadiusEdgeImg = dRadiusEdgeSpotImg;
			return NIPL_ERR_SUCCESS;
		}

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_BIN_2)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE_2)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE_2)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN_2)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE_2)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE_2)) {
			pResult->Clear();
		}

		DebugPrint("Radius Edge ==== Double Line, Second Line, %s", bMaxRadius ? "MaxRadius" : "MinRadius");

		nMarginY = nEndY;
		Rect rcLowerPart = Rect(0, nMarginY + 1, nRadiusEdgeImgSizeX, nRadiusEdgeAreaHeight - nMarginY - 1);
		Mat dLowerPart(dRadiusEdgeImg, rcLowerPart);

		Mat dLowerPartBin;
		nErr = MagCore_Inspection_RadiusEdge_Binarize(dLowerPart, dLowerPartBin, dRadiusEdgeSpotImg, dRadiusEdgeSpotMask, pParam, bMaxRadius, nEndY, true);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		nErr = MagCore_Inspection_RadiusEdge_CheckHole(dLowerPart, dLowerPartBin, dRadiusEdgeHoleImg, dRadiusEdgeHoleMask, pParam, bMaxRadius, true);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		DebugPrint("Radius Edge, Check Second Line, %s", bMaxRadius ? "MaxRadius" : "MinRadius");

		nErr = MagCore_Inspection_RadiusEdge_Line(dLowerPartBin, dRadiusEdgeImg, dRadiusEdgeSpotMask, dRadiusEdgeHoleMask, pParam, pResult, nImgSizeY, nMarginY, bFoundDefect, bMaxRadius, true);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE_2)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE_2)) {

			dRadiusEdgeImg = dRadiusEdgeHoleImg;
			return NIPL_ERR_SUCCESS;
		}
	}
	else {
		DebugPrint("Radius Edge ==== Single Line, %s", bMaxRadius ? "MaxRadius" : "MinRadius");

		Mat dRadiusEdgeBinImg;
		nErr = MagCore_Inspection_RadiusEdge_Binarize(dRadiusEdgeImg, dRadiusEdgeBinImg, dRadiusEdgeSpotImg, dRadiusEdgeSpotMask, pParam, bMaxRadius, nEndY);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		nErr = MagCore_Inspection_RadiusEdge_CheckHole(dRadiusEdgeImg, dRadiusEdgeBinImg, dRadiusEdgeHoleImg, dRadiusEdgeHoleMask, pParam, bMaxRadius);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		DebugPrint("Radius Edge, Check Line, %s", bMaxRadius ? "MaxRadius" : "MinRadius");

		nErr = MagCore_Inspection_RadiusEdge_Line(dRadiusEdgeBinImg, dRadiusEdgeImg, dRadiusEdgeSpotMask, dRadiusEdgeHoleMask, pParam, pResult, nImgSizeY, nMarginY, bFoundDefect, bMaxRadius);

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_SPOT)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_SPOT)) {
			dRadiusEdgeImg = dRadiusEdgeSpotImg;
			return NIPL_ERR_SUCCESS;
		}

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE)) {

			dRadiusEdgeImg = dRadiusEdgeHoleImg;
			return NIPL_ERR_SUCCESS;
		}
	}

	return nErr;
}

NIPL_ERR NIPLCustom::MagCore_Inspection_RadiusEdge_Inside(Mat dImg, NIPLParam_MagCore *pParam, NIPLResult_Defect_MagCore *pResult, bool &bFoundDefect)
{
	float nRadiusEdgeInsideThreshold = pParam->m_nMinRadiusEdgeInsideThreshold;
	wstring strShowImage = pParam->m_strShowImage;

	int nImgSizeX = dImg.cols;
	int nImgSizeY = dImg.rows;

	Scalar dMean;
	Scalar dStd;
	meanStdDev(dImg, dMean, dStd);
	float nMean = (float)dMean[0];
	float nStd = (float)dStd[0];

	DebugPrint("Radius Edge, Check Inside, Std : %.1f > %.1f", nStd, nRadiusEdgeInsideThreshold);

	if (nStd > nRadiusEdgeInsideThreshold) {
		DebugPrint("  ==> Defect");

		Rect rcDefect(nImgSizeX * 0.125f, 0, nImgSizeX * 0.5f, nImgSizeY);

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_INSIDE) 
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE)) {
			rcDefect.x = 0;
			rcDefect.width = nImgSizeX;
		}

		NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_EDGE, rcDefect, nImgSizeX * nImgSizeY);
		pResult->m_listDefect.push_back(dDefect);

		bFoundDefect = true;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::MagCore_Inspection_RadiusEdge_Binarize(Mat dImg, Mat &dRadiusEdgeBinImg, Mat &dRadiusEdgeSpotImg, Mat &dRadiusEdgeSpotMask, NIPLParam_MagCore *pParam, bool bMaxRadius, int &nRadiusEdgeEndY, bool bSecondLine)
{
	bool bRadiusEdgeLineBlack;
	if (bMaxRadius) {
		bRadiusEdgeLineBlack = pParam->m_bMaxRadiusEdgeBlack;
	}
	else {
		bRadiusEdgeLineBlack = pParam->m_bMinRadiusEdgeBlack;
	}

	float nRadiusEdgeBinarizeThreshold;
	float nRadiusEdgeSpotBinarizeThreshold;
	if (bMaxRadius) {
		if (bSecondLine) {
			nRadiusEdgeBinarizeThreshold = pParam->m_nMaxRadiusEdgeBinarizeThreshold2;
		}
		else {
			nRadiusEdgeBinarizeThreshold = pParam->m_nMaxRadiusEdgeBinarizeThreshold;
			nRadiusEdgeSpotBinarizeThreshold = pParam->m_nMaxRadiusEdgeSpotBinarizeThreshold;
		}
	}
	else {
		if (bSecondLine) {
			nRadiusEdgeBinarizeThreshold = pParam->m_nMinRadiusEdgeBinarizeThreshold2;
		}
		else {
			nRadiusEdgeBinarizeThreshold = pParam->m_nMinRadiusEdgeBinarizeThreshold;
			nRadiusEdgeSpotBinarizeThreshold = pParam->m_nMinRadiusEdgeSpotBinarizeThreshold;
		}
	}
	int nRadiusEdgeHoleMinSize = pParam->m_nRadiusEdgeHoleMinSize;
	int nRadiusEdgeMinSize = pParam->m_nRadiusEdgeMinSize;
	int nRadiusEdgeSpotMinSize = pParam->m_nRadiusEdgeSpotMinSize;
	wstring strShowImage = pParam->m_strShowImage;

	bool bCheckSpot = (!bSecondLine && nRadiusEdgeSpotBinarizeThreshold > 0.f);

	int nImgSizeX = dImg.cols;
	int nImgSizeY = dImg.rows;

	// Thresholding
	NIPLInput dInput;
	NIPLOutput dOutput;
	NIPL_ERR nErr;

	NIPLParam_Thresholding dParam_Threshold;
	if (bRadiusEdgeLineBlack) {
		dParam_Threshold.m_nMethod = NIPLParam_Thresholding::METHOD_LOWER_OTSU;
	}
	else {
		dParam_Threshold.m_nMethod = NIPLParam_Thresholding::METHOD_UPPER_OTSU;
	}
	dParam_Threshold.m_nThreshold = nRadiusEdgeBinarizeThreshold;

	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_Threshold;
	nErr = Thresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dBinImg = dOutput.m_dImg;

	// Spot Bin Image
	Mat dSpotBinImg;
	if (bCheckSpot) {
		dParam_Threshold.m_nThreshold = nRadiusEdgeSpotBinarizeThreshold;

		dInput.m_dImg = dImg;
		dInput.m_pParam = &dParam_Threshold;
		nErr = Thresholding(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}
		dSpotBinImg = dOutput.m_dImg;
	}

	// Connect horizontal pixels
	dInput.Clear();
	dOutput.Clear();

/*
	NIPLParam_MorphologyOperate dParam_MorphologyOperate;
	dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_CLOSE;
	dParam_MorphologyOperate.m_bCircleFilter = true;
	dParam_MorphologyOperate.m_nFilterSizeX = 7;
	dParam_MorphologyOperate.m_nFilterSizeY = 1;

	dInput.m_dImg = dBinImg;
	dInput.m_pParam = &dParam_MorphologyOperate;
	nErr = MorphologyOperate(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
	return nErr;
	}
	dBinImg = dOutput.m_dImg;
*/

	// cut thin vertical line
	dInput.Clear();
	dOutput.Clear();
//	dParam_MorphologyOperate.Clear();

	NIPLParam_MorphologyOperate dParam_MorphologyOperate;
	dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_OPEN;
	dParam_MorphologyOperate.m_bCircleFilter = true;
	dParam_MorphologyOperate.m_nFilterSizeX = 9;
	dParam_MorphologyOperate.m_nFilterSizeY = 3;

	dInput.m_dImg = dBinImg;
	dInput.m_pParam = &dParam_MorphologyOperate;
	nErr = MorphologyOperate(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dBinImg = dOutput.m_dImg;

	// fill inside vertical hole
	dInput.Clear();
	dOutput.Clear();
	dParam_MorphologyOperate.Clear();

	dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_CLOSE;
	dParam_MorphologyOperate.m_bCircleFilter = true;
	dParam_MorphologyOperate.m_nFilterSizeX = 1;
	dParam_MorphologyOperate.m_nFilterSizeY = 3;

	dInput.m_dImg = dBinImg;
	dInput.m_pParam = &dParam_MorphologyOperate;
	nErr = MorphologyOperate(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dBinImg = dOutput.m_dImg;

	// Eliminate Noise
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_EliminateNoise dParam_EliminateNoise;
	dParam_EliminateNoise.m_nMethod = NIPLParam_EliminateNoise::METHOD_SIZE;
	dParam_EliminateNoise.m_nMinSize = 500;

	dInput.m_dImg = dBinImg;
	dInput.m_pParam = &dParam_EliminateNoise;
	nErr = EliminateNoise(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dBinImg = dOutput.m_dImg;

	dInput.Clear();
	dOutput.Clear();

	NIPLParam_FindBlob dParam_FindBlob;
	dParam_FindBlob.m_bFindInRange = true;
	dParam_FindBlob.m_nMinSize = nImgSizeX * 5;

	//		dInput.m_dImg = dUpperPartBin2;
	dInput.m_dImg = dBinImg;
	dInput.m_pParam = &dParam_FindBlob;
	nErr = FindBlob(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	nRadiusEdgeEndY = nImgSizeY - 1;

	int nEdgeMarginY = 5;
	int nEdgeMarginStartY = -1;
	int nEdgeMarginEndY = -1;
	if (dOutput.m_pResult != nullptr) {
		int nStartY = -1;
		int nEndY = -1;
		NIPLResult_FindBlob *pResult_FindBlob = (NIPLResult_FindBlob *)dOutput.m_pResult;
		for (auto dBlob : pResult_FindBlob->m_listBlob) {
			if (nStartY < 0 || dBlob.m_rcBoundingBox.y < nStartY) {
				nStartY = dBlob.m_rcBoundingBox.y;
			}
			if (nEndY < 0 || dBlob.m_rcBoundingBox.y + dBlob.m_rcBoundingBox.height > nEndY) {
				nEndY = dBlob.m_rcBoundingBox.y + dBlob.m_rcBoundingBox.height;
			}
		}

		delete pResult_FindBlob;

		if (nStartY > 0) {
			Mat dUpperPart(dBinImg, Rect(0, 0, nImgSizeX, nStartY));
			dUpperPart = 0;

			nEdgeMarginStartY = min(nStartY - nEdgeMarginY, 0);


		}
		if (nEndY >= 0 && nEndY < (nImgSizeY - 1)) {
			Mat dLowerPart(dBinImg, Rect(0, nEndY + 1, nImgSizeX, nImgSizeY - nEndY - 1));
			dLowerPart = 0;

			nEdgeMarginEndY = min(nEndY + nEdgeMarginY, nImgSizeY - 1);
			nRadiusEdgeEndY = nEdgeMarginEndY;
		}
	}

	dRadiusEdgeBinImg = dBinImg;

	dRadiusEdgeSpotMask = Mat::zeros(1, nImgSizeX, CV_8UC1);
	if (bCheckSpot) {
		if (nEdgeMarginStartY > 0) {
			// remove upper part of SpotBin out of margin
			Mat dUpperPart(dSpotBinImg, Rect(0, 0, nImgSizeX, nEdgeMarginStartY));
			dUpperPart = 0;
		}

		if (nEdgeMarginEndY >= 0 && nEdgeMarginEndY < (nImgSizeY - 1)) {
			// remove lower part of SpotBin out of margin
			Mat dLowerPart(dSpotBinImg, Rect(0, nEdgeMarginEndY + 1, nImgSizeX, nImgSizeY - nEdgeMarginEndY - 1));
			dLowerPart = 0;
		}

		// cut thin vertical line
		dInput.Clear();
		dOutput.Clear();
		dParam_MorphologyOperate.Clear();

		dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_DILATE;
		dParam_MorphologyOperate.m_bCircleFilter = true;
		dParam_MorphologyOperate.m_nFilterSizeX = 5;
		dParam_MorphologyOperate.m_nFilterSizeY = 5;

		dInput.m_dImg = dBinImg;
		dInput.m_pParam = &dParam_MorphologyOperate;
		nErr = MorphologyOperate(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}
		Mat dExtBinImg = dOutput.m_dImg;

		dSpotBinImg -= dExtBinImg;

		dInput.Clear();
		dOutput.Clear();

		NIPLParam_FindBlob dParam_FindBlob;
		dParam_FindBlob.m_bFindInRange = true;
		dParam_FindBlob.m_nMinSize = nRadiusEdgeSpotMinSize;
//		dParam_FindBlob.m_nMaxSize = 300;

		dInput.m_dImg = dSpotBinImg;
		dInput.m_pParam = &dParam_FindBlob;
		nErr = FindBlob(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		if (nEdgeMarginStartY < 0) nEdgeMarginStartY = 0;
		if (nEdgeMarginEndY < 0) nEdgeMarginEndY = nImgSizeY - 1;
		int nMidPosY = (nEdgeMarginStartY + nEdgeMarginEndY) * 0.5f;

		if (dOutput.m_pResult != nullptr) {
			NIPLResult_FindBlob *pResult_FindBlob = (NIPLResult_FindBlob *)dOutput.m_pResult;
			for (auto dBlob : pResult_FindBlob->m_listBlob) {
				int nSpotMask = RADIUS_EDGE_SPOT_POS_UPPER;
				if (dBlob.m_rcBoundingBox.y > nMidPosY) {
					Mat dVertUpperLine(dBinImg, Rect(dBlob.m_rcBoundingBox.x, 0, 1, dBlob.m_rcBoundingBox.y));
					if (countNonZero(dVertUpperLine) > 0) {
						// spot located under
						nSpotMask = RADIUS_EDGE_SPOT_POS_LOWER;
					}
				}

				for (int i = dBlob.m_rcBoundingBox.x; i < dBlob.m_rcBoundingBox.x + dBlob.m_rcBoundingBox.width; i++) {
					dRadiusEdgeSpotMask.at<UINT8>(0, i) = dRadiusEdgeSpotMask.at<UINT8>(0, i) | nSpotMask;
				}
			}

			delete pResult_FindBlob;
		}

		Mat((dBinImg * 0.25f) | (dSpotBinImg * 0.5f) | dOutput.m_dImg).copyTo(dRadiusEdgeSpotImg);
	}

	DebugPrint("Radius Edge, Binarize, %s, BinarizeThreshold : %.2f", bMaxRadius ? "MaxRadius" : "MinRadius", nRadiusEdgeBinarizeThreshold);

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::MagCore_Inspection_RadiusEdge_CheckHole(Mat dImg, Mat dBinImg, Mat &dRadiusEdgeHoleImg, Mat &dRadiusEdgeHoleMask, NIPLParam_MagCore *pParam, bool bMaxRadius, bool bSecondLine)
{
	bool bRadiusEdgeLineBlack;
	if (bMaxRadius) {
		bRadiusEdgeLineBlack = pParam->m_bMaxRadiusEdgeBlack;
	}
	else {
		bRadiusEdgeLineBlack = pParam->m_bMinRadiusEdgeBlack;
	}

	float nRadiusEdgeBinarizeThreshold;
	float nRadiusEdgeHoleBinarizeThreshold;
	if (bMaxRadius) {
		if (bSecondLine) {
			nRadiusEdgeBinarizeThreshold = pParam->m_nMaxRadiusEdgeBinarizeThreshold2;
			nRadiusEdgeHoleBinarizeThreshold = pParam->m_nMaxRadiusEdgeHoleBinarizeThreshold2;
		}
		else {
			nRadiusEdgeBinarizeThreshold = pParam->m_nMaxRadiusEdgeBinarizeThreshold;
			nRadiusEdgeHoleBinarizeThreshold = pParam->m_nMaxRadiusEdgeHoleBinarizeThreshold;
		}
	}
	else {
		if (bSecondLine) {
			nRadiusEdgeBinarizeThreshold = pParam->m_nMinRadiusEdgeBinarizeThreshold2;
			nRadiusEdgeHoleBinarizeThreshold = pParam->m_nMinRadiusEdgeHoleBinarizeThreshold2;
		}
		else {
			nRadiusEdgeBinarizeThreshold = pParam->m_nMinRadiusEdgeBinarizeThreshold;
			nRadiusEdgeHoleBinarizeThreshold = pParam->m_nMinRadiusEdgeHoleBinarizeThreshold;
		}
	}
	int nRadiusEdgeHoleMinSize = pParam->m_nRadiusEdgeHoleMinSize;
	int nRadiusEdgeHoleMaxSize = pParam->m_nRadiusEdgeHoleMaxSize;
	int nRadiusEdgeMinSize = pParam->m_nRadiusEdgeMinSize;
	wstring strShowImage = pParam->m_strShowImage;

	int nImgSizeX = dImg.cols;
	int nImgSizeY = dImg.rows;

	dRadiusEdgeHoleImg = Mat::zeros(dImg.size(), CV_8UC1);
	dRadiusEdgeHoleMask = Mat::zeros(1, nImgSizeX, CV_8UC1);

	if (nRadiusEdgeHoleBinarizeThreshold == 0.f) {
		Mat(dBinImg * 0.75f).copyTo(dRadiusEdgeHoleImg);

		return NIPL_ERR_SUCCESS;
	}

	NIPLInput dInput;
	NIPLOutput dOutput;
	NIPL_ERR nErr;

// Fill inside horizontally
	NIPLParam_MorphologyOperate dParam_MorphologyOperate;
	dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_CLOSE;
	dParam_MorphologyOperate.m_bCircleFilter = true;
	dParam_MorphologyOperate.m_nFilterSizeX = nRadiusEdgeHoleMaxSize * 0.2f;
	dParam_MorphologyOperate.m_nFilterSizeY = 1;

	dInput.m_dImg = dBinImg;
	dInput.m_pParam = &dParam_MorphologyOperate;
	nErr = MorphologyOperate(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dLineBinImg = dOutput.m_dImg;

/*
// Finding Line Edge
	dInput.Clear();
	dOutput.Clear();
	dParam_MorphologyOperate.Clear();

	dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_ERODE;
	dParam_MorphologyOperate.m_bCircleFilter = true;
	dParam_MorphologyOperate.m_nFilterSizeX = 3;
	dParam_MorphologyOperate.m_nFilterSizeY = 3;

	dInput.m_dImg = dLineBinImg;
	dInput.m_pParam = &dParam_MorphologyOperate;
	nErr = MorphologyOperate(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dLineEdgeBinImg = dLineBinImg - dOutput.m_dImg;

	// Remove Line Edge
	dLineBinImg = dLineBinImg - dLineEdgeBinImg;
*/

	// Find hole
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_Thresholding dParam_Threshold;
	if (bRadiusEdgeLineBlack) {
		dParam_Threshold.m_nMethod = NIPLParam_Thresholding::METHOD_UPPER_OTSU;
	}
	else {
		dParam_Threshold.m_nMethod = NIPLParam_Thresholding::METHOD_LOWER_OTSU;
	}
	dParam_Threshold.m_nThreshold = nRadiusEdgeHoleBinarizeThreshold;

	dInput.m_dImg = dImg;
	dInput.m_dMask = dLineBinImg;
	dInput.m_pParam = &dParam_Threshold;
	nErr = Thresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dHoleBinImg = dOutput.m_dImg;

	// Connect and fill Inside
	dInput.Clear();
	dOutput.Clear();
	dParam_MorphologyOperate.Clear();

	dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_CLOSE;
	dParam_MorphologyOperate.m_bCircleFilter = true;
	dParam_MorphologyOperate.m_nFilterSizeX = 5;
	dParam_MorphologyOperate.m_nFilterSizeY = 3;

	dInput.m_dImg = dHoleBinImg;
	dInput.m_pParam = &dParam_MorphologyOperate;
	nErr = MorphologyOperate(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dHoleBinImg = dOutput.m_dImg;

	DebugPrint("Radius Edge, Check Hole, %s, FindHoleThreshold : %.2f", bMaxRadius ? "MaxRadius" : "MinRadius", dParam_Threshold.m_nThreshold);

	// check if it's real hole (because noise above/below line can exist)
	// set empty on all columns corresponding to hole x position 
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_FindBlob dParam_FindBlob;
	dParam_FindBlob.m_bFindInRange = true;
	dParam_FindBlob.m_nMinSize = 3;

	dInput.m_dImg = dHoleBinImg;
	dInput.m_pParam = &dParam_FindBlob;
	nErr = FindBlob(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	dRadiusEdgeHoleImg = dLineBinImg * 0.5f;
	if (dOutput.m_pResult != nullptr) {
		NIPLResult_FindBlob *pResult_FindBlob = (NIPLResult_FindBlob *)dOutput.m_pResult;
		for (auto dBlob : pResult_FindBlob->m_listBlob) {
			bool bPerfectHole = false;
			bool bHole = MagCore_Inspection_RadiusEdge_CheckHoleInsideLine(dLineBinImg, dHoleBinImg, dBlob.m_rcBoundingBox, bPerfectHole);

			if (bHole) {
				if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE_2)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE_2)) {
					Mat dHoleBlob(dHoleBinImg, dBlob.m_rcBoundingBox);
					Mat dHoleBlob2(dRadiusEdgeHoleImg, dBlob.m_rcBoundingBox);
					Mat(dHoleBlob | dHoleBlob2).copyTo(dHoleBlob2);
				}

				// set mask
				int nHoleMask = bPerfectHole ? RADIUS_EDGE_PERFECT_HOLE : RADIUS_EDGE_HOLE;
				Mat dHoleBlobMask(dRadiusEdgeHoleMask, Rect(dBlob.m_rcBoundingBox.x, 0, dBlob.m_rcBoundingBox.width, 1));
				dHoleBlobMask = nHoleMask;

				DebugPrint("    ==> Found %s, %s, Size : %d, Pos : (%d, %d) ~ (%d, %d)", 
					bPerfectHole ? "Perfect Hole" : "Hole", bMaxRadius ? "MaxRadius" : "MinRadius",
					dBlob.m_nSize, dBlob.m_rcBoundingBox.x, dBlob.m_rcBoundingBox.y, dBlob.m_rcBoundingBox.x + dBlob.m_rcBoundingBox.width, dBlob.m_rcBoundingBox.y + dBlob.m_rcBoundingBox.height);
			}
			else {
				if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE_2)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE_2)) {
					Mat dHoleBlob(dHoleBinImg, dBlob.m_rcBoundingBox);
					Mat dHoleBlob2(dRadiusEdgeHoleImg, dBlob.m_rcBoundingBox);
					Mat((dHoleBlob * 0.75f) | dHoleBlob2).copyTo(dHoleBlob2);
				}
			}
		}

		delete pResult_FindBlob;
	}

	return NIPL_ERR_SUCCESS;
}

bool NIPLCustom::MagCore_Inspection_RadiusEdge_CheckHoleInsideLine(Mat dLineImg, Mat dHoleImg, Rect rcHole, bool &bPerfectHole)
{
	int nImgSizeX = dLineImg.cols;
	int nImgSizeY = dLineImg.rows;

	// check perfect hole
	if (rcHole.x > 0 && (rcHole.x + rcHole.width - 1) < (nImgSizeX - 1) && rcHole.y > 0 && (rcHole.y + rcHole.height - 1) < (nImgSizeY - 1)) {
		Rect rcBouningBox(rcHole.x - 1, rcHole.y - 1, rcHole.width + 2, rcHole.height + 2);
		Mat dBoundingBox(dLineImg, rcBouningBox);
		Mat dCenterPart(dBoundingBox, Rect(1, 1, rcHole.width, rcHole.height));
		Mat dHole(dHoleImg, rcHole);
		Mat(dCenterPart | dHole).copyTo(dCenterPart);

		if (countNonZero(dBoundingBox) == rcBouningBox.width * rcBouningBox.height) {
			bPerfectHole = true;
			return true;
		}
	}


	// check if there's pixel at both upper and lower position in hole area
	int nHoleLeftStartY = -1;
	int nHoleLeftEndY = -1;
	int nHoleRightStartY = -1;
	int nHoleRightEndY = -1;
	float nSideRatio = 0.2f;
	int nLeftSideX = rcHole.x + rcHole.width * nSideRatio;
	int nRightSideX = rcHole.x + rcHole.width * (1.f - nSideRatio);

	bool bFoundUpper = false;
	bool bFoundLower = false;
	for (int j = rcHole.x; j < rcHole.x + rcHole.width; j++) {
		int nStartY = -1;
		int nEndY = -1;
		for (int i = rcHole.y; i < rcHole.y + rcHole.height; i++) {
			if (dHoleImg.at<UINT8>(i, j) > 0) {
				if (nStartY < 0) {
					nStartY = i;
					nEndY = i;
				}
				else {
					nEndY = i;
				}
			}
		}
		if (nStartY < 0) {
			continue;
		}

		if (j <= nLeftSideX) {
			if (nHoleLeftStartY < 0) {
				nHoleLeftStartY = nStartY;
				nHoleLeftEndY = nEndY;
			}
			else {
				if (nStartY < nHoleLeftStartY) nHoleLeftStartY = nStartY;
				if (nEndY > nHoleLeftEndY) nHoleLeftEndY = nEndY;
			}
		}
		else if (j >= nRightSideX) {
			if (nHoleRightStartY < 0) {
				nHoleRightStartY = nStartY;
				nHoleRightEndY = nEndY;
			}
			else {
				if (nStartY < nHoleRightStartY) nHoleRightStartY = nStartY;
				if (nEndY > nHoleRightEndY) nHoleRightEndY = nEndY;
			}
		}

		if (!bFoundUpper) {
			int nCount = 0;
			for (int i = nStartY - 1; i >= rcHole.y; i--) {
				if (dLineImg.at<UINT8>(i, j) > 0) {
					nCount++;
					if (nCount >= 2) {
						bFoundUpper = true;
						break;
					}
				}
			}
		}
		if (!bFoundLower) {
			int nCount = 0;
			for (int i = nEndY + 1; i < rcHole.y + rcHole.height; i++) {
				if (dLineImg.at<UINT8>(i, j) > 0) {
					nCount++;
					if (nCount >= 2) {
						bFoundLower = true;
						break;
					}
				}
			}
		}

		if (bFoundUpper && bFoundLower) {
			DebugPrint("  Hole inside Line, by Checking Upper/Lower side. Hole Pos (%d, %d) Size (%d, %d), StartY : %d, EndY : %d",
				rcHole.x, rcHole.y, rcHole.width, rcHole.height, nStartY, nEndY);
			return true;
		}
	}

	// check if it's not exist on boundary. Blob going into the middle of line would be assumed as hole
	int nLeftX = rcHole.x - 1;
	int nRightX = rcHole.x + rcHole.width;
	int nLeftStartY = -1;
	int nLeftEndY = -1;
	int nRightStartY = -1;
	int nRightEndY = -1;
	for (int i = 0; i < nImgSizeY; i++) {
		// Outside Hole
		if (nLeftX >= 0 && dLineImg.at<UINT8>(i, nLeftX) > 0) {
			if (nLeftStartY < 0) {
				nLeftStartY = i;
				nLeftEndY = i;
			}
			else {
				nLeftEndY = i;
			}
		}
		if (nRightX < nImgSizeX && dLineImg.at<UINT8>(i, nRightX) > 0) {
			if (nRightStartY < 0) {
				nRightStartY = i;
				nRightEndY = i;
			}
			else {
				nRightEndY = i;
			}
		}
	}

	bool bHole = false;
	int nBaseThickness = 7;
	if (nLeftStartY >= 0 && nHoleLeftStartY >= 0) {
		int nSize = nLeftEndY - nLeftStartY + 1;
		int nAddEndY = (nSize % 2 == 0) ? 1 : 0;
		int nMidY = (nLeftStartY + nLeftEndY) * 0.5f;
		int nMidStartY = nMidY;
		int nMidEndY = nMidY + nAddEndY;
		int nAdjustY = (nSize >= 10) ? nSize / nBaseThickness : 0;

		if (nSize <= nBaseThickness) {
			if(nSize % 2 == 0) {
				// if the thickness is think, adjust mid position to make it harder to detect holes
				if (nHoleLeftStartY <= nMidY) {
					nMidStartY++;
				}
				else {
					nMidEndY--;
				}
			}
		}
		else {
			// if the thickness is over base thickness, enlarge the range of mid point
			nMidStartY -= nAdjustY;
			nMidEndY += nAdjustY;
		}

		if (nMidEndY >= nHoleLeftStartY && nMidStartY <= nHoleLeftEndY) {
			bHole = true;
		}

		if (bHole) {
			DebugPrint("  Hole inside Line, Left, StatX : %d, Size : %d, Adjust : %d, MidEndY : %d >= %d, MidStartY : %d <= %d",
				rcHole.x, nSize, nAdjustY, nMidEndY, nHoleLeftStartY, nMidStartY, nHoleLeftEndY);
		}
	}
	if (!bHole && nRightStartY >= 0 && nHoleRightStartY >= 0) {
		int nSize = nRightEndY - nRightStartY + 1;
		int nAddEndY = (nSize % 2 == 0) ? 1 : 0;
		int nMidY = (nRightStartY + nRightEndY) * 0.5f;
		int nMidStartY = nMidY;
		int nMidEndY = nMidY + nAddEndY;
		int nAdjustY = (nSize >= 10) ? nSize / nBaseThickness : 0;

		if (nSize <= nBaseThickness) {
			if (nSize % 2 == 0) {
				// if the thickness is think, adjust mid position to make it harder to detect holes
				if (nHoleRightStartY <= nMidY) {
					nMidStartY++;
				}
				else {
					nMidEndY--;
				}
			}
		}
		else {
			// if the thickness is over base thickness, enlarge the range of mid point
			nMidStartY -= nAdjustY;
			nMidEndY += nAdjustY;
		}

		if (nMidEndY >= nHoleRightStartY && nMidStartY <= nHoleRightEndY) {
			bHole = true;
		}

		if (bHole) {
			DebugPrint("  Hole inside Line, Right, StatX : %d, Size : %d, Adjust : %d, MidEndY : %d >= %d, MidStartY : %d <= %d",
				rcHole.x, nSize, nAdjustY, nMidEndY, nHoleRightStartY, nMidStartY, nHoleRightEndY);
		}
	}

	return bHole;
}


NIPL_ERR NIPLCustom::MagCore_Inspection_RadiusEdge_Line(Mat dImg, Mat &dRadiusEdgeImg, Mat dRadiusEdgeSpotMask, Mat dRadiusEdgeHoleMask, NIPLParam_MagCore *pParam, NIPLResult_Defect_MagCore *pResult, int nFullImgSizeY, int nMarginY, bool &bFoundDefect, bool bMaxRadius, bool bSecondLine)
{
	bool bRadiusEdgeDoubleLine = pParam->m_bRadiusEdgeDoubleLine;
	float nRadiusEdgeBinarizeThreshold;
	float nRadiusEdgeProfileThreshold;
	if (bMaxRadius) {
		if (bSecondLine) {
			nRadiusEdgeBinarizeThreshold = pParam->m_nMaxRadiusEdgeBinarizeThreshold2;
			nRadiusEdgeProfileThreshold = pParam->m_nMaxRadiusEdgeProfileThreshold2;
		}
		else {
			nRadiusEdgeBinarizeThreshold = pParam->m_nMaxRadiusEdgeBinarizeThreshold;
			nRadiusEdgeProfileThreshold = pParam->m_nMaxRadiusEdgeProfileThreshold;
		}
	}
	else {
		if (bSecondLine) {
			nRadiusEdgeBinarizeThreshold = pParam->m_nMinRadiusEdgeBinarizeThreshold2;
			nRadiusEdgeProfileThreshold = pParam->m_nMinRadiusEdgeProfileThreshold2;
		}
		else {
			nRadiusEdgeBinarizeThreshold = pParam->m_nMinRadiusEdgeBinarizeThreshold;
			nRadiusEdgeProfileThreshold = pParam->m_nMinRadiusEdgeProfileThreshold;
		}
	}
	int nRadiusEdgeMinSize = pParam->m_nRadiusEdgeMinSize;
	int nRadiusEdgeMaxSize = pParam->m_nRadiusEdgeMaxSize;
	wstring strShowImage = pParam->m_strShowImage;

	int nImgSizeX = dImg.cols;
	int nImgSizeY = dImg.rows;

/*
	// Extend the width to check the defect on the right side as min size
	Mat dExtRadiusEdgeBinImg;
	dExtRadiusEdgeBinImg.create(nImgSizeY, nImgSizeX + nRadiusEdgeMaxSize, dImg.type());
	Mat dOrigPart(dExtRadiusEdgeBinImg, Rect(0, 0, nImgSizeX, nImgSizeY));
	dImg.copyTo(dOrigPart);
	Mat dExtPart(dExtRadiusEdgeBinImg, Rect(nImgSizeX, 0, nRadiusEdgeMaxSize, nImgSizeY));
	Mat dRadiusEdgeBinExtPart(dImg, Rect(0, 0, nRadiusEdgeMaxSize, nImgSizeY));
	dRadiusEdgeBinExtPart.copyTo(dExtPart);

	// Extend the spot mask 
	Mat dExtRadiusEdgeSpotMask;
	dExtRadiusEdgeSpotMask.create(1, nImgSizeX + nRadiusEdgeMaxSize, dRadiusEdgeSpotMask.type());
	dOrigPart = Mat(dExtRadiusEdgeSpotMask, Rect(0, 0, nImgSizeX, 1));
	dRadiusEdgeSpotMask.copyTo(dOrigPart);
	dExtPart = Mat(dExtRadiusEdgeSpotMask, Rect(nImgSizeX, 0, nRadiusEdgeMaxSize, 1));
	Mat dRadiusEdgeSpotMaskExtPart(dRadiusEdgeSpotMask, Rect(0, 0, nRadiusEdgeMaxSize, 1));
	dRadiusEdgeSpotMaskExtPart.copyTo(dExtPart);

	// Extend the hole mask 
	Mat dExtRadiusEdgeHoleMask;
	dExtRadiusEdgeHoleMask.create(1, nImgSizeX + nRadiusEdgeMaxSize, dRadiusEdgeHoleMask.type());
	dOrigPart = Mat(dExtRadiusEdgeHoleMask, Rect(0, 0, nImgSizeX, 1));
	dRadiusEdgeHoleMask.copyTo(dOrigPart);
	dExtPart = Mat(dExtRadiusEdgeHoleMask, Rect(nImgSizeX, 0, nRadiusEdgeMaxSize, 1));
	Mat dRadiusEdgeHoleMaskExtPart(dRadiusEdgeHoleMask, Rect(0, 0, nRadiusEdgeMaxSize, 1));
	dRadiusEdgeHoleMaskExtPart.copyTo(dExtPart);

	int nExtImgSizeX = dExtRadiusEdgeBinImg.cols;
	int nExtImgSizeY = dExtRadiusEdgeBinImg.rows;
*/

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE_2)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE_2)) {
		pResult->Clear();
	}

	// Test Thickness Profile
	Mat dRadiusEdgeProfileLine = Mat::zeros(1, nImgSizeX, CV_16UC1);
	Mat dRadiusEdgeProfileLineOutput = Mat::zeros(1, nImgSizeX, CV_8UC1);
	Mat dEmptyLineImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	vector<pair<int, bool>> listProfileCurvePoint;

	DebugPrint("Radius Edge, Check Profile, %s, ProfileThreshold : %.1f", bMaxRadius ? "MaxRadius" : "MinRadius", nRadiusEdgeProfileThreshold);

	MagCore_Inspection_RadiusEdge_GetLineProfile(dImg, dRadiusEdgeProfileLine, dRadiusEdgeHoleMask, dEmptyLineImg, nRadiusEdgeMaxSize);
	MagCore_Inspection_RadiusEdge_GetLineProfileCurvePoint(dRadiusEdgeProfileLine, dRadiusEdgeSpotMask, dRadiusEdgeHoleMask, pParam, listProfileCurvePoint);
	MagCore_Inspection_RadiusEdge_CheckLineProfile(dRadiusEdgeProfileLine, dRadiusEdgeProfileLineOutput, listProfileCurvePoint, dRadiusEdgeSpotMask, dRadiusEdgeHoleMask, pParam, bFoundDefect, bMaxRadius, bSecondLine);

	if (bFoundDefect) {
		NIPLInput dInput;
		NIPLOutput dOutput;
		NIPL_ERR nErr;

		// FindBlob
		NIPLParam_FindBlob dParam_FindBlob;
		dParam_FindBlob.m_bFindInRange = true;

		dInput.m_dImg = dRadiusEdgeProfileLineOutput;
		dInput.m_pParam = &dParam_FindBlob;
		nErr = FindBlob(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		if (dOutput.m_pResult != nullptr) {
			NIPLResult_FindBlob *pResult_FindBlob = (NIPLResult_FindBlob *)dOutput.m_pResult;
			for (auto dBlob : pResult_FindBlob->m_listBlob) {
				dBlob.m_rcBoundingBox.y += nMarginY;
				dBlob.m_rcBoundingBox.height = nImgSizeY;

				if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_BIN)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_BIN_2)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_SPOT)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE_2)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN_2)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_SPOT)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE_2)) {
						dBlob.m_rcBoundingBox.y = 0;
						dBlob.m_rcBoundingBox.x--;
						dBlob.m_rcBoundingBox.width += 2;
				}

				if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE_2)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE)
					|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE_2)) {
					dBlob.m_rcBoundingBox.y = 0;
					dBlob.m_rcBoundingBox.height *= 4;
					dBlob.m_rcBoundingBox.x--;
					dBlob.m_rcBoundingBox.width += 2;
				}

				if (bMaxRadius) {
					if (!CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE)
						&& !CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN)
						&& !CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN_2)
						&& !CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_SPOT)
						&& !CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE)
						&& !CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE_2)
						&& !CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE)
						&& !CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE_2)) {
						dBlob.m_rcBoundingBox.y = nFullImgSizeY - (dBlob.m_rcBoundingBox.y + dBlob.m_rcBoundingBox.height);
					}
				}

				NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_EDGE, dBlob.m_rcBoundingBox, dBlob.m_nSize * nImgSizeY);
				pResult->m_listDefect.push_back(dDefect);
				bFoundDefect = true;
			}

			delete pResult_FindBlob;
		}
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_BIN)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_BIN_2)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN_2)) {
		Mat((dImg | dEmptyLineImg) - (dEmptyLineImg * 0.5f)).copyTo(dRadiusEdgeImg);
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE_2)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE_2)) {

		MagCore_Inspection_RadiusEdge_GetLineProfile(dImg, dRadiusEdgeProfileLine, dRadiusEdgeHoleMask, dEmptyLineImg, nRadiusEdgeMaxSize);

		int nProfileImgSizeY = nImgSizeY * 4;
		dRadiusEdgeImg = Mat::zeros(nProfileImgSizeY, nImgSizeX, CV_8UC1);
		for (int j = 0; j < nImgSizeX; j++) {
			int nY = dRadiusEdgeProfileLine.at<UINT16>(0, j);
			if (nY >= 0 && nY <= nImgSizeX - 1) {
				dRadiusEdgeImg.at<UINT8>(nY, j) = 128;
			}

			int nSpotMask = dRadiusEdgeSpotMask.at<UINT8>(0, j);
			if (nSpotMask & RADIUS_EDGE_SPOT_POS_UPPER) {
				dRadiusEdgeImg.at<UINT8>(1, j) = 255;
				dRadiusEdgeImg.at<UINT8>(2, j) = 255;
			}
			if (nSpotMask & RADIUS_EDGE_SPOT_POS_LOWER) {
				dRadiusEdgeImg.at<UINT8>(nProfileImgSizeY - 3, j) = 255;
				dRadiusEdgeImg.at<UINT8>(nProfileImgSizeY - 2, j) = 255;
			}
		}

		for (auto dPoint : listProfileCurvePoint) {
			int nX = dPoint.first;
			int nY = dRadiusEdgeProfileLine.at<UINT16>(0, nX);
			if (nY >= 0 && nY <= nImgSizeX - 1) {
				dRadiusEdgeImg.at<UINT8>(nY, nX) = 255;
			}

		}
	}

	return NIPL_ERR_SUCCESS;
}

void NIPLCustom::MagCore_Inspection_RadiusEdge_GetLineProfile(Mat dRadiusEdgeBinImg, Mat &dRadiusEdgeProfileLine, Mat &dRadiusEdgeHoleMask, Mat &dEmptyLineImg, int nRadiusEdgeMaxSize)
{
	int nImgSizeX = dRadiusEdgeBinImg.cols;
	int nImgSizeY = dRadiusEdgeBinImg.rows;

	int nBaseY = nImgSizeY * 2;
	int nMaxY = nImgSizeY * 4;

	Mat dEmptyMask = Mat::zeros(dRadiusEdgeProfileLine.size(), CV_8UC1);
	Mat dThicknessProfile = Mat::zeros(dRadiusEdgeProfileLine.size(), CV_16UC1);
	Mat dUpperProfile = Mat::zeros(dRadiusEdgeProfileLine.size(), CV_16UC1);
	Mat dLowerProfile = Mat::zeros(dRadiusEdgeProfileLine.size(), CV_16UC1);
	Mat dUpperGradient = Mat::zeros(dRadiusEdgeProfileLine.size(), CV_16SC1);
	Mat dLowerGradient = Mat::zeros(dRadiusEdgeProfileLine.size(), CV_16SC1);

	for (int j = 0; j < nImgSizeX; j++) {
		bool bEmpty = false;
		if (dRadiusEdgeHoleMask.at<UINT8>(0, j) > 0) {
			bEmpty = true;
		}

		int nUpperY = -1;
		int nLowerY = -1;
		if (!bEmpty) {
			// set upper
			for (int i = 0; i < nImgSizeY; i++) {
				if (dRadiusEdgeBinImg.at<UINT8>(i, j) > 0) {
					nUpperY = i;

					dUpperProfile.at<UINT16>(0, j) = nBaseY + nUpperY;
					if (j > 0) {
						dUpperGradient.at<INT16>(0, j) = dUpperProfile.at<UINT16>(0, j) - dUpperProfile.at<UINT16>(0, j - 1);
					}
					break;
				}
			}
			if (nUpperY < 0) {	// If empty column
				bEmpty = true;
			}
		}

		if (!bEmpty) {
			// set lower
			for (int i = nImgSizeY - 1; i >= nUpperY; i--) {		// should include nUpperY
				if (dRadiusEdgeBinImg.at<UINT8>(i, j) > 0) {
					nLowerY = i;

					dLowerProfile.at<UINT16>(0, j) = nBaseY + nLowerY;
					if (j > 0) {
						dLowerGradient.at<INT16>(0, j) = dLowerProfile.at<UINT16>(0, j) - dLowerProfile.at<UINT16>(0, j - 1);
					}
					break;
				}
			}

			// check if there's a hole
			for (int i = nLowerY - 1; i > nUpperY; i--) {
				if (dRadiusEdgeBinImg.at<UINT8>(i, j) == 0) {
					// if there's hole, treat it as an empty column
					bEmpty = true;
					break;
				}
			}
		}

		if (bEmpty) {
			// keep gradients and thickness with 0
			dEmptyMask.at<UINT8>(0, j) = 1;
			dRadiusEdgeHoleMask.at<UINT8>(0, j) |= RADIUS_EDGE_HOLE;

			if (j > 0) {	// set previous values
				dUpperProfile.at<UINT16>(0, j) = dUpperProfile.at<UINT16>(0, j - 1);
				dLowerProfile.at<UINT16>(0, j) = dLowerProfile.at<UINT16>(0, j - 1);
				dThicknessProfile.at<UINT16>(0, j) = dThicknessProfile.at<UINT16>(0, j - 1);
			}

			Mat dEmptyLineSrc(dRadiusEdgeBinImg, Rect(j, 0, 1, nImgSizeY));
			Mat dEmptyLineDst(dEmptyLineImg, Rect(j, 0, 1, nImgSizeY));
			if (countNonZero(dEmptyLineSrc) == 0) {
				dEmptyLineDst = 128;
			}
			else {
				dEmptyLineSrc.copyTo(dEmptyLineDst);
			}
		}
		else {
			dThicknessProfile.at<UINT16>(0, j) = nLowerY - nUpperY + 1;
		}
	}

	// find isolated block
	int nStartX = -1;
	for (int j = 1; j < nImgSizeX; j++) {
		int nPrevUpperProfile = dUpperProfile.at<INT16>(0, j - 1);
		int nPrevLowerProfile = dLowerProfile.at<INT16>(0, j - 1);
		int nCurUpperProfile = dUpperProfile.at<INT16>(0, j);
		int nCurLowerProfile = dLowerProfile.at<INT16>(0, j);

		if (dEmptyMask.at<UINT8>(0, j) == 1) {
			continue;
		}

		if (nCurLowerProfile < (nPrevUpperProfile - 1) || nCurUpperProfile > (nPrevLowerProfile + 1)) {
			if (nStartX < 0) {
				nStartX = j;
				continue;
			}

			int nEndX = j - 1;
			int nSize = nEndX - nStartX + 1;

			if (nSize <= nRadiusEdgeMaxSize) {
				// found isolated block, set them empty 
				for (int k = nStartX; k <= nEndX; k++) {
					dEmptyMask.at<UINT8>(0, k) = 1;

					// set previous values
					dUpperProfile.at<UINT16>(0, k) = dUpperProfile.at<UINT16>(0, k - 1);
					dLowerProfile.at<UINT16>(0, k) = dLowerProfile.at<UINT16>(0, k - 1);
					dThicknessProfile.at<UINT16>(0, k) = dThicknessProfile.at<UINT16>(0, k - 1);
				}

				dUpperGradient.at<INT16>(0, j) = dUpperProfile.at<UINT16>(0, j) - dUpperProfile.at<UINT16>(0, j - 1);
				dLowerGradient.at<INT16>(0, j) = dLowerProfile.at<UINT16>(0, j) - dLowerProfile.at<UINT16>(0, j - 1);

				Mat dEmptyLineSrc(dRadiusEdgeBinImg, Rect(nStartX, 0, nEndX - nStartX + 1, nImgSizeY));
				Mat dEmptyLineDst(dEmptyLineImg, Rect(nStartX, 0, nEndX - nStartX + 1, nImgSizeY));
				dEmptyLineSrc.copyTo(dEmptyLineDst);
			}
				
			nStartX = j;
		}
	}

	// set line profile
	bool bFirst = true;
	int nPrevY = 0;
	int nPrevSign = 0;
	for (int j = 0; j < nImgSizeX; j++) {
		int nY;
		int nEmptyMask = dEmptyMask.at<UINT8>(0, j);
		if (nEmptyMask == 1) {
			nY = 0;
		}
		else if (bFirst) {
			bFirst = false;

			nY = nBaseY;
			nPrevY = nY;

			int nCurUpperGradient = dUpperGradient.at<INT16>(0, j);
			nPrevSign = sign(nCurUpperGradient);
		}
		else {
			int nCurUpperGradient = dUpperGradient.at<INT16>(0, j);
			int nCurLowerGradient = dLowerGradient.at<INT16>(0, j);
			int nCurThickness = dThicknessProfile.at<UINT16>(0, j);
			int nPrevThickness = dThicknessProfile.at<UINT16>(0, j - 1);

			int nSign = sign(nCurUpperGradient);
			if (abs(nCurUpperGradient) == abs(nCurLowerGradient) && sign(nCurUpperGradient) != sign(nCurLowerGradient)) {
				nSign = nPrevSign;
			}
			else if (abs(nCurUpperGradient) < abs(nCurLowerGradient)) {
				nSign = sign(nCurLowerGradient);
			}
			int nGradient = max(abs(nCurUpperGradient), abs(nCurLowerGradient));
			int nDeltaThickness = nCurThickness - nPrevThickness;
			int nDeltaProfile = nSign * max(abs(nGradient), abs(nDeltaThickness));

			nY = clamp(nPrevY + nDeltaProfile, 0, nMaxY - 1);
			nPrevY = nY;
			nPrevSign = nSign;
		}

		dRadiusEdgeProfileLine.at<UINT16>(0, j) = nY;
	}
}

void NIPLCustom::MagCore_Inspection_RadiusEdge_GetLineProfileCurvePoint(Mat dProfileLine, Mat dRadiusEdgeSpotMask, Mat dRadiusEdgeHoleMask, NIPLParam_MagCore *pParam, vector<pair<int, bool>> &listCurvePoint)
{
	int nBarSize = pParam->m_nRadiusEdgeProfileCurveBarSize;
	float nRadiusEdgeProfileCurveAngle = pParam->m_nRadiusEdgeProfileCurveAngle;
	int nRadiusEdgeSpotMaxSize = pParam->m_nRadiusEdgeSpotMaxSize;

	int nLineLength = dProfileLine.cols;

	bool bHoleOrSpot = false;
	bool bSpot = false;
	int nLastTwoX = -1;
	int nLastX = -1;
	for (int i = nBarSize; i < nLineLength - 1 - nBarSize; i++) {
		if (dRadiusEdgeHoleMask.at<UINT8>(0, i) > 0) {
			bHoleOrSpot = true;
			continue;
		}

		if (dRadiusEdgeSpotMask.at<UINT8>(0, i) > 0) {
			bHoleOrSpot = true;
		}

		Point2f ptCenter(i, dProfileLine.at<UINT16>(0, i));

		Mat dLeftBar(dProfileLine, Rect(i - nBarSize, 0, nBarSize, 1));
		float nMean = mean(dLeftBar)[0];
		Point2f ptLeft((i - 1) - (nBarSize - 1) * 0.5f, nMean);

		Mat dRightBar(dProfileLine, Rect(i + 1, 0, nBarSize, 1));
		nMean = mean(dRightBar)[0];
		Point2f ptRight((i + 1) + (nBarSize - 1) * 0.5f, nMean);

		float nAngle = calc_angle(ptLeft, ptCenter, ptRight);
		if (nAngle <= nRadiusEdgeProfileCurveAngle || (nLastTwoX >= 0 && (i - nLastTwoX) > nRadiusEdgeSpotMaxSize)) { // force to add one point if gap between points over max spot size in order to detect spot
			listCurvePoint.push_back(make_pair(i, bHoleOrSpot));

			nLastTwoX = nLastX;
			nLastX = i;
			bHoleOrSpot = false;
		}
	}
}

void NIPLCustom::MagCore_Inspection_RadiusEdge_CheckLineProfile(Mat dProfileLine, Mat &dProfileLineOutput, const vector<pair<int, bool>> &listCurvePoint, Mat dRadiusEdgeSpotMask, Mat dRadiusEdgeHoleMask, NIPLParam_MagCore *pParam, bool &bFoundDefect, bool bMaxRadius, bool bSecondLine)
{
	float nRadiusEdgeProfileThreshold = 0.f;
	float nRadiusEdgeSpotProfileThreshold = 0.f;
	if (bMaxRadius) {
		if (bSecondLine) {
			nRadiusEdgeProfileThreshold = pParam->m_nMaxRadiusEdgeProfileThreshold2;
			// Do not check edge spot on the second line
		}
		else {
			nRadiusEdgeProfileThreshold = pParam->m_nMaxRadiusEdgeProfileThreshold;
			nRadiusEdgeSpotProfileThreshold = pParam->m_nMaxRadiusEdgeSpotProfileThreshold;
		}
	}
	else {
		if (bSecondLine) {
			nRadiusEdgeProfileThreshold = pParam->m_nMinRadiusEdgeProfileThreshold2;
			// Do not check edge spot on the second line
		}
		else {
			nRadiusEdgeProfileThreshold = pParam->m_nMinRadiusEdgeProfileThreshold;
			nRadiusEdgeSpotProfileThreshold = pParam->m_nMinRadiusEdgeSpotProfileThreshold;
		}
	}

	int nRadiusEdgeSpotMinSize = pParam->m_nRadiusEdgeSpotMinSize;
	int nRadiusEdgeSpotMaxSize = pParam->m_nRadiusEdgeSpotMaxSize;
	int nRadiusEdgeHoleMinSize = pParam->m_nRadiusEdgeHoleMinSize;
	int nRadiusEdgeHoleMaxSize = pParam->m_nRadiusEdgeHoleMaxSize;
	int nRadiusEdgePerfectHoleMinSize = pParam->m_nRadiusEdgePerfectHoleMinSize;
	int nRadiusEdgeMinSize = pParam->m_nRadiusEdgeMinSize;
	int nRadiusEdgeMaxSize = pParam->m_nRadiusEdgeMaxSize;
	float nRadiusEdgeProfileDensity = pParam->m_nRadiusEdgeProfileDensity;

	int nLineLength = dProfileLine.cols;

	int nCurvePointCount = (int)listCurvePoint.size();
	int nFoundTargetStartX = -1;
	for (int k = 0; k < nCurvePointCount - 2; k++) {
		int nStartX = listCurvePoint[k].first;
		if (nFoundTargetStartX >= 0 && nStartX < nFoundTargetStartX) {
			continue;
		}

		nFoundTargetStartX = -1;
		for (int k2 = k + 2; k2 < nCurvePointCount; k2++) {
			int nEndX = listCurvePoint[k2].first;
			int nBarSize = nEndX - nStartX + 1;
			if (nBarSize - 2 < nRadiusEdgeMinSize) {
				continue;
			}
			if (nBarSize - 2 > nRadiusEdgeMaxSize) {
				break;
			}

			int nStartY = dProfileLine.at<UINT16>(0, nStartX);
			int nEndY = dProfileLine.at<UINT16>(0, nEndX);

			if (abs(nEndY - nStartY) > nRadiusEdgeProfileThreshold) {
				continue;
			}

			float nDeltaX = ((float)(nEndX - nStartX)) / (nBarSize - 1);
			float nDeltaY = ((float)(nEndY - nStartY)) / (nBarSize - 1);

			// curve point test first
			int nTargetStartX = -1;
			for (int k3 = k + 1; k3 < k2; k3++) {
				int nPrevX = listCurvePoint[k3 - 1].first;
				bool bHoleOrSpot = listCurvePoint[k3].second;
				if (bHoleOrSpot) {
					nTargetStartX = nPrevX;
					break;
				}

				int nX = listCurvePoint[k3].first;
				int nY = dProfileLine.at<UINT16>(0, nX);
				float nEstY = nStartY + nDeltaY * (nX - nStartX);
				float nDiff = abs(nY - nEstY);

				bool bSpot = (dRadiusEdgeSpotMask.at<UINT8>(0, nX) > 0);
				float nProfileThreshold = bSpot ? nRadiusEdgeSpotProfileThreshold : nRadiusEdgeProfileThreshold;
				if (nDiff > nProfileThreshold) {
					nTargetStartX = nPrevX;
					break;
				}
			}
			if (nTargetStartX < 0) {
				continue;
			}

			int nTargetEndX = -1;
			for (int k3 = k2 - 1; k3 > k; k3--) {
				int nNextX = listCurvePoint[k3 + 1].first;
				bool bHoleOrSpot = listCurvePoint[k3 + 1].second;
				if (bHoleOrSpot) {
					nTargetEndX = nNextX;
					break;
				}

				int nX = listCurvePoint[k3].first;
				int nY = dProfileLine.at<UINT16>(0, nX);
				float nEstY = nStartY + nDeltaY * (nX - nStartX);
				float nDiff = abs(nY - nEstY);

				bool bSpot = (dRadiusEdgeSpotMask.at<UINT8>(0, nX) > 0);
				float nProfileThreshold = bSpot ? nRadiusEdgeSpotProfileThreshold : nRadiusEdgeProfileThreshold;
				if (nDiff > nProfileThreshold) {
					nTargetEndX = nNextX;
					break;
				}
			}
			if (nTargetEndX < 0) {
				continue;
			}

			int nBlobStartX = -1;
			int nBlobEndX = -1;
			int nSpotBlobStartX = -1;
			int nSpotBlobEndX = -1;
			float nSumDiff = 0;
			float nSpotSumDiff = 0;

			int nCount = 0;
			int nSize = 0;
			bool bSpot = false;
			bool bHole = false;
			bool bPerfectHole = false;
			for (int i = nTargetStartX; i <= nTargetEndX; i++) {
				int nHoleMask = dRadiusEdgeHoleMask.at<UINT8>(0, i);
				bool bHolePoint = (nHoleMask > 0);
				if (!bHole && bHolePoint && (nBarSize - 2) < nRadiusEdgeHoleMaxSize) {
					bHole = true;

					if (nRadiusEdgePerfectHoleMinSize > 0 && nHoleMask & RADIUS_EDGE_PERFECT_HOLE) {
						bPerfectHole = true;
					}
				}

				int nY = dProfileLine.at<UINT16>(0, i);
				float nEstY = nStartY + nDeltaY * (i - nStartX);
				float nDiff = abs(nY - nEstY);
				if (nDiff > nRadiusEdgeProfileThreshold) {
					nSumDiff += nDiff;
					nCount++;

					if (nBlobStartX < 0) {
						nBlobStartX = i;
						nBlobEndX = i;
					}
					else {
						int nSize = (i - nBlobStartX + 1);
						float nDensity = ((float)nCount) / nSize;
						if (nDensity >= nRadiusEdgeProfileDensity) {
							nBlobEndX = i;
						}
					}
				}

				int nSpotMask = dRadiusEdgeSpotMask.at<UINT8>(0, i);
				if (!bHolePoint && nRadiusEdgeSpotProfileThreshold > 0.f && (nBarSize - 2) < nRadiusEdgeSpotMaxSize && nSpotMask > 0) {
					if (nDiff > nRadiusEdgeSpotProfileThreshold) {
						bool bBlobPosLower = (nY > nEstY);
						if ((bBlobPosLower && (nSpotMask & RADIUS_EDGE_SPOT_POS_UPPER)) || (!bBlobPosLower && (nSpotMask & RADIUS_EDGE_SPOT_POS_LOWER))) {
							nSpotSumDiff += nDiff;

							if (nSpotBlobStartX < 0) {
								nSpotBlobStartX = i;
								nSpotBlobEndX = i;
							}
							else if (i == nSpotBlobEndX + 1) {	// enlarge size only if it comes continuously
								nSpotBlobEndX = i;
							}
							else {
								nSize = nSpotBlobEndX - nSpotBlobStartX + 1;
								if (nSize < nRadiusEdgeSpotMinSize) {
									// initialize to check again
									nSpotBlobStartX = -1;
									nSpotBlobEndX = -1;
								}
							}
						}
					}
				}
			}

			// check if finding blob
			bool bFound = false;
			nSize = (nBlobEndX - nBlobStartX + 1);
			int nMinSize = bPerfectHole ? nRadiusEdgePerfectHoleMinSize : (bHole ? nRadiusEdgeHoleMinSize : nRadiusEdgeMinSize);
			if (nSize >= nMinSize && nSize <= nRadiusEdgeMaxSize) {
				float nMeanDiff = nSumDiff / nCount;
				DebugPrint("  ==> Defect, %s, BarSize : %d (%d ~ %d), TargetX : (%d ~ %d), DefectSize : %d (%d ~ %d) >= %d, MeanDiff : %.1f > %.1f",
					bPerfectHole ? "Perfect Hole" : (bHole ? "Hole" : "Normal"), nBarSize, nStartX, nEndX, nTargetStartX, nTargetEndX,
					nSize, nBlobStartX, nBlobEndX, nMinSize, nMeanDiff, nRadiusEdgeProfileThreshold);

				for (int j = nBlobStartX; j <= nBlobEndX; j++) {
					dProfileLineOutput.at<UINT8>(0, j) = 255;
				}

				bFound = true;
				bFoundDefect = true;
			}

			nSize = nSpotBlobEndX - nSpotBlobStartX + 1;
			if (nSize >= nRadiusEdgeSpotMinSize) {
				float nMeanDiff = nSpotSumDiff / nSize;

				DebugPrint("  ==> Defect, Spot, BarSize : %d (%d ~ %d), TargetX : (%d ~ %d), DefectSize : %d (%d ~ %d) >= %d, MeanDiff : %.1f > %.1f",
					nBarSize, nStartX, nEndX, nTargetStartX, nTargetEndX,
					nSize, nSpotBlobStartX, nSpotBlobEndX, nRadiusEdgeSpotMinSize, nMeanDiff, nRadiusEdgeSpotProfileThreshold);

				for (int j = nSpotBlobStartX; j <= nSpotBlobEndX; j++) {
					dProfileLineOutput.at<UINT8>(0, j) = 255;
				}

				bFound = true;
				bFoundDefect = true;
			}

			if (bFound) {
				nFoundTargetStartX = nTargetStartX;
				break;
			}
		}
	}
}

NIPL_ERR NIPLCustom::MagCore_Inspection_Surface(Mat dImg, Mat &dSurfaceImg, Mat dMask, NIPLParam_MagCore *pParam, NIPLResult_Defect_MagCore *pResult, bool &bFoundDefect)
{
	wstring strShowImage = pParam->m_strShowImage;

	int nBlockCount = pParam->m_nSurfaceBlockCount;
	int nMinRadiusEdgeMargin = pParam->m_nSurfaceMinRadiusEdgeMargin;
	int nMaxRadiusEdgeMargin = pParam->m_nSurfaceMaxRadiusEdgeMargin;
	float nSurfaceBlockBinarizeThreshold = pParam->m_nSurfaceBlockBinarizeThreshold;
	float nSurfaceBlockVarianceRatio = pParam->m_nSurfaceBlockVarianceRatio;
	float nSurfaceBlockVarianceMax = pParam->m_nSurfaceBlockVarianceMax;
	float nSurfaceBlockVarianceThreshold = pParam->m_nSurfaceBlockVarianceThreshold;

	int nImgSizeX = dImg.cols;
	int nImgSizeY = dImg.rows;

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE_BLOCK)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE_BLOCK_BIN)) {
		pResult->Clear();
	}

	if (nBlockCount <= 0) {
		dSurfaceImg = Mat::zeros(1, 1, dImg.type());
		return NIPL_ERR_SUCCESS;
	}

	Rect rcSurfaceROI(0, nMinRadiusEdgeMargin, nImgSizeX, nImgSizeY - (nMinRadiusEdgeMargin + nMaxRadiusEdgeMargin));
	if (rcSurfaceROI.height < 0) {
		dSurfaceImg = Mat::zeros(1, 1, dImg.type());
		return NIPL_ERR_SUCCESS;
	}

	Mat(dImg, rcSurfaceROI).copyTo(dSurfaceImg);

	Mat dSurfaceMask;
	if (!CHECK_EMPTY_IMAGE(dMask)) {
		Mat(dMask, rcSurfaceROI).copyTo(dSurfaceMask);
	}

	nImgSizeX = dSurfaceImg.cols;
	nImgSizeY = dSurfaceImg.rows;

	NIPLInput dInput;
	NIPLOutput dOutput;
	NIPL_ERR nErr;

	// FitBackground
	NIPLParam_FitBackground dParam_FitBackground;
	dParam_FitBackground.m_nBlockCountX = 32;
	dParam_FitBackground.m_nBlockCountY = 8;
	dParam_FitBackground.m_nDegree = 3;
	dParam_FitBackground.m_nThreshold = 3.f;
	dParam_FitBackground.m_bSubtrackFromImage = true;

	dInput.m_dImg = dSurfaceImg;
	dInput.m_dMask = dSurfaceMask;
	dInput.m_pParam = &dParam_FitBackground;
	nErr = FitBackground(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dSurfaceImg = dOutput.m_dImg;

	int nBlockSizeX = nImgSizeX / nBlockCount;
	int nBlockSizeY = nImgSizeY;

	int nBlockImgSizeX = 100;
	int nBlockImgSizeY = 100;
	Mat dBlockImg = Mat::zeros(nBlockImgSizeY, nBlockImgSizeX * nBlockCount, CV_8UC1);
	Mat dBlockBinImg = Mat::zeros(nBlockImgSizeY, nBlockImgSizeX * nBlockCount, CV_8UC1);
	Mat dBlockMask = Mat::ones(1, nBlockCount, CV_8UC1);
	Mat dBlockStd = Mat::ones(1, nBlockCount, CV_32FC1);

	vector<pair<int, float>> dBlockStdForSort;

	Rect rcBlock;
	Rect rcBlockImg;
	Scalar dMean;
	Scalar dStd;
	for (int j = 0; j < nBlockCount; j++) {
		rcBlock = Rect(j * nBlockSizeX, 0, nBlockSizeX, nBlockSizeY);
		rcBlockImg = Rect(j * nBlockImgSizeX, 0, nBlockImgSizeX, nBlockImgSizeY);

		if (!CHECK_EMPTY_IMAGE(dSurfaceMask)) {
			Mat dTempMask(dSurfaceMask, rcBlock);
			if (countNonZero(dTempMask) != rcBlock.width * rcBlock.height) {
				dBlockMask.at<UINT8>(0, j) = 0;
				continue;
			}
		}

		Mat dSurfaceBlock(dSurfaceImg, rcBlock);
		Mat dBlock(dBlockImg, rcBlockImg);
		resize(dSurfaceBlock, dBlock, dBlock.size());

		// Find Outliers and exclude them on calculating std.
		dInput.Clear();
		dOutput.Clear();

		NIPLParam_Binarize dParam_Binarize;
		dParam_Binarize.m_nMethod = NIPLParam_Binarize::METHOD_BOTH;
		dParam_Binarize.m_nThreshold = nSurfaceBlockBinarizeThreshold;

		dInput.m_dImg = dBlock;
		dInput.m_pParam = &dParam_Binarize;
		nErr = Binarize(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		Mat dBinMask = dOutput.m_dImg;

		// Extended Size
		dInput.Clear();
		dOutput.Clear();

		NIPLParam_MorphologyOperate dParam_MorphologyOperate;
		dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_DILATE;
		dParam_MorphologyOperate.m_bCircleFilter = true;
		dParam_MorphologyOperate.m_nFilterSizeX = 5;
		dParam_MorphologyOperate.m_nFilterSizeY = 5;

		dInput.m_dImg = dBinMask;
		dInput.m_pParam = &dParam_MorphologyOperate;
		nErr = MorphologyOperate(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}
		dBinMask = ~dOutput.m_dImg;

		Mat dBlockBin(dBlockBinImg, rcBlockImg);
		dBinMask.copyTo(dBlockBin);

		meanStdDev(dBlock, dMean, dStd, dBinMask);
		float nStd = (float)dStd[0];

		dBlockStd.at<FLOAT>(0, j) = (float)dStd[0];
		dBlockStdForSort.push_back(make_pair(j, nStd));

		DebugPrint("Surface Inspection. Block Std (%d) : %.3f", j, dStd[0]);

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE_BLOCK)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE_BLOCK_BIN)) {
			NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_BLOCK, rcBlockImg, (int)(nBlockImgSizeX * nBlockImgSizeY));
			pResult->m_listDefect.push_back(dDefect);
		}
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE_BLOCK)) {
		dSurfaceImg = dBlockImg;
	}
	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE_BLOCK_BIN)) {
		dSurfaceImg = dBlockBinImg;
	}

	int nValidBlockCount = countNonZero(dBlockMask);
	if (nValidBlockCount < 3) {		// at least 4 blocks are needed to be processed.
		DebugPrint("Surface Inspection. Black Similarity skip, not enough valid blocks : %d", nValidBlockCount);
		return NIPL_ERR_SUCCESS;
	}

	Mat dBlockMaskLowPart = Mat::zeros(1, nBlockCount, CV_8UC1);

	sort(dBlockStdForSort.begin(), dBlockStdForSort.end(), [](const pair<int, float> &left, const pair<int, float> &right) {
		return left.second < right.second;
	});
	int nPartCount = (int)(nValidBlockCount * nSurfaceBlockVarianceRatio);
	int nCount = (int)dBlockStdForSort.size();
	int nMaxIndex = 0;
	float nMaxStd = 0.f;

	for (int i = 0; i < nCount; i++) {
		auto dSimilarity = dBlockStdForSort[i];
		if (i < nPartCount) {
			dBlockMaskLowPart.at<UINT8>(0, dSimilarity.first) = 1;
			DebugPrint(" Sort Std (%d) : %.4f, Low Part", dSimilarity.first, dSimilarity.second);
		}
		else if (i == nCount - 1) {
			nMaxIndex = dSimilarity.first;
			nMaxStd = dSimilarity.second;
			DebugPrint(" Sort Std (%d) : %.4f, Max", dSimilarity.first, dSimilarity.second);
		}
		else {
			DebugPrint(" Sort Std (%d) : %.4f", dSimilarity.first, dSimilarity.second);
		}
	}

	meanStdDev(dBlockStd, dMean, dStd, dBlockMaskLowPart);
	float nBlockMeanOfStdLowPart = (float)dMean[0];
	float nBlockStdOfStdLowPart = (float)dStd[0];
	float nValue = abs(nMaxStd - nBlockMeanOfStdLowPart) / nBlockStdOfStdLowPart;

	DebugPrint("Surface Inspection. Block Ratio : %.2f, Max : %.2f > %.2f, Value : %.2f > %.2f",
		nSurfaceBlockVarianceRatio, nMaxStd, nSurfaceBlockVarianceMax, nValue, nSurfaceBlockVarianceThreshold);

	if (nMaxStd > nSurfaceBlockVarianceMax && nValue >= nSurfaceBlockVarianceThreshold) {
		DebugPrint("  ==> Defect (%d)", nMaxIndex);

		rcBlock = Rect(nMaxIndex * nBlockSizeX, nMinRadiusEdgeMargin, nBlockSizeX, nBlockSizeY);

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE)) {
			rcBlock = Rect(nMaxIndex * nBlockSizeX, 0, nBlockSizeX, nBlockSizeY);
		}
			
		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE_BLOCK)
			|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_SURFACE_BLOCK_BIN)) {
			rcBlock = Rect(nMaxIndex * nBlockImgSizeX, 0, nBlockImgSizeX, nBlockImgSizeY);
		}

		NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_SURFACE, rcBlock, (int)(nBlockSizeX * nBlockSizeY));
		pResult->m_listDefect.push_back(dDefect);

		bFoundDefect = true;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::MagCore_Inspection_Hole(Mat dImg, Mat &dHoleImg, Mat dMask, NIPLParam_MagCore *pParam, NIPLResult_Defect_MagCore *pResult, bool &bFoundDefect)
{
	wstring strShowImage = pParam->m_strShowImage;

	int nBlockCountX = pParam->m_nHoleBlockCountX;
	int nBlockCountY = pParam->m_nHoleBlockCountY;
	int nMinRadiusEdgeMargin = pParam->m_nHoleMinRadiusEdgeMargin;
	int nMaxRadiusEdgeMargin = pParam->m_nHoleMaxRadiusEdgeMargin;
	float nHoleBinarizeThreshold = pParam->m_nHoleBinarizeThreshold;
	int nHoleMinSize = pParam->m_nHoleMinSize;
	int nHoleMaxSize = pParam->m_nHoleMaxSize;
	float nHoleSizeRatio = pParam->m_nHoleSizeRatio;
	float nHoleFitRatio = pParam->m_nHoleFitRatio;
	float nHoleSizeFitRatio = pParam->m_nHoleSizeFitRatio;
	float nHoleFillRatio = pParam->m_nHoleFillRatio;
	int nHoleSmallSize = pParam->m_nHoleSmallSize;
	float nHoleSmallSizeRatio = pParam->m_nHoleSmallSizeRatio;
	float nHoleSmallFitRatio = pParam->m_nHoleSmallFitRatio;
	float nHoleSmallSizeFitRatio = pParam->m_nHoleSmallSizeFitRatio;
	int nHoleBigSize = pParam->m_nHoleBigSize;
	float nHoleBigSizeRatio = pParam->m_nHoleBigSizeRatio;

	int nImgSizeX = dImg.cols;
	int nImgSizeY = dImg.rows;

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN)
		|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN_CIRCLE)) {
		pResult->Clear();
	}

	if (nBlockCountX <= 0 || nBlockCountY <= 0) {
		dHoleImg = Mat::zeros(1, 1, dImg.type());
		return NIPL_ERR_SUCCESS;
	}

	Rect rcHoleROI(0, nMinRadiusEdgeMargin, nImgSizeX, nImgSizeY - (nMinRadiusEdgeMargin + nMaxRadiusEdgeMargin));
	if (rcHoleROI.height < 0) {
		dHoleImg = Mat::zeros(1, 1, dImg.type());
		return NIPL_ERR_SUCCESS;
	}

	int nExtSize = sqrt(nHoleMaxSize);

	Mat(dImg, rcHoleROI).copyTo(dHoleImg);
	MagCore_Inspection_ExtendImage(dHoleImg, dHoleImg, nExtSize);

	Mat dHoleMask;
	if (!CHECK_EMPTY_IMAGE(dMask)) {
		Mat(dMask, rcHoleROI).copyTo(dHoleMask);
		MagCore_Inspection_ExtendImage(dHoleMask, dHoleMask, nExtSize);
	}

	nImgSizeX = dHoleImg.cols;
	nImgSizeY = dHoleImg.rows;

	float nOverlapRate = 0.25f;
	float nBlockSizeX_F = nImgSizeX / (nBlockCountX + nOverlapRate - nBlockCountX * nOverlapRate);
	float nBlockOverlapX_F = nBlockSizeX_F * nOverlapRate;
	int nBlockSizeX = (int)nBlockSizeX_F;
	float nBlockSizeY_F = nImgSizeY / (nBlockCountY + nOverlapRate - nBlockCountY * nOverlapRate);
	float nBlockOverlapY_F = nBlockSizeY_F * nOverlapRate;
	int nBlockSizeY = (int)nBlockSizeY_F;

	// adjust width ratio between rect image and circle image
	float nRatioOfHeightPerWidth = 1.6f;

	Mat dBlockImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	Mat dBlockBinImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	Mat dBlockBinCircleImg = Mat::zeros(nImgSizeY, nImgSizeX * nRatioOfHeightPerWidth, CV_8UC1);
//	Mat dDoneMask = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

	Rect rcBlock;
	NIPLInput dInput;
	NIPLOutput dOutput;

	for (int j = 0; j < nBlockCountX; j++) {
		for (int i = 0; i < nBlockCountY; i++) {
			rcBlock = Rect((int)(j * (nBlockSizeX_F - nBlockOverlapX_F)), (int)(i * (nBlockSizeY_F - nBlockOverlapY_F)), nBlockSizeX, nBlockSizeY);

			if (!CHECK_EMPTY_IMAGE(dHoleMask)) {
				Mat dTempMask(dHoleMask, rcBlock);
				if (countNonZero(dTempMask) != rcBlock.width * rcBlock.height) {
					continue;
				}
			}

			// set block images
			Mat dBlock(dBlockImg, rcBlock);
			Mat(dHoleImg, rcBlock).copyTo(dBlock);

			//Mat dBlockDoneMask(dDoneMask, rcBlock);

			if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK)) {
				NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_BLOCK, rcBlock, (int)(nBlockSizeX * nBlockSizeY));
				pResult->m_listDefect.push_back(dDefect);
			}

			// FitBackground
			dInput.Clear();
			dOutput.Clear();

			NIPLParam_FitBackground dParam_FitBackground;
			dParam_FitBackground.m_nBlockCountX = 8;
			dParam_FitBackground.m_nBlockCountY = 8;
			dParam_FitBackground.m_nDegree = 3;
			dParam_FitBackground.m_nThreshold = 3.f;
			dParam_FitBackground.m_bSubtrackFromImage = true;

			dInput.m_dImg = dBlock;
			dInput.m_pParam = &dParam_FitBackground;
			NIPL_ERR nErr = FitBackground(&dInput, &dOutput);
			if (NIPL_FAIL(nErr)) {
				return nErr;
			}
			if (!NIPL_PASS(nErr)) {
				dOutput.m_dImg.copyTo(dBlock);
			}

			// Binarize
			dInput.Clear();
			dOutput.Clear();

			NIPLParam_Binarize dParam_Binarize;
			dParam_Binarize.m_nMethod = NIPLParam_Binarize::METHOD_BOTH;
			dParam_Binarize.m_nThreshold = nHoleBinarizeThreshold;

			dInput.m_dImg = dBlock;
			dInput.m_pParam = &dParam_Binarize;
			nErr = Binarize(&dInput, &dOutput);
			if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
				return nErr;
			}
			Mat dBinImg = dOutput.m_dImg;

			// remove none defect area
			//dBinImg -= dBlockDoneMask;

			dInput.Clear();
			dOutput.Clear();

			NIPLParam_EliminateNoise dParam_EliminateNoise;
			dParam_EliminateNoise.m_nMethod = NIPLParam_EliminateNoise::METHOD_SIZE;
			dParam_EliminateNoise.m_nMinSize = nHoleMinSize * 0.5f;
			dParam_EliminateNoise.m_nMaxSize = nHoleMaxSize;

			dInput.m_dImg = dBinImg;
			dInput.m_pParam = &dParam_EliminateNoise;
			nErr = EliminateNoise(&dInput, &dOutput);
			if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
				return nErr;
			}
			dBinImg = dOutput.m_dImg;

			if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN)) {
				Mat dBlockBin(dBlockBinImg, rcBlock);
				Mat(dBlockBin | dBinImg).copyTo(dBlockBin);

				NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_BLOCK, rcBlock, (int)(nBlockSizeX * nBlockSizeY));
				pResult->m_listDefect.push_back(dDefect);
			}

			Rect rcAdjustedBlock = rcBlock;
			rcAdjustedBlock.x *= nRatioOfHeightPerWidth;
			rcAdjustedBlock.width *= nRatioOfHeightPerWidth;

			if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN_CIRCLE)) {
				NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_BLOCK, rcAdjustedBlock, (int)(nBlockSizeX * nBlockSizeY));
				pResult->m_listDefect.push_back(dDefect);
			}

			// rescaling to match ratio betwen horiz and vert
			resize(dBinImg, dBinImg, rcAdjustedBlock.size(), 0, 0, INTER_LINEAR);

			// Fill inside
			dInput.Clear();
			dOutput.Clear();

			NIPLParam_MorphologyOperate dParam_MorphologyOperate;
			dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_CLOSE;
			dParam_MorphologyOperate.m_bCircleFilter = true;
			dParam_MorphologyOperate.m_nFilterSizeX = 3; // cvRound(sizeDefect.width * 0.5f);
			dParam_MorphologyOperate.m_nFilterSizeY = 3; // cvRound(sizeDefect.height * 0.5f);

			dInput.m_dImg = dBinImg;
			dInput.m_pParam = &dParam_MorphologyOperate;
			nErr = MorphologyOperate(&dInput, &dOutput);
			if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
				return nErr;
			}
			dBinImg = dOutput.m_dImg;

			// FindBlob
			dInput.Clear();
			dOutput.Clear();

			NIPLParam_FindBlob dParam_FindBlob;
			dParam_FindBlob.m_bFindInRange = true;

			dInput.m_dImg = dBinImg;
			dInput.m_pParam = &dParam_FindBlob;
			nErr = FindBlob(&dInput, &dOutput);
			if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
				return nErr;
			}

			if (dOutput.m_pResult != nullptr) {
				NIPLResult_FindBlob *pResult_FindBlob = (NIPLResult_FindBlob *)dOutput.m_pResult;
				for (auto dBlob : pResult_FindBlob->m_listBlob) {
					Size sizeDefect(dBlob.m_rcBoundingBox.width, dBlob.m_rcBoundingBox.height);
					Mat dDefectImg(dBinImg, dBlob.m_rcBoundingBox);

					int nDefectSize = dBlob.m_nSize; // countNonZero(dAdjustedDefectImg);
					if (nDefectSize < nHoleMinSize) {
						continue;
					}

					bool bSmallDefect = (nDefectSize <= nHoleSmallSize);
					bool bBigDefect = (nHoleBigSize > 0 && nDefectSize >= nHoleBigSize);

					DebugPrint("Hole Inspection. (%d, %d), Hole Pos(%d, %d) Adjusted Pos(%d, %d), Box Size(%d, %d), Size : %d MinMax(%d, %d) SmallBig(%d, %d) => %s", i, j,
						rcBlock.x + (int)(dBlob.m_rcBoundingBox.x / nRatioOfHeightPerWidth), rcBlock.y + dBlob.m_rcBoundingBox.y, 
						rcBlock.x + dBlob.m_rcBoundingBox.x, rcBlock.y + dBlob.m_rcBoundingBox.y, dBlob.m_rcBoundingBox.width, dBlob.m_rcBoundingBox.height,
						nDefectSize, nHoleMinSize, nHoleMaxSize, nHoleSmallSize, nHoleBigSize, bSmallDefect ? "Small" : (bBigDefect ? "Big" : "Normal")); 

					float nSizeRatio = ((float)max(sizeDefect.width, sizeDefect.height)) / min(sizeDefect.width, sizeDefect.height);

					if (bBigDefect) {
						DebugPrint("  Fit Test, SizeRatio : %.2f < %.2f", nSizeRatio, nHoleBigSizeRatio);

						if (nSizeRatio > nHoleBigSizeRatio) {
							DebugPrint("    > Not Fit Size Ratio, Skip");
							continue;
						}

						Rect rcDefect(rcBlock.x + dBlob.m_rcBoundingBox.x / nRatioOfHeightPerWidth, rcBlock.y + dBlob.m_rcBoundingBox.y + nMinRadiusEdgeMargin, sizeDefect.width / nRatioOfHeightPerWidth, sizeDefect.height);
						if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE)
							|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK)
							|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN)) {
							rcDefect = Rect(rcDefect.x - 1, rcDefect.y - 1 - nMinRadiusEdgeMargin, rcDefect.width + 2, rcDefect.height + 2);
						}

						if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN_CIRCLE)) {
							rcDefect = Rect(rcBlock.x * nRatioOfHeightPerWidth + dBlob.m_rcBoundingBox.x, rcBlock.y + dBlob.m_rcBoundingBox.y, sizeDefect.width, sizeDefect.height);

							Mat dBinCircleImg(dBlockBinCircleImg, rcDefect);
							Mat(dBinCircleImg | dDefectImg).copyTo(dBinCircleImg);

							// enlarge box to show the whole defect 
							rcDefect = Rect(rcDefect.x - 1, rcDefect.y - 1, rcDefect.width + 2, rcDefect.height + 2);
						}

						DebugPrint("    ==> Big Size Defect. Pos (%d, %d)", rcDefect.x, rcDefect.y);

						NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_HOLE, rcDefect, dBlob.m_nSize);
						pResult->m_listDefect.push_back(dDefect);
						 
						bFoundDefect = true;
					}
					else {
						float nSizeRatio = ((float)max(sizeDefect.width, sizeDefect.height)) / min(sizeDefect.width, sizeDefect.height);
						float nSizeRatioThreshold = bSmallDefect ? nHoleSmallSizeRatio : nHoleSizeRatio;

						DebugPrint("  Fit Test, SizeRatio : %.2f < %.2f", nSizeRatio, nSizeRatioThreshold);

						if (nSizeRatio > nSizeRatioThreshold) {
							DebugPrint("    > Not Fit Size Ratio, Skip");
							continue;
						}

						// Find Ellipse
						NIPLInput dInput2;
						NIPLOutput dOutput2;

						NIPLParam_FindEllipse dParam_FindEllipse;
						dParam_FindEllipse.m_bFindOnlyOne = true;

						dParam_FindEllipse.m_bCheckCenterPos = true;
						dParam_FindEllipse.m_nCenterPosX = sizeDefect.width * 0.5f;
						dParam_FindEllipse.m_nCenterPosY = sizeDefect.height * 0.5f;
						dParam_FindEllipse.m_nCenterPosRange = max((int)(min(sizeDefect.width, sizeDefect.height) * 0.15f), 2);

						dInput2.m_dImg = dDefectImg;
						dInput2.m_pParam = &dParam_FindEllipse;
						nErr = FindEllipse(&dInput2, &dOutput2);
						if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
							return nErr;
						}
						Mat dFitCircleImg = dOutput2.m_dImg;

						NIPLResult_FindEllipse *pResult_FindEllipse = (NIPLResult_FindEllipse *)dOutput2.m_pResult;
						if (pResult_FindEllipse == nullptr) {
							DebugPrint("    > Not Found Circle, Skip");
							continue;
						}

						NIPLEllipse dFoundCircle = pResult_FindEllipse->m_listEllipse[0];
						delete pResult_FindEllipse;

						nSizeRatio = ((float)max(dFoundCircle.m_rcEllipse.size.width, dFoundCircle.m_rcEllipse.size.height))
							/ min(dFoundCircle.m_rcEllipse.size.width, dFoundCircle.m_rcEllipse.size.height);
						bool bRotated = (dFoundCircle.m_rcEllipse.angle > 45.f && dFoundCircle.m_rcEllipse.angle < 135.f);

						DebugPrint("  Fit Test, Found Ellipse Size (%.f, %.f), SizeRatio : %.2f < %.2f, , nAngle : %.1f (%s)",
							dFoundCircle.m_rcEllipse.size.width, dFoundCircle.m_rcEllipse.size.height, nSizeRatio, nSizeRatioThreshold,
							dFoundCircle.m_rcEllipse.angle, bRotated ? "Rotated" : "Not Rotated");

						if (nSizeRatio > nSizeRatioThreshold) {
							DebugPrint("    > Not Fit Size Ratio, Skip");
							continue;
						}

						Mat dFullCircleImg = Mat::zeros(sizeDefect, CV_8UC1);
						ellipse(dFullCircleImg, dFoundCircle.m_rcEllipse, Scalar(255, 255, 255), 1, LINE_8);
						int nTotalPixelCount = countNonZero(dFullCircleImg);

						Mat dOverlapImg = dFitCircleImg & dDefectImg;
						int nPixelCount = countNonZero(dOverlapImg);
						float nFitRatio = (float)nPixelCount / nTotalPixelCount;
						float nFitRatioThreshold = bSmallDefect ? nHoleSmallFitRatio : nHoleFitRatio;

						float nCircleWidthRatio = (bRotated ? dFoundCircle.m_rcEllipse.size.height : dFoundCircle.m_rcEllipse.size.width) / sizeDefect.width;
						float nCircleHeightRatio = (bRotated ? dFoundCircle.m_rcEllipse.size.width : dFoundCircle.m_rcEllipse.size.height) / sizeDefect.height;
						float nSizeFitRatio = max(nCircleWidthRatio, nCircleHeightRatio);
						float nSizeFitRatioThreshold = bSmallDefect ? nHoleSmallSizeFitRatio : nHoleSizeFitRatio;

						// Fill inside
						ellipse(dFullCircleImg, dFoundCircle.m_rcEllipse, Scalar(255, 255, 255), FILLED, LINE_8);
						dOverlapImg = ~dFullCircleImg & dDefectImg;
						int nFillTotalPixelCount = countNonZero(dFullCircleImg);
						int nFillOutsidePixelCount = countNonZero(dOverlapImg);
						float nFillRatio = 1.f - (float)nFillOutsidePixelCount / nFillTotalPixelCount;
						// if small defect, don't care of fill ratio
						float nFillRatioThreshold = bSmallDefect ? 0 : nHoleFillRatio;

						DebugPrint("  Fit Test, FitRatio : %.2f (%d/%d) > %.2f, FillRatio : %.2f (%d/%d) > %.2f, SizeFitRatio : %.2f < %.2f",
							nFitRatio, nPixelCount, nTotalPixelCount, nFitRatioThreshold,
							nFillRatio, nFillTotalPixelCount - nFillOutsidePixelCount, nFillTotalPixelCount, nFillRatioThreshold,
							nSizeFitRatio, nSizeFitRatioThreshold);

						if (nFitRatio == 1.f || (nFitRatio >= nFitRatioThreshold && nFillRatio >= nFillRatioThreshold)
							&& nSizeFitRatio <= nSizeFitRatioThreshold) {
							Rect rcDefect(rcBlock.x + dBlob.m_rcBoundingBox.x / nRatioOfHeightPerWidth, rcBlock.y + dBlob.m_rcBoundingBox.y + nMinRadiusEdgeMargin, sizeDefect.width / nRatioOfHeightPerWidth, sizeDefect.height);

							if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE)
								|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK)
								|| CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN)) {
								rcDefect = Rect(rcDefect.x - 1, rcDefect.y - 1 - nMinRadiusEdgeMargin, rcDefect.width + 2, rcDefect.height + 2);
							}

							if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN_CIRCLE)) {
								rcDefect = Rect(rcBlock.x * nRatioOfHeightPerWidth + dBlob.m_rcBoundingBox.x, rcBlock.y + dBlob.m_rcBoundingBox.y, sizeDefect.width, sizeDefect.height);

								Mat dBinCircleImg(dBlockBinCircleImg, rcDefect);
								dFitCircleImg = dFitCircleImg | (dDefectImg * 0.5f);
								dFitCircleImg.copyTo(dBinCircleImg);

								// enlarge box to show the whole defect 
								rcDefect = Rect(rcDefect.x - 1, rcDefect.y - 1, rcDefect.width + 2, rcDefect.height + 2);
							}

							DebugPrint("    ==> Defect. Pos (%d, %d)", rcDefect.x, rcDefect.y);

							NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_HOLE, rcDefect, dBlob.m_nSize);
							pResult->m_listDefect.push_back(dDefect);

							bFoundDefect = true;
						}
					}

					// if it's not located in block boundary, set it done block
/*
					if (dBlob.m_rcBoundingBox.x != 0 && dBlob.m_rcBoundingBox.x + dBlob.m_rcBoundingBox.width != rcBlock.width) {
						Mat dMask(dBlockDoneMask, dBlob.m_rcBoundingBox);
						Mat(dMask | dDefectImg).copyTo(dMask);
						DebugPrint("  > Set Done Area");
					}
*/
				}

				delete pResult_FindBlob;
			}
		}
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK)) {
		dHoleImg = dBlockImg;
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN)) {
		dHoleImg = dBlockBinImg;
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN_CIRCLE)) {
		dHoleImg = dBlockBinCircleImg;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::MagCore_Inspection_Character(Mat dImg, Mat &dCharImg, NIPLParam_MagCore *pParam, NIPLResult_Defect_MagCore *pResult, bool &bBrokenChar)
{
	Mat dCharTemplateImg = pParam->m_dCharTemplateImage;
	bool bCharBlack = pParam->m_bCharBlack;
	float nCharBinarizeThreshold = pParam->m_nCharBinarizeThreshold;
	int nCharConnectDistance = pParam->m_nCharConnectDistance;
	int nCharAreaSize = pParam->m_nCharAreaSize;
	float nCharLotNumberAreaRatio = pParam->m_nCharLotNumberAreaRatio;
	int nCharMinRadiusEdgeMargin = pParam->m_nCharMinRadiusEdgeMargin;
	int nCharMaxRadiusEdgeMargin = pParam->m_nCharMaxRadiusEdgeMargin;
	float nCharFillRatioY = pParam->m_nCharFillRatioY;
	wstring strShowImage = pParam->m_strShowImage;

	int nImgSizeX = dImg.cols;
	int nImgSizeY = dImg.rows;

	int nCharImgSizeX = dCharTemplateImg.cols;
	int nCharImgSizeY = dCharTemplateImg.rows;

	if (CHECK_EMPTY_IMAGE(dCharTemplateImg) && CHECK_STRING(strShowImage, STR_SHOW_IMAGE_OUTPUT)) {
		return NIPL_ERR_INVALID_TEMPLATE;
	}

	// Binarize
	NIPLInput dInput;
	NIPLOutput dOutput;

	NIPLParam_Binarize dParam_Binarize_Char;
	if (bCharBlack) dParam_Binarize_Char.m_nMethod = NIPLParam_Binarize::METHOD_LOWER;
	else dParam_Binarize_Char.m_nMethod = NIPLParam_Binarize::METHOD_UPPER;
	dParam_Binarize_Char.m_nThreshold = nCharBinarizeThreshold;

	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_Binarize_Char;
	dTimeProfile_MagCore.SetStartTime();
	NIPL_ERR nErr = Binarize(&dInput, &dOutput);
	dTimeProfile_MagCore.SetTime(dTimeProfile_MagCore.m_nInspection_CharCheck_Binarize);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		delete pResult;
		return nErr;
	}
	Mat dCharBinImg = dOutput.m_dImg;

	// Remove Edge Margin Area
	Mat dEdge(dCharBinImg, Rect(0, 0, nImgSizeX, nCharMinRadiusEdgeMargin));
	dEdge = 0;
	dEdge = Mat(dCharBinImg, Rect(0, nImgSizeY - nCharMaxRadiusEdgeMargin, nImgSizeX, nCharMaxRadiusEdgeMargin));
	dEdge = 0;

	int nFilterSize = pParam->m_nCharConnectDistance;
	if (nFilterSize % 2 == 0) nFilterSize++;	// make filter size of odd number
	morphologyEx(dCharBinImg, dCharBinImg, MORPH_DILATE, Mat::ones(nFilterSize, nFilterSize, dCharBinImg.type()));

	// extend image row size because there maybe exists cutting character on both sides
	Mat dExtCharBinImg;
	dExtCharBinImg.create(nImgSizeY, nImgSizeX * 2, dCharBinImg.type());
	Mat dLeftPart(dExtCharBinImg, Rect(0, 0, nImgSizeX, nImgSizeY));
	dCharBinImg.copyTo(dLeftPart);
	Mat dRightPart(dExtCharBinImg, Rect(nImgSizeX, 0, nImgSizeX, nImgSizeY));
	dCharBinImg.copyTo(dRightPart);

	// connect near pixels horizontally line by line
	Mat dExtCharRegionBinImg;
	dExtCharBinImg.copyTo(dExtCharRegionBinImg);
	Mat dOutputLine;
	nFilterSize = 9;
	for (int i = 0; i < nImgSizeY; i++) {
		Mat dLine(dExtCharRegionBinImg, Rect(0, i, nImgSizeX * 2, 1));
		morphologyEx(dLine, dOutputLine, MORPH_CLOSE, Mat::ones(1, nFilterSize, dLine.type()));
		dOutputLine.copyTo(dLine);
	}

	// FindBlob
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_FindBlob dParam_FindBlob;
	dParam_FindBlob.m_bFindInRange = true;
	dParam_FindBlob.m_nMinSize = (float)nCharAreaSize;

	dInput.m_dImg = dExtCharRegionBinImg;
	dInput.m_pParam = &dParam_FindBlob;
	nErr = FindBlob(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	if (dOutput.m_pResult == nullptr) {
		// No Characters
		bBrokenChar = true;
		dCharImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

		Rect dBoundRect(0, 0, nCharImgSizeX, nCharImgSizeY);
		NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_BROKEN_CHARACTER, dBoundRect, 0);
		pResult->m_listDefect.push_back(dDefect);
		return NIPL_ERR_SUCCESS;
	}

	NIPLResult_FindBlob *pResult_FindBlob = (NIPLResult_FindBlob *)dOutput.m_pResult;
	Rect dCharBoundRect;
	int nMaxSize = 0;
	for (auto dBlob : pResult_FindBlob->m_listBlob) {
		if (dBlob.m_nSize > nMaxSize) {
			dCharBoundRect = dBlob.m_rcBoundingBox;
			nMaxSize = dBlob.m_nSize;
		}
	}
	delete pResult_FindBlob;
	
	// Adjust Bounding Box size
	MagCore_Inspection_Character_AdjustBoundingBox(dExtCharRegionBinImg, dCharBoundRect, pParam);

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_BIN_CHAR)) {
		dCharImg = dExtCharRegionBinImg;

		pResult->Clear();

		bBrokenChar = true;
		NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_BROKEN_CHARACTER, dCharBoundRect, 0);
		pResult->m_listDefect.push_back(dDefect);

		return NIPL_ERR_SUCCESS;
	}

	DebugPrint(L"Maximum Characters ROI Area Size : %d > %d", nMaxSize, nCharAreaSize);

	Mat dExtImg;
	dExtImg.create(nImgSizeY, nImgSizeX * 2, dImg.type());
	dLeftPart = Mat(dExtImg, Rect(0, 0, nImgSizeX, nImgSizeY));
	dImg.copyTo(dLeftPart);
	dRightPart = Mat(dExtImg, Rect(nImgSizeX, 0, nImgSizeX, nImgSizeY));
	dImg.copyTo(dRightPart);

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_EXT_CHAR)) {
		dCharImg = dExtImg;

		pResult->Clear();

		bBrokenChar = true;
		NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_BROKEN_CHARACTER, dCharBoundRect, 0);
		pResult->m_listDefect.push_back(dDefect);

		return NIPL_ERR_SUCCESS;
	}

	if (CHECK_EMPTY_IMAGE(dCharTemplateImg)) {
		return NIPL_ERR_INVALID_TEMPLATE;
	}

	// Reset Y of Char BoundRect to check whole Y direction
//	dCharBoundRect.y = 0;
//	dCharBoundRect.height = nImgSizeY;

/*
	Mat dEqExtImg;
	Mat dEqCharTemplateImg;
	equalizeHist(dExtImg, dEqExtImg);
	equalizeHist(dCharTemplateImg, dEqCharTemplateImg);
*/
	Mat dExtROIImg;
	Mat(dExtImg, dCharBoundRect).copyTo(dExtROIImg);
	float nSizeRatio = ((float)dCharBoundRect.width / nCharImgSizeX);
	if (nSizeRatio < 0.5f || nSizeRatio > 2.f) {
		// No Characters
		bBrokenChar = true;
		dCharImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

		Rect dBoundRect(0, 0, nCharImgSizeX, nCharImgSizeY);
		NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_BROKEN_CHARACTER, dBoundRect, 0);
		pResult->m_listDefect.push_back(dDefect);
		return NIPL_ERR_SUCCESS;
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_BIN_CHAR_REGION)) {
		dCharImg = dExtROIImg;
		return NIPL_ERR_SUCCESS;
	}

	nCharImgSizeX = cvRound(nCharImgSizeX * nSizeRatio);
	nCharImgSizeY = cvRound(nCharImgSizeY * nSizeRatio);
	Size dResize(nCharImgSizeX, nCharImgSizeY);
	Mat dCharTemplateROIImg;
	resize(dCharTemplateImg, dCharTemplateROIImg, dResize);

	Mat dMatchedCharTemplateROIImg;
	Point ptPos;
	float nMatchRatio1;
	bool bFoundChar = MagCore_Inspection_Character_Compare(dExtROIImg, dCharTemplateROIImg, pParam, ptPos, bBrokenChar, nMatchRatio1, 0.f);
	dCharTemplateROIImg.copyTo(dMatchedCharTemplateROIImg);

	Point ptPos2;
	float nMatchRatio2;
	bool bBrokenChar2 = false;
	Mat dAppliedCharTemplateImg2;
	bool bFoundChar2 = MagCore_Inspection_Character_Compare(dExtROIImg, dCharTemplateROIImg, pParam, ptPos2, bBrokenChar2, nMatchRatio2, 180.f);
	if (nMatchRatio1 < nMatchRatio2) {
		bFoundChar = bFoundChar2;
		bBrokenChar = bBrokenChar2;
		ptPos = ptPos2;
		dCharTemplateROIImg.copyTo(dMatchedCharTemplateROIImg);
	}

	if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MATCHED_CHAR_TEMPLATE)) {
		dCharImg = dMatchedCharTemplateROIImg;
		return NIPL_ERR_SUCCESS;
	}

	bool bLowerCharPos = false;
	if (ptPos.y > dCharBoundRect.height * 0.3f) {
		bLowerCharPos = true;
	}

	ptPos.x += dCharBoundRect.x;
	ptPos.y += dCharBoundRect.y;

	dCharImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

	// adjust char image size Y to cover lot number
	int nCharImgExtSizeY = (int)(nCharImgSizeY * 1.20f);
	int nCharImgReduceSizeY = (int)(nCharImgSizeY * 0.8f);

	if (bFoundChar) {
		// Copy Matched Char
		Rect dMatchedCharBoundRect(ptPos.x, ptPos.y, nCharImgSizeX, nCharImgSizeY);
		if (dMatchedCharBoundRect.x < 0) {
			dMatchedCharBoundRect.width += dMatchedCharBoundRect.x;
			dMatchedCharBoundRect.x = 0;
		}
		if (dMatchedCharBoundRect.x + dMatchedCharBoundRect.width >= nImgSizeX * 2) dMatchedCharBoundRect.width = nImgSizeX * 2 - dMatchedCharBoundRect.x - 1;
		if (dMatchedCharBoundRect.y < 0) {
			dMatchedCharBoundRect.height += dMatchedCharBoundRect.y;
			dMatchedCharBoundRect.y = 0;
		}
		if (dMatchedCharBoundRect.y + dMatchedCharBoundRect.height >= nImgSizeY) dMatchedCharBoundRect.height = nImgSizeY - dMatchedCharBoundRect.y - 1;

		Mat dMatchedCharImg;
		Mat(dExtImg, dMatchedCharBoundRect).copyTo(dMatchedCharImg);

		if (bBrokenChar) {
			DebugPrint("Broken Character, Test again by rescaling Y size");

			// Check Again by changing character Y scale.
			dInput.Clear();
			dOutput.Clear();

			NIPLParam_Binarize dParam_Binarize_MatchedChar;
			if (bCharBlack) dParam_Binarize_MatchedChar.m_nMethod = NIPLParam_Binarize::METHOD_LOWER;
			else dParam_Binarize_MatchedChar.m_nMethod = NIPLParam_Binarize::METHOD_UPPER;
			dParam_Binarize_MatchedChar.m_nThreshold = 1.2f;

			dInput.m_dImg = dMatchedCharImg;
			dInput.m_pParam = &dParam_Binarize_MatchedChar;
			nErr = Binarize(&dInput, &dOutput);
			if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
				delete pResult;
				return nErr;
			}
			Mat dMatchedCharBinImg = dOutput.m_dImg;

			nFilterSize = 3;
			morphologyEx(dMatchedCharBinImg, dMatchedCharBinImg, MORPH_CLOSE, Mat::ones(nFilterSize, nFilterSize, dMatchedCharBinImg.type()));

			float nPixelRatio;
			int nStartY = 0;
			for (int i = nStartY; i < dMatchedCharBoundRect.height; i++) {
				Mat dLine(dMatchedCharBinImg, Rect(0, i, dMatchedCharBoundRect.width, 1));
				nPixelRatio = ((float)countNonZero(dLine)) / dMatchedCharBoundRect.width;
//				DebugPrint("StartY %d, Pixel Ratio : %.2f", i, nPixelRatio);
				if (nPixelRatio >= nCharFillRatioY) {
					nStartY = (i == 0) ? i : i - 1;
					DebugPrint("  ==> Adjust StartY : %d, Ratio : %.2f > %.2f", nStartY, nPixelRatio, nCharFillRatioY);
					break;
				}
			}
			int nEndY = nStartY + 1;
			for (int i = nEndY; i < dMatchedCharBoundRect.height; i++) {
				Mat dLine(dMatchedCharBinImg, Rect(0, i, dMatchedCharBoundRect.width, 1));
				nPixelRatio = ((float)countNonZero(dLine)) / dMatchedCharBoundRect.width;
//				DebugPrint("EndY %d, Pixel Ratio : %.2f", i, nPixelRatio);
				if (nPixelRatio < nCharFillRatioY) {
					nEndY = i;
					DebugPrint("  ==> Adjust EndY : %d, Ratio : %.2f < %.2f", nEndY, nPixelRatio, nCharFillRatioY);
					break;
				}
			}

			int nHeight = nEndY - nStartY + 1;
			if (nHeight > 0) {
				Size dResize(dMatchedCharBoundRect.width, nHeight);
				resize(dMatchedCharTemplateROIImg, dMatchedCharTemplateROIImg, dResize);

				Mat dRotateCharTemplateROIImg;
				float nRotateAngleRange = 2.f;
				float nRotateAngleGap = 0.5f;
				for (float nRotateAngle = -nRotateAngleRange; nRotateAngle <= (nRotateAngleRange + nRotateAngleGap * 0.1f); nRotateAngle += nRotateAngleGap) {
					dRotateCharTemplateROIImg = dMatchedCharTemplateROIImg;
					bFoundChar2 = MagCore_Inspection_Character_Compare(dExtROIImg, dRotateCharTemplateROIImg, pParam, ptPos2, bBrokenChar2, nMatchRatio2, nRotateAngle);
					if (nMatchRatio1 < nMatchRatio2) {
						bFoundChar = bFoundChar2;
						bBrokenChar = bBrokenChar2;
						ptPos = ptPos2;

						bLowerCharPos = false;
						if (ptPos.y > dCharBoundRect.height * 0.3f) {
							bLowerCharPos = true;
						}

						ptPos.x += dCharBoundRect.x;
						ptPos.y += dCharBoundRect.y;
					}

					if (!bBrokenChar) {
						break;
					}
				}
			}

			if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MATCHED_CHAR_BIN)) {
				dCharImg = dMatchedCharBinImg;
				return NIPL_ERR_SUCCESS;
			}

			dMatchedCharBoundRect = Rect(ptPos.x, ptPos.y, nCharImgSizeX, nCharImgSizeY);
			if (dMatchedCharBoundRect.x < 0) {
				dMatchedCharBoundRect.width += dMatchedCharBoundRect.x;
				dMatchedCharBoundRect.x = 0;
			}
			if (dMatchedCharBoundRect.x + dMatchedCharBoundRect.width >= nImgSizeX * 2) dMatchedCharBoundRect.width = nImgSizeX * 2 - dMatchedCharBoundRect.x - 1;
			if (dMatchedCharBoundRect.y < 0) {
				dMatchedCharBoundRect.height += dMatchedCharBoundRect.y;
				dMatchedCharBoundRect.y = 0;
			}
			if (dMatchedCharBoundRect.y + dMatchedCharBoundRect.height >= nImgSizeY) dMatchedCharBoundRect.height = nImgSizeY - dMatchedCharBoundRect.y - 1;

			Mat(dExtImg, dMatchedCharBoundRect).copyTo(dMatchedCharImg);
		}

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_MATCHED_CHAR)) {
			dCharImg = dMatchedCharImg;
			return NIPL_ERR_SUCCESS;
		}

		if (!bBrokenChar) {
			Rect dLotNumBoundRect;
			if (bLowerCharPos) {
				DebugPrint(L"Check LotNumber in Upper Part");
				dLotNumBoundRect = Rect(ptPos.x, ptPos.y - nCharImgReduceSizeY, nCharImgSizeX, nCharImgReduceSizeY);
			}
			else {
				DebugPrint(L"Check LotNumber in Lower Part");
				dLotNumBoundRect = Rect(ptPos.x, ptPos.y + nCharImgSizeY, nCharImgSizeX, nCharImgReduceSizeY);
			}

			if (dLotNumBoundRect.x < 0) {
				dLotNumBoundRect.width += dLotNumBoundRect.x;
				dLotNumBoundRect.x = 0;
			}
			if (dLotNumBoundRect.x + dLotNumBoundRect.width >= nImgSizeX * 2) dLotNumBoundRect.width = nImgSizeX * 2 - dLotNumBoundRect.x - 1;
			if (dLotNumBoundRect.y < 0) {
				dLotNumBoundRect.height += dLotNumBoundRect.y;
				dLotNumBoundRect.y = 0;
			}
			if (dLotNumBoundRect.y + dLotNumBoundRect.height >= nImgSizeY) dLotNumBoundRect.height = nImgSizeY - dLotNumBoundRect.y - 1;

			if (dLotNumBoundRect.height <= 0) {
				DebugPrint(L"Check Characters Area, No Space for LotNumber");
				DebugPrint("  ==> Broken");
				bBrokenChar = true;
			}
			else {
				Mat dLotNumCharImg(dExtCharBinImg, dLotNumBoundRect);

				int nTotalAreaSize = dLotNumBoundRect.width * dLotNumBoundRect.height;
				int nLotNumAreaSize = countNonZero(dLotNumCharImg);
				float nAreaRatio = ((float)nLotNumAreaSize) / nTotalAreaSize;
				DebugPrint(L"Check Characters Area, LotNumber : %d, Total : %d, AreaRatio : %.2f (< %.2f)", nLotNumAreaSize, nTotalAreaSize, nAreaRatio, nCharLotNumberAreaRatio);
				if (nAreaRatio < nCharLotNumberAreaRatio) {
					bBrokenChar = true;
					DebugPrint("  ==> Broken");
				}
				else {
					DebugPrint("  ==> Not Broken");
				}

				if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_CHAR_LOT_NUMBER)) {
					dCharImg = dLotNumCharImg;
					return NIPL_ERR_SUCCESS;
				}
			}
		}

		// Make Char Image
		int nMarginX = 9;
		int nMarginY = 9;
		Rect dBoundRect;
		if (bLowerCharPos) {
			dBoundRect = Rect(ptPos.x - nMarginX, ptPos.y - nMarginY - nCharImgExtSizeY, nCharImgSizeX + 2 * nMarginX, nCharImgSizeY + nCharImgExtSizeY + 2 * nMarginY);
		}
		else {
			dBoundRect = Rect(ptPos.x - nMarginX, ptPos.y - nMarginY, nCharImgSizeX + 2 * nMarginX, nCharImgSizeY + nCharImgExtSizeY + 2 * nMarginY);
		}

		if (dBoundRect.x < 0) {
			dBoundRect.width += dBoundRect.x;
			dBoundRect.x = 0;
		}
		if (dBoundRect.x + dBoundRect.width >= nImgSizeX * 2) dBoundRect.width = nImgSizeX * 2 - dBoundRect.x - 1;
		if (dBoundRect.y < 0) {
			dBoundRect.height += dBoundRect.y;
			dBoundRect.y = 0;
		}
		if (dBoundRect.y + dBoundRect.height >= nImgSizeY) dBoundRect.height = nImgSizeY - dBoundRect.y - 1;

		Mat dFullCharImg(dExtCharBinImg, dBoundRect);

		int nStartX = dBoundRect.x;
		int nStartY = dBoundRect.y;
		int nEndX = dBoundRect.x + dBoundRect.width;
		int nEndY = dBoundRect.y + dBoundRect.height;
		int nWidth = dBoundRect.width;
		int nHeight = dBoundRect.height;

		if (nEndX < nImgSizeX) {
			Mat dSubImg(dCharImg, dBoundRect);
			dFullCharImg.copyTo(dSubImg);
		}
		else if (nStartX >= nImgSizeX) {
			Mat dSubImg(dCharImg, Rect(nStartX - nImgSizeX, nStartY, nWidth, nHeight));
			dFullCharImg.copyTo(dSubImg);
		}
		else {
			int nRightPartWidth = nEndX - nImgSizeX;
			int nLeftPartWidth = nWidth - nRightPartWidth;

			Mat dLeftCharImg(dFullCharImg, Rect(0, 0, nLeftPartWidth, nHeight));
			Mat dLeftSubImg(dCharImg, Rect(nStartX, nStartY, nLeftPartWidth, nHeight));
			dLeftCharImg.copyTo(dLeftSubImg);

			Mat dRightCharImg(dFullCharImg, Rect(nLeftPartWidth, 0, nRightPartWidth, nHeight));
			Mat dRightSubImg(dCharImg, Rect(0, nStartY, nRightPartWidth, nHeight));
			dRightCharImg.copyTo(dRightSubImg);
		}

		if (bBrokenChar) {
			int nAreaSize = countNonZero(dCharImg);
			NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_BROKEN_CHARACTER, dBoundRect, nAreaSize);
			pResult->m_listDefect.push_back(dDefect);
		}

		if (CHECK_STRING(strShowImage, STR_SHOW_IMAGE_RECT_BROKEN_CHAR)) {
			Mat dCharBrokenImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
			if (bBrokenChar) {
				dCharImg.copyTo(dCharBrokenImg);
			}

			dCharImg = dCharBrokenImg;
			return NIPL_ERR_SUCCESS;
		}
	}
	else {
		// No Characters
		Rect dBoundRect(0, 0, nCharImgSizeX, nCharImgSizeY);
		NIPLDefect_MagCore dDefect(NIPLDefect_MagCore::DEFECT_TYPE_BROKEN_CHARACTER, dBoundRect, 0);
		pResult->m_listDefect.push_back(dDefect);
	}

	return NIPL_ERR_SUCCESS;
}

void NIPLCustom::MagCore_Inspection_Character_AdjustBoundingBox(Mat dImg, Rect &dBoundRect, NIPLParam_MagCore *pParam)
{
	float nCharFillRatioX = pParam->m_nCharFillRatioX;
	float nCharFillRatioY = pParam->m_nCharFillRatioY;

	// left/right
	Mat dLine;
	float nPixelRatio;
	for (int j = dBoundRect.x; j <= dBoundRect.x + dBoundRect.width - 1; j++) {
		dLine = Mat(dImg, Rect(j, dBoundRect.y, 1, dBoundRect.height));
		nPixelRatio = ((float)countNonZero(dLine)) / dBoundRect.height;
		if (nPixelRatio >= nCharFillRatioX) {
			dBoundRect.width -= (j - dBoundRect.x);
			dBoundRect.x = j;
			DebugPrint("  ==> Adjust StartX : %d, Ratio : %.2f > %.2f", j, nPixelRatio, nCharFillRatioX);
			break;
		}
	}
	for (int j = dBoundRect.x + dBoundRect.width - 1; j >= dBoundRect.x; j--) {
		dLine = Mat(dImg, Rect(j, dBoundRect.y, 1, dBoundRect.height));
		nPixelRatio = ((float)countNonZero(dLine)) / dBoundRect.height;
		if (nPixelRatio >= nCharFillRatioX) {
			dBoundRect.width = (j - dBoundRect.x);
			DebugPrint("  ==> Adjust EndX : %d, Ratio : %.2f < %.2f", j, nPixelRatio, nCharFillRatioX);
			break;
		}
	}

	// top/bottom
	for (int i = dBoundRect.y; i <= dBoundRect.y + dBoundRect.height - 1; i++) {
		dLine = Mat(dImg, Rect(dBoundRect.x, i, dBoundRect.width, 1));
		nPixelRatio = ((float)countNonZero(dLine)) / dBoundRect.width;

		if (nPixelRatio >= nCharFillRatioY) {
			dBoundRect.height -= (i - dBoundRect.y);
			dBoundRect.y = i;
			DebugPrint("  ==> Adjust StartY : %d, Ratio : %.2f > %.2f", i, nPixelRatio, nCharFillRatioY);
			break;
		}
	}

	for (int i = dBoundRect.y + dBoundRect.height - 1; i >= dBoundRect.y; i--) {
		dLine = Mat(dImg, Rect(dBoundRect.x, i, dBoundRect.width, 1));
		nPixelRatio = ((float)countNonZero(dLine)) / dBoundRect.width;

		if (nPixelRatio >= nCharFillRatioY) {
			dBoundRect.height = (i - dBoundRect.y);
			DebugPrint("  ==> Adjust EndY : %d, Ratio : %.2f > %.2f", i, nPixelRatio, nCharFillRatioY);
			break;
		}
	}
}

bool NIPLCustom::MagCore_Inspection_Character_Compare(Mat dImg, Mat &dCharTemplateImg, NIPLParam_MagCore *pParam, Point &ptPos, bool &bBrokenChar, float &nMatchRatio, float nRotateAngle)
{
	float nCharRecogRatio = pParam->m_nCharRecogRatio;
	float nCharMatchRatio = pParam->m_nCharMatchRatio;

	int nImgSizeX = dImg.cols;
	int nImgSizeY = dImg.rows;

	int nCharImgSizeX = dCharTemplateImg.cols;
	int nCharImgSizeY = dCharTemplateImg.rows;
	bool bCharBlack = pParam->m_bCharBlack;

	if (nCharImgSizeX == 0 || nCharImgSizeX == 0) {
		// No Characters
		DebugPrint("  ==> No Character Template loaded");
		bBrokenChar = true;
		ptPos = Point(0, 0);

		return false;
	}

	if (nImgSizeX < nCharImgSizeX || nImgSizeY < nCharImgSizeY) {
		// No Characters
		DebugPrint("  ==> No Characters by not enough size");
		bBrokenChar = true;
		ptPos = Point(0, 0);

		return false;
	}

	if (abs(nRotateAngle) > 0.01f) {		// assume 0 if it's under 0.01
		// check rotate image one more
		NIPLInput dInput;
		NIPLOutput dOutput;

		NIPLParam_Rotate dParam_Rotate;
		dParam_Rotate.m_nCenterPosX = (nCharImgSizeX - 1) * 0.5f;
		dParam_Rotate.m_nCenterPosY = (nCharImgSizeY - 1) * 0.5f;
		dParam_Rotate.m_nAngle = nRotateAngle;
		dParam_Rotate.m_nScale = 1.f;

		dInput.m_dImg = dCharTemplateImg;
		dInput.m_pParam = &dParam_Rotate;
		NIPL_ERR nErr = Rotate(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return false;
		}
		dCharTemplateImg = dOutput.m_dImg;
	}

/*
	int nMeanImg = (int)mean(dImg)[0];
	dImg += (128 - nMeanImg);
	int nMeanCharTemplateImg = (int)mean(dCharTemplateImg)[0];
	dCharTemplateImg += (128 - nMeanCharTemplateImg);
*/
/*
	Mat dResultImg1 = Mat::zeros(nImgSizeY, nImgSizeX, CV_32FC1);
	matchTemplate(dImg, dCharTemplateImg, dResultImg1, CV_TM_CCOEFF_NORMED);
	Mat dResultImg2 = Mat::zeros(nImgSizeY, nImgSizeX, CV_32FC1);
	matchTemplate(dImg, dCharTemplateImg, dResultImg2, CV_TM_CCORR_NORMED);
	Mat dResultImg3 = Mat::zeros(nImgSizeY, nImgSizeX, CV_32FC1);
	matchTemplate(dImg, dCharTemplateImg, dResultImg3, CV_TM_SQDIFF_NORMED);

	double nMin1;
	double nMax1;
	Point ptMin1;
	Point ptMax1;
	minMaxLoc(dResultImg1, &nMin1, &nMax1, &ptMin1, &ptMax1);

	double nMin2;
	double nMax2;
	Point ptMin2;
	Point ptMax2;
	minMaxLoc(dResultImg2, &nMin2, &nMax2, &ptMin2, &ptMax2);

	double nMin3;
	double nMax3;
	Point ptMin3;
	Point ptMax3;
	minMaxLoc(dResultImg3, &nMin3, &nMax3, &ptMin3, &ptMax3);

	double nMax;
	if (nMax1 > nMax2) {
		nMax = nMax1;
		ptPos = ptMax1;
	}
	else {
		nMax = nMax2;
		ptPos = ptMax2;
	}
	nMatchRatio = (float)nMax;
*/
	Mat dResultImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_32FC1);
	matchTemplate(dImg, dCharTemplateImg, dResultImg, CV_TM_CCOEFF_NORMED);

	double nMin;
	double nMax;
	Point ptMin;
	Point ptMax;
	minMaxLoc(dResultImg, &nMin, &nMax, &ptMin, &ptMax);

	nMatchRatio = (float)nMax;
	ptPos = ptMax;

	DebugPrint(L"Compare Characters, RotateAngle : %.1f, MatchRatio : %.2f, CharRecogRatio : %.2f, CharMatchRatio : %.2f, Pos : (%d, %d)", nRotateAngle, nMax, nCharRecogRatio, nCharMatchRatio, ptPos.x, ptPos.y);
	if (nMatchRatio >= nCharRecogRatio) {		// Found Location
		if (nMatchRatio >= nCharMatchRatio) {
			DebugPrint("  ==> Matched");
			bBrokenChar = false;
		}
		else {
			DebugPrint("  ==> Broken");
			bBrokenChar = true;
		}

		return true;
	}

	// No Characters
	DebugPrint("  ==> No Characters");
	bBrokenChar = true;
	ptPos = Point(0, 0);

	return false;
}

void NIPLCustom::MagCore_Inspection_Convert_Rect(Rect &rc, NIPLParam_Circle2Rect *pParam)
{
	int nLeft = rc.x;
	int nTop = rc.y;
	int nRight = rc.x + rc.width;
	int nBottom = rc.y + rc.height;

	Point pt1 = Point(nLeft, nTop);
	Point pt2 = Point(nLeft, nBottom);
	Point pt3 = Point(nRight, nTop);
	Point pt4 = Point(nRight, nBottom);

	MagCore_Inspection_Convert_Pos(pt1, pParam);
	MagCore_Inspection_Convert_Pos(pt2, pParam);
	MagCore_Inspection_Convert_Pos(pt3, pParam);
	MagCore_Inspection_Convert_Pos(pt4, pParam);

	nLeft = min(min(min(pt1.x, pt2.x), pt3.x), pt4.x);
	nTop = min(min(min(pt1.y, pt2.y), pt3.y), pt4.y);
	nRight = max(max(max(pt1.x, pt2.x), pt3.x), pt4.x);
	nBottom = max(max(max(pt1.y, pt2.y), pt3.y), pt4.y);

	rc.x = nLeft;
	rc.y = nTop;
	rc.width = nRight - nLeft;
	rc.height = nBottom - nTop;
}

void NIPLCustom::MagCore_Inspection_Convert_Pos(Point &pt, NIPLParam_Circle2Rect *Param)
{
	int nCenterPosX = Param->m_nCenterPosX;
	int nCenterPosY = Param->m_nCenterPosY;
	int nMinRadius = Param->m_nMinRadius;
	float nAngleStep = Param->m_nAngleStep;

	int nRectImgPosX = pt.x;
	int nRectImgPosY = pt.y;
	float nAngle = -nAngleStep * nRectImgPosX;
	int nRadius = nMinRadius + nRectImgPosY;

	int nImgPosX = cvRound(nCenterPosX + nRadius * cos(nAngle * CV_PI / 180));
	int nImgPosY = cvRound(nCenterPosY + nRadius * sin(nAngle * CV_PI / 180));

	pt.x = nImgPosX;
	pt.y = nImgPosY;
}

void NIPLCustom::MagCore_Inspection_ExtendImage(Mat dImg, Mat &dOutputImg, int nExtSize)
{
	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	Mat dExtImg(nImgSizeY, nImgSizeX + nExtSize, dImg.type());
	Mat dOrigPart(dExtImg, Rect(0, 0, nImgSizeX, nImgSizeY));
	dImg.copyTo(dOrigPart);
	Mat dExtPart(dExtImg, Rect(nImgSizeX, 0, nExtSize, nImgSizeY));
	Mat dExtPartImg(dImg, Rect(0, 0, nExtSize, nImgSizeY));
	dExtPartImg.copyTo(dExtPart);

	dOutputImg = dExtImg;
}
