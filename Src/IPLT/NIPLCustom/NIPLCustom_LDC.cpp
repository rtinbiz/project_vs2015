// NIPLCustom.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NIPLCustom.h"
#include "Xml/xml3.h"
using namespace XML3;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define SET_OUTPUT_IMAGE(dImg) \
	dInput.Clear(); \
	dOutput.Clear(); \
	NIPLParam_CopySubImageTo dParam_CopySubImageTo; \
	dParam_CopySubImageTo.m_nStartPosX = nStartPosX; \
	dParam_CopySubImageTo.m_nStartPosY = nStartPosY; \
	dParam_CopySubImageTo.m_dTargetImg = dOutputImg; \
	dInput.m_dImg = dImg; \
	dInput.m_pParam = &dParam_CopySubImageTo; \
	nErr = CopySubImageTo(&dInput, &dOutput); \
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) { \
		return nErr; \
	} \
	dOutputImg = dOutput.m_dImg;

float _CalcAngle(Point ptLeft, Point ptCenter, Point ptRight)
{
	Point pt1 = Point(ptLeft.x - ptCenter.x, ptLeft.y - ptCenter.y);
	Point pt2 = Point(ptRight.x - ptCenter.x, ptRight.y - ptCenter.y);;

	float nDenom = (sqrtf(powf((float)pt1.x, 2.f) + powf((float)pt1.y, 2.f)) * sqrtf(powf((float)pt2.x, 2.f) + powf((float)pt2.y, 2.f)));
	if (nDenom == 0.f) {
		return 0.f;
	}

	float nCos = (pt1.x * pt2.x + pt1.y * pt2.y) / nDenom;
	if (nCos < -1.f) nCos = -1.f;
	else if (nCos > 1.f) nCos = 1.f;
	float nAngle = acosf(nCos) * (180.f / (float)CV_PI);

	return nAngle;
}

bool NIPLCustom::LDC_LoadData_Calibration(NIPLParam_LDC_Calibration *pParam)
{
	wstring strDataPath = pParam->m_strDataPath;

	XML dXML;
	auto nResult = dXML.Load(strDataPath.c_str());
	if (nResult != XML_PARSE::OK) {
		return false;
	}

	auto dRoot = dXML.GetRootElement();
	auto listData = dRoot.GetChildren();
	for (auto pData : listData) {
		wstring strElementName = str2wstr(pData->GetElementName());
		if (CHECK_STRING(strElementName, L"GuideLine")) {
			wstring strPos = str2wstr(pData->v("pos"));
			if (CHECK_STRING(strPos, L"left")) {
				int nStartX = stoi(pData->v("startx"));
				int nStartY = stoi(pData->v("starty"));
				int nEndX = stoi(pData->v("endx"));
				int nEndY = stoi(pData->v("endy"));

				pParam->m_ptStartLeftGuidLine = Point(nStartX, nStartY);
				pParam->m_ptEndLeftGuidLine = Point(nEndX, nEndY);
			}
			else if (CHECK_STRING(strPos, L"right")) {
				int nEndX = stoi(pData->v("endx"));
				int nEndY = stoi(pData->v("endy"));
				int nStartX = nEndX - (pParam->m_ptEndLeftGuidLine.x - pParam->m_ptStartLeftGuidLine.x);
				int nStartY = nEndY - (pParam->m_ptEndLeftGuidLine.y - pParam->m_ptStartLeftGuidLine.y);

				pParam->m_ptStartRightGuidLine = Point(nStartX, nStartY);
				pParam->m_ptEndRightGuidLine = Point(nEndX, nEndY);
			}
		}
		else if (CHECK_STRING(strElementName, L"AutoDetect")) {
			pParam->m_nAutoDetectLeftRange = stoi(pData->v("left"));
			pParam->m_nAutoDetectRightRange = stoi(pData->v("right"));
		}
		else if (CHECK_STRING(strElementName, L"Normalize")) {
			pParam->m_nNormalizeWidth = stoi(pData->v("width"));
		}
	}

	return true;
}

bool NIPLCustom::LDC_LoadData_Terminal(NIPLParam_LDC *pParam)
{
	wstring strDataPath = pParam->m_strDataPath;

	XML dXML;
	auto nResult = dXML.Load(strDataPath.c_str());
	if (nResult != XML_PARSE::OK) {
		return false;
	}

	auto dRoot = dXML.GetRootElement();
	auto listData = dRoot.GetChildren();
	for (auto pData : listData) {
		wstring strElementName = str2wstr(pData->GetElementName());
		if (CHECK_STRING(strElementName, STR_LDC_COMP_TERMINAL)) {
			int nCenterX = stoi(pData->v("centerx"));
			int nCenterY = stoi(pData->v("centery"));
			int nRadius = stoi(pData->v("radius"));

			NIPL_LDC_ROI dROI;

			dROI.m_rcBoundingBox.x = nCenterX - nRadius;
			dROI.m_rcBoundingBox.y = nCenterY - nRadius;
			dROI.m_rcBoundingBox.width = 2 * nRadius + 1;
			dROI.m_rcBoundingBox.height = 2 * nRadius + 1;

			pParam->m_listROI.push_back(dROI);
		}
	}

	return true;
}

bool NIPLCustom::LDC_LoadData_Tube(NIPLParam_LDC *pParam)
{
	wstring strDataPath = pParam->m_strDataPath;

	XML dXML;
	auto nResult = dXML.Load(strDataPath.c_str());
	if (nResult != XML_PARSE::OK) {
		return false;
	}

	auto dRoot = dXML.GetRootElement();
	auto listData = dRoot.GetChildren();
	for (auto pData : listData) {
		wstring strElementName = str2wstr(pData->GetElementName());
		if (CHECK_STRING(strElementName, STR_LDC_COMP_TUBE)) {
			wstring strColor = str2wstr(pData->v("color"));
			int nStartX = stoi(pData->v("startx"));
			int nStartY = stoi(pData->v("starty"));
			int nEndX = stoi(pData->v("endx"));
			int nEndY = stoi(pData->v("endy"));
			int nRefX = 0;
			try { nRefX = stoi(pData->v("refx")); }	
			catch (...) {}
			int nRefY = 0;
			try { nRefY = stoi(pData->v("refy")); }	
			catch (...) {}
			int nSetId = 0;
			try { nSetId = stoi(pData->v("set")); } 
			catch (...) {}

			NIPL_LDC_ROI dROI;
			dROI.m_nSetId = nSetId;

			if (CHECK_STRING(strColor, L"black")) {
				dROI.m_nType = NIPL_LDC_ROI::COLOR_BLACK;
			}
			else if (CHECK_STRING(strColor, L"red")) {
				dROI.m_nType = NIPL_LDC_ROI::COLOR_RED;
			}

			dROI.m_rcBoundingBox.x = nStartX;
			dROI.m_rcBoundingBox.y = nStartY;
			dROI.m_rcBoundingBox.width = nEndX - nStartX;
			dROI.m_rcBoundingBox.height = nEndY - nStartY;

			dROI.m_ptRef.x = nRefX;
			dROI.m_ptRef.y = nRefY;

			pParam->m_listROI.push_back(dROI);
		}
	}

	return true;
}

bool NIPLCustom::LDC_LoadData(NIPLParam_LDC *pParam, wstring strComp)
{
	wstring strDataPath = pParam->m_strDataPath;

	XML dXML;
	auto nResult = dXML.Load(strDataPath.c_str());
	if (nResult != XML_PARSE::OK) {
		return false;
	}

	auto dRoot = dXML.GetRootElement();
	auto listData = dRoot.GetChildren();
	for (auto pData : listData) {
		wstring strElementName = str2wstr(pData->GetElementName());
		if (CHECK_STRING(strElementName, strComp)) {
			int nStartX = stoi(pData->v("startx"));
			int nStartY = stoi(pData->v("starty"));
			int nEndX = stoi(pData->v("endx"));
			int nEndY = stoi(pData->v("endy"));
			int nRefX = 0;
			try { nRefX = stoi(pData->v("refx")); }
			catch (...) {}
			int nRefY = 0;
			try { nRefY = stoi(pData->v("refy")); }
			catch (...) {}
			int nSetId = 0;
			try { nSetId = stoi(pData->v("set")); }
			catch (...) {}

			NIPL_LDC_ROI dROI;
			dROI.m_nSetId = nSetId;

			dROI.m_rcBoundingBox.x = nStartX;
			dROI.m_rcBoundingBox.y = nStartY;
			dROI.m_rcBoundingBox.width = nEndX - nStartX;
			dROI.m_rcBoundingBox.height = nEndY - nStartY;

			dROI.m_ptRef.x = nRefX;
			dROI.m_ptRef.y = nRefY;

			pParam->m_listROI.push_back(dROI);
		}
	}

	return true;
}


NIPL_CUSTOM_METHOD_IMPL(LDC_Calibration)
{
	VERIFY_PARAMS();

	NIPLParam_LDC_Calibration *pParam_LDC_Calibration = (NIPLParam_LDC_Calibration *)pInput->m_pParam;
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	wstring strDataPath = pParam_LDC_Calibration->m_strDataPath;
	if (!LDC_LoadData_Calibration(pParam_LDC_Calibration)) {
		return NIPL_ERR_FAIL_TO_OPEN_FILE;
	}

	int nLeftGuideLineStartX = pParam_LDC_Calibration->m_ptStartLeftGuidLine.x;
	int nLeftGuideLineStartY = pParam_LDC_Calibration->m_ptStartLeftGuidLine.y;
	int nLeftGuideLineEndX = pParam_LDC_Calibration->m_ptEndLeftGuidLine.x;
	int nLeftGuideLineEndY = pParam_LDC_Calibration->m_ptEndLeftGuidLine.y;
	int nRightGuideLineStartX = pParam_LDC_Calibration->m_ptStartRightGuidLine.x;
	int nRightGuideLineStartY = pParam_LDC_Calibration->m_ptStartRightGuidLine.y;
	int nRightGuideLineEndX = pParam_LDC_Calibration->m_ptEndRightGuidLine.x;
	int nRightGuideLineEndY = pParam_LDC_Calibration->m_ptEndRightGuidLine.y;
	int nAutoDetectLeftRange = pParam_LDC_Calibration->m_nAutoDetectLeftRange;
	int nAutoDetectRightRange = pParam_LDC_Calibration->m_nAutoDetectLeftRange;
	bool bAutoDetect = pParam_LDC_Calibration->m_bAutoDetect;
	int nNormalizeWidth = pParam_LDC_Calibration->m_nNormalizeWidth;

	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	float nCenterPosX = ((float)nRightGuideLineEndX + nLeftGuideLineStartX) * 0.5f;
	float nCenterPosY = ((float)nRightGuideLineEndY + nLeftGuideLineStartY) * 0.5f;

	int nImgCenterX = (int)(nImgSizeX * 0.5f);
	int nImgCenterY = (int)(nImgSizeY * 0.5f);

	float nOffsetX = nImgCenterX - nCenterPosX;
	float nOffsetY = nImgCenterY - nCenterPosY;

	Point ptLeft = Point(nRightGuideLineEndX, nRightGuideLineEndY);
	Point ptCenter = Point(nLeftGuideLineEndX, nLeftGuideLineEndY);
	Point ptRight = Point(nRightGuideLineEndX, nLeftGuideLineEndY);
	float nAngle = -_CalcAngle(ptLeft, ptCenter, ptRight);

	float nWidth = sqrtf(powf((float)nRightGuideLineStartX - nLeftGuideLineStartX, 2.f) + powf((float)nRightGuideLineStartY - nLeftGuideLineStartY, 2.f));
	float nScale = nNormalizeWidth / nWidth;

	NIPLParam_Move dParam_Move;
	dParam_Move.m_nOffsetX = nOffsetX;
	dParam_Move.m_nOffsetY = nOffsetY;

	NIPLInput dInput;
	NIPLOutput dOutput;
	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_Move;
	nErr = Move(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	Mat dMoveImg = dOutput.m_dImg;

	// Rotate Image
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_Rotate dParam_Rotate;
	dParam_Rotate.m_nCenterPosX = (float)nImgCenterX;
	dParam_Rotate.m_nCenterPosY = (float)nImgCenterY;
	dParam_Rotate.m_nAngle = nAngle;
	dParam_Rotate.m_nScale = 1;

	dInput.m_dImg = dMoveImg;
	dInput.m_pParam = &dParam_Rotate;
	nErr = Rotate(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	pOutput->m_dImg = dOutput.m_dImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_CUSTOM_METHOD_IMPL(LDC_Terminal)
{
	VERIFY_PARAMS();

	NIPLParam_LDC *pParam = (NIPLParam_LDC *)pInput->m_pParam;
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	wstring strDataPath = pParam->m_strDataPath;
	if (!LDC_LoadData_Terminal(pParam)) {
		return NIPL_ERR_FAIL_TO_OPEN_FILE;
	}
	if (pParam->m_listROI.size() == 0) {
		return NIPL_ERR_PASS;
	}

	bool bForceDetectAll = pParam->m_bForceDetectAll;

	NIPLInput dInput;
	NIPLOutput dOutput;
	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	NIPLResult_Defect_LDC *pResult = new NIPLResult_Defect_LDC();
	if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	for (auto dROI : pParam->m_listROI) {
		bool bDefect = false;
		bool bSkip = false;

		if (bForceDetectAll) {
			bDefect = true;

			NIPLDefect_LDC dDefect(NIPLDefect_LDC::DEFECT_TYPE_TERMINAL, dROI.m_rcBoundingBox, dROI.m_nSetId);
			pResult->m_listDefect.push_back(dDefect);
		}
		else {
			if (dROI.m_nSetId > 0 && pResult->FindNonDefectBySetId(dROI.m_nSetId)) {
				bSkip = true;
			}

			if (!bSkip) {
				float nThreshold = pParam->m_nThreshold;
				nErr = LDC_Terminal_CheckExist(dImg, dOutputImg, dROI.m_rcBoundingBox, nThreshold, bDefect);
				if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
					return nErr;
				}
			}
		}

		if (bDefect) {
			NIPLDefect_LDC dDefect(NIPLDefect_LDC::DEFECT_TYPE_TERMINAL, dROI.m_rcBoundingBox, dROI.m_nSetId);
			pResult->m_listDefect.push_back(dDefect);
		}
		else if (!bSkip) {
			if (dROI.m_nSetId > 0) {
				pResult->SetNonDefectBySetId(dROI.m_nSetId);
			}
		}
	}

	pOutput->m_dImg = dOutputImg;

	if (pResult->m_listDefect.size() > 0) {
		pOutput->m_pResult = pResult;
	}
	else {
		delete pResult;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::LDC_Terminal_CheckExist(Mat dImg, Mat &dOutputImg, Rect rcBoundingBox, float nThreshold, bool &bDefect)
{
	bDefect = false;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nRadius = (int)(min(rcBoundingBox.width, rcBoundingBox.height) * 0.5f);

	int nStartPosX = rcBoundingBox.x;
	int nStartPosY = rcBoundingBox.y;
	int nEndPosX = nStartPosX + rcBoundingBox.width - 1;
	int nEndPosY = nStartPosY + rcBoundingBox.height - 1;

	if (nStartPosX < 0) nStartPosX = 0;
	if (nStartPosY < 0) nStartPosY = 0;
	if (nEndPosX >= nImgSizeX) nEndPosX = nImgSizeX - 1;
	if (nEndPosY >= nImgSizeY) nEndPosY = nImgSizeY - 1;

	NIPLParam_CopySubImageFrom dParam_CopySubImageFrom;
	dParam_CopySubImageFrom.m_nStartPosX = nStartPosX;
	dParam_CopySubImageFrom.m_nStartPosY = nStartPosY;
	dParam_CopySubImageFrom.m_nEndPosX = nEndPosX;
	dParam_CopySubImageFrom.m_nEndPosY = nEndPosY;

	// Get SubImage
	NIPLInput dInput;
	NIPLOutput dOutput;
	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_CopySubImageFrom;
	NIPL_ERR nErr = CopySubImageFrom(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dSubImage = dOutput.m_dImg;

	int nSubImgSizeY = dSubImage.rows;
	int nSubImgSizeX = dSubImage.cols;

	// Color Thresholding
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_ColorThresholding dParam_ColorThresholding;

	dParam_ColorThresholding.m_nMethod_R = NIPLParam_ColorThresholding::METHOD_UPPER;
	dParam_ColorThresholding.m_nUpperValue_R = 0;
	dParam_ColorThresholding.m_nMethod_G = NIPLParam_ColorThresholding::METHOD_UPPER;
	dParam_ColorThresholding.m_nUpperValue_G = 0;
	dParam_ColorThresholding.m_nMethod_B = NIPLParam_ColorThresholding::METHOD_LOWER;
	dParam_ColorThresholding.m_nLowerValue_B = 200;


	dParam_ColorThresholding.m_nLowerRatio_R_G = 1.0f;
	dParam_ColorThresholding.m_nUpperRatio_R_G = 1.6f;
	dParam_ColorThresholding.m_nLowerRatio_R_B = 1.0f;
	dParam_ColorThresholding.m_nUpperRatio_R_B = 255.f;
	dParam_ColorThresholding.m_nLowerRatio_G_B = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_G_B = 255.f;

	dInput.m_dImg = dSubImage;
	dInput.m_pParam = &dParam_ColorThresholding;
	nErr = ColorThresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	Mat dBinImage1 = dOutput.m_dImg;

	Mat dTemp;
	LDC_Tube_NormalizeBrightness(dSubImage, dTemp);

	dParam_ColorThresholding.m_nLowerRatio_R_G = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_R_G = 1.1f;
	dParam_ColorThresholding.m_nLowerRatio_R_B = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_R_B = 1.1f;
	dParam_ColorThresholding.m_nLowerRatio_G_B = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_G_B = 1.1f;

	dInput.m_dImg = dTemp;
	dInput.m_pParam = &dParam_ColorThresholding;
	nErr = ColorThresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dBinImage2 = dOutput.m_dImg;

	Mat dBinImage = dBinImage1 & ~dBinImage2;

	Point ptCenter = Point((int)(nSubImgSizeX * 0.5f), (int)(nSubImgSizeY * 0.5f));
	Mat dCircleROIImage;
	LDC_Terminal_ExcludeOutOfRadiusRange(dBinImage, dCircleROIImage, ptCenter, nRadius);
	dBinImage = dCircleROIImage;

	int nCount = countNonZero(dBinImage);
	float nTotal = (float)CV_PI * (float)pow(nRadius, 2);
	float nRatio = nCount / nTotal;

	DebugPrint(L"Terminal Bar (%d, %d), Ratio : %.2f > %.2f", nStartPosX, nStartPosY, nRatio, nThreshold);

	if (nRatio > nThreshold) {
		DebugPrint(L"  => Defect");

		bDefect = true;
	}

	SET_OUTPUT_IMAGE(dBinImage);

	return NIPL_ERR_SUCCESS;
}

NIPL_CUSTOM_METHOD_IMPL(LDC_Tube)
{
	VERIFY_PARAMS();

	NIPLParam_LDC *pParam = (NIPLParam_LDC *)pInput->m_pParam;
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	wstring strDataPath = pParam->m_strDataPath;
	if(!LDC_LoadData_Tube(pParam)) {
		return NIPL_ERR_FAIL_TO_OPEN_FILE;
	}
	if (pParam->m_listROI.size() == 0) {
		return NIPL_ERR_PASS;
	}

	bool bForceDetectAll = pParam->m_bForceDetectAll;

	NIPLInput dInput;
	NIPLOutput dOutput;
	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	NIPLResult_Defect_LDC *pResult = new NIPLResult_Defect_LDC();
	if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	for (auto dROI : pParam->m_listROI) {
		bool bDefect = false;
		bool bSkip = false;

		if (bForceDetectAll) {
			bDefect = true;
		}
		else {
			if (dROI.m_nSetId > 0 && pResult->FindNonDefectBySetId(dROI.m_nSetId)) {
				bSkip = true;
			}

			if (!bSkip) {
				bool bRedColor = (dROI.m_nType == NIPL_LDC_ROI::COLOR_RED);

				float nThreshold = pParam->m_nThreshold;
				float nRefThreshold = pParam->m_nRefThreshold;
				nErr = LDC_Tube_CheckColor(dImg, dOutputImg, dROI.m_rcBoundingBox, dROI.m_ptRef, nThreshold, nRefThreshold, bRedColor, bDefect);
				if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
					return nErr;
				}
			}
		}

		if (bDefect) {
			int nType = (dROI.m_nType == NIPL_LDC_ROI::COLOR_RED) ? NIPLDefect_LDC::DEFECT_TYPE_TUBE_RED : NIPLDefect_LDC::DEFECT_TYPE_TUBE_BLACK;
			NIPLDefect_LDC dDefect(nType, dROI.m_rcBoundingBox, dROI.m_nSetId);
			pResult->m_listDefect.push_back(dDefect);
		}
		else if (!bSkip) {
			if (dROI.m_nSetId > 0) {
				pResult->SetNonDefectBySetId(dROI.m_nSetId);
			}
		}
	}

	pOutput->m_dImg = dOutputImg;
	if (pResult->m_listDefect.size() > 0) {
		pOutput->m_pResult = pResult;
	}
	else {
		delete pResult;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::LDC_Tube_CheckColor(Mat dImg, Mat &dOutputImg, Rect rcBoundingBox, Point ptRef, float nThreshold, float nRefThreshold, bool bRedColor, bool &bDefect)
{
	bDefect = false;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nStartPosX = rcBoundingBox.x;
	int nStartPosY = rcBoundingBox.y;
	int nEndPosX = nStartPosX + rcBoundingBox.width - 1;
	int nEndPosY = nStartPosY + rcBoundingBox.height - 1;

	if (nStartPosX < 0) nStartPosX = 0;
	if (nStartPosY < 0) nStartPosY = 0;
	if (nEndPosX >= nImgSizeX) nEndPosX = nImgSizeX - 1;
	if (nEndPosY >= nImgSizeY) nEndPosY = nImgSizeY - 1;

	NIPLParam_CopySubImageFrom dParam_CopySubImageFrom;
	dParam_CopySubImageFrom.m_nStartPosX = nStartPosX;
	dParam_CopySubImageFrom.m_nStartPosY = nStartPosY;
	dParam_CopySubImageFrom.m_nEndPosX = nEndPosX;
	dParam_CopySubImageFrom.m_nEndPosY = nEndPosY;

	// Get SubImage
	NIPLInput dInput;
	NIPLOutput dOutput;
	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_CopySubImageFrom;
	NIPL_ERR nErr = CopySubImageFrom(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dSubImage = dOutput.m_dImg;

	// Color Thresholding
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_ColorThresholding dParam_ColorThresholding;

	Mat dBinImage;
	if (bRedColor) {
		dParam_ColorThresholding.m_nMethod_R = NIPLParam_ColorThresholding::METHOD_UPPER;
		dParam_ColorThresholding.m_nUpperValue_R = 70;
		dParam_ColorThresholding.m_nMethod_G = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_G = 200;
		dParam_ColorThresholding.m_nMethod_B = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_B = 200;

		dParam_ColorThresholding.m_nLowerRatio_R_G = 1.5f;
		dParam_ColorThresholding.m_nLowerRatio_R_B = 1.5f;

		dInput.m_dImg = dSubImage;
		dInput.m_pParam = &dParam_ColorThresholding;
		nErr = ColorThresholding(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		dBinImage = dOutput.m_dImg;
	}
	else {
		float nMeanR = 75.f;
		float nMeanG = 75.f;
		float nMeanB = 75.f;

		if (ptRef.x > 0 && ptRef.y > 0) {
			int nMargin = 2;
			int nStartPosX = ptRef.x - nMargin;
			int nStartPosY = ptRef.y - nMargin;
			int nEndPosX = ptRef.x + nMargin;
			int nEndPosY = ptRef.y + nMargin;

			if (nStartPosX < 0) nStartPosX = 0;
			if (nEndPosX >= nImgSizeX) nEndPosX = nImgSizeX - 1;
			if (nStartPosY < 0) nStartPosY = 0;
			if (nEndPosY >= nImgSizeY) nEndPosY = nImgSizeY - 1;

			int nRefImgSizeX = nEndPosX - nStartPosX + 1;
			int nRefImgSizeY = nEndPosY - nStartPosY + 1;
			Mat dRefImg(dImg, Rect(nStartPosX, nStartPosY, nRefImgSizeX, nRefImgSizeY));

			Scalar dMean;
			Scalar dStd;
			meanStdDev(dRefImg, dMean, dStd);
			nMeanR = (float)dMean[2];
			nMeanG = (float)dMean[1];
			nMeanB = (float)dMean[0];
		}

		dParam_ColorThresholding.m_nMethod_R = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_R = nMeanR * nRefThreshold;
		dParam_ColorThresholding.m_nMethod_G = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_G = nMeanG * nRefThreshold;
		dParam_ColorThresholding.m_nMethod_B = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_B = nMeanB * nRefThreshold;

		dInput.m_dImg = dSubImage;
		dInput.m_pParam = &dParam_ColorThresholding;
		nErr = ColorThresholding(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}
		dBinImage = dOutput.m_dImg;
	}

	int nCount = countNonZero(dBinImage);
	float nRatio = ((float)nCount) / (dBinImage.rows * dBinImage.cols);

	if (nRatio < nThreshold) {
		bDefect = true;
	}

	DebugPrint(L"RedColor : %d, Ratio : %.3f, nCount : %d, nTotal : %d, bDefect : %d\n",
		bRedColor, nRatio, nCount, (dBinImage.rows * dBinImage.cols), bDefect);

	SET_OUTPUT_IMAGE(dBinImage);
	

	return NIPL_ERR_SUCCESS;
}

NIPL_CUSTOM_METHOD_IMPL(LDC_Guide)
{
	VERIFY_PARAMS();

	NIPLParam_LDC *pParam = (NIPLParam_LDC *)pInput->m_pParam;
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	wstring strDataPath = pParam->m_strDataPath;
	if (!LDC_LoadData(pParam, STR_LDC_COMP_GUIDE)) {
		return NIPL_ERR_FAIL_TO_OPEN_FILE;
	}
	if (pParam->m_listROI.size() == 0) {
		return NIPL_ERR_PASS;
	}

	bool bForceDetectAll = pParam->m_bForceDetectAll;

	NIPLInput dInput;
	NIPLOutput dOutput;
	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	NIPLResult_Defect_LDC *pResult = new NIPLResult_Defect_LDC();
	if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;
	
	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	for (auto dROI : pParam->m_listROI) {
		bool bDefect = false;
		bool bSkip = false;

		if (bForceDetectAll) {
			bDefect = true;
		}
		else {
			if (dROI.m_nSetId > 0 && pResult->FindNonDefectBySetId(dROI.m_nSetId)) {
				bSkip = true;
			}

			if (!bSkip) {
				float nThreshold = pParam->m_nThreshold;
				nErr = LDC_Guide_CheckColor(dImg, dOutputImg, dROI.m_rcBoundingBox, nThreshold, bDefect);
				if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
					return nErr;
				}
			}
		}

		if (bDefect) {
			NIPLDefect_LDC dDefect(NIPLDefect_LDC::DEFECT_TYPE_GUIDE, dROI.m_rcBoundingBox, dROI.m_nSetId);
			pResult->m_listDefect.push_back(dDefect);
		}
		else if (!bSkip) {
			if (dROI.m_nSetId > 0) {
				pResult->SetNonDefectBySetId(dROI.m_nSetId);
			}
		}
	}

	pOutput->m_dImg = dOutputImg;
	if (pResult->m_listDefect.size() > 0) {
		pOutput->m_pResult = pResult;
	}
	else {
		delete pResult;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::LDC_Guide_CheckColor(Mat dImg, Mat &dOutputImg, Rect rcBoundingBox, float nThreshold, bool &bDefect)
{
	bDefect = false;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nStartPosX = rcBoundingBox.x;
	int nStartPosY = rcBoundingBox.y;
	int nEndPosX = nStartPosX + rcBoundingBox.width - 1;
	int nEndPosY = nStartPosY + rcBoundingBox.height - 1;

	if (nStartPosX < 0) nStartPosX = 0;
	if (nStartPosY < 0) nStartPosY = 0;
	if (nEndPosX >= nImgSizeX) nEndPosX = nImgSizeX - 1;
	if (nEndPosY >= nImgSizeY) nEndPosY = nImgSizeY - 1;

	NIPLParam_CopySubImageFrom dParam_CopySubImageFrom;
	dParam_CopySubImageFrom.m_nStartPosX = nStartPosX;
	dParam_CopySubImageFrom.m_nStartPosY = nStartPosY;
	dParam_CopySubImageFrom.m_nEndPosX = nEndPosX;
	dParam_CopySubImageFrom.m_nEndPosY = nEndPosY;

	// Get SubImage
	NIPLInput dInput;
	NIPLOutput dOutput;
	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_CopySubImageFrom;
	NIPL_ERR nErr = CopySubImageFrom(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dSubImage = dOutput.m_dImg;

	// Color Thresholding
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_ColorThresholding dParam_ColorThresholding;

	dParam_ColorThresholding.m_nMethod_R = NIPLParam_ColorThresholding::METHOD_LOWER;
	dParam_ColorThresholding.m_nLowerValue_R = 230;
	dParam_ColorThresholding.m_nMethod_G = NIPLParam_ColorThresholding::METHOD_LOWER;
	dParam_ColorThresholding.m_nLowerValue_G = 230;
	dParam_ColorThresholding.m_nMethod_B = NIPLParam_ColorThresholding::METHOD_LOWER;
	dParam_ColorThresholding.m_nLowerValue_B = 230;

	/*
	dParam_ColorThresholding.m_nLowerRatio_R_G = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_R_G = 1.1f;
	dParam_ColorThresholding.m_nLowerRatio_R_B = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_R_B = 1.1f;
	dParam_ColorThresholding.m_nLowerRatio_G_B = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_G_B = 1.1f;
	*/

	dInput.m_dImg = dSubImage;
	dInput.m_pParam = &dParam_ColorThresholding;
	nErr = ColorThresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	Mat dBinImage = dOutput.m_dImg;

	// Eliminate Noise
/*
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_EliminateNoise dParam_EliminateNoise;
	dParam_EliminateNoise.m_nMethod = NIPLParam_EliminateNoise::METHOD_MORPHOLOGY_OPS;
	dParam_EliminateNoise.m_nMorphologyFilterSize = 3;

	dInput.m_dImg = dBinImage;
	dInput.m_pParam = &dParam_EliminateNoise;
	nErr = EliminateNoise(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dBinImage = dOutput.m_dImg;
*/

	int nCount = countNonZero(dBinImage);
	float nRatio = ((float)nCount) / (dBinImage.rows * dBinImage.cols);

	if (nRatio < nThreshold) {
		bDefect = true;
	}

	TRACE("\nRatio : %.3f, nCount : %d, nTotal : %d, bDefect : %d\n",
		nRatio, nCount, (dSubImage.rows * dSubImage.cols), bDefect);

	SET_OUTPUT_IMAGE(~dBinImage);

	return NIPL_ERR_SUCCESS;
}

NIPL_CUSTOM_METHOD_IMPL(LDC_Bolt)
{
	VERIFY_PARAMS();

	NIPLParam_LDC *pParam = (NIPLParam_LDC *)pInput->m_pParam;
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	wstring strDataPath = pParam->m_strDataPath;
	if (!LDC_LoadData(pParam, STR_LDC_COMP_BOLT)) {
		return NIPL_ERR_FAIL_TO_OPEN_FILE;
	}
	if (pParam->m_listROI.size() == 0) {
		return NIPL_ERR_PASS;
	}

	bool bForceDetectAll = pParam->m_bForceDetectAll;

	NIPLInput dInput;
	NIPLOutput dOutput;
	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	NIPLResult_Defect_LDC *pResult = new NIPLResult_Defect_LDC();
	if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	for (auto dROI : pParam->m_listROI) {
		bool bDefect = false;
		bool bSkip = false;

		if (bForceDetectAll) {
			bDefect = true;
		}
		else {
			if (dROI.m_nSetId > 0 && pResult->FindNonDefectBySetId(dROI.m_nSetId)) {
				bSkip = true;
			}

			if (!bSkip) {
				float nThreshold = pParam->m_nThreshold;
				nErr = LDC_Bolt_CheckColor(dImg, dOutputImg, dROI.m_rcBoundingBox, nThreshold, bDefect);
				if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
					return nErr;
				}
			}
		}

		if (bDefect) {
			NIPLDefect_LDC dDefect(NIPLDefect_LDC::DEFECT_TYPE_BOLT, dROI.m_rcBoundingBox, dROI.m_nSetId);
			pResult->m_listDefect.push_back(dDefect);
		}
		else if (!bSkip) {
			if (dROI.m_nSetId > 0) {
				pResult->SetNonDefectBySetId(dROI.m_nSetId);
			}
		}
	}

	pOutput->m_dImg = dOutputImg;
	if (pResult->m_listDefect.size() > 0) {
		pOutput->m_pResult = pResult;
	}
	else {
		delete pResult;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::LDC_Bolt_CheckColor(Mat dImg, Mat &dOutputImg, Rect rcBoundingBox, float nThreshold, bool &bDefect)
{
	bDefect = false;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nStartPosX = rcBoundingBox.x;
	int nStartPosY = rcBoundingBox.y;
	int nEndPosX = nStartPosX + rcBoundingBox.width - 1;
	int nEndPosY = nStartPosY + rcBoundingBox.height - 1;

	if (nStartPosX < 0) nStartPosX = 0;
	if (nStartPosY < 0) nStartPosY = 0;
	if (nEndPosX >= nImgSizeX) nEndPosX = nImgSizeX - 1;
	if (nEndPosY >= nImgSizeY) nEndPosY = nImgSizeY - 1;

	NIPLParam_CopySubImageFrom dParam_CopySubImageFrom;
	dParam_CopySubImageFrom.m_nStartPosX = nStartPosX;
	dParam_CopySubImageFrom.m_nStartPosY = nStartPosY;
	dParam_CopySubImageFrom.m_nEndPosX = nEndPosX;
	dParam_CopySubImageFrom.m_nEndPosY = nEndPosY;

	// Get SubImage
	NIPLInput dInput;
	NIPLOutput dOutput;
	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_CopySubImageFrom;
	NIPL_ERR nErr = CopySubImageFrom(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dSubImage = dOutput.m_dImg;

	// Color Thresholding
/*
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_ColorThresholding dParam_ColorThresholding;

	dParam_ColorThresholding.m_nMethod_R = NIPLParam_ColorThresholding::METHOD_UPPER;
	dParam_ColorThresholding.m_nUpperValue_R = 120;
	dParam_ColorThresholding.m_nMethod_G = NIPLParam_ColorThresholding::METHOD_UPPER;
	dParam_ColorThresholding.m_nUpperValue_G = 120;
	dParam_ColorThresholding.m_nMethod_B = NIPLParam_ColorThresholding::METHOD_UPPER;
	dParam_ColorThresholding.m_nUpperValue_B = 120;

	dInput.m_dImg = dSubImage;
	dInput.m_pParam = &dParam_ColorThresholding;
	nErr = ColorThresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	Mat dBinImage = dOutput.m_dImg;

	int nCount = countNonZero(dBinImage);
	float nRatio = ((float)nCount) / (dBinImage.rows * dBinImage.cols);
*/

	// Convert to gray
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_Color2Gray dParam_Color2Gray;

	dParam_Color2Gray.m_nGrayLevel = NIPLParam_Color2Gray::GRAYLEVEL_256;
	dInput.m_dImg = dSubImage;
	dInput.m_pParam = &dParam_Color2Gray;
	nErr = Color2Gray(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	Mat dGrayImage = dOutput.m_dImg;

	Scalar dMean;
	Scalar dStd;
	meanStdDev(dGrayImage, dMean, dStd);

//	double nMin, nMax;
//	minMaxLoc(dGrayImage, &nMin, &nMax);

//	float nMean = (float)dMean[0];
	float nStd = (float)dStd[0];

	TRACE("\n========> Std : %.3f", nStd);
	//	TRACE("\n========> Mean : %.3f, Std : %.3f, Min : %.3f, Max : %.3f\n", nMean, nStd, nMin, nMax);

	if (nStd < nThreshold) {
		bDefect = true;
	}

	SET_OUTPUT_IMAGE(dGrayImage);

	return NIPL_ERR_SUCCESS;
}

NIPL_CUSTOM_METHOD_IMPL(LDC_Paper)
{
	VERIFY_PARAMS();

	NIPLParam_LDC *pParam = (NIPLParam_LDC *)pInput->m_pParam;
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	wstring strDataPath = pParam->m_strDataPath;
	if (!LDC_LoadData(pParam, STR_LDC_COMP_PAPER)) {
		return NIPL_ERR_FAIL_TO_OPEN_FILE;
	}
	if (pParam->m_listROI.size() == 0) {
		return NIPL_ERR_PASS;
	}

	bool bForceDetectAll = pParam->m_bForceDetectAll;

	NIPLInput dInput;
	NIPLOutput dOutput;
	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	NIPLResult_Defect_LDC *pResult = new NIPLResult_Defect_LDC();
	if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	for (auto dROI : pParam->m_listROI) {
		bool bDefect = false;
		bool bSkip = false;

		if (bForceDetectAll) {
			bDefect = true;
		}
		else {
			if (dROI.m_nSetId > 0 && pResult->FindNonDefectBySetId(dROI.m_nSetId)) {
				bSkip = true;
			}

			if (!bSkip) {
				float nThreshold = pParam->m_nThreshold;
				float nRefThreshold = pParam->m_nRefThreshold;
				nErr = LDC_Paper_CheckColor(dImg, dOutputImg, dROI.m_rcBoundingBox, dROI.m_ptRef, nThreshold, nRefThreshold, bDefect);
				if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
					return nErr;
				}
			}
		}

		if (bDefect) {
			NIPLDefect_LDC dDefect(NIPLDefect_LDC::DEFECT_TYPE_PAPER, dROI.m_rcBoundingBox, dROI.m_nSetId);
			pResult->m_listDefect.push_back(dDefect);
		}
		else if (!bSkip) {
			if (dROI.m_nSetId > 0) {
				pResult->SetNonDefectBySetId(dROI.m_nSetId);
			}
		}
	}

	pOutput->m_dImg = dOutputImg;
	if (pResult->m_listDefect.size() > 0) {
		pOutput->m_pResult = pResult;
	}
	else {
		delete pResult;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::LDC_Paper_CheckColor(Mat dImg, Mat &dOutputImg, Rect rcBoundingBox, Point ptRef, float nThreshold, float nRefThreshold, bool &bDefect)
{
	bDefect = false;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nStartPosX = rcBoundingBox.x;
	int nStartPosY = rcBoundingBox.y;
	int nEndPosX = nStartPosX + rcBoundingBox.width - 1;
	int nEndPosY = nStartPosY + rcBoundingBox.height - 1;

	if (nStartPosX < 0) nStartPosX = 0;
	if (nStartPosY < 0) nStartPosY = 0;
	if (nEndPosX >= nImgSizeX) nEndPosX = nImgSizeX - 1;
	if (nEndPosY >= nImgSizeY) nEndPosY = nImgSizeY - 1;

	NIPLParam_CopySubImageFrom dParam_CopySubImageFrom;
	dParam_CopySubImageFrom.m_nStartPosX = nStartPosX;
	dParam_CopySubImageFrom.m_nStartPosY = nStartPosY;
	dParam_CopySubImageFrom.m_nEndPosX = nEndPosX;
	dParam_CopySubImageFrom.m_nEndPosY = nEndPosY;

	// Get SubImage
	NIPLInput dInput;
	NIPLOutput dOutput;
	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_CopySubImageFrom;
	NIPL_ERR nErr = CopySubImageFrom(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dSubImage = dOutput.m_dImg;

	int nSubImgSizeY = dSubImage.rows;
	int nSubImgSizeX = dSubImage.cols;

	bool bHorizLine = (nSubImgSizeX > nSubImgSizeY) ? true : false;
	int nLineLength = max(nSubImgSizeX, nSubImgSizeY);
	int nLineWidth = min(nSubImgSizeX, nSubImgSizeY);

	float nMean = 200.f;

	if (ptRef.x > 0 && ptRef.y > 0) {
		int nMargin = 2;
		int nStartPosX = ptRef.x - nMargin;
		int nStartPosY = ptRef.y - nMargin;
		int nEndPosX = ptRef.x + nMargin;
		int nEndPosY = ptRef.y + nMargin;

		if (nStartPosX < 0) nStartPosX = 0;
		if (nEndPosX >= nImgSizeX) nEndPosX = nImgSizeX - 1;
		if (nStartPosY < 0) nStartPosY = 0;
		if (nEndPosY >= nImgSizeY) nEndPosY = nImgSizeY - 1;

		int nRefImgSizeX = nEndPosX - nStartPosX + 1;
		int nRefImgSizeY = nEndPosY - nStartPosY + 1;
		Mat dRefImg(dImg, Rect(nStartPosX, nStartPosY, nRefImgSizeX, nRefImgSizeY));

		Scalar dMean;
		Scalar dStd;
		meanStdDev(dRefImg, dMean, dStd);
		nMean = (float)(dMean[0] + dMean[1] + dMean[2]) / 3.f;
	}

	DebugPrint("Paper, Start(%d, %d) End(%d, %d), Mean : %.2f, RefThreshold : %.2f => Color : %.2f", nStartPosX, nStartPosY, nEndPosX, nEndPosY, nMean, nRefThreshold, nMean * nRefThreshold);

	// Color Thresholding
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_ColorThresholding dParam_ColorThresholding;

	dParam_ColorThresholding.m_nMethod_R = NIPLParam_ColorThresholding::METHOD_UPPER;
	dParam_ColorThresholding.m_nUpperValue_R = nMean * nRefThreshold;
	dParam_ColorThresholding.m_nMethod_G = NIPLParam_ColorThresholding::METHOD_UPPER;
	dParam_ColorThresholding.m_nUpperValue_G = nMean * nRefThreshold;
	dParam_ColorThresholding.m_nMethod_B = NIPLParam_ColorThresholding::METHOD_UPPER;
	dParam_ColorThresholding.m_nUpperValue_B = nMean * nRefThreshold;

	dInput.m_dImg = dSubImage;
	dInput.m_pParam = &dParam_ColorThresholding;
	nErr = ColorThresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	Mat dBinImage = dOutput.m_dImg;

	bool bFoundLine = false;
	int nLineThickness = 2;

	for (int i = 0; i <= nLineWidth - nLineThickness; i++) {
		Mat dLine;
		if (bHorizLine) {
			dLine = Mat(dBinImage, Rect(0, i, nLineLength, nLineThickness));
		}
		else {
			dLine = Mat(dBinImage, Rect(i, 0, nLineThickness, nLineLength));
		}

		int nCount = countNonZero(dLine);
		float nRatio = ((float)nCount) / (nLineThickness * nLineLength);

		if (nRatio > nThreshold) {
			DebugPrint("  ==> Find [%d] Thickness : %d, Ratio : %.2f > %.2f", i, nLineThickness, nRatio, nThreshold);
			bFoundLine = true;
			break;
		}
	}

	if (!bFoundLine) {
		bDefect = true;
	}

	SET_OUTPUT_IMAGE(dBinImage);

	return NIPL_ERR_SUCCESS;
}

NIPL_CUSTOM_METHOD_IMPL(LDC_Clip)
{
	VERIFY_PARAMS();

	NIPLParam_LDC *pParam = (NIPLParam_LDC *)pInput->m_pParam;
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	wstring strDataPath = pParam->m_strDataPath;
	if (!LDC_LoadData(pParam, STR_LDC_COMP_CLIP)) {
		return NIPL_ERR_FAIL_TO_OPEN_FILE;
	}
	if (pParam->m_listROI.size() == 0) {
		return NIPL_ERR_PASS;
	}

	bool bForceDetectAll = pParam->m_bForceDetectAll;

	NIPLInput dInput;
	NIPLOutput dOutput;
	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	NIPLResult_Defect_LDC *pResult = new NIPLResult_Defect_LDC();
	if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	for (auto dROI : pParam->m_listROI) {
		bool bDefect = false;
		bool bSkip = false;

		if (bForceDetectAll) {
			bDefect = true;
		}
		else {
			if (dROI.m_nSetId > 0 && pResult->FindNonDefectBySetId(dROI.m_nSetId)) {
				bSkip = true;
			}

			if (!bSkip) {
				float nThreshold = pParam->m_nThreshold;
				nErr = LDC_Clip_CheckColor(dImg, dOutputImg,  dROI.m_rcBoundingBox, nThreshold, bDefect);
				if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
					return nErr;
				}
			}
		}

		if (bDefect) {
			NIPLDefect_LDC dDefect(NIPLDefect_LDC::DEFECT_TYPE_CLIP, dROI.m_rcBoundingBox, dROI.m_nSetId);
			pResult->m_listDefect.push_back(dDefect);
		}
		else if (!bSkip) {
			if (dROI.m_nSetId > 0) {
				pResult->SetNonDefectBySetId(dROI.m_nSetId);
			}
		}
	}

	pOutput->m_dImg = dOutputImg;
	if (pResult->m_listDefect.size() > 0) {
		pOutput->m_pResult = pResult;
	}
	else {
		delete pResult;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::LDC_Clip_CheckColor(Mat dImg, Mat &dOutputImg, Rect rcBoundingBox, float nThreshold, bool &bDefect)
{
	bDefect = false;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nStartPosX = rcBoundingBox.x;
	int nStartPosY = rcBoundingBox.y;
	int nEndPosX = nStartPosX + rcBoundingBox.width - 1;
	int nEndPosY = nStartPosY + rcBoundingBox.height - 1;

	if (nStartPosX < 0) nStartPosX = 0;
	if (nStartPosY < 0) nStartPosY = 0;
	if (nEndPosX >= nImgSizeX) nEndPosX = nImgSizeX - 1;
	if (nEndPosY >= nImgSizeY) nEndPosY = nImgSizeY - 1;

	NIPLParam_CopySubImageFrom dParam_CopySubImageFrom;
	dParam_CopySubImageFrom.m_nStartPosX = nStartPosX;
	dParam_CopySubImageFrom.m_nStartPosY = nStartPosY;
	dParam_CopySubImageFrom.m_nEndPosX = nEndPosX;
	dParam_CopySubImageFrom.m_nEndPosY = nEndPosY;

	// Get SubImage
	NIPLInput dInput;
	NIPLOutput dOutput;
	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_CopySubImageFrom;
	NIPL_ERR nErr = CopySubImageFrom(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dSubImage = dOutput.m_dImg;

	// Convert to gray
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_Color2Gray dParam_Color2Gray;

	dParam_Color2Gray.m_nGrayLevel = NIPLParam_Color2Gray::GRAYLEVEL_256;
	dInput.m_dImg = dSubImage;
	dInput.m_pParam = &dParam_Color2Gray;
	nErr = Color2Gray(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	Mat dGrayImage = dOutput.m_dImg;

	Scalar dMean;
	Scalar dStd;
	meanStdDev(dGrayImage, dMean, dStd);
//	float nMean = (float)dMean[0];
	float nStd = (float)dStd[0];

	TRACE("\n========> Std : %.3f\n", nStd);

	if (nStd < nThreshold) {
		bDefect = true;
	}

	return NIPL_ERR_SUCCESS;
}

void NIPLCustom::LDC_Terminal_ExcludeOutOfRadiusRange(Mat dImg, Mat &dOutputImage, Point ptCenter, int nMaxRadius)
{
	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	dImg.copyTo(dOutputImage);
	float nDistance;
	for (int nY = 0; nY < nImgSizeY; nY++) {
		for (int nX = 0; nX < nImgSizeX; nX++) {
			nDistance = sqrtf(powf((float)(ptCenter.x - nX), 2.f) + powf(float(ptCenter.y - nY), 2.f));
			if (nDistance > nMaxRadius) {
				dOutputImage.at<UINT8>(nY, nX) = 0;
			}
		}
	}
}

void NIPLCustom::LDC_Tube_NormalizeBrightness(Mat dImg, Mat &dOutputImg)
{
	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	dImg.copyTo(dOutputImg);

	Vec3b dPixel;
	UINT8 nR, nG, nB;
	int nMean;
	for (int nY = 0; nY < nImgSizeY; nY++) {
		for (int nX = 0; nX < nImgSizeX; nX++) {
			dPixel = dOutputImg.at<Vec3b>(nY, nX);
			nR = dPixel[2];
			nG = dPixel[1];
			nB = dPixel[0];

			nMean = (nR + nG + nB) / 3;

			nR += (128 - nMean);
			nG += (128 - nMean);
			nB += (128 - nMean);

			dOutputImg.at<Vec3b>(nY, nX) = Vec3b(nB, nG, nR);
		}
	}
}