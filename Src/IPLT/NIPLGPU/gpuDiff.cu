#include "gpuAPIs.cuh"

texture<uchar, cudaTextureType2D> texImg;
texture<uchar, cudaTextureType2D> texImg2;
surface<void, cudaSurfaceType2D> surfOutputImg;

__global__ 
void kernelDiff(uchar *pData, uchar *pData2, uchar *pOutputData, int nSizeX, int nSizeY, int nStep, int nMaskSize)
{
	const int nX = blockIdx.x*blockDim.x + threadIdx.x;
	const int nY = blockIdx.y*blockDim.y + threadIdx.y;
	if (nX >= nSizeX || nY >= nSizeY) {
		return;
	}

	int nX2, nY2;
	int nValue;
	int nValue2;
	int nDiffValue;
	int nMinValue = 9999;
	int nHalfMasklSize = nMaskSize / 2;

	nValue = (int)(pData[nY*nStep + nX]);
	for (int i = -nHalfMasklSize; i <= nHalfMasklSize; i++) {
		nY2 = nY + i;
		if (nY2 < 0 || nY2 >= nSizeY) {
			continue;
		}

		for (int j = -nHalfMasklSize; j <= nHalfMasklSize; j++) {
			nX2 = nX + j;
			if (nX2 < 0 || nX2 >= nSizeX) {
				continue;
			}

			nValue2 = (int)(pData2[nY2*nStep + nX2]);
			nDiffValue = abs(nValue - nValue2);

			if (nDiffValue < nMinValue) {
				nMinValue = nDiffValue;
			}
		}
	}

	pOutputData[nY*nStep + nX] = (uchar)nMinValue;
}

void gpuDiff(Mat dImg, Mat dImg2, Mat dOutputImg, int nMaskSize)
{
//	gpuDiffByTexture(dImg, dOutputImg, nMasklSize);
//	return;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;
	int nImgStep = dImg.step1();

	uchar *pData = dImg.data;
	uchar *pData2 = dImg2.data;
	uchar *pOutputData = dOutputImg.data;

	uchar *pDevData;
	uchar *pDevData2;
	uchar *pDevOutputData;
	cudaMalloc((void**)&pDevData, nImgSizeX*nImgSizeY * sizeof(uchar));
	cudaMalloc((void**)&pDevData2, nImgSizeX*nImgSizeY * sizeof(uchar));
	cudaMalloc((void**)&pDevOutputData, nImgSizeX*nImgSizeY * sizeof(uchar));

	cudaMemcpy(pDevData, pData, nImgSizeX*nImgSizeY * sizeof(uchar), cudaMemcpyHostToDevice);
	cudaMemcpy(pDevData2, pData2, nImgSizeX*nImgSizeY * sizeof(uchar), cudaMemcpyHostToDevice);

	dim3 dBlockDims(GPU_BLOCK_SIZE, GPU_BLOCK_SIZE);
	dim3 dGridDims(cvCeil((double)nImgSizeX / GPU_BLOCK_SIZE), cvCeil((double)nImgSizeY / GPU_BLOCK_SIZE));
	kernelDiff <<< dGridDims, dBlockDims >>> (pDevData, pDevData2, pDevOutputData, nImgSizeX, nImgSizeY, nImgStep, nMaskSize);

	cudaThreadSynchronize();

	cudaMemcpy(pOutputData, pDevOutputData, nImgSizeX*nImgSizeY * sizeof(uchar), cudaMemcpyDeviceToHost);

	cudaFree(pDevData);
	cudaFree(pDevData2);
	cudaFree(pDevOutputData);
}
