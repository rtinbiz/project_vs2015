// NIPLGPU.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NIPLGPU.h"
#include "gpuAPIs.cuh"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

shared_ptr<NIPLGPU> NIPLGPU::GetInstance(BOOL bNew)
{
	if (bNew) {
		return shared_ptr<NIPLGPU>(new NIPLGPU);
	}

	static shared_ptr<NIPLGPU> pThis(new NIPLGPU);

	return pThis;
}

NIPLGPU::NIPLGPU() : NIPLCV()
{
}

NIPLGPU::~NIPLGPU()
{
}

NIPL_GPU_METHOD_IMPL(Smoothing)
{
	VERIFY_PARAMS();
	NIPLParam_Smoothing *pParam_Smoothing = (NIPLParam_Smoothing *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nMethod = pParam_Smoothing->m_nMethod;
	int nFilterSize = pParam_Smoothing->m_nFilterSize;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (dImg.channels() != 1 || dImg.depth() != CV_8U) {
		return NIPL_ERR_INVALID_IMAGE_TYPE;
	}

	Mat dOutputImg;
	if (nMethod & NIPLParam_Smoothing::METHOD_FILTER) {
		if (nMethod > 0) {
			dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
			gpuSmoothing(dImg, dOutputImg, nFilterSize);

			pOutput->m_dImg = dOutputImg;
		}
	}

	if (CHECK_EMPTY_IMAGE(pOutput->m_dImg)) {
		return NIPL_ERR_PASS;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_GPU_METHOD_IMPL(Diff)
{
	VERIFY_PARAMS();
	NIPLParam_Diff *pParam_Diff = (NIPLParam_Diff *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	Mat dTargetImg = pParam_Diff->m_dTargetImg;
	int nMaskSize = 0;// pParam_Diff->m_nMaskSize;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (dImg.size != dTargetImg.size) {
		return NIPL_ERR_FAIL_NOT_MATCH_IMAGE_SIZE;
	}

	Mat dImg2 = dTargetImg;

	if (dImg.channels() != 1) {
		cvtColor(dImg, dImg, CV_BGR2GRAY);
	}
	if (dImg2.channels() != 1) {
		cvtColor(dImg2, dImg2, CV_BGR2GRAY);
	}

	if (dImg.type() != dImg2.type()) {
		CHECK_EXCEPTION(dImg2.convertTo(dImg2, dImg.type()));
	}

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	gpuDiff(dImg, dImg2, dOutputImg, nMaskSize);

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

