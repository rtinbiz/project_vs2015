#pragma once

#include "NIPLCV.h"
#include "NIPLGPUParam.h"
#include "NIPLGPUResult.h"

#ifndef _BUILD_NIPL_GPU
#ifdef _DEBUG
#pragma comment (lib, "NIPLGPU_d.lib")
#else
#pragma comment (lib, "NIPLGPU.lib")
#endif
#endif

#define NIPL_GPU_METHOD_IMPL(FuncName) NIPL_ERR NIPLGPU::FuncName(NIPLInput *pInput, NIPLOutput *pOutput)

class AFX_EXT_CLASS NIPLGPU : public NIPLCV {
public :
	static shared_ptr<NIPLGPU> GetInstance(BOOL bNew = FALSE);

	NIPL_METHOD_DECL(Smoothing);
	NIPL_METHOD_DECL(Diff);

	NIPLGPU();
	virtual ~NIPLGPU();
};